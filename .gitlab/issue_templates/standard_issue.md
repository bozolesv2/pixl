___
# **Standard-issue**
___

## OS Version :

- [Replace this text]

## Machine Configuration

- CPU : 
- GPU : 
- System Storage device : 

## Concerned application :

- Replace this text

## Describe the bug :

- Replace this text

## Steps to reproduce :

1. - Replace this text
2. - Replace this text
3. - Replace this text

## Relevant logs and/or screenshots :

- [Replace this text] Logs
- [Replace this text] Screenshots

## Possible fixes :

- Replace this text

___