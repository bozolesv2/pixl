################################################################################
#
# winetricks
#
################################################################################

WINETRICKS_VERSION = 20240105
WINETRICKS_SITE = $(call github,Winetricks,winetricks,$(WINETRICKS_VERSION))
WINETRICKS_LICENSE = LGPL-2.1 license

define WINETRICKS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/wine
	$(INSTALL) -D $(@D)/src/winetricks $(TARGET_DIR)/usr/wine/winetricks
endef

$(eval $(generic-package))
