################################################################################
#
# wine-x86-lutris-proton
#
################################################################################

# version 
WINE_X86_LUTRIS_PROTON_VERSION = Lutris7.2-2-Proton-8.0-4c
WINE_X86_LUTRIS_PROTON_SOURCE = wine-x86-$(WINE_X86_LUTRIS_PROTON_VERSION).tar.lzma
WINE_X86_LUTRIS_PROTON_SITE = https://github.com/pixl-os/wine-x86/releases/download/$(WINE_X86_LUTRIS_PROTON_VERSION)

#to avoid error to build 32 bits library needed for wine
WINE_X86_LUTRIS_PROTON_BIN_ARCH_EXCLUDE = /usr/bin32 /usr/wine /usr/lib32 /lib32

define WINE_X86_LUTRIS_PROTON_EXTRACT_CMDS
	mkdir -p $(@D)/target && cd $(@D)/target && tar xf $(DL_DIR)/$(WINE_X86_LUTRIS_PROTON_DL_SUBDIR)/$(WINE_X86_LUTRIS_PROTON_SOURCE)
endef

define WINE_X86_LUTRIS_PROTON_INSTALL_TARGET_CMDS
	cp -prn $(@D)/target/* $(TARGET_DIR)
endef

$(eval $(generic-package))
