#!/bin/bash
# MAME;mame;0.267
emulatorName="mame"
coreName="mame"
#echo for debug
#echo "$emulatorName"
#echo "$coreName"

#direct reading of *.corenames to extract and return version string
result=""
result="$(cat /recalbox/share/system/configs/$emulatorName.corenames | grep -i $coreName';' | awk -v FS='(;)' '{print $3}' | tr -d '\n' | tr -d '\r')"
if test -n "$result"
then
	#not null: we could return value normally
	#important to add the v front of version number in case of mame, pegasus will parse always with v
	echo "v$result" | tr -d '\n' | tr -d '\r'
else
	#value is not found or null - force to 0.0.0.0 in this case
	echo v0.0.0.0 | tr -d '\n' | tr -d '\r'
fi
exit 0