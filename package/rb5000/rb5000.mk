################################################################################
#
# RB5000 - Phillips VG 5000
#
################################################################################

# BUILD deactivated partially / using binary only due to repo not yet public for the moment

#RB5000_VERSION = c25e4a884cc3ffada20328f20be03a9c088648a1
#RB5000_SITE = https://gitlab-ci-token-rb5000:$(GITLAB_TOKEN_RB5000)@gitlab.com/bkg2k/rb5000.git
#RB5000_SITE_METHOD = git
RB5000_DEPENDENCIES = sdl2 sdl2_ttf libgl
RB5000_LICENSE = GPL-2.0
RB5000_LICENSE_FILES = COPYING
RB5000_VERSION_CORE = 1.0

## print version of core in PixL
define RB5000_PRE_CONFIGURE
	echo "RB5000;rb5000;$(RB5000_VERSION_CORE)" > $(TARGET_DIR)/recalbox/share_init/system/configs/rb5000.corenames
endef
RB5000_PRE_CONFIGURE_HOOKS += RB5000_PRE_CONFIGURE

define RB5000_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/bin
	$(INSTALL) -D -m 0755 "$(BR2_EXTERNAL_RECALBOX_PATH)/package/rb5000/binary/rb5000" \
		"$(TARGET_DIR)/usr/bin/rb5000"
endef

#define RB5000_INSTALL_TARGET_CMDS
#	$(INSTALL) -D $(@D)/rb5000 $(TARGET_DIR)/usr/bin/
#endef

#ifeq ($(BR2_PACKAGE_HAS_LIBGL),y)
#RB5000_DEPENDENCIES += libgl
#endif

#ifeq ($(BR2_PACKAGE_HAS_LIBGLES),y)
#RB5000_DEPENDENCIES += libgles
#endif

#RB5000_CONF_OPTS += -DCMAKE_C_ARCHIVE_CREATE="<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>"
#RB5000_CONF_OPTS += -DCMAKE_C_ARCHIVE_FINISH=true
#RB5000_CONF_OPTS += -DCMAKE_CXX_ARCHIVE_CREATE="<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>"
#RB5000_CONF_OPTS += -DCMAKE_CXX_ARCHIVE_FINISH=true
#RB5000_CONF_OPTS += -DCMAKE_AR="$(TARGET_CC)-ar"
#RB5000_CONF_OPTS += -DCMAKE_C_COMPILER="$(TARGET_CC)"
#RB5000_CONF_OPTS += -DCMAKE_CXX_COMPILER="$(TARGET_CXX)"
#RB5000_CONF_OPTS += -DCMAKE_LINKER="$(TARGET_LD)"
#RB5000_CONF_OPTS += -DCMAKE_C_FLAGS="$(COMPILER_COMMONS_CFLAGS_LTO)"
#RB5000_CONF_OPTS += -DCMAKE_CXX_FLAGS="$(COMPILER_COMMONS_CXXFLAGS_LTO)"
#RB5000_CONF_OPTS += -DCMAKE_LINKER_EXE_FLAGS="$(COMPILER_COMMONS_LDFLAGS_LTO)"

#$(eval $(cmake-package))
$(eval $(generic-package))
