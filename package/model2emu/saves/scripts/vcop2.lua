require("model2")

function Init()

	ScanlinesSurface = Video_CreateSurfaceFromFile("scripts\\scanlines_default.png");
	wide=false
	scanlines=false
	overlay=true
	press=0
	OverlaySurface = Video_CreateSurfaceFromFile("/tmp/model2emu_overlay.png");
-- for crosshair management
	P1_Crosshair_Visibility=true
	P2_Crosshair_Visibility=false
	P1_Crosshair = Video_CreateSurfaceFromFile("./scripts/crosshairs/sharps1.png")
	P2_Crosshair = Video_CreateSurfaceFromFile("./scripts/crosshairs/sharps2.png")

end

function Frame()
--    I960_WriteDWord(TMAPGFXBASE+0x1060,0);
--    I960_WriteDWord(TMAPGFXBASE+0x1064,0);
--    I960_WriteDWord(TMAPGFXBASE+0x1068,0);
--    I960_WriteDWord(TMAPGFXBASE+0x106C,0);
--    I960_WriteDWord(TMAPGFXBASE+0x1070,0);
--    I960_WriteDWord(TMAPGFXBASE+0x1074,0);
--    I960_WriteDWord(TMAPGFXBASE+0x1078,0);
--    I960_WriteDWord(TMAPGFXBASE+0x107C,0);
	if Input_IsKeyPressed(0x3F)==1 and press==0 then wide=not wide press=1
	elseif Input_IsKeyPressed(0x3F)==0 and press==1 then press=0
	end

	if wide==true then
		Model2_SetStretchBHigh(1)
	 	Model2_SetStretchBLow(1)
		Model2_SetWideScreen(1)
	else
		Model2_SetStretchBHigh(0)
	 	Model2_SetStretchBLow(0)
		Model2_SetWideScreen(0)
	end
end

function PostDraw()
-- To display scanlines
	if Options.scanlines.value==1 or scanlines==true then
		Video_DrawSurface(ScanlinesSurface,0,0);
	end
-- To display overlay
	if (Options.overlay.value==1 or overlay==true) then
		Video_DrawSurface(OverlaySurface,0,-1);
	end
-- to have size of the screen
	width,height = Video_GetScreenSize();
-- To display crosshairs
	if P1_Crosshair_Visibility==true then
		--min val 149 max val 644-- 
		P1AX = (I960_ReadWord(0x507214))
		--min val 38 max val 421--
		P1AY = (I960_ReadWord(0x507218))
		P1P=P1AX..P1AY
		XPOS = (P1AX - 149) / 495;
		XPOS = XPOS * width;

		YPOS = ((P1AY - 38) / 383);
		YPOS = (YPOS * height);
		Video_DrawSurface(P1_Crosshair, XPOS-32, YPOS-32)
	end
	if P2_Crosshair_Visibility==true then
		P2AX = (I960_ReadWord(0x50721c))
		P2AY = (I960_ReadWord(0x507220))
		P2P=P2AX..P2AY
		XPOS2 = (P2AX - 149) / 495;
		XPOS2 = XPOS2 * width;

		YPOS2 = ((P2AY - 38) / 383);
		YPOS2 = (YPOS2 * height);
		Video_DrawSurface(P2_Crosshair, XPOS2-32, YPOS2-32)
	end
end

function health_1p_cheat_f(value)
        I960_WriteWord(RAMBASE+0x2090,9); -- 1P 9 health
end

function ammo_1p_cheat_f(value)
        I960_WriteWord(RAMBASE+0x7260,6); -- 1P 6 ammo
end

Options =
{
	health_1p_cheat={name="1P Infinite Health",values={"Off","On"},runfunc=health_1p_cheat_f},
	ammo_1p_cheat={name="1P Infinite Ammo",values={"Off","On"},runfunc=ammo_1p_cheat_f},
	scanlines={name="Scanlines (50%)",values={"Off","On"}},
	overlay={name="Overlay",values={"Off","On"}}
}
