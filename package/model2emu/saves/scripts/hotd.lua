require("model2")

function Init()

	ScanlinesSurface = Video_CreateSurfaceFromFile("scripts\\scanlines_default.png");
	wide=false
	scanlines=false
	overlay=true
	press=0
	OverlaySurface = Video_CreateSurfaceFromFile("/tmp/model2emu_overlay.png");
-- for crosshair management
	P1_Crosshair_Visibility=true
	P2_Crosshair_Visibility=false
	P1_Crosshair = Video_CreateSurfaceFromFile("./scripts/crosshairs/sharps1.png")
	P2_Crosshair = Video_CreateSurfaceFromFile("./scripts/crosshairs/sharps2.png")

end

function Frame() -- (Nuexzz's values)
--	I960_WriteDWord(TMAPGFXBASE+0x9220,0); -- So simple
--	I960_WriteDWord(TMAPGFXBASE+0x9224,0); -- Why did it take me months to figure this out?
--	I960_WriteDWord(TMAPGFXBASE+0x9228,0); -- Is there a shorter way to patch this without a loop?
--	I960_WriteDWord(TMAPGFXBASE+0x922C,0); -- Because loops causes choppiness 
--	I960_WriteDWord(TMAPGFXBASE+0x9230,0); -- Believe me, I've tried to make this shorter
--	I960_WriteDWord(TMAPGFXBASE+0x9234,0); -- But those black bars would slide out very choppy
--	I960_WriteDWord(TMAPGFXBASE+0x9238,0); -- This is what happens when you're stuck using DWord
--	I960_WriteDWord(TMAPGFXBASE+0x923C,0); -- QWord wouldn't have been any better though

	Romset_PatchDWord(ROMBASE,0x18610,0xa000000)--no flash screen (by_egregiousguy)
		if Input_IsKeyPressed(0x3F)==1 and press==0 then wide=not wide press=1
		elseif Input_IsKeyPressed(0x3F)==0 and press==1 then press=0
		end
		
	if wide==true then
		local gameState = I960_ReadByte(0x51EE14)
		 if gameState==0x2
		 then
		Model2_SetWideScreen(0)
		Model2_SetStretchBLow(0)
		Model2_SetStretchBHigh(0)
		else
		Model2_SetWideScreen(1)
		Model2_SetStretchBLow(1)
		Model2_SetStretchBHigh(1)
		end
	else
		Model2_SetWideScreen(0)
		Model2_SetStretchBLow(0)
		Model2_SetStretchBHigh(0)
		end
end

function PostDraw()
-- To display scanlines
	if Options.scanlines.value==1 or scanlines==true then
		Video_DrawSurface(ScanlinesSurface,0,0);
	end
-- To display overlay
	if (Options.overlay.value==1 or overlay==true) then
		Video_DrawSurface(OverlaySurface,0,-1);
	end
-- to have size of the screen
	width,height = Video_GetScreenSize();
-- To display crosshairs
	if P1_Crosshair_Visibility==true then
		--min val 173 max val 668-- 
		P1AX = (I960_ReadWord(0x51eef0))
		--min val 86 max val 469--
		P1AY = (I960_ReadWord(0x51eef2))
		P1P=P1AX..P1AY
		XPOS = (P1AX - 173) / 495;
		XPOS = XPOS * width;

		YPOS = ((P1AY - 86) / 383);
		YPOS = (YPOS * height);
		Video_DrawSurface(P1_Crosshair, XPOS-32, YPOS-32)
	end
	if P2_Crosshair_Visibility==true then
		P2AX = (I960_ReadWord(0x51ef0c))
		P2AY = (I960_ReadWord(0x51ef0e))
		P2P=P2AX..P2AY
		XPOS2 = (P2AX - 173) / 495;
		XPOS2 = XPOS2 * width;

		YPOS2 = ((P2AY - 86) / 383);
		YPOS2 = (YPOS2 * height);
		Video_DrawSurface(P2_Crosshair, XPOS2-32, YPOS2-32)
	end
end

function health_1p_cheat_f(value)
        I960_WriteWord(RAMBASE+0x1EC10,5); -- 1P 5 health
end

function ammo_1p_cheat_f(value)
        I960_WriteWord(RAMBASE+0x1EC41,6); -- 1P 6 ammo
end

Options =
{
	health_1p_cheat={name="1P Infinite Health",values={"Off","On"},runfunc=health_1p_cheat_f},
	ammo_1p_cheat={name="1P Infinite Ammo",values={"Off","On"},runfunc=ammo_1p_cheat_f},
	scanlines={name="Scanlines (50%)",values={"Off","On"}},
	overlay={name="Overlay",values={"Off","On"}}
}
