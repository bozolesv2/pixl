require("model2")

function Init()

	ScanlinesSurface = Video_CreateSurfaceFromFile("scripts\\scanlines_default.png");
	wide=false
	scanlines=false
	overlay=true
	press=0
	OverlaySurface = Video_CreateSurfaceFromFile("/tmp/model2emu_overlay.png");

end

function Frame()
		if Input_IsKeyPressed(0x3F)==1 and press==0 then wide=not wide press=1
		elseif Input_IsKeyPressed(0x3F)==0 and press==1 then press=0
		end
		
	if wide==true then
		Model2_SetStretchAHigh(1)
	 	Model2_SetStretchALow(1)
		Model2_SetStretchBHigh(1)
	 	Model2_SetStretchBLow(1)
		Model2_SetWideScreen(1)
	else
		Model2_SetStretchAHigh(0)
	 	Model2_SetStretchALow(0)
		Model2_SetStretchBHigh(0)
	 	Model2_SetStretchBLow(0)
		Model2_SetWideScreen(0)
	end
end

function PostDraw()
-- To display scanlines
	if Options.scanlines.value==1 or scanlines==true then
		Video_DrawSurface(ScanlinesSurface,0,0);
	end
-- To display overlay
	if (Options.overlay.value==1 or overlay==true) then
		Video_DrawSurface(OverlaySurface,0,-1);
	end
end

function health2p(value)
	I960_WriteWord(RAMBASE+0x12DAC,220); -- 2P full health
end

function mahler(value)
	I960_WriteWord(RAMBASE+0x15DC7,2048); -- 1P mahler
	I960_WriteWord(RAMBASE+0x15DC9,8); -- 2P mahler
end

Options =
{
	health1p={name="1P Infinite Health",values={"Off","On"},runfunc=health1p},
	health2p={name="2P Infinite Health",values={"Off","On"},runfunc=health2p},
--	mahler={name="Mahler Enabled",values={"Off","On"},runfunc=mahler},
	scanlines={name="Scanlines (50%)",values={"Off","On"}},
	overlay={name="Overlay",values={"Off","On"}}
}
