################################################################################
#
# libretro-same_cdi
#
################################################################################

#Commit version of 11/12/2022
# - Add Redump BIN/CUE format ;-)
LIBRETRO_SAME_CDI_VERSION = 2ee1200d3c9bbceb64e1f0ec9e5224a16cdc0061
LIBRETRO_SAME_CDI_SITE = $(call github,libretro,same_cdi,$(LIBRETRO_SAME_CDI_VERSION))
LIBRETRO_SAME_CDI_LICENSE = GPL

define LIBRETRO_SAME_CDI_BUILD_CMDS
	$(HOST_CONFIGURE_OPTS) $(MAKE) -C $(@D)/3rdparty/genie/build/gmake.linux -f genie.make
	$(TARGET_CONFIGURE_OPTS) $(MAKE) CXX="$(TARGET_CXX)" CC="$(TARGET_CC)" GIT_VERSION="" -C $(@D) -f Makefile.libretro
endef

define LIBRETRO_SAME_CDI_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/same_cdi_libretro.so \
		$(TARGET_DIR)/usr/lib/libretro/same_cdi_libretro.so
endef

$(eval $(generic-package))
