################################################################################
#
# PCSX2
#
################################################################################

#Version: Commits on Dec 21, 2023
PCSX2_VERSION = v1.7.5326
PCSX2_SITE = https://github.com/pcsx2/pcsx2.git
PCSX2_LICENSE = GPLv2 GPLv3 LGPLv2.1 LGPLv3
PCSX2_LICENSE_FILES = COPYING.GPLv2
PCSX2_DEPENDENCIES = xserver_xorg-server alsa-lib freetype zlib libpng wxwidgets \
			libaio portaudio libsoundtouch sdl2 libpcap yaml-cpp xorgproto \
			libsamplerate fmt libgtk3 qt6base qt6tools qt6svg ecm

PCSX2_SITE_METHOD = git
PCSX2_GIT_SUBMODULES = YES
PCSX2_SUPPORTS_IN_SOURCE_BUILD = NO

PCSX2_EXTRA_DOWNLOADS = https://github.com/PCSX2/pcsx2_patches/releases/download/latest/patches.zip

## print version of core in PixL
define PCSX2_PRE_CONFIGURE
	echo "Pcsx2;pcsx2;$(PCSX2_VERSION)" > $(TARGET_DIR)/recalbox/share_init/system/configs/pcsx2.corenames
endef
PCSX2_PRE_CONFIGURE_HOOKS += PCSX2_PRE_CONFIGURE

PCSX2_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
PCSX2_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
PCSX2_CONF_OPTS += -DPCSX2_TARGET_ARCHITECTURES=x86_64
PCSX2_CONF_OPTS += -DCMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE
PCSX2_CONF_OPTS += -DENABLE_TESTS=OFF
PCSX2_CONF_OPTS += -DUSE_LINKED_FFMPEG=ON
PCSX2_CONF_OPTS += -DDISABLE_ADVANCE_SIMD=OFF
PCSX2_CONF_OPTS += -DUSE_VTUNE=OFF
PCSX2_CONF_OPTS += -DLTO_PCSX2_CORE=ON
PCSX2_CONF_OPTS += -DX11_API=ON
PCSX2_CONF_OPTS += -DWAYLAND_API=ON
PCSX2_CONF_OPTS += -DUSE_OPENGL=ON
PCSX2_CONF_OPTS += -DUSE_VULKAN=ON
PCSX2_CONF_OPTS += -DUSE_SYSTEM_LIBS=AUTO
PCSX2_CONF_OPTS += -DUSE_DISCORD_PRESENCE=OFF
PCSX2_CONF_OPTS += -DUSE_ACHIEVEMENTS=ON
PCSX2_CONF_OPTS += -DQT_BUILD=TRUE
# The following flag is misleading and *needed* ON to avoid doing -march=native
PCSX2_CONF_OPTS += -DDISABLE_ADVANCE_SIMD=ON

define PCSX2_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(@D)/buildroot-build/bin/pcsx2-qt $(TARGET_DIR)/usr/bin/PCSX2/pcsx2
	cp -pr $(@D)/buildroot-build/bin/{translations,resources} $(TARGET_DIR)/usr/bin/PCSX2
	mkdir -p $(TARGET_DIR)/recalbox/share_upgrade/bios/ps2
	# use pegasus-frontend SDL config
	rm $(TARGET_DIR)/usr/bin/PCSX2/resources/game_controller_db.txt
	# Add patch on resources folder
	cp -pr $(PCSX2_DL_DIR)/patches.zip $(TARGET_DIR)/usr/bin/PCSX2/resources/

	# For Create a update online package
	mkdir -p $(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/update-resources/package/
	$(INSTALL) -D $(@D)/buildroot-build/bin/pcsx2-qt \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/update-resources/package/pcsx2
	cp -pr $(@D)/buildroot-build/bin/{translations,resources} \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/update-resources/package
	cp -pr $(PCSX2_DL_DIR)/patches.zip \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/update-resources/package
endef

# Hotkeys using evmapy
define PCSX2_EVMAPY
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/pcsx2/pcsx2.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
PCSX2_POST_INSTALL_TARGET_HOOKS += PCSX2_EVMAPY

$(eval $(cmake-package))
