################################################################################
#
# nvidia-driver version 390.157
#
################################################################################

NVIDIA_DRIVER_390_RECALBOX_VERSION = 390.157
NVIDIA_DRIVER_390_RECALBOX_SUFFIX = $(if $(BR2_x86_64),_64)
NVIDIA_DRIVER_390_RECALBOX_SITE = http://download.nvidia.com/XFree86/Linux-x86$(NVIDIA_DRIVER_390_RECALBOX_SUFFIX)/$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
NVIDIA_DRIVER_390_RECALBOX_SOURCE = NVIDIA-Linux-x86$(NVIDIA_DRIVER_390_RECALBOX_SUFFIX)-$(NVIDIA_DRIVER_390_RECALBOX_VERSION).run
NVIDIA_DRIVER_390_RECALBOX_LICENSE = NVIDIA Software License
NVIDIA_DRIVER_390_RECALBOX_LICENSE_FILES = LICENSE
NVIDIA_DRIVER_390_RECALBOX_REDISTRIBUTE = NO
NVIDIA_DRIVER_390_RECALBOX_INSTALL_STAGING = YES
NVIDIA_DRIVER_390_RECALBOX_NON_COMMERCIAL = y

NVIDIA_DRIVER_390_RECALBOX_VERSION_GL = 1.7.0
NVIDIA_DRIVER_390_RECALBOX_VERSION_GLX = 0
NVIDIA_DRIVER_390_RECALBOX_VERSION_EGL = 1.1.0
NVIDIA_DRIVER_390_RECALBOX_VERSION_GLDISPATCH = 0
NVIDIA_DRIVER_390_RECALBOX_VERSION_GLESv1_CM = 1.2.0
NVIDIA_DRIVER_390_RECALBOX_VERSION_GLESv2 = 2.1.0
NVIDIA_DRIVER_390_RECALBOX_VERSION_WAYLAND = 1.0.2
NVIDIA_DRIVER_390_RECALBOX_VERSION_OPENCL = 1.0.0
NVIDIA_DRIVER_390_RECALBOX_VERSION_OPENGL = 0

# To avoid error to build 32 bits library needed for wine
NVIDIA_DRIVER_390_RECALBOX_BIN_ARCH_EXCLUDE = /usr/lib32

ifeq ($(BR2_PACKAGE_NVIDIA_DRIVER_390_RECALBOX_XORG),y)
# Since nvidia-driver are binary blobs, the below dependencies are not
# strictly speaking build dependencies of nvidia-driver. However, they
# are build dependencies of packages that depend on nvidia-driver, so
# they should be built prior to those packages, and the only simple
# way to do so is to make nvidia-driver depend on them.
NVIDIA_DRIVER_390_RECALBOX_DEPENDENCIES += xlib_libX11 xlib_libXext

# pixL - enable both mesa and nvidia
ifneq ($(BR2_PACKAGE_MESA3D),y)
NVIDIA_DRIVER_390_RECALBOX_PROVIDES = libgl libegl libgles
endif

# pixL - enable both mesa and nvidia
ifneq ($(BR2_PACKAGE_MESA3D),y)
NVIDIA_DRIVER_390_RECALBOX_DEPENDENCIES += mesa3d-headers
endif

# libGL.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) is the 390 libGL.so library; it
# has been replaced with libGL.so.1.7.0. Installing both is technically
# possible, but great care must be taken to ensure they do not conflict,
# so that EGL still works. The 390 library exposes an NVidia-specific
# API, so it should not be needed, except for legacy, binary-only
# applications (in other words: we don't care).
#
# libGL.so.1.7.0 is the new vendor-neutral library, aimed at replacing
# the old libGL.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) library. The latter contains
# NVidia extensions (which is deemed bad now), while the former follows
# the newly-introduced vendor-neutral "dispatching" API/ABI:
#   https://github.com/aritger/linux-opengl-abi-proposal/blob/master/linux-opengl-abi-proposal.txt
# However, this is not very usefull to us, as we don't support multiple
# GL providers at the same time on the system, which this proposal is
# aimed at supporting.
# In v173.14.39 This module can cause the NVIDIA libGL to be loaded into the same process (the X server) as the NVIDIA libglx.so extension module, which is not a supported use case.
# So we only install the legacy library for now.
NVIDIA_DRIVER_390_RECALBOX_LIBS_GL = \
	libGL.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_GL) \
	libGLdispatch.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_GLDISPATCH) \
	libOpenGL.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_OPENGL) \
	libGLX.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_GLX) \
	libGLX_nvidia.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)

NVIDIA_DRIVER_390_RECALBOX_LIBS_EGL = \
	libEGL.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_EGL) \
	libEGL_nvidia.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)

NVIDIA_DRIVER_390_RECALBOX_LIBS_GLES = \
	libGLESv1_CM.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_GLESv1_CM) \
	libGLESv1_CM_nvidia.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libGLESv2.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_GLESv2) \
	libGLESv2_nvidia.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)

# In v340.108e Updated nvidia-installer for better compatibility with ncurses when libncurses.so.6 exposes the ncurses reentrant ABI, such as on openSUSE Leap 15 and SUSE Linux Enterprise 15.
NVIDIA_DRIVER_390_RECALBOX_LIBS_MISC = \
	libnvidia-cfg.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-eglcore.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-egl-wayland.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_WAYLAND) \
	libnvidia-glcore.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-glsi.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	tls/libnvidia-tls.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libvdpau_nvidia.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-ml.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-gtk2.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-gtk3.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)

ifeq ($(BR2_PACKAGE_NVIDIA_DRIVER_390_RECALBOX_NGX),y)
NVIDIA_DRIVER_390_RECALBOX_LIBS += \
	libnvidia-ngx.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
endif

NVIDIA_DRIVER_390_RECALBOX_LIBS = \
	$(NVIDIA_DRIVER_390_RECALBOX_LIBS_GL) \
	$(NVIDIA_DRIVER_390_RECALBOX_LIBS_EGL) \
	$(NVIDIA_DRIVER_390_RECALBOX_LIBS_GLES) \
	$(NVIDIA_DRIVER_390_RECALBOX_LIBS_MISC)

# pixL for 32 bits libraries
NVIDIA_DRIVER_390_RECALBOX_LIBS_32_MISC = \
	libnvidia-eglcore.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-glcore.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-glsi.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libvdpau_nvidia.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	tls/libnvidia-tls.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-ml.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)

# pixL for 32 bits libraries
NVIDIA_DRIVER_390_RECALBOX_LIBS_32 = \
	$(NVIDIA_DRIVER_390_RECALBOX_LIBS_GL) \
	$(NVIDIA_DRIVER_390_RECALBOX_LIBS_EGL) \
	$(NVIDIA_DRIVER_390_RECALBOX_LIBS_GLES) \
	$(NVIDIA_DRIVER_390_RECALBOX_LIBS_32_MISC)

# Install the gl.pc file
define NVIDIA_DRIVER_390_RECALBOX_INSTALL_GL_DEV
	$(INSTALL) -D -m 0644 $(@D)/libGL.la \
		$(STAGING_DIR)/usr/lib/libGL.la
	$(SED) 's:__GENERATED_BY__:Buildroot:' \
		$(STAGING_DIR)/usr/lib/libGL.la
	$(SED) 's:__LIBGL_PATH__:/usr/lib:' \
		$(STAGING_DIR)/usr/lib/libGL.la
	$(SED) 's:-L[^[:space:]]\+::' \
		$(STAGING_DIR)/usr/lib/libGL.la
	$(INSTALL) -D -m 0644 package/nvidia-driver/gl.pc \
		$(STAGING_DIR)/usr/lib/pkgconfig/gl.pc
	$(INSTALL) -D -m 0644 package/nvidia-driver/egl.pc \
		$(STAGING_DIR)/usr/lib/pkgconfig/egl.pc
endef

# Install the gl.pc file for 32 bits libraries
define NVIDIA_DRIVER_390_RECALBOX_INSTALL_GL_DEV_32
	$(INSTALL) -D -m 0644 $(@D)/32/libGL.la \
		$(STAGING_DIR)/usr/lib32/libGL.la
	$(SED) 's:__GENERATED_BY__:Buildroot:' \
		$(STAGING_DIR)/usr/lib32/libGL.la
	$(SED) 's:__LIBGL_PATH__:/usr/lib32:' \
		$(STAGING_DIR)/usr/lib32/libGL.la
	$(SED) 's:-L[^[:space:]]\+::' \
		$(STAGING_DIR)/usr/lib32/libGL.la
endef

# Those libraries are 'private' libraries requiring an agreement with
# NVidia to develop code for those libs. There seems to be no restriction
# on using those libraries (e.g. if the user has such an agreement, or
# wants to run a third-party program developped under such an agreement).
ifeq ($(BR2_PACKAGE_NVIDIA_DRIVER_390_RECALBOX_PRIVATE_LIBS),y)
NVIDIA_DRIVER_390_RECALBOX_LIBS += \
	libnvidia-ifr.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-fbc.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
endif

# We refer to the destination path; the origin file has no directory component
NVIDIA_DRIVER_390_RECALBOX_X_MODS = \
	drivers/nvidia_drv.so \
	extensions/libglx.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-wfb.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
endif # X drivers

ifeq ($(BR2_PACKAGE_NVIDIA_DRIVER_390_RECALBOX_CUDA),y)
NVIDIA_DRIVER_390_RECALBOX_LIBS += \
	libcuda.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-compiler.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvcuvid.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-fatbinaryloader.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-ptxjitcompiler.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-encode.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION) \
	libnvidia-ml.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)

ifeq ($(BR2_PACKAGE_NVIDIA_DRIVER_390_RECALBOX_CUDA_PROGS),y)
NVIDIA_DRIVER_390_RECALBOX_PROGS = nvidia-cuda-mps-control nvidia-cuda-mps-server
endif
endif

ifeq ($(BR2_PACKAGE_NVIDIA_DRIVER_390_RECALBOX_OPENCL),y)
NVIDIA_DRIVER_390_RECALBOX_LIBS += \
	libOpenCL.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION_OPENCL) \
	libnvidia-opencl.so.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
endif

# Build and install the kernel modules if needed
ifeq ($(BR2_PACKAGE_NVIDIA_DRIVER_390_RECALBOX_MODULE),y)
NVIDIA_DRIVER_390_RECALBOX_MODULES = nvidia nvidia-modeset nvidia-drm
NVIDIA_DRIVER_390_RECALBOX_MODULES += nvidia-uvm

# They can't do everything like everyone. They need those variables,
# because they don't recognise the usual variables set by the kernel
# build system. We also need to tell them what modules to build.
NVIDIA_DRIVER_390_RECALBOX_MODULE_MAKE_OPTS = \
	NV_KERNEL_SOURCES="$(LINUX_DIR)" \
	NV_KERNEL_OUTPUT="$(LINUX_DIR)" \
	NV_KERNEL_MODULES="$(NVIDIA_DRIVER_390_RECALBOX_MODULES)" \
	IGNORE_CC_MISMATCH="1"

NVIDIA_DRIVER_390_RECALBOX_MODULE_SUBDIRS = kernel

$(eval $(kernel-module))

endif # BR2_PACKAGE_NVIDIA_DRIVER_390_RECALBOX_MODULE == y

# The downloaded archive is in fact an auto-extract script. So, it can run
# virtually everywhere, and it is fine enough to provide useful options.
# Except it can't extract into an existing (even empty) directory.
define NVIDIA_DRIVER_390_RECALBOX_EXTRACT_CMDS
	$(SHELL) $(NVIDIA_DRIVER_390_RECALBOX_DL_DIR)/$(NVIDIA_DRIVER_390_RECALBOX_SOURCE) --extract-only --target \
		$(@D)/tmp-extract
	chmod u+w -R $(@D)
	mv $(@D)/tmp-extract/* $(@D)/tmp-extract/.manifest $(@D)
	rm -rf $(@D)/tmp-extract
endef

# Helper to install libraries
# $1: destination directory (target or staging)
#
# For all libraries, we install them and create a symlink using
# their SONAME, so we can link to them at runtime; we also create
# the no-version symlink, so we can link to them at build time.
define NVIDIA_DRIVER_390_RECALBOX_INSTALL_LIBS
	$(foreach lib,$(NVIDIA_DRIVER_390_RECALBOX_LIBS),\
		$(INSTALL) -D -m 0755 $(@D)/$(lib) $(1)/usr/lib/extra/nvidia-$(NVIDIA_DRIVER_390_RECALBOX_VERSION)/$(notdir $(lib))
		libsoname="$$( $(TARGET_READELF) -d "$(@D)/$(lib)" \
			|sed -r -e '/.*\(SONAME\).*\[(.*)\]$$/!d; s//\1/;' )";
	)
endef

# pixL install 32bit libraries
# FIXME libvdpau_nvidia.so is needed in /usr/lib32/vdpau/
define NVIDIA_DRIVER_390_RECALBOX_INSTALL_LIBS_32
	mkdir -p $(TARGET_DIR)/usr/lib32/vdpau
	$(foreach lib,$(NVIDIA_DRIVER_390_RECALBOX_LIBS_32),\
		$(INSTALL) -D -m 0755 $(@D)/32/$(lib) $(1)/usr/lib32/extra/nvidia-$(NVIDIA_DRIVER_390_RECALBOX_VERSION)/$(notdir $(lib))
		libsoname="$$( $(TARGET_READELF) -d "$(@D)/$(lib)" \
			|sed -r -e '/.*\(SONAME\).*\[(.*)\]$$/!d; s//\1/;' )";
	)
endef

# pixl nvidia libs are runtime linked via libglvnd
# For staging, install libraries and development files
define NVIDIA_DRIVER_390_RECALBOX_INSTALL_STAGING_CMDS
	$(call NVIDIA_DRIVER_390_RECALBOX_INSTALL_LIBS,$(STAGING_DIR))
	$(call NVIDIA_DRIVER_390_RECALBOX_INSTALL_LIBS_32,$(STAGING_DIR))
	$(NVIDIA_DRIVER_390_RECALBOX_INSTALL_GL_DEV)
	$(NVIDIA_DRIVER_390_RECALBOX_INSTALL_GL_DEV_32)
endef

# For target, install libraries and X.org modules
define NVIDIA_DRIVER_390_RECALBOX_INSTALL_TARGET_CMDS
	$(call NVIDIA_DRIVER_390_RECALBOX_INSTALL_LIBS,$(TARGET_DIR))
	$(call NVIDIA_DRIVER_390_RECALBOX_INSTALL_LIBS_32,$(TARGET_DIR))
	$(foreach m,$(NVIDIA_DRIVER_390_RECALBOX_X_MODS), \
		$(INSTALL) -D -m 0755 $(@D)/$(notdir $(m)) \
			$(TARGET_DIR)/usr/lib/extra/nvidia-$(NVIDIA_DRIVER_390_RECALBOX_VERSION)/xorg/$(m)
	)
	$(foreach p,$(NVIDIA_DRIVER_390_RECALBOX_PROGS), \
		$(INSTALL) -D -m 0755 $(@D)/$(p) \
			$(TARGET_DIR)/usr/bin/$(p).$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
	)
	$(NVIDIA_DRIVER_390_RECALBOX_INSTALL_KERNEL_MODULE)

# install nvidia-modprobe, required
	$(INSTALL) -D -m 0755 $(@D)/nvidia-modprobe \
		$(TARGET_DIR)/usr/bin/nvidia-modprobe

# pixl install files needed by libglvnd
	$(INSTALL) -D -m 0644 $(@D)/10_nvidia.json \
		$(TARGET_DIR)/usr/share/glvnd/egl_vendor.d/10_nvidia.json.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
# pixl install more needed
	$(INSTALL) -D -m 0644 $(@D)/10_nvidia_wayland.json \
		$(TARGET_DIR)/usr/share/glvnd/egl_vendor.d/10_nvidia_wayland.json.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
	$(INSTALL) -D -m 0644 $(@D)/nvidia-drm-outputclass.conf \
		$(TARGET_DIR)/usr/share/X11/xorg.conf.d/10-nvidia-drm.conf.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
	$(INSTALL) -D -m 0755 $(@D)/nvidia-settings \
		$(TARGET_DIR)/usr/bin/nvidia-settings.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
	$(INSTALL) -D -m 0755 $(@D)/nvidia-smi \
		$(TARGET_DIR)/usr/bin/nvidia-smi.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
	$(INSTALL) -D -m 0755 $(@D)/nvidia-xconfig \
		$(TARGET_DIR)/usr/bin/nvidia-xconfig.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)

	mkdir -p $(TARGET_DIR)/etc/nvidia
	$(INSTALL) -D -m 0755 $(@D)/nvidia-application-profiles-$(NVIDIA_DRIVER_390_RECALBOX_VERSION)-rc \
		$(TARGET_DIR)/etc/nvidia/nvidia-application-profiles-$(NVIDIA_DRIVER_390_RECALBOX_VERSION)-rc
	mkdir -p $(TARGET_DIR)/usr/share/nvidia
	$(INSTALL) -D -m 0755 $(@D)/nvidia-application-profiles-$(NVIDIA_DRIVER_390_RECALBOX_VERSION)-key-documentation \
		$(TARGET_DIR)/usr/share/nvidia/nvidia-application-profiles-$(NVIDIA_DRIVER_390_RECALBOX_VERSION)-key-documentation
endef

define NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_KERNEL_MODULES_X86_64
	$(INSTALL) -D -m 0644 $(@D)/nvidia_icd.json.template \
		$(TARGET_DIR)/usr/share/vulkan/icd.d/nvidia_icd.template.json.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
		sed -i -e s+'"library_path": "libGLX_nvidia'+'"library_path": "/usr/lib/libGLX_nvidia'+ \
			$(TARGET_DIR)/usr/share/vulkan/icd.d/nvidia_icd.template.json.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
endef

define NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_KERNEL_MODULES_X86
	$(INSTALL) -D -m 0644 $(@D)/nvidia_icd.json.template \
		$(TARGET_DIR)/usr/share/vulkan/icd.d/nvidia_icd.template.i686.json.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
		sed -i -e s+'"library_path": "libGLX_nvidia'+'"library_path": "/usr/lib32/libGLX_nvidia'+ \
			$(TARGET_DIR)/usr/share/vulkan/icd.d/nvidia_icd.template.i686.json.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
endef

# pixL hardware script will handle kernel module version
define NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_KERNEL_MODULES
	mv $(TARGET_DIR)/lib/modules/$(LINUX_VERSION)/updates/nvidia-drm.ko \
		$(TARGET_DIR)/lib/modules/$(LINUX_VERSION)/updates/nvidia-drm.ko.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
	mv $(TARGET_DIR)/lib/modules/$(LINUX_VERSION)/updates/nvidia-modeset.ko \
		$(TARGET_DIR)/lib/modules/$(LINUX_VERSION)/updates/nvidia-modeset.ko.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
	mv $(TARGET_DIR)/lib/modules/$(LINUX_VERSION)/updates/nvidia-uvm.ko \
		$(TARGET_DIR)/lib/modules/$(LINUX_VERSION)/updates/nvidia-uvm.ko.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
	mv $(TARGET_DIR)/lib/modules/$(LINUX_VERSION)/updates/nvidia.ko \
		$(TARGET_DIR)/lib/modules/$(LINUX_VERSION)/updates/nvidia.ko.$(NVIDIA_DRIVER_390_RECALBOX_VERSION)
endef

NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_HOOKS += NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_KERNEL_MODULES_X86_64
NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_HOOKS += NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_KERNEL_MODULES_X86
NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_HOOKS += NVIDIA_DRIVER_390_RECALBOX_POST_INSTALL_TARGET_KERNEL_MODULES

$(eval $(generic-package))
