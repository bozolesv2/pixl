################################################################################
#
# DUCKSTATION
#
################################################################################

# Commit from 2023/12/18
DUCKSTATION_VERSION = v0.1-6168
DUCKSTATION_SITE = https://github.com/stenzek/duckstation.git
DUCKSTATION_DEPENDENCIES = boost ffmpeg fmt libdrm sdl2 libevdev libcurl libegl \
							ecm qt6base qt6tools qt6svg
DUCKSTATION_SITE_METHOD = git
DUCKSTATION_GIT_SUBMODULES = YES
DUCKSTATION_LICENSE = GPLv2

# Don't build in source tree
DUCKSTATION_SUPPORTS_IN_SOURCE_BUILD = NO

## print version of core in PixL
define DUCKSTATION_PRE_CONFIGURE
	echo "Duckstation;duckstation;$(DUCKSTATION_VERSION)" > $(TARGET_DIR)/recalbox/share_init/system/configs/duckstation.corenames
endef
DUCKSTATION_PRE_CONFIGURE_HOOKS += DUCKSTATION_PRE_CONFIGURE

DUCKSTATION_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release -Wno-dev
DUCKSTATION_CONF_OPTS += -DBUILD_SHARED_LIBS=FALSE
DUCKSTATION_CONF_OPTS += -DENABLE_DISCORD_PRESENCE=OFF
DUCKSTATION_CONF_OPTS += -DANDROID=OFF -DBUILD_LIBRETRO_CORE=OFF
DUCKSTATION_CONF_OPTS += -DUSE_DRMKMS=ON
DUCKSTATION_CONF_OPTS += -DUSE_WAYLAND=ON
DUCKSTATION_CONF_OPTS += -DENABLE_VULKAN=ON
DUCKSTATION_CONF_OPTS += -DUSE_X11=ON
DUCKSTATION_CONF_OPTS += -DUSE_GLX=ON
DUCKSTATION_CONF_OPTS += -DUSE_EGL=OFF
DUCKSTATION_CONF_OPTS += -DBUILD_QT_FRONTEND=ON

DUCKSTATION_CONF_OPTS += -DCMAKE_C_ARCHIVE_CREATE="<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>"
DUCKSTATION_CONF_OPTS += -DCMAKE_C_ARCHIVE_FINISH=true
DUCKSTATION_CONF_OPTS += -DCMAKE_CXX_ARCHIVE_CREATE="<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>"
DUCKSTATION_CONF_OPTS += -DCMAKE_CXX_ARCHIVE_FINISH=true
DUCKSTATION_CONF_OPTS += -DCMAKE_AR="$(TARGET_CC)-ar"
DUCKSTATION_CONF_OPTS += -DCMAKE_C_COMPILER="$(TARGET_CC)"
DUCKSTATION_CONF_OPTS += -DCMAKE_CXX_COMPILER="$(TARGET_CXX)"
DUCKSTATION_CONF_OPTS += -DCMAKE_LINKER="$(TARGET_LD)"
DUCKSTATION_CONF_OPTS += -DCMAKE_C_FLAGS="$(COMPILER_COMMONS_CFLAGS_SO) $(DUCKSTATION_SIDE_LIBS)"
DUCKSTATION_CONF_OPTS += -DCMAKE_CXX_FLAGS="$(COMPILER_COMMONS_CXXFLAGS_SO) $(DUCKSTATION_SIDE_LIBS)"
DUCKSTATION_CONF_OPTS += -DCMAKE_LINKER_EXE_FLAGS="$(COMPILER_COMMONS_LDFLAGS_SO)"

DUCKSTATION_CONF_ENV += LDFLAGS=-lpthread

define DUCKSTATION_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/bin/duckstation
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/psx/duckstation/{inputprofiles,covers,shaders}
	mkdir -p $(TARGET_DIR)/recalbox/share_init/cheats/psx/duckstation
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/psx/duckstation/{textures,savestates,memcards,dump,gamesettings}
	
	$(INSTALL) -D $(@D)/buildroot-build/bin/duckstation-qt $(TARGET_DIR)/usr/bin/duckstation/duckstation
	cp -R $(@D)/buildroot-build/bin/{translations,resources} $(TARGET_DIR)/usr/bin/duckstation/
endef

$(eval $(cmake-package))
