#! /bin/sh

EXIT=0
BASEFILE="sinden-lightguns"
LOCKFILE="/var/run/${BASEFILE}-service.pid" #lock file contain the pid value
COUNTFILE="/var/run/${BASEFILE}.count" #file to count sinden lightguns connected
BINFILE="LightgunMono.exe"
LOGFILE="/recalbox/share/system/logs/${BASEFILE}.log"
WORKDIR="/recalbox/share/system/.config/sinden"
BINDIR="/usr/bin/sinden"

checkPID () {
	SPID=$(ps ax | grep -i mono-service | grep -i ${BINFILE} | awk '{print $1}')
	recallog -s "${BASEFILE}" -t "checkPID" "Sinden Lightgun Service is running with pid : ${SPID}"
	echo "${SPID}"
}

checkLock () {
	if (test -e "${LOCKFILE}" ); then
		recallog -s "${BASEFILE}" -t "checkLock" "Sinden Lightgun Service is locked"
		return 0
	else
		recallog -s "${BASEFILE}" -t "checkLock" "Sinden Lightgun Service is not locked"
		return 1
	fi
}

unlockService() {
	checkLock
	if [ $? -eq 0 ]; then
		rm "${LOCKFILE}"
		if [ $? -eq 0 ]; then
			recallog -s "${BASEFILE}" -t "unlockService" "lock file ${LOCKFILE} removed"
		else
			recallog -s "${BASEFILE}" -t "unlockService" "lock file ${LOCKFILE} removing failed :-(, access right issue ?"
			EXIT=1
		fi
	else
		EXIT=1
	fi
}

killService() {
	SPID=$( checkPID )
	if [ ! -n "$SPID" ]; then
		recallog -s "${BASEFILE}" -t "killService" "No Sinden Lightgun Service found"
		return 1
	else
		#to kill properly
		kill -TERM "${SPID}"
		if [ $? -eq 0 ]; then
			recallog -s "${BASEFILE}" -t "killService" "Sinden Lightgun Service killed"
			return 0
		else
			recallog -s "${BASEFILE}" -t "killService" "Sinden Lightgun Service killing failed :-("
			return 1
		fi
	fi
}

checkPIDAndUnlockIfNeeded() {
#this function return 0 if no service launched
#this function return 1 service already launched
	checkLock
	if [ $? -eq 0 ]; then
		LPID=$(cat "${LOCKFILE}")
		recallog -s "${BASEFILE}" -t "checkPIDAndUnlockIfNeeded" "Existing Sinden Lightgun Service lock file contains pid : ${LPID}"
		SPID=$( checkPID )
		if [ -n "$SPID" ]; then
			return 1
		else
			recallog -s "${BASEFILE}" -t "checkPIDAndUnlockIfNeeded" "Unlock the Sinden Lightgun Service due to pid missing/mistmatch"
			unlockService
			return 0
		fi
	else
		return 0
	fi
}

randomWait() {
	#random sleep (0 to 5999 ms)
	random_number=$(printf "%d.%d\n" $((RANDOM%5)) $((RANDOM%999)))
	recallog -s "${BASEFILE}" -t "randomWait" "The random 'wait' time is $random_number second(s)"
	echo "The random 'wait' time is $random_number second(s)"
	sleep $random_number
}

checkConfAndRestoreIfNeeded() {
	if (test -e "${WORKDIR}/${BINFILE}.config" && test -e "${WORKDIR}/${BINFILE}" && test -e "${WORKDIR}/libSdlInterface.so" && test -e "${WORKDIR}/libCameraInterface.so"); then
		recallog -s "${BASEFILE}" -t "checkConfAndRestoreIfNeeded" "Sinden Lightgun files ready"
	else
		install -m 0644 -D "${BINDIR}/${BINFILE}" "${WORKDIR}/${BINFILE}"
		install -m 0644 -D "${BINDIR}/libSdlInterface.so" "${WORKDIR}/libSdlInterface.so"
		install -m 0644 -D "${BINDIR}/libCameraInterface.so" "${WORKDIR}/libCameraInterface.so"
		install -m 0644 -D "${BINDIR}/${BINFILE}.config" "${WORKDIR}/${BINFILE}.config"
		recallog -s "${BASEFILE}" -t "checkConfAndRestoreIfNeeded" "Sinden Lightgun files restored"
	fi
	return 0
}

increaseNumberOfGun() {
	if ( test -e "${COUNTFILE}" ); then
		EXISTINGNUMBER=$(cat "${COUNTFILE}")
	else
		EXISTINGNUMBER=$(echo "0")
	fi
	if [[ ${EXISTINGNUMBER} -lt 2 ]]; then
		TOTAL=$(expr "${EXISTINGNUMBER}" + 1)
	else
		TOTAL=$(echo "2")
	fi
	printf "${TOTAL}" > "${COUNTFILE}"
	echo "${TOTAL}"
}

decreaseNumberOfGun() {
	if ( test -e "${COUNTFILE}" ); then
		EXISTINGNUMBER=$(cat "${COUNTFILE}")
		if [[ ${EXISTINGNUMBER} -ne 0 ]]; then
			TOTAL=$(expr "${EXISTINGNUMBER}" - 1)
		else
			TOTAL=$(echo "0")
		fi
	else
		TOTAL=$(echo "0")
	fi
	printf "${TOTAL}" > "${COUNTFILE}"
	echo "${TOTAL}"
}

starting() {
		randomWait
		checkPIDAndUnlockIfNeeded
		if [ $? -eq 0 ]; then
			#need to wait that hardware is really ready to avoid to launch service without devices (he could stop)
			recallog -s "${BASEFILE}" -t "start" "Sinden Lightgun Service is launching..."
			sleep 1
			checkConfAndRestoreIfNeeded
			PATH=/bin:/sbin:/usr/bin:/usr/sbin nohup /usr/bin/mono-service --debug -l:"${LOCKFILE}" -d:"${WORKDIR}" --no-daemon "${WORKDIR}/${BINFILE}" > "${LOGFILE}" &
			if [ $? -eq 0 ]; then
				recallog -s "${BASEFILE}" -t "start" "Sinden Lightgun Service is launched"
			else
				recallog -s "${BASEFILE}" -t "start" "Sinden Lightgun Service launching is failed :-("
			fi
		else
			recallog -s "${BASEFILE}" -t "start" "Sinden Lightgun Service already launched"
		fi
}

stopping() {
		recallog -s "${BASEFILE}" -t "stop" "Sinden Lightgun Service is stopping..."
		killService
		if [ $? -eq 1 ]; then
			recallog -s "${BASEFILE}" -t "stop" "Sinden Lightgun Service not stopped finally"
			EXIT=1
		fi
		checkPIDAndUnlockIfNeeded
		if [ $? -eq 0 ]; then
			recallog -s "${BASEFILE}" -t "stop" "Sinden Lightgun Service stopped"
			EXIT=0
		else
			recallog -s "${BASEFILE}" -t "stop" "Sinden Lightgun Service not well stopped :-("
			EXIT=1
		fi
}

#Carry out specific functions when asked to by the system
case "$1" in
	start)
		starting
	;;
	add)
	recallog -s "${BASEFILE}" -t "add" "Sinden Lightgun added..."
	randomWait
	TOTAL=$( increaseNumberOfGun )
	echo "${TOTAL} Sinden Lightgun(s) connected now"
	recallog -s "${BASEFILE}" -t "add" "${TOTAL} Sinden Lightgun(s) connected now"
	starting
	;;
	remove)
	recallog -s "${BASEFILE}" -t "remove" "Sinden Lightgun removed..."
	TOTAL=$( decreaseNumberOfGun )
	echo "${TOTAL} Sinden Lightgun(s) connected now"
	recallog -s "${BASEFILE}" -t "remove" "${TOTAL} Sinden Lightgun(s) connected now"
	;;
	stop)
		stopping
	;;
	status)
		SPID=$( checkPID )
		if [ -n "$SPID" ]; then
			echo "Sinden Lightgun Service is running with pid : ${SPID}"
			cat "${LOGFILE}"
		else
			echo "Sinden Lightgun Service is not running for the moment"
		fi
		if ( test -e "${COUNTFILE}" ); then
			EXISTINGNUMBER=$(cat "${COUNTFILE}")
			echo "${EXISTINGNUMBER} sinden lightgun(s) count"
		fi
	;;
	restart|reload)
		stopping
		sleep 2
		starting
	;;
	*)
		echo "Usage: $0 {start|stop|status|restart|reload|add|remove}"
		EXIT=1
	;;
esac

#echo "exit with value: ${EXIT}"
exit ${EXIT}

#examples of command for debugging

#to launch the service for sinden lightgun
#


# command to know if lightgunMono.exe is running and the process id associated
#ps ax | grep -i mono-service | grep -i LightgunMono.exe

#to kill process using pid
#kill -15 XXXX

