################################################################################
#
# sinden lightgun v2
#
################################################################################

SINDEN_LIGHTGUN_V2_VERSION = 2.05c
SINDEN_LIGHTGUN_V2_SOURCE=LinuxBeta$(SINDEN_LIGHTGUN_V2_VERSION).zip
SINDEN_LIGHTGUN_V2_SITE=https://www.sindenlightgun.com/software
SINDEN_LIGHTGUN_V2_LICENSE = Custom
SINDEN_LIGHTGUN_V2_LICENSE_FILES = licence.txt
SINDEN_LIGHTGUN_V2_ARCHIVE_DIR=LinuxBeta$(SINDEN_LIGHTGUN_V2_VERSION)/PCversion/Lightgun

define SINDEN_LIGHTGUN_V2_EXTRACT_CMDS
	mkdir -p $(@D) && cd $(@D) && unzip -x $(DL_DIR)/$(SINDEN_LIGHTGUN_V2_DL_SUBDIR)/$(SINDEN_LIGHTGUN_V2_SOURCE)
endef

define SINDEN_LIGHTGUN_V2_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL_RECALBOX_PATH)/package/sinden-lightgun-v2/S99sindenlightgun $(TARGET_DIR)/etc/init.d/S99sindenlightgun
	$(INSTALL) -m 0644 -D $(BR2_EXTERNAL_RECALBOX_PATH)/package/sinden-lightgun-v2/99-sinden-lightgun.rules $(TARGET_DIR)/etc/udev/rules.d/99-sinden-lightgun.rules

	$(INSTALL) -m 0644 -D $(BR2_EXTERNAL_RECALBOX_PATH)/package/sinden-lightgun-v2/LightgunMono.exe.config.template $(TARGET_DIR)/usr/bin/sinden/LightgunMono.exe.config

	$(INSTALL) -m 0644 -D $(@D)/$(SINDEN_LIGHTGUN_V2_ARCHIVE_DIR)/LightgunMono.exe      $(TARGET_DIR)/usr/bin/sinden/LightgunMono.exe
	$(INSTALL) -m 0644 -D $(@D)/$(SINDEN_LIGHTGUN_V2_ARCHIVE_DIR)/libSdlInterface.so    $(TARGET_DIR)/usr/bin/sinden/libSdlInterface.so
	$(INSTALL) -m 0644 -D $(@D)/$(SINDEN_LIGHTGUN_V2_ARCHIVE_DIR)/libCameraInterface.so $(TARGET_DIR)/usr/bin/sinden/libCameraInterface.so
endef

$(eval $(generic-package))
