################################################################################
#
# rpcs3
#
################################################################################

# Version: 0.0.30 # RPCN 1.1 - Commits on feb 15, 2024
RPCS3_CORE_VERSION = 0.0.30-eb8325af
RPCS3_VERSION = eb8325afcb9d374c880498139fdd06eae37fa947
RPCS3_SITE = https://github.com/RPCS3/rpcs3.git
RPCS3_SITE_METHOD=git
RPCS3_GIT_SUBMODULES=YES
RPCS3_LICENSE = GPLv2
RPCS3_DEPENDENCIES += alsa-lib llvm ffmpeg libevdev libglew libglu \
						libpng libusb mesa3d ncurses openal qt6base \
						qt6declarative qt6multimedia qt6svg wolfssl \
						libxml2 rtmpdump jack2

## print version of core in PixL
define RPCS3_PRE_CONFIGURE
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs
	echo "rpcs3;rpcs3;$(RPCS3_CORE_VERSION)" > $(TARGET_DIR)/recalbox/share_init/system/configs/rpcs3.corenames
endef
RPCS3_PRE_CONFIGURE_HOOKS += RPCS3_PRE_CONFIGURE

RPCS3_SUPPORTS_IN_SOURCE_BUILD = NO

RPCS3_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
RPCS3_CONF_OPTS += -DCMAKE_INSTALL_PREFIX=/usr
RPCS3_CONF_OPTS += -DCMAKE_CROSSCOMPILING=ON
RPCS3_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
RPCS3_CONF_OPTS += -DUSE_NATIVE_INSTRUCTIONS=OFF
RPCS3_CONF_OPTS += -DLLVM_DIR=$(STAGING_DIR)/usr/lib/cmake/llvm/
RPCS3_CONF_OPTS += -DUSE_PRECOMPILED_HEADERS=OFF
RPCS3_CONF_OPTS += -DSTATIC_LINK_LLVM=OFF
RPCS3_CONF_OPTS += -DBUILD_LLVM=OFF
RPCS3_CONF_OPTS += -DUSE_SYSTEM_FFMPEG=OFF
RPCS3_CONF_OPTS += -DUSE_SYSTEM_CURL=ON
RPCS3_CONF_OPTS += -DUSE_SYSTEM_LIBUSB=ON
RPCS3_CONF_OPTS += -DUSE_SDL=ON
RPCS3_CONF_OPTS += -DUSE_LIBEVDEV=OFF
RPCS3_CONF_OPTS += -DUSE_SYSTEM_FAUDIO=OFF
RPCS3_CONF_OPTS += -DENABLE_AMD_EXTENSIONS=ON
RPCS3_CONF_OPTS += -DENABLE_NV_EXTENSIONS=ON
RPCS3_CONF_OPTS += -DFFMPEG_LIB_AVCODEC=../3rdparty/ffmpeg/lib/linux/ubuntu-22.04/x86_64/libavcodec.a
RPCS3_CONF_OPTS += -DFFMPEG_LIB_AVFORMAT=../3rdparty/ffmpeg/lib/linux/ubuntu-22.04/x86_64/libavformat.a
RPCS3_CONF_OPTS += -DFFMPEG_LIB_AVUTIL=../3rdparty/ffmpeg/lib/linux/ubuntu-22.04/x86_64/libavutil.a
RPCS3_CONF_OPTS += -DFFMPEG_LIB_SWSCALE=../3rdparty/ffmpeg/lib/linux/ubuntu-22.04/x86_64/libswscale.a
RPCS3_CONF_OPTS += -DFFMPEG_LIB_SWRESAMPLE=../3rdparty/ffmpeg/lib/linux/ubuntu-22.04/x86_64/libswresample.a
RPCS3_CONF_ENV = LIBS="-ncurses -ltinfo"

define RPCS3_BUILD_CMDS
	mkdir -p $(TARGET_DIR)/recalbox/share_init/bios/ps3
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/ps3/rpcs3
	$(TARGET_CONFIGURE_OPTS) \
		$(MAKE) -C $(@D)/buildroot-build
endef

define RPCS3_INSTALL_EVMAPY
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/rpcs3/rpcs3.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef

RPCS3_POST_INSTALL_TARGET_HOOKS = RPCS3_INSTALL_EVMAPY

$(eval $(cmake-package))
