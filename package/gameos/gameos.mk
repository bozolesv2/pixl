################################################################################
#
# pixL themes for Pegasus : https://github.com/pixl-os/gameOS
#
################################################################################

# commit from pixL-master
# https://github.com/pixl-os/gameOS/
# version 1.28 - 2024-09-14
GAMEOS_VERSION = 77c82231a9902676f3d1ab852ec88d33645f9c8a
GAMEOS_SITE = $(call github,pixl-os,gameOS,$(GAMEOS_VERSION))
GAMEOS_LICENSE = GPL-3.0

define GAMEOS_INSTALL_TARGET_CMDS
	# create directories if not already done
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes/gameOS
	# copy theme files in share_init
	cp -R $(@D)/* $(TARGET_DIR)/recalbox/share_init/themes/gameOS
	# remove project files
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "*.pro" | xargs rm
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "*.ts" | xargs rm
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "CODEOWNERS" | xargs rm
endef

$(eval $(generic-package))
