# layout screen by Strodown
# 07/02/2024: updated to manage default value for primary screen
# 01/03/2024: updated to be able to manage screen independently (we could enabled primary and/or secondary now)


#!/bin/bash

export DISPLAY=:0
xrandrOutput=$(xrandr)

## Get info from xrandr
## Show all output and your state ( connected/disconnected )
allOutputs=$(echo "${xrandrOutput}" | awk '/connected/{print $1,$2}')

## Show only connected monitor 
connectedOutputs=$(echo "${allOutputs}" | awk '$2 == "connected"{print $1}')

## List all resolutions for all outputs
listOututResolution=$(echo "${xrandrOutput}" | awk -v 'monitor=\"^%1 connected\"' '/connected/ {p = 0} $0 ~ monitor {p = 1} p' | awk '{if(NR>1)print $1}')

## Check selected output resolution if not more 1080p
OUTPUT_MAXRES=$(echo "${xrandrOutput}" | awk '$1!~"[0-9]*x[0-9]*i?" {if (MAXRES!="") {print OUTPUT ";" MAXRES ";" FALLBACKRES;} OUTPUT=$1; MAXRES=""; FALLBACKRES=""; NBRESO=1} $1~"[0-9]*x[0-9]*i?" {if (NBRESO==1) {MAXRES=$1; NBRESO++;} HEIGHT=$1; gsub(/^[0-9]+x/,"",HEIGHT); if ((FALLBACKRES=="") && (HEIGHT+0<=1080)) {FALLBACKRES=$1;}} END {if (MAXRES!="") {print OUTPUT ";" MAXRES ";" FALLBACKRES;}}')

## Prefered output first screen and force resolution from recalbox.conf
PRIMARY_SCREEN_ENABLED=$(recalbox_settings -command load -key system.primary.screen.enabled)
PRIMARY_SCREEN=$(recalbox_settings -command load -key system.primary.screen)
PRIMARY_SCREEN_RES=$(recalbox_settings -command load -key system.primary.screen.resolution)
PRIMARY_SCREEN_FREQ=$(recalbox_settings -command load -key system.primary.screen.frequency)
PRIMARY_SCREEN_ROTATION=$(recalbox_settings -command load -key system.primary.screen.rotation)

## Prefered output second screen and force resolution from recalbox.conf
SECONDARY_SCREEN_ENABLED=$(recalbox_settings -command load -key system.secondary.screen.enabled)
SECONDARY_SCREEN=$(recalbox_settings -command load -key system.secondary.screen)
SECONDARY_SCREEN_RES=$(recalbox_settings -command load -key system.secondary.screen.resolution)
SECONDARY_SCREEN_FREQ=$(recalbox_settings -command load -key system.secondary.screen.frequency)
SECONDARY_SCREEN_ROTATION=$(recalbox_settings -command load -key system.secondary.screen.rotation)
SECONDARY_SCREEN_POSITION=$(recalbox_settings -command load -key system.secondary.screen.position)

echo "List all ouputs and states:"
echo "$allOutputs"
echo "list of connected monitor(s) :"
echo "$connectedOutputs"
echo "selected in recalbox.conf:"
echo "Primary Screen: $PRIMARY_SCREEN_ENABLED; $PRIMARY_SCREEN; $PRIMARY_SCREEN_RES; $PRIMARY_SCREEN_FREQ; $PRIMARY_SCREEN_ROTATION"
echo "Secondary Screen: $SECONDARY_SCREEN_ENABLED; $SECONDARY_SCREEN; $SECONDARY_SCREEN_RES; $SECONDARY_SCREEN_FREQ; $SECONDARY_SCREEN_ROTATION; $SECONDARY_SCREEN_POSITION"
echo "$listOututResolution"

#check if empty to get default screen
if [ -z "$PRIMARY_SCREEN" ]; then
  #take the first one found as primary
  PRIMARY_SCREEN=$(xrandr | grep -v disconnected | grep -i connected | grep -i primary |  awk '{print $1}')
  recallog -s "Externalscreen.sh" -t "primary screen" "save default value"
  recalbox_settings -command save -key system.primary.screen -value "${PRIMARY_SCREEN}"
  if [ -z "$SECONDARY_SCREEN_ENABLED" ] || [ $SECONDARY_SCREEN_ENABLED eq 0 ]; then
    recallog -s "Externalscreen.sh" -t "primary screen" "enable by default"
    recalbox_settings -command save -key system.primary.screen.enabled -value "1"
  fi
fi

#check if empty to get default resolution and save it for first time
if [ -z "$PRIMARY_SCREEN_RES" ]; then
  PRIMARY_SCREEN_RES=$(xrandr -q | grep -i "*" | awk '{print $1}')
  recallog -s "Externalscreen.sh" -t "primary screen resolution" "save default value"
  recalbox_settings -command save -key system.primary.screen.resolution -value "${PRIMARY_SCREEN_RES}"
fi

#check if empty to get default frequency and save it for first time
if [ -z "$PRIMARY_SCREEN_FREQ" ]; then
  PRIMARY_SCREEN_FREQ=$(xrandr -q | grep -i "*" | awk '{print $2}' | cut -d "*" -f 1)
  recallog -s "Externalscreen.sh" -t "primary screen frequency" "save default value"
  recalbox_settings -command save -key system.primary.screen.frequency -value "${PRIMARY_SCREEN_FREQ}"
fi

#check if empty to get default rotation
if [ -z "$PRIMARY_SCREEN_ROTATION" ]; then
  # Detect 'vertical' screen (as "phone" one used on steam deck in 800x1200) for example
  if [ "$(cut -d, -f1 /sys/class/graphics/fb0/virtual_size)" -le "$(cut -d, -f2 /sys/class/graphics/fb0/virtual_size)" ]; then
    recallog -s "Externalscreen.sh" -t "vertical screen" "clockwize rotation needed for X11 !"
    PRIMARY_SCREEN_ROTATION="right"
  else      
    PRIMARY_SCREEN_ROTATION="normal"
  fi
  recallog -s "Externalscreen.sh" -t "primary screen rotation" "save default value"
  recalbox_settings -command save -key system.primary.screen.rotation -value "${PRIMARY_SCREEN_ROTATION}"
fi

## prepare output arguments for Primary Screen (resolution, freq, rotation)
if [[ -n ${PRIMARY_SCREEN_RES} ]] && [[ -n ${PRIMARY_SCREEN_FREQ} ]] && [[ -n ${PRIMARY_SCREEN_ROTATION} ]]; then
  setResolution="--mode ${PRIMARY_SCREEN_RES} --refresh ${PRIMARY_SCREEN_FREQ} --rotate ${PRIMARY_SCREEN_ROTATION}"

elif [[ -n ${PRIMARY_SCREEN_RES} ]] && [[ -n ${PRIMARY_SCREEN_FREQ} ]]; then
  setResolution="--mode ${PRIMARY_SCREEN_RES} --refresh ${PRIMARY_SCREEN_FREQ}"

elif [[ -n ${PRIMARY_SCREEN_RES} ]]; then 
  setResolution="--mode ${PRIMARY_SCREEN_RES}"
fi

## prepare output arguments for Secondary Screen (resolution, freq, rotation)
if [[ -n ${SECONDARY_SCREEN_RES} ]] && [[ -n ${SECONDARY_SCREEN_FREQ} ]] && [[ -n ${SECONDARY_SCREEN_ROTATION} ]]; then
  setSecondResolution="--mode ${SECONDARY_SCREEN_RES} --refresh ${SECONDARY_SCREEN_FREQ} --rotate ${SECONDARY_SCREEN_ROTATION}"

elif [[ -n ${SECONDARY_SCREEN_RES} ]] && [[ -n ${SECONDARY_SCREEN_FREQ} ]]; then
  setSecondResolution="--mode ${SECONDARY_SCREEN_RES} --refresh ${SECONDARY_SCREEN_FREQ}"

elif [[ -n ${SECONDARY_SCREEN_RES} ]]; then
  setSecondResolution="--mode ${SECONDARY_SCREEN_RES}"
fi

## Check primary screen enabled
if [[ "${PRIMARY_SCREEN_ENABLED}" == "1" ]]; then
  ## Test if primary screen is connected
  if [[ -n "${PRIMARY_SCREEN}" ]]; then
    PRIMARY_SCREEN_OUTPUT=$(echo "${connectedOutputs}" | grep "${PRIMARY_SCREEN}" | awk 'BEGIN {FS=";"} {print $1}')
    if [[ -n "${PRIMARY_SCREEN}" ]] && [[ -n "${PRIMARY_SCREEN_OUTPUT}" ]]; then
      PRIMARY_SCREEN_CONNECTED="${PRIMARY_SCREEN_OUTPUT}"
    fi
  fi
  #restore backlight brightness level from recalbox.conf if needed
  /recalbox/system/hardware/device/pixl-backlight.sh restore_brightness
else
  #set backlight brightness level to 0 (but not saved in recalbox.conf in this case to be able to restore later)
  /recalbox/system/hardware/device/pixl-backlight.sh brightness 0
fi

## Check secondary screen enabled
if [[ "${SECONDARY_SCREEN_ENABLED}" == "1" ]]; then
  ## Test if secondary screen is connected
  if [[ -n "${SECONDARY_SCREEN}" ]]; then
    SECONDARY_SCREEN_OUTPUT=$(echo "${connectedOutputs}" | grep "${SECONDARY_SCREEN}" | awk 'BEGIN {FS=";"} {print $1}')
    if [[ -n "${SECONDARY_SCREEN}" ]] && [[ -n "${SECONDARY_SCREEN_OUTPUT}" ]]; then
      SECONDARY_SCREEN_CONNECTED="${SECONDARY_SCREEN_OUTPUT}"
    fi
  fi
fi

## List all output position
declare -A position=( ["above"]="--above" \
                      ["below"]="--below" \
                      ["left"]="--left-of" \
                      ["right"]="--right-of"
                    )

# initialize variables
xrandr_cmd="xrandr "

# build xrandr_cmd configuration
## set 2 monitors
if [[ -n ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -n ${SECONDARY_SCREEN_CONNECTED} ]]; then
  videomode=$(recalbox_settings -command load -key system.video.screens.mode)
  if [[ $videomode = "clone" ]]; then
    ## we will clone the first one to the second one (doesn't work with all emulator)
	## example : xrandr --output eDP --mode 800x1280 --refresh 60.00 --rotate right --scale 1.5x1.35 --output DisplayPort-0 --same-as eDP --mode 1920x1080 --scale 1x1
    ## need to calculate ratio betweent the 2 resolutions of each screen
    #split resolution to have x and y value
	PRIMARY_SCREEN_RES_SPLIT=(${PRIMARY_SCREEN_RES//x/ })
	SECONDARY_SCREEN_RES_SPLIT=(${SECONDARY_SCREEN_RES//x/ })
	#x_ratio=$(( ${SECONDARY_SCREEN_RES_SPLIT[0]} / ${PRIMARY_SCREEN_RES_SPLIT[0]} ))
	#y_ratio=$(( ${SECONDARY_SCREEN_RES_SPLIT[1]} / ${PRIMARY_SCREEN_RES_SPLIT[1]} ))
	#echo ${SECONDARY_SCREEN_RES_SPLIT[0]} / ${PRIMARY_SCREEN_RES_SPLIT[0]}
	#echo ${SECONDARY_SCREEN_RES_SPLIT[1]} / ${PRIMARY_SCREEN_RES_SPLIT[1]}
	if [[ ${PRIMARY_SCREEN_RES_SPLIT[0]} -gt ${PRIMARY_SCREEN_RES_SPLIT[1]} ]]; then 
		x_ratio=$(echo ${SECONDARY_SCREEN_RES_SPLIT[0]} / ${PRIMARY_SCREEN_RES_SPLIT[0]} | bc -l)
		y_ratio=$(echo ${SECONDARY_SCREEN_RES_SPLIT[1]} / ${PRIMARY_SCREEN_RES_SPLIT[1]} | bc -l)
	else # to manage vertical primary screen as on some "handheld" pc
		x_ratio=$(echo ${SECONDARY_SCREEN_RES_SPLIT[0]} / ${PRIMARY_SCREEN_RES_SPLIT[1]} | bc -l)
		y_ratio=$(echo ${SECONDARY_SCREEN_RES_SPLIT[1]} / ${PRIMARY_SCREEN_RES_SPLIT[0]} | bc -l)
	fi
	## --same-as to clone from a specific one
	## --scale to adapt first screen to second one
	##xrandr_cmd=${xrandr_cmd}"--output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --scale 1.5x1.35 --output ${SECONDARY_SCREEN_CONNECTED} --same-as ${PRIMARY_SCREEN_CONNECTED} ${setSecondResolution} --scale 1x1"
	##xrandr_cmd=${xrandr_cmd}"--output ${SECONDARY_SCREEN_CONNECTED} ${setSecondResolution} --output ${PRIMARY_SCREEN_CONNECTED} --same-as ${SECONDARY_SCREEN_CONNECTED} ${setResolution} --scale-from ${SECONDARY_SCREEN_RES}"
	xrandr_cmd=${xrandr_cmd}"--output ${SECONDARY_SCREEN_CONNECTED} ${setSecondResolution} --output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --same-as ${SECONDARY_SCREEN_CONNECTED} --scale ${x_ratio}x${y_ratio}"
  
  else # for video mode "extended" (default mode)
    xrandr_cmd=${xrandr_cmd}"--output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --output ${SECONDARY_SCREEN_CONNECTED} ${setSecondResolution} ${position[${SECONDARY_SCREEN_POSITION}]} ${PRIMARY_SCREEN_CONNECTED}"
  fi
## set only selected primary screen
## enable first screen and disable second screen
elif [[ -n ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -z ${SECONDARY_SCREEN_CONNECTED} ]] && [[ -n ${SECONDARY_SCREEN} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --transform none --output ${SECONDARY_SCREEN} --off"

## set only selected secondary screen
## enable second screen and disable first screen
elif [[ -n ${SECONDARY_SCREEN_CONNECTED} ]] && [[ -z ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -n ${PRIMARY_SCREEN} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${SECONDARY_SCREEN_CONNECTED} ${setSecondResolution} --transform none --output ${PRIMARY_SCREEN} --off"

## enable first screen only but we don't know a secondary one in this case
elif [[ -n ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -z ${SECONDARY_SCREEN_CONNECTED} ]] && [[ -z ${SECONDARY_SCREEN} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --transform none"

# disable all uneeded monitors and set default if no monitor connected
elif [[ -z ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -z ${SECONDARY_SCREEN_CONNECTED} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${connectedOutputs} ${setResolution}"
  recallog -s "Externalscreen.sh" -t "primary screen" "save default value"
  recalbox_settings -command save -key system.primary.screen -value "${connectedOutputs}"
fi

## check if the xrandr_cmd setup needs to be executed then run it
echo "check recent resolution and maximum resolution for 4k reduce"
echo "$OUTPUT_MAXRES"
echo "Command: $xrandr_cmd"
## Launch cmd 
`$xrandr_cmd`

