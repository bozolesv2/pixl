#!/bin/bash
# the goal of this script is to mount internal drive in /recalbox/share/internals/drive0 to 8
# first we have to list all devices except "external" as usb, mmc, sd, etc...

# Filter known amovible card device names
excluded_sd_patterns="fd|loop|mmcblk|optical|rom"
# reset counter for drive index
counter=0
# Filter internal drives based on characteristics and exclude SD cards
#blkid | awk -F':' '{print $1}'  | grep -vE "^(loop|mmcblk|sd|optical|rom)"
internal_drives=$(blkid | awk -F':' '{print $1}' | grep -Ev "$excluded_sd_patterns")
mounted_drives=$(df | awk '{print $1}' | grep '/dev')

# Check if any internal drives found
if [ -z "$internal_drives" ]; then
  echo "No internal drives found."
else
  echo "Internal Drive(s):"
  for drive in $internal_drives; do
     if [[ $mounted_drives =~ $drive ]]; then
         echo " $drive is already mount"
     else
        # Get size and additional information using blkid
        info=$(blkid -o list "$drive")
        if [[ $drive =~ "p" ]]; then #for internal drive using /dev/XXXXXpX as /nvme0n1p10
            disk=$(echo "$drive" | awk -F'p' '{print $1}')
            partition=$(echo "$drive" | awk -F'p' '{print $2}')
        else #for internal drive using /dev/sdx or /dev/hdx
            disk=${drive//[0-9]/}
            partition=$(echo "$drive" | sed 's/[^0-9.]//g')
        fi
        #echo " $disk / $partition ($info)"
        # Get arguments
        start_marker="Disk $disk"
        end_marker="Disk /"
        disk_info=$(fdisk -l | grep -m1 -A100000 "$start_marker" | grep -m2 -B100000 "$end_marker" | head -n -1 )
        #echo "disk_info 1: $disk_info"
        if [ -z "$disk_info" ]; then
            disk_info=$(fdisk -l | grep -m1 -A100000 "$start_marker")
            #echo "disk_info 2: $disk_info"
        fi
        #disk_info=$(fdisk -l | grep -m1 -A100000 "$start_marker" | grep -m2 -B100000 "$end_marker" | head -n -1 )
        #to detect format of disk details/partitions
        number_size_format=$(echo "$disk_info" | grep -i "Number" | grep -i "Size")
        #echo "number_size_format: $number_size_format"
        device_size_format=$(echo "$disk_info" | grep -i "Device" | grep -i "Size")
        #echo "device_size_format: $device_size_format"
        size=""
        if [ -n "$number_size_format" ]; then
          #to extract partition inof only
          size=$(echo "$disk_info" | grep -i " $partition " | awk -F' ' '{print $4}')
        elif [ -n "$device_size_format" ]; then
          #to extract partition info only
          size=$(echo "$disk_info" | grep -i "$drive" | sed 's/*//' | awk -F' ' '{print $7}')
        fi
        #check size in Giga (else the partition is ignored)
        #echo "$disk / $partition size : $size"
        if [[ $size =~ "G" ]]; then
            echo " $disk / $partition ($info)"
            FSTYPE=$(echo "$info" | awk -F'TYPE' '{print $2}' | awk -F'"' '{print $2}')
            FSDEV=${drive}
            MOUNTPOINT="/recalbox/share/internals/drive${counter}"
            (( counter++ ))  # Increment counter within the loop
            #echo "/recalbox/scripts/recalbox-mount.sh ${FSTYPE} 1 ${FSDEV} ${MOUNTPOINT}"
			if [[ "${FSTYPE}" != "ext4" ]]; then #only for extfat or vfat or ntfs
				echo "significant size detected: $size (could be mount as internal one)"
				/recalbox/scripts/recalbox-mount.sh "${FSTYPE}" 1 "${FSDEV}" "${MOUNTPOINT}"
				echo " mount !"
			else
				echo " not mount ! (can't mount ext4 on share using exfat)"
			fi
        fi
     fi
  done
fi
