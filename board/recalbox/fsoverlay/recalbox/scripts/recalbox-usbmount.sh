#!/bin/bash

# the aim of this script
# is to make things happend in this way :
# udev
# mounting /recalbox/share
# mounting other devices
#
# because:
# 1) there are 2 drivers for ntfs ; not the 2 are possible at the same time
# 2) for ntfs, if the device is not correctly removed, we've to try to fix (ntfsfix) or fallback in case of error, and the device must not be mounted
# 3) for some others actions, such as /dev/mmcblk0p3 fs growing, it must not be mounted

BASEFILE=$(basename "$0")

saveMountDataInTmp(){
    # save all in tmp files to help pegasus in term of quick display ;-)
    # save blkid to tmp file
    blkid > /tmp/blkid
    # save mount devices to tmp file
    mount > /tmp/mount
    # save blkid to tmp file
    df -kP > /tmp/df-kP
    # save fdisk -l to tmp file
    fdisk -l > /tmp/fdisk-list
}

# if share is already mounted, just use the basic usbmount
if test -e /var/run/recalbox.share.mounted
then
    #check if it is /overlay, /boot or /recalbox/share to avoid to mount it as externals !!!
    #to load env variable as parameters
    set -e
    TOEXCLUDE=$(df | grep -E " /overlay$| /recalbox/share$| /boot$" | grep -i "$DEVNAME" | wc -l)
    if [ $TOEXCLUDE -eq 1 ] ; then
        recallog -s "${BASEFILE}" -t "usbmount" "device ${DEVNAME} should be exclude"
        exit 0
    fi
    /usr/share/usbmount/usbmount "$1"
    return=$?
    if [ $return -eq 0 ]; then
        device=$DEVNAME
        recallog -s "${BASEFILE}" -t "usbmount" "device ${device} $1 with success"
        if [[ "$1" == "add" ]]; then
            #get mountpoint
            mountpoint=$(mount | grep ${device} | awk -F' ' '{print $3}')
            return=$?
            recallog -s "${BASEFILE}" -t "usbmount" "mount point: ${mountpoint}"
            #check if ROMS directory exists from mount point
            if [ -d "$mountpoint/roms" ] || [ -d "$mountpoint/pixl/roms" ] || [ -d "$mountpoint/recalbox/roms" ] ; then
                recallog -s "${BASEFILE}" -t "usbmount" "roms directory found"
                timeout 0.2 curl "http://localhost:1234/api?action=usbmount-$1"
                return=$?
            fi
            #check if RETRODE 2 detectable from mount point
            if [ -f "$mountpoint/RETRODE.CFG" ] ; then
                recallog -s "${BASEFILE}" -t "usbmount" "RETRODE detected"
                echo $device > /tmp/RETRODE.device
                echo $mountpoint > /tmp/RETRODE.mountpoint
                timeout 0.2 curl "http://localhost:1234/api?action=retrode-$device-$mountpoint"
                return=$?
            fi
            #check if USB-NES detectable from mount point
            if [ -f "$mountpoint/version.txt" ] && grep -q USB-NES "$mountpoint/version.txt" ; then
                recallog -s "${BASEFILE}" -t "usbmount" "USB-NES detected"
                echo $device > /tmp/USBNES.device
                echo $mountpoint > /tmp/USBNES.mountpoint
                timeout 0.2 curl "http://localhost:1234/api?action=usbnes-$device-$mountpoint"
                return=$?
            fi
        elif [[ "$1" == "remove" ]]; then
            if [ -f "/tmp/RETRODE.device" ] && grep -q "$device" /tmp/RETRODE.device; then
                recallog -s "${BASEFILE}" -t "usbmount" "RETRODE unplugged"
                rm /tmp/RETRODE.device
                rm /tmp/RETRODE.mountpoint
                timeout 0.2 curl "http://localhost:1234/api?action=retrode-$1"
                return=$?
            elif [ -f "/tmp/USBNES.device" ] && grep -q "$device" /tmp/USBNES.device; then
                recallog -s "${BASEFILE}" -t "usbmount" "USB-NES unplugged"
                rm /tmp/USBNES.device
                rm /tmp/USBNES.mountpoint
                timeout 0.2 curl "http://localhost:1234/api?action=usbnes-$1"
                return=$?
            else
                timeout 0.2 curl "http://localhost:1234/api?action=usbmount-$1"
                return=$?
            fi
        else
            return=0
        fi
    else
        recallog -s "${BASEFILE}" -t "usbmount" "device ${device} $1 with error $?"
        timeout 0.2 curl "http://localhost:1234/api?popup&title=issue with USB device&message=USB device ${device} not well $1&icon=&delay=5"
        return=$?
    fi
    saveMountDataInTmp
    exit ${return}
fi

# if not, delay the automounting by saving the context for later
# it will be played by the S11share script after the mounting of /recalbox/share
if mkdir -p /var/run/usbmount.delay
then
    N=$(ls /var/run/usbmount.delay | wc -l)
    let N++
    set |
    grep -E '^DEVNAME=|^ID_FS_USAGE=|^ID_FS_UUID=|^ID_FS_TYPE=|^ID_FS_LABEL=' |
    sed -e s+'^'+'export '+ > /var/run/usbmount.delay/"$N"."$1"
fi
