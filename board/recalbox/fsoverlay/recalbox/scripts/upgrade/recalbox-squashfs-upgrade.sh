#!/bin/bash

source /recalbox/scripts/recalbox-utils.sh
IMAGE_PATH=$(getInstallUpgradeImagePath)
INIT_SCRIPT=$(basename "$0")

UPDATEDIR=/boot/update
SHAREUPDATEDIR=/recalbox/share/system/upgrade

SCREEN_ORIENTATION=""

clean_boot_update() {
  mount -o remount,rw /boot
  rm -rf "$UPDATEDIR"/* .*.
  mount -o remount,ro /boot
}

clean_share_upgrade() {
  mount -o remount,rw /
  rm -rf "$SHAREUPDATEDIR"/* .*.
  mount -o remount,ro /
}

do_update() {

  #Start update...
  fbv2 -f -i -r "$IMAGE_PATH/update-7-pixl${SCREEN_ORIENTATION}.jpg"
  sleep 1

  UPDATEFILE="pixl_no_upgrade"

  for file in /recalbox/share/system/upgrade/pixl-*.img; do
    if [[ -f $file ]]; then
      UPDATEFILE=${file}
      break
    fi
  done

  # Mount
  echo "mount image"
  fbv2 -f -i -r "$IMAGE_PATH/update-8-pixl${SCREEN_ORIENTATION}.jpg"
  sleep 1
  LBA=$(lba-finder "${UPDATEFILE}")
  echo "intiating a losetup for lba ${LBA} (offset $((LBA * 512))s)"
  LOOPFILE=$(losetup -f --show "${UPDATEFILE}" -o $((LBA * 512))) || return 2
  mount "$LOOPFILE" /mnt || return 3

  echo "remounting /boot R/W"
  fbv2 -f -i -r "$IMAGE_PATH/update-9-pixl${SCREEN_ORIENTATION}.jpg"
  sleep 1
  mount -o remount,rw /boot/ || return 4

  # Files copy
  echo "copying update boot files"
  fbv2 -f -i -r "$IMAGE_PATH/update-10-pixl${SCREEN_ORIENTATION}.jpg"
  if [ -f /mnt/boot.lst ]; then
    while read -r file; do
      echo "  processing $file"
      [[ ! -d /boot/update/$(dirname "$file") ]] && mkdir -p "/boot/update/$(dirname "$file")"
      cp "/mnt/$file" "/boot/update/$file" || return 5
    done < /mnt/boot.lst
    cp /mnt/boot.lst /boot/update/ || return 6
  else
    return 7
  fi
  fbv2 -f -i -r "$IMAGE_PATH/update-11-pixl${SCREEN_ORIENTATION}.jpg"
  sleep 1

  # Umount
  echo "unmount image"
  umount /mnt/ || return 8
  fbv2 -f -i -r "$IMAGE_PATH/update-12-pixl${SCREEN_ORIENTATION}.jpg"
  sleep 1

  return 0

}

log() {
  while read -r line; do
    /usr/bin/recallog -s "${INIT_SCRIPT}" -t "UPGRADE" -f upgrade.log -e "$line"
  done
}

# redirect STDOUT and STDERR to recallog
exec &> >(log)
echo "starting upgrade"

# Detect 'vertical' screen (as "phone" one used on steam deck in 800x1200)
if [ "$(cut -d, -f1 /sys/class/graphics/fb0/virtual_size)" -le "$(cut -d, -f2 /sys/class/graphics/fb0/virtual_size)" ]; then
  SCREEN_ORIENTATION="-vertical"
fi

do_update
RC=$?
if [ $RC -eq 0 ]; then
  if [ -f "/boot/update/pre-upgrade.sh" ]; then
    echo "running pre-upgrade script"
    bash /boot/update/pre-upgrade.sh
  fi
  # Reboot
  echo "upgrade successfull, rebooting"
  fbv2 -f -i -r "$IMAGE_PATH/update-completed-pixl${SCREEN_ORIENTATION}.jpg"
  sleep 3
else
  echo "upgrade failed return code $RC" >&2
  fbv2 -f -i -r "$IMAGE_PATH/update-error-pixl${SCREEN_ORIENTATION}.jpg"
  sleep 3
fi
sync
shutdown -r now
exit 0
