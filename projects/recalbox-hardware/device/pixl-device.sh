# pixl-device
#
# how to use this file  ?
# in your bash script add this following line:
#
# source /recalbox/scripts/pixl-device.sh
#

DEVICEINFO=""

getDeviceInfo() {
	SYS_VENDOR=$(cat /sys/devices/virtual/dmi/id/sys_vendor | tr '[:upper:]' '[:lower:]')
	PRODUCT_FAMILY=$(cat /sys/devices/virtual/dmi/id/product_family | tr '[:upper:]' '[:lower:]')
	PRODUCT_VERSION=$(cat /sys/devices/virtual/dmi/id/product_version | tr '[:upper:]' '[:lower:]')
	PRODUCT_NAME=$(cat /sys/devices/virtual/dmi/id/product_name | tr '[:upper:]' '[:lower:]')
	recallog -s "${BASEFILE}" -t "getDeviceInfo" "Device detected : ${SYS_VENDOR}_${PRODUCT_FAMILY}_${PRODUCT_VERSION}_${PRODUCT_NAME}"
	echo "${SYS_VENDOR}_${PRODUCT_FAMILY}_${PRODUCT_VERSION}_${PRODUCT_NAME}"
}

checkDeviceConf() {
	DEVICEINFO=$( getDeviceInfo )
	CONFFILE="/recalbox/system/hardware/device/${DEVICEINFO}.cfg"
	if (test -e "${CONFFILE}" ); then
		recallog -s "${BASEFILE}" -t "checkDeviceConf" "device configuration ${CONFFILE} found"
		# Check if configuration file is already updated
		if [[ $( grep -c deviceconf=${DEVICEINFO} ${FILETOUPDATE}) -gt 0 ]] ; then
			recallog -s "${BASEFILE}" -t "updateConf" "Configuration of ${FILETOUPDATE} already done"
			return 1
		else
			return 0
		fi
	else
		recallog -s "${BASEFILE}" -t "checkDeviceConf" "device configuration ${CONFFILE} not found"
		return 1
	fi
}

updateConf() {
	if [[ $FILETOUPDATE = *-boot.conf* ]]; then
		FILTER1="grep -i boot."
		FILTER2="grep -i ="
		TOREMOVE="boot."
	else
		FILTER1="grep -v boot."
		FILTER2="grep -i ="
		TOREMOVE=""
	fi
	cat ${CONFFILE} | ${FILTER1} | ${FILTER2} | while read line
	do
	  #remove value if necessary
	  line=${line//"$TOREMOVE"/}
	  #echo "line : ${line}"
	  #split line to have parameter and value
	  array=(${line//=/ })
	  #echo ${array[0]}
	  #echo ${array[1]}
	  #update line
	  if [[ $( grep -c ${array[0]}= ${FILETOUPDATE}) -gt 0 ]] ; then
		#update of existing parameter=value
		#echo "sed -i '/'${array[0]}'/s/=.*/='${array[1]}'/' ${FILETOUPDATE}"
		sed -i '/'${array[0]}'=/s/=.*/='${array[1]}'/' ${FILETOUPDATE}
	  else
		#creation of new parameter=value
		printf "\n${line}" >> ${FILETOUPDATE}
	  fi
	done
	if [[ $( grep -c deviceconf= ${FILETOUPDATE}) -gt 0 ]] ; then
		#echo "sed -i '/deviceconf=/s/=.*/=$'{DEVICEINFO}'/' ${FILETOUPDATE}"
		sed -i '/deviceconf=/s/=.*/='${DEVICEINFO}'/' ${FILETOUPDATE}
	else
		#echo "\ndeviceconf=${DEVICEINFO}"
		printf "\ndeviceconf=${DEVICEINFO}" >> ${FILETOUPDATE}
	fi
	recallog -s "${BASEFILE}" -t "updateConf" "Reconfiguration of ${FILETOUPDATE} done"
}

