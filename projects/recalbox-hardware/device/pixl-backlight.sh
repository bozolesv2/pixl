#!/bin/sh

#check first if any backlight device is defined for this device (as for steamdeck)
DEVICE=$(recalbox_settings -command load -key system.primary.screen.backlight.device)
# test if DEVICE is set or not
if [ ! -z "${DEVICE}" ] ; then
	D="/sys/class/backlight/${DEVICE}"
else
	# search any backlight device
	D=$(find /sys/class/backlight/* 2>/dev/null | grep backlight | head -n1 | tr -d '%' | tr -d '[:space:]')
fi
#echo "BACKLIGHT DEVICE : ${D}"
EXIT=0
BASEFILE=$(basename "$0")
config_file="/recalbox/share/system/recalbox.conf"
NEWVAL=""
XMAX=""
LOCKFILE=/var/run/$BASEFILE.lock

# check if brightness parameter exists
if test ! -e "${D}"/brightness
then
    echo "no brightness found" >&2
    exit 1
else
    B="${D}"/brightness
fi

setValue() {
    #echo "NEWVAL : ${NEWVAL}"
    #echo "XMAX : ${XMAX}"
    test "${NEWVAL}" -lt 0         && NEWVAL=0
    test "${NEWVAL}" -gt "${XMAX}" && NEWVAL="${XMAX}"
    #echo "NEWVAL : ${NEWVAL}"
    echo "${NEWVAL}" > "${B}"
}

getBrightness() {
    BRIGHTNESS_VALUE=$(cat "${B}")
}

getBrightnessMax() {
    #check maximum value
    XMAX=$(cat "${D}"/max_brightness)
}

saveBrightness(){
	#recallog -s "${BASEFILE}" -t "saveBrightness" "in ${config_file}"
	recalbox_settings -command save -key screen.brightness -value "${BRIGHTNESS_PERCENTAGE}"
}

loadBrightness(){
	#recallog -s "${BASEFILE}" -t "loadBrightness" "in ${config_file}"
	BRIGHTNESS_PERCENTAGE=$(recalbox_settings -command load -key screen.brightness)
}

alertPegasusOnBrightnessChange(){
	#recallog -s "${BASEFILE}" -t "alertPegasusOnBrightnessChange" "for brightness : ${BRIGHTNESS_PERCENTAGE}"
	timeout 0.2 curl "http://localhost:1234/api?conf=reload&parameter=screen.brightness"
}

case "$1" in
	brightness)
		getBrightnessMax
		# test if brightness is set or not
		if [ ! -z "$2" ] ; then
			NEWVAL=$(expr "${2}" '*' ${XMAX} / 100)
			setValue
			#no save of value or alert to Pegasus-Frontend in thi case
			#this case will be used to force value to 0 or restore previous one
		fi
		getBrightness
		#calculate percentage
		BRIGHTNESS_PERCENTAGE=$(expr "${BRIGHTNESS_VALUE}" '*' 100 / ${XMAX})
		echo "${BRIGHTNESS_PERCENTAGE}" | tr -d '%' | tr -d '[:space:]'
	;;
	restore_brightness)
		getBrightnessMax # from sytem value
		loadBrightness # from recalbox in percentage
		NEWVAL=$(expr "${BRIGHTNESS_PERCENTAGE}" '*' ${XMAX} / 100)
		setValue
		#getBrightness # from sytem value
		#echo "${BRIGHTNESS_VALUE}" | tr -d '%' | tr -d '[:space:]'
	;;
	brightness_max)
		getBrightnessMax # from sytem value
		echo "${XMAX}" | tr -d '%' | tr -d '[:space:]'
	;;
	brightness-)
		if (! test -e "${LOCKFILE}" ); then
			echo "brightness locked"
			echo "brightness locked" > $LOCKFILE
			sleep 0.2
			getBrightness
			getBrightnessMax
			#echo "BRIGHTNESS_VALUE : ${BRIGHTNESS_VALUE}"
			#echo "PERCENTAGE : ${2}"
			#echo "BRIGHTNESS MAX : ${XMAX}"
			DELTA=$(expr "${2}" '*' ${XMAX} / 100)
			NEWVAL=$(expr "${BRIGHTNESS_VALUE}" - "${DELTA}")
			setValue
			#calculate new percentage
			BRIGHTNESS_PERCENTAGE=$(expr "${NEWVAL}" '*' 100 / ${XMAX})
			#echo "BRIGHTNESS_PERCENTAGE : ${BRIGHTNESS_PERCENTAGE}"
			saveBrightness
			alertPegasusOnBrightnessChange
			rm $LOCKFILE
			echo "brightness unlocked"
		else
			echo "brightness already locked - aborted"
		fi
	;;
	brightness+)
		if (! test -e "${LOCKFILE}" ); then
			echo "brightness locked"
			echo "brightness locked" > $LOCKFILE
			sleep 0.2
			getBrightness
			getBrightnessMax
			#echo "BRIGHTNESS_VALUE : ${BRIGHTNESS_VALUE}"
			#echo "PERCENTAGE : ${2}"
			#echo "BRIGHTNESS MAX : ${XMAX}"
			DELTA=$(expr "${2}" '*' ${XMAX} / 100)
			NEWVAL=$(expr "${BRIGHTNESS_VALUE}" + "${DELTA}")
			setValue
			#calculate new percentage
			BRIGHTNESS_PERCENTAGE=$(expr "${NEWVAL}" '*' 100 / ${XMAX})
			#echo "NEW BRIGHTNESS_PERCENTAGE : ${BRIGHTNESS_PERCENTAGE}"
			saveBrightness
			alertPegasusOnBrightnessChange
			rm $LOCKFILE
			echo "brightness unlocked"
		else
			echo "brightness already locked - aborted"
		fi
	;;
	*)
		echo "Usage: $0 {brightness|brightness_max|brightness XX|brightness+ XX|brightness- XX|restore_brightness}"
		EXIT=1
	;;
esac

#echo "exit with value: ${EXIT}"
exit ${EXIT}

