#!/usr/bin/env python
from __future__ import division
import os
import sys

# Set a specific video mode
from configgen.settings.keyValueSettings import keyValueSettings

import datetime
def Log(txt):
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + str(txt))
    return 0

def setVideoMode(videoMode: str, delay=0.5):
    # The user mentionned default for the videomode
    # video mode can be default, a "CEA 4 HDMI" like, a hdmi_cvt pattern or even a hdmi_timings pattern
    # Anything else should result in a crash
    if videoMode == "default":
        return "default"
    videoMode = videoMode.strip()
    if videoMode == "auto":
        videoSetting = checkAutoMode()
        # autoMode can have replied "default"
        if videoSetting == "default":
            return "default"
        cmd = createVideoModeLine(videoSetting)
    elif "auto" in videoMode:
        realSetting = videoMode.split(' ', 1)[1]
        videoSetting = checkAutoMode(realSetting)
        # autoMode can have replied "default"
        if videoSetting == "default":
            return "default"
        cmd = createVideoModeLine(videoSetting)
    else:
        cmd = createVideoModeLine(videoMode)

    if cmd:
        os.system(cmd)
        import time
        time.sleep(delay)
        return cmd
    else:
        Log("Error: Could not find a suitable video mode")
        sys.exit(1)


def createVideoModeLine(videoMode):
    import re
    # pattern (CEA|DMT) [0-9]{1,2} HDMI
    if re.match("^(CEA|DMT) [0-9]{1,2}( HDMI)?$", videoMode):
        return "tvservice -e '{}'".format(videoMode)
    if re.match("^hdmi_cvt [0-9 ]{10,20}$", videoMode):
        return "vcgencmd {} && tvservice -e 'DMT 87'".format(videoMode)
    if re.match("^hdmi_timings [0-9 ]{48,58}$", videoMode):
        return "vcgencmd {} && tvservice -e 'DMT 87'".format(videoMode)
    Log("{} is not a valid video mode, abort".format(videoMode))
    return ''


# Set a specific video mode
def isSupported(group="CEA", mode='', drive="HDMI"):
    groups = ['CEA', 'DMT']
    if group not in groups:
        Log("Error: {} is an unknown group. Can't switch to {} {} {}".format(group, group, mode, drive))
        sys.exit(1)

    drives = ['HDMI', 'DVI']
    if drive not in drives:
        Log("Error: {} is an unknown drive. Can't switch to {} {} {}".format(drive, group, mode, drive))
        sys.exit(1)

    import subprocess
    proc = subprocess.Popen(["tvservice -j -m {}".format(group)], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    # print "program output:", out
    import json
    tvmodes = json.loads(out)

    for tvmode in tvmodes:
        Log("Testing {} vs {}".format(tvmode["code"], int(mode)))
        if tvmode["code"] == int(mode):
            return True

    Log("The resolution for '{} {} {}' is not supported by your monitor".format(group, mode, drive))
    return False


# Switch to prefered mode
def setPrefered(recalboxOptions: keyValueSettings):
    esVideoMode = recalboxOptions.getString("system.es.videomode", "")
    # Scary bug in tvservice : setting preferred mode on composite makes CEA 1 DVI !
    # See https://github.com/raspberrypi/firmware/issues/901
    # Once this issue is solved, just tvservice -p
    if esVideoMode in ("", "auto"):
        os.system("tvservice -p")
    else:
        setVideoMode(esVideoMode)


# Check auto mode, return the expected value
def checkAutoMode(expectedMode=None):
    # Resolutions to handle :
    # state 0x40001 [NTSC 4:3], 720x480 @ 60.00Hz, interlaced
    # state 0x12000a [HDMI CEA (16) RGB lim 16:9], 1920x1080 @ 60.00Hz, progressive
    # state 0x400000 [LCD], 800x480 @ 0.00Hz, progressive
    # state 0x400000 [LCD], 320x240 @ 0.00Hz, progressive
    # state 0x120006 [DVI DMT (58) RGB full 16:10], 1680x1050 @ 60.00Hz, progressive

    import subprocess
    proc = subprocess.Popen(["tvservice -s"], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()

    # This one does match what i need ! Everything ! Passes the 5 cases listed above
    import re
    regex = r".*\[([A-Z]{3,4}) ?(.*[^0-9:]?) ?([0-9]{1,2})?:?([0-9]{1,2})?\], ([0-9]{3,4})x([0-9]{3,4}) @ ([0-9.]{1,6})Hz, (progressive|interlaced).*"

    matches = re.match(str(regex), str(out))
    if not matches:
        # We should log the out var and log that it doesn't match any known pattern
        Log('auto mode -> had to set default')
        return "default"
    drive, details, wRatio, hRatio, width, height, refreshRate, progressiveOrInterlace = matches.groups()

    # Now the magic
    # if the screen supports CEA 4, and its current format is at least 16:9, go for CEA 4
    if drive not in ["HDMI", "DVI"]:
        Log("{} is not among HDMI/DVI, fallback to default".format(drive))
        return "default"
    if expectedMode is not None:
        try:
            autoGroup, autoMode, autoDrive = expectedMode.split()
        except:
            Log("{} is not a valid format".format(expectedMode))
            raise
    else:
        autoGroup, autoMode, autoDrive = ['CEA', 4, drive]
    if isSupported(autoGroup, autoMode, autoDrive):
        Log("auto mode -> {} {} {} is valid".format(autoGroup, autoMode, autoDrive))
        return "{} {} {}".format(autoGroup, autoMode, autoDrive)
    # Otherwise (composite output, 5:4 screens, mini DPI screens etc ...) -> default
    else:
        Log("auto mode -> CEA 4 HDMI/DVI not supported, fallback to default")
        return "default"


# Return the current resolution
def getCurrentResolution():
    # This is really dirty, I must admit ...
    # Call tvservice -s
    import subprocess
    proc = subprocess.Popen(["tvservice -s"], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    # print "program output:", out
    # If it's a valid json : we're not on pi, so we have the current resolution
    try:
        import json
        
        # to convert to string and replace characters
        content = str(out)
        # to remove all end of lines and spaces
        content = content.replace("\\n"," ")
        content = content.replace(" ","")
        # to remove "binary" format
        content = content.replace("b'[","[")
        content = content.replace("]'","]")
        # to remove multi-screens delimiter to be compliant with JSON format
        content = content.replace("][",",")
        
        Log("content : " + content)
        tvmodes = json.loads(content)
        Log(tvmodes)
        if tvmodes[0]["width"] > tvmodes[0]["height"]:
            return tvmodes[0]["width"], tvmodes[0]["height"]
        else: #case of 'vertical' screens like designed for phone
            return tvmodes[0]["height"], tvmodes[0]["width"]
    except ValueError:
        # else we're on pi, parse the output
        import re
        regex = r".*\[([A-Z]{3,4}) ?(.*[^0-9:]?) ?([0-9]{1,2})?:?([0-9]{1,2})?\], ([0-9]{3,4})x([0-9]{3,4}) @ ([0-9.]{1,6})Hz, (progressive|interlaced).*"

        matches = re.match(str(regex), str(out))
        if not matches:
            # We should log the out var and log that it doesn't match any known pattern
            Log("getCurrentResolution - Couldn't parse output: {}".format(out))
            raise ValueError("getCurrentResolution - Couldn't parse output: {}".format(out))
        drive, details, wRatio, hRatio, width, height, refreshRate, progressiveOrInterlace = matches.groups()
        if width > height:
            return width, height
        else: #case of 'vertical' screens like designed for phone
            return height, width

# Return the current resolution as [int, int] from framebuffer
def getCurrentFramebufferResolution():
    import re
    f = open("/sys/class/graphics/fb0/modes", "r")
    line = f.read()
    f.close()
    regex = r".*?([0-9]{3,4})x([0-9]{3,4}).*"
    matches = re.match(regex, line)

    if matches:
        width, height = matches.groups()
    else:
        Log("getCurrentFramebufferResolution - Couldn't parse output: {}".format(line))
        with open("/sys/class/graphics/fb0/virtual_size", 'r') as f:
            width, height = f.readline().split(',', 2)

    f = open("/sys/devices/virtual/graphics/fbcon/rotate", "r")
    rotate = f.readline().rstrip()
    f.close()
    if rotate == "1" or rotate == "3":
        return int(height), int(width)
    else:
        return int(width), int(height)

# Return video renderer OpenGl / vulkan for many emulators
# Have vulkan support ?
def GetHasVulkanSupport():
    import os
    if os.path.exists("/tmp/vulkaninfo.tmp"):
        for line in open("/tmp/vulkaninfo.tmp"):
            if "Vulkan Instance Version:" in line:
                if os.path.exists("/recalbox/share/system/recalbox.conf"):
                    for line in open("/recalbox/share/system/recalbox.conf"):
                        if "system.video.driver.vulkan=1" in line:
                            return "1"
    return "0"