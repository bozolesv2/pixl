#!/usr/bin/env python3

#modules
import os
import subprocess
import sys
import configgen.recalboxFiles as recalboxFiles
import configgen.utils.popup as popup

#classes
from configgen.Command import Command
from configgen.Emulator import Emulator
from configgen.settings.keyValueSettings import keyValueSettings
from time import sleep

import datetime
def Log(txt):
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

class wine:

    #variable accessible from class for parent
    env = {}
    #to be accessible without ask wine to check version itself
    wineversion = ""
    
    # constructor
    def __init__(self, system: Emulator, recalboxOptions: keyValueSettings, letFSUnlocked : bool = False, pixLWineBinPath: str = ""):
        # initial settings
        self.system: Emulator = system
        self.recalboxOptions: keyValueSettings = recalboxOptions
        # to know if we need to let unlock or not the FS, usefull for testing during dev
        self.letFSUnlocked = letFSUnlocked
        #values that we could modify for testing new wine appimage
        #appimage = "wine-stable_8.0.1-x86_64.AppImage" # image well compatible with DInput(now)/Xinput/vibration
        #TO DO to manage from pegasus-frontend and from emulator menu ;-)
        # using new parameters from recalbox.conf:
        # {emulator}.wineappimage
        # {emulator}.winearch
        # {emulator}.wineprefix
        # {emulator}.winetricks
        # {emulator}.winver
        # {emulator}.winedlloverrides
        
        #default appimage value is selected if no other wine selected from generator
        self.appimage = "wine-staging-linux-x86-v5.11-PlayOnLinux-x86_64.AppImage"
        self.winetricks = "winetricks" # could be "winetricks" from /usr/wine, or other name as "winetricks64" or "winetricks.20200412" also from /usr/wine
                                       # but in could be also "internal" from appimage called by command "wine winetricks" in this case
        #SOUND
        if self.recalboxOptions.hasOption(self.system.Emulator + ".wineaudiodriver") :
            self.wineaudiodriver = self.recalboxOptions.getString(self.system.Emulator + ".wineaudiodriver", "alsa")
        else:
            self.wineaudiodriver = "alsa" #use alsa by default
        
        #VIDEO RENDERER
        self.winesoftrenderer = self.recalboxOptions.getInt(self.system.Emulator + ".winesoftrenderer",0) #not activated by default
        self.wineengine = pixLWineBinPath
        #now get Wine configuration also from recalbox if exists for wine binary or appimage
        if(self.recalboxOptions.getString(self.system.Emulator + ".wine","") != ""):
            self.wineengine = self.recalboxOptions.getString(self.system.Emulator + ".wine", pixLWineBinPath)
            self.appimage = ""
            self.appimagePath = ""
        elif(self.recalboxOptions.getString(self.system.Emulator + ".wineappimage","") != ""):
            self.appimagePath = self.recalboxOptions.getString(self.system.Emulator + ".wineappimage", "/usr/wine/" + self.appimage)
            self.appimage = self.appimagePath.split('/')[-1]
            self.wineengine = ""
        elif (self.wineengine == ""):
            self.appimagePath = "/usr/wine/" + self.appimage
        else:
            self.appimage = ""
            self.appimagePath = ""
        Log("self.appimagePath : " + self.appimagePath)
        Log("self.appimage : " + self.appimage)            

        #WINARCH
        self.winearch = self.recalboxOptions.getString(self.system.Emulator + ".winearch", "")

        #WINVER
        self.winver = self.recalboxOptions.getString(self.system.Emulator + ".winver", "")

        self.winedlloverrides = "mshtml=d" # Could be set with "mscoree=d" and/or "mshtml=d" usually or any other dll override
        self.winedlloverrides = self.recalboxOptions.getString(self.system.Emulator + ".winedlloverrides", self.winedlloverrides)
        #by default we keep wine-mono: Mono is an open-source and cross-platform implementation of the .NET Framework. Wine can use a Windows build of Mono to run.
        #by default we deactivate wine-gecko: Wine implements its own version of Internet Explorer. The implementation is based on a custom version of Mozilla's Gecko Layout Engine.
       
        self.wineprefixroot="/recalbox" #by default, we store it because we could write at this place as linux partition accessible
        self.wineprefixroot = self.recalboxOptions.getString(self.system.Emulator + ".wineprefixroot", self.wineprefixroot)
        
        #fixed path
        self.wineDirPath = "/usr/wine"
        self.winePath = "/usr/wine/wine"
        self.wineserverPath = "/usr/wine/wineserver"
        self.winetricksPath = "/usr/wine/" + self.winetricks # default path used for winetricks when it is extrenal to appimage
        if self.recalboxOptions.hasOption(self.system.Emulator + ".winetricks"):
            if(self.recalboxOptions.getString(self.system.Emulator + ".winetricks", self.winetricks)) == "internal":
                self.winetricks = "internal"
                self.winetricksPath = ""

        Log("emulator : " + self.system.Emulator)

        # in case of wine, we need temporary read/write access to overlay partition (to install and/or update registry)
        # pass in read/write now
        os.system('mount -o remount,rw /')
        
        popup.Message("Symlinks creation/update, please wait...", 3)

        if self.winetricks != "internal":
            cmd = 'chmod +x ' + self.winetricksPath
            Log(f"command: {cmd}")
            os.system(cmd)
            
        #set image as executable + create or recreate symbolic links (done systematically to each check/install)
        if self.appimage != "":
            cmd = 'chmod +x ' + self.appimagePath
            Log(f"command: {cmd}")
            os.system(cmd)
            cmd = 'ln -sfn ' + self.appimagePath + ' ' + self.winePath
            Log(f"command: {cmd}")
            os.system(cmd)
            cmd = 'ln -sfn ' + self.appimagePath + ' ' + self.wineserverPath
            Log(f"command: {cmd}")
            os.system(cmd)
        elif self.wineengine != "":
            cmd = 'ln -sfn ' + self.wineengine + ' ' + self.winePath
            Log(f"command: {cmd}")
            os.system(cmd)
            pixLWineServerBinPath = self.wineengine
            pixLWineServerBinPath = pixLWineServerBinPath.replace("/bin/wine","/bin/wineserver")
            #remove also the 32/64 if wine32/wine64 binary is used
            pixLWineServerBinPath = pixLWineServerBinPath.replace("64","")
            pixLWineServerBinPath = pixLWineServerBinPath.replace("32","")
            cmd = 'ln -sfn ' + pixLWineServerBinPath + ' ' + self.wineserverPath
            Log(f"command: {cmd}")
            os.system(cmd)
            #added here quickly to do version() for wine32/wow64 internally installed
            if (not "wine64" in self.wineengine) :
                pixLWineLibPath = self.wineengine
                os.environ["LD_LIBRARY_PATH"] ="/lib32:" + pixLWineLibPath.replace("/bin/wine","/lib/wine").replace("32","")
        
        #to get version from wine
        self.wineversion = self.version()
        self.wineversion = self.wineversion.replace(" ", "_")
        self.wineversion = self.wineversion.replace("(", "")
        self.wineversion = self.wineversion.replace(")", "")
        
        #to get engine from wine if exist (not applicable for appimage
        wineengine = self.wineengine
        wineengine = wineengine.replace("/usr/wine/","")
        wineengine = wineengine.replace("/bin/wine","")
        
        #create wine prefix using emulator name + wine version + wine engine/app image(optional) + wine arch(optional)
        self.wineprefix = self.wineprefixroot + "/" + "." + self.system.Emulator + "_" + self.wineversion
        if (wineengine != "") :
            self.wineprefix = self.wineprefix + "_" + wineengine
        else:
            self.wineprefix = self.wineprefix + "_" + "AppImage"
        if (self.winearch != "") :
            self.wineprefix = self.wineprefix + "_" + self.winearch

        Log("The Wine prefix is : " + self.wineprefix)
        # set reference to registry
        self.userRegistryFile = self.wineprefix + "/user.reg"
        self.systemRegistryFile = self.wineprefix + "/system.reg"
        
        #WINEDLLOVERRIDES are here to avoid to launch additional popups to install wine-mono and gecko
        #LC_ALL="en_US.UTF-8" to be sure to stay in english for all
        #since 24/06/2024: wine is forced to work in english only to avoid issue with naming of each windows
        self.env = { "LC_ALL": "en_US.UTF-8",
                     "WINEDLLOVERRIDES": self.winedlloverrides, 
                     "WINEPREFIX": self.wineprefix, 
                     "PATH": self.wineDirPath + ":/bin:/sbin:/usr/bin:/usr/sbin", 
                     "WINEDEBUG": "-all"}
        
        #force also update in system variable to be sure at 100%
        os.environ["LC_ALL"] = "en_US.UTF-8"
        os.environ["WINEDLLOVERRIDES"] = self.winedlloverrides
        os.environ["WINEPREFIX"] = self.wineprefix
        os.environ["WINEDEBUG"]= "-all" #to avoid to generate logs that could impact performance
        os.environ["PATH"] = self.wineDirPath + ":/bin:/sbin:/usr/bin:/usr/sbin"
        
        #set WINEARCH only if not empty
        if self.winearch != "" : 
            self.env.update({"WINEARCH": self.winearch})
            os.environ["WINEARCH"] = self.winearch

        # added only for pixL Wine 32 bits
        if (not "wine64" in self.wineengine) and (self.wineengine != "") :
            pixLWineLibPath = self.wineengine
            # set the environment variables for game execution
            self.env.update(
                {"LD_LIBRARY_PATH": "/lib32:" + pixLWineLibPath.replace("/bin/wine","/lib/wine"),
                 "LIBGL_DRIVERS_PATH": "/lib32/dri",
                 "SPA_PLUGIN_DIR": "/usr/lib/spa-0.2:/lib32/spa-0.2"}
            )
            
            # set os environment variables for immediate usage
            os.environ["LD_LIBRARY_PATH"] = "/lib32:" + pixLWineLibPath.replace("/bin/wine","/lib/wine")
            os.environ["LIBGL_DRIVERS_PATH"] = "/lib32/dri"
            os.environ["SPA_PLUGIN_DIR"] = "/usr/lib/spa-0.2:/lib32/spa-0.2"

        #added to manage better display also with ge-custom for example
        if (self.winesoftrenderer == 1):
            self.env.update({
                "__GLX_VENDOR_LIBRARY_NAME": "mesa",
                "MESA_LOADER_DRIVER_OVERRIDE": "llvmpipe",
                "GALLIUM_DRIVER": "llvmpipe"
            })
            os.environ["__GLX_VENDOR_LIBRARY_NAME"] = "mesa"
            os.environ["MESA_LOADER_DRIVER_OVERRIDE"] = "llvmpipe"
            os.environ["GALLIUM_DRIVER"] = "llvmpipe"
        else:
            if "__GLX_VENDOR_LIBRARY_NAME" in self.env: self.env.pop("__GLX_VENDOR_LIBRARY_NAME")
            if "MESA_LOADER_DRIVER_OVERRIDE" in self.env: self.env.pop("MESA_LOADER_DRIVER_OVERRIDE")
            if "GALLIUM_DRIVER" in self.env: self.env.pop("GALLIUM_DRIVER")
        
        #"TODO": for wine32 & wine64 simultaneously and for multi-version of wine if possible
        ## Export Wine libs
        #PATH=$PATH:PATH=$PATH:${DIR}/${WINE_VERSION}/bin
        #export LD_LIBRARY_PATH="/lib32:${WINE_LIB32_DIR}/i386-unix:/lib:/usr/lib:${WINE_LIB64_DIR}/x86_64-unix"
        #export GST_PLUGIN_SYSTEM_PATH_1_0="/usr/lib/gstreamer-1.0:/lib32/gstreamer-1.0"
        #export GST_REGISTRY_1_0="/userdata/system/.cache/gstreamer-1.0/registry.x86_64.bin:/userdata/system/.cache/gstreamer-1.0/registry..bin"
        #export LIBGL_DRIVERS_PATH="/lib32/dri:/usr/lib/dri"
        #export WINEDLLPATH="${WINE_LIB32_DIR}/i386-windows:${WINE_LIB64_DIR}/x86_64-windows"
        # hum pw 0.2 and 0.3 are hardcoded, not nice
        #export SPA_PLUGIN_DIR="/usr/lib/spa-0.2:/lib32/spa-0.2"
        #export PIPEWIRE_MODULE_DIR="/usr/lib/pipewire-0.3:/lib32/pipewire-0.3"

        # in case of wine, we need temporary read/write access to overlay partition (to install and/or update registry)
        # come back in readonly now
        if not self.letFSUnlocked: os.system('mount -o remount,ro /')

    #to check and install wine if needed
    def InitializeWine(self):

        # in case of wine, we need temporary read/write access to overlay partition (to install and/or update registry)
        # pass in read/write now
        os.system('mount -o remount,rw /')

        #check first if wineprefix exists and if AppImage/Wine Engine changed or not
        wineengine = self.wineengine
        wineengine = wineengine.replace("/usr/wine/","")
        wineengine = wineengine.replace("/bin/wine","")
        if os.path.exists(self.wineprefix):
            if self.appimage != "":
                if not os.path.exists(self.wineprefix + "/" + self.appimage + ".used"):
                    #remove existing wineprefix to reinstall it
                    popup.Message("Clean previous wine installation found, please wait...", 3)
                    os.system('rm -r ' + self.wineprefix)
            else:
                if not os.path.exists(self.wineprefix + "/" + wineengine + ".used"):
                    #remove existing wineprefix to reinstall it
                    popup.Message("Clean previous wine installation found, please wait...", 3)
                    os.system('rm -r ' + self.wineprefix)
            
        #create bottle if not yet created or just removed before ;-)
        if not os.path.exists(self.wineprefix):
            popup.Message("First launched : wine installation requested, please wait...", 80)
            os.makedirs(self.wineprefix)
            #run wine fake function to create wine bottle ;-)
            if not os.path.exists(self.wineprefix + "/bottle.done"):
                #usefull command to know to identify windows/dialogbox
                # xdotool getactivewindow getwindowname
                # or
                # xdotool getactivewindow getwindowclassname
                
                #previous method
                # we try after 4 seconds and each seconds after to launch wine mono installation asap until 10 seconds
                #os.system('wine wineboot & sleep 4.0 ; xdotool key Return ; sleep 1.0 ; xdotool key Return; sleep 1.0 ; xdotool key Return; sleep 1.0 ; xdotool key Return; sleep 1.0 ; xdotool key Return; sleep 1.0 ; xdotool key Return; xdotool key Return; sleep 1.0 ; xdotool key Return')
                
                #new method
                #now we search the window, take the focus, and only after we could send the key return
                #since 24/06/2024: wine is forced to work in english only to avoid issue with naming of each windows
                xdotoolCommandArray = []
                if not "mscoree=d" in self.winedlloverrides:
                    xdotoolCommandArray.append('(xdotool windowfocus $(xdotool search --sync --onlyvisible --name "Wine Mono Installer" ); sleep 0.2 ; xdotool key Return)&')
                if not "mshtml=d" in self.winedlloverrides:
                    xdotoolCommandArray.append('(xdotool windowfocus $(xdotool search --sync --onlyvisible --name "Wine Gecko Installer" ); sleep 0.2 ; xdotool key Return)&')
                if xdotoolCommandArray != []:
                    # launch xdotool commands in background in this case
                    Log("xdotool commands: " + " ".join(xdotoolCommandArray))
                    os.system(" ".join(xdotoolCommandArray))
                # launch wine init command in foreground
                cmd = ["wine","wineboot","--init"]
                Log("Wine initialization command: " + " ".join(cmd))
                os.system(" ".join(cmd))
                with open(self.wineprefix + "/bottle.done", "w") as f:
                    f.write("done")
                if self.appimage != "":
                    with open(self.wineprefix + "/" + self.appimage + ".used", "w") as f:
                        f.write("used")
                else:
                    with open(self.wineprefix + "/" + wineengine + ".used", "w") as f:
                        f.write("used")
                #kill potential existing xdotool process (if missing control boxes for example in some wine versions)
                os.system("pkill xdotool")

        # Set Windows version to avoid some issues if needed ;-)
        if self.winver != "" :
            if not os.path.exists(self.wineprefix + "/" + self.winver + ".done"):
                popup.Message("Set Windows version, please wait...", 10)
                if self.winetricks != "internal" : 
                    cmd = [self.winetricksPath, "-q", self.winver]
                else:
                    cmd = [self.winePath, "winetricks", "-q", self.winver]
                Log(f"command: {str(cmd)}")
                proc = subprocess.Popen(cmd, env=self.env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = proc.communicate()
                exitcode = proc.returncode
                Log(out.decode())
                Log(err.decode())
                with open(self.wineprefix + "/" + self.winver + ".done", "w") as f:
                    f.write("done")

        # Set on alsa or pulse ;-)
        if not os.path.exists(self.wineprefix + "/" + self.wineaudiodriver + ".done"):
            popup.Message("Alsa driver settings, please wait...", 5)
            self.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\Drivers", "Audio", self.wineaudiodriver, self.env, self.userRegistryFile)
            if self.winetricks != "internal" : 
                cmd = [self.winetricksPath, "-q", "sound=" + self.wineaudiodriver]
            else:
                cmd = [self.winePath, "winetricks", "-q", "sound=" + self.wineaudiodriver]
            Log(f"command: {str(cmd)}")
            proc = subprocess.Popen(cmd, env=self.env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = proc.communicate()
            exitcode = proc.returncode
            Log(out.decode())
            Log(err.decode())
            with open(self.wineprefix + "/" + self.wineaudiodriver + ".done", "w") as f:
                f.write("done")

        #only Open GL supported for the moment for this emulator
        videoDriver = "gl"
        
        if not os.path.exists(self.wineprefix + "/" + videoDriver + ".done"):
            popup.Message(videoDriver + " driver settings, please wait...", 5)
            #remove all renderer drivers file before to process update of drivers
            os.system("rm -f '" + self.wineprefix + "/vulkan.done'")
            os.system("rm -f '" + self.wineprefix + "/gl.done'")
            if self.winetricks != "internal" : 
                cmd = [self.winetricksPath, "-q", "renderer=" + videoDriver]
            else:
                cmd = [self.winePath, "winetricks", "-q", "renderer=" + videoDriver]
            Log(f"command: {str(cmd)}")
            proc = subprocess.Popen(cmd, env=self.env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = proc.communicate()
            exitcode = proc.returncode
            Log(out.decode())
            Log(err.decode())
            with open(self.wineprefix + "/" + videoDriver + ".done", "w") as f:
                f.write("done")

        # We do it at the end now to be sure that audio is free
        # from audio.device parameter to find device in registry to set it as default for wine used
        alsaRef = self.recalboxOptions.getString("audio.device", "")

        #split alsa Reference
        alsaCardRef = alsaRef.split(':')[0]
        alsaDeviceRef = alsaRef.split(':')[1]
        Log("alsaCardRef : " + alsaCardRef)
        Log("alsaDeviceRef : " + alsaDeviceRef)
        
        #search alsa card name (from pulse audio controls)
        Log('command: pactl list | grep -A19 -e "Name: {}" | grep -m1 -e "alsa.card_name"'.format(alsaCardRef))
        p = subprocess.Popen('pactl list | grep -A19 -e "Name: {}" | grep -m1 -e "alsa.card_name"'.format(alsaCardRef), shell=True,
                     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output1 = p.stdout
        result = output1.readline().rstrip().split('"')
        if len(result) > 1: 
            alsaCardName = result[1]
        else:
            alsaCardName = ""
        Log("alsaCardName : " + alsaCardName)
        
        #search alsa Device name (from pulse audio controls)
        Log('command: pactl list | grep -B49 -e "Active Port: {}" | grep -m1 -e "alsa.name"'.format(alsaDeviceRef))
        p = subprocess.Popen('pactl list | grep -B49 -e "Active Port: {}" | grep -m1 -e "alsa.name"'.format(alsaDeviceRef), shell=True,
                     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output2 = p.stdout
        result = output2.readline().rstrip().split('"')
        if len(result) > 1: 
            alsaDeviceName = result[1]
        else:
            alsaDeviceName = ""
        Log("alsaDeviceName : " + alsaDeviceName)
        
        #since wine 8.0.1 : search Device product name (from pulse audio controls) as name of screen usually or detailed info about  chipset
        Log('command: pactl list | grep -A4 -e "{}: " | grep -m1 -e "device.product.name"'.format(alsaDeviceRef))
        p = subprocess.Popen('pactl list | grep -A4 -e "{}: " | grep -m1 -e "device.product.name"'.format(alsaDeviceRef), shell=True,
                     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output2bis = p.stdout
        result = output2bis.readline().rstrip().split('"')
        
        if len(result) > 1: 
            alsaDeviceProductName = result[1]
        else:
            alsaDeviceProductName = ""
        Log("alsaDeviceProductName : " + alsaDeviceProductName)
        
        #since buildroot using kernel 6.X : search alsa Device id (from pulse audio controls)
        Log('command: pactl list | grep -B49 -e "Active Port: {}" | grep -m1 -e "alsa.id"'.format(alsaDeviceRef))
        p = subprocess.Popen('pactl list | grep -B49 -e "Active Port: {}" | grep -m1 -e "alsa.id"'.format(alsaDeviceRef), shell=True,
                     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output2ter = p.stdout
        result = output2ter.readline().rstrip().split('"')
        if len(result) > 1: 
            alsaDeviceId = result[1]
        else:
            alsaDeviceId = ""
        Log("alsaDeviceId : " + alsaDeviceId)
        
        #search class ID of output properties (by alsaDeviceId) - done first for new buildroot (kernel 6.X)
        Log('command: grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceId,self.wineprefix))
        p = subprocess.Popen('grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceId,self.wineprefix), shell=True,
                     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output3 = ''
        output3 = p.stdout
        valueFound = ''
        valueFound = output3.readline().rstrip()
        Log("first try (by alsaDeviceId)  : '" + valueFound + "'")
        #if nothing found in the existing system.reg file (could be for first boot or if audio conf/devices changed)
        if(valueFound == ''):
            #search class ID of output properties (by alsaDeviceName)
            Log('command: grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceName,self.wineprefix))
            p = subprocess.Popen('grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceName,self.wineprefix), shell=True,
                         stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output3 = ''
            output3 = p.stdout
            valueFound = ''
            valueFound = output3.readline().rstrip()
            Log("first try (by alsaDeviceName) : '" + valueFound + "'")
            #if nothing found in the existing system.reg file (could be for first boot or if audio conf/devices changed)
            if(valueFound == ''):
                #search class ID of output properties (by alsaDeviceProductName)
                Log('command: grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceProductName,self.wineprefix))
                p = subprocess.Popen('grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceProductName,self.wineprefix), shell=True,
                             stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
                p.wait()
                output3 = ''
                output3 = p.stdout
                valueFound = ''
                valueFound = output3.readline().rstrip()
                Log("first try (by alsaDeviceProductName)  : '" + valueFound + "'")
        
        #if nothing found in the existing system.reg file (could be for first boot or if audio conf/devices changed)
        if(valueFound == ''):
            popup.Message("Enumarate Audio Outputs, please wait...", 6)
            #kill all other instance of wine if present in memory before to install "huge" framework
            os.system('sleep 1.0')
            os.system('wineserver -k')
            os.system('sleep 1.0')
            # Launch winecfg audio to re/generate .reg file with enumaration of audio devices (need time to wait writing and kill if needed)
            os.system('wine winecfg & sleep 4.0 ; xdotool mousemove 150 15 click 1 ; sleep 1.0 ; xdotool key Return; sleep 2.0; wineserver -k')
            #search class ID of output properties (by alsaDeviceId)
            Log('command: grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceId,self.wineprefix))
            p = subprocess.Popen('grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceId,self.wineprefix), shell=True,
                         stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output3 = ''
            output3 = p.stdout
            valueFound = ''
            valueFound = output3.readline().rstrip()
            Log("second try (by alsaDeviceId) : '" + valueFound + "'")
            if(valueFound == ''):
                #search class ID of output properties (by alsaDeviceName)
                Log('command: grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceName,self.wineprefix))
                p = subprocess.Popen('grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceName,self.wineprefix), shell=True,
                             stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
                p.wait()
                output3 = ''
                output3 = p.stdout
                valueFound = ''
                valueFound = output3.readline().rstrip()
                Log("third try (by alsaDeviceName) : '" + valueFound + "'")
                if(valueFound == ''):
                    #search class ID of output properties (by alsaDeviceProductName)
                    Log('command: grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceProductName,self.wineprefix))
                    p = subprocess.Popen('grep -m1 "Out: {} - {}" "{}/system.reg" -B4 | grep "Properties"'.format(alsaCardName,alsaDeviceProductName,self.wineprefix), shell=True,
                                 stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
                    p.wait()
                    output3 = ''
                    output3 = p.stdout
                    valueFound = ''
                    valueFound = output3.readline().rstrip()
                    Log("fourth and last try (by alsaDeviceProductName)  : '" + valueFound + "'")
        
        #if any found / we can update the default ones now
        if(valueFound != ''):
            classID = valueFound.split('{')[1].split('}')[0]
            Log("classID : " + classID)
            #select default output using the class ID
            popup.Message("Audio Default Output to set, please wait...", 5)
            self.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\Drivers\\winealsa.drv", "DefaultOutput", "{0.0.0.00000000}.{" + classID + "}", self.env, self.userRegistryFile)
            popup.Message("Audio Default Voice Output to set, please wait...", 5)
            self.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\Drivers\\winealsa.drv", "DefaultVoiceOutput", "{0.0.0.00000000}.{" + classID + "}", self.env, self.userRegistryFile)

        #kill all other instance of wine if present in memory
        os.system('sleep 1.0')
        os.system('wineserver -k')
        
        # in case of wine, we need temporary read/write access to overlay partition (to install and/or update registry)
        # come back in readonly now
        if not self.letFSUnlocked: os.system('mount -o remount,ro /')

    #to install missing library
    def InstallDlls(self, Dllname: str, timeout: float, forceExit: bool):
        #kill all other instance of wine if present in memory before to install "huge" framework
        os.system('wineserver -k')
        os.system('sleep 1.0')

        #in case of wine, we need temporary read/write access to overlay partition (to install and/or update registry)
        #pass in read/write now
        os.system('mount -o remount,rw /')
        
        if not os.path.exists(self.wineprefix + "/" + Dllname + ".done"):
            #to do: add grp to get full name of library from "winetricks dlls list" command
            popup.Message(Dllname + " installation, please wait...", timeout)
            cmd = []
            if forceExit == True:
                cmd.append("timeout")
                cmd.append(str(int(timeout)))
            if self.winetricks != "internal" : 
                cmd.append(self.winetricksPath)
            else:
                cmd.append(self.winePath)
                cmd.append("winetricks")
            cmd.append("-q")
            cmd.append(Dllname)
            Log(f"command: {str(cmd)}")
            command = Command(videomode=self.system.VideoMode, array=cmd, env=self.env, delay=0.5, cwdPath="/usr/wine", postExec=None)
            proc = subprocess.Popen(command.array, env=command.env, cwd=command.cwdPath,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            out, err = proc.communicate()
            exitcode = proc.returncode
            Log(out.decode())
            Log(err.decode())
            with open(self.wineprefix + "/" + Dllname + ".done", "w") as f:
                f.write("done")
        
        #kill all other instance of wine to be sure to clear memory
        os.system('wineserver -k')
        os.system('sleep 1.0')
        
        # in case of wine, we need temporary read/write access to overlay partition (to install and/or update registry)
        # come back in readonly now
        if not self.letFSUnlocked: os.system('mount -o remount,ro /')
        
    #to return version of wine
    def version(self):
        p = subprocess.Popen(self.wineDirPath + "/" + "wine --version 2> /dev/null | grep -i 'wine-' | tr -d '\\n' | tr -d '\\r'",
                     shell=True, stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output1 = p.stdout
        valueFound = output1.readline().rstrip()
        Log("valueFound : " + valueFound)
        #keep full name version for the moment
        Log("The version of Wine is : " + valueFound)
        return valueFound

    #to remve any value from registry 
    def removeValueFromRegistry(self, path: str, value: str, env: dict = None, regfile: str = None):
        if env == None: env = self.env
        if regfile == None: regfile = self.userRegistryFile
        #need to check in user.reg or system.reg usually in this case
        pathtofind=path.replace("HKEY_CURRENT_USER\\","")
        pathtofind=pathtofind.replace("HKEY_LOCAL_MACHINE\\","")
        pathtofind=pathtofind.replace('\\','\\\\')
        Log("command : " + "grep -F '[" + pathtofind + "]' " + "'" + regfile + "' -A100 | grep -F '" + '"' + value + '"=' + "'")
        p = subprocess.Popen("grep -F '[" + pathtofind + "]' " + "'" + regfile + "' -A100 | grep -F '" + '"' + value + '"=' + "'",
                     shell=True,
                     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output1 = p.stdout
        valueFound = output1.readline().rstrip()
        Log('Registry - String found : ' + valueFound)
        if ('"' + value + '"=') in valueFound:
            cmd = ["wine", "REG", "DELETE", path,
                    "/v", value,"/f"]
            Log(f"command: {str(cmd)}")
            proc = subprocess.Popen(cmd, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            proc.wait()
            out, err = proc.communicate()
            exitcode = proc.returncode
            Log(out.decode())
            Log(err.decode())
        else:
            Log('No update of registry needed - value do not exist for : [' + path + '] "' + value + '"=')

    #to add Dword value in registry 
    def addDwordValueInRegistry(self, path: str, value: str, data: str, env: dict = None, regfile: str = None, forced: bool = False):
        if env == None: env = self.env
        if regfile == None: regfile = self.userRegistryFile
        #need to check in user.reg or system.reg usually in this case
        pathtofind=path.replace("HKEY_CURRENT_USER\\","")
        pathtofind=pathtofind.replace("HKEY_LOCAL_MACHINE\\","")
        pathtofind=pathtofind.replace('\\','\\\\')
        Log("command : " + "grep -F '[" + pathtofind + "]' '" + regfile + "' -A100 | grep -F '" + '"' + value + '"="' + data + '"' + "'")
        p = subprocess.Popen("grep -F '[" + pathtofind + "]' '" + regfile + "' -A100 | grep -F '" + '"' + value + '"="' + data + '"' + "'",
                     shell=True,
                     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output1 = p.stdout
        valueFound = output1.readline().rstrip()
        Log('Registry - String found : ' + valueFound)
        if (valueFound == '"' + value + '"="' + data + '"') and (not forced):
            Log('No update of registry - String already exists for : [' + path + '] "' + value + '"="' + data + '"')
            return 0
        cmd = ["wine", "REG", "ADD", path,
                "/v", value, "/t", "REG_DWORD" , "/d", data,"/f"]
        Log(f"command: {str(cmd)}")
        proc = subprocess.Popen(cmd, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.wait()
        out, err = proc.communicate()
        exitcode = proc.returncode
        Log(out.decode())
        Log(err.decode())

    #to add String value in registry 
    def addStringValueInRegistry(self, path: str, value: str, data: str, env: dict = None, regfile: str = None, forced: bool = False):
        if env == None: env = self.env
        if regfile == None: regfile = self.userRegistryFile
        #need to check in user.reg or system.reg usually in this case
        pathtofind=path.replace("HKEY_CURRENT_USER\\","")
        pathtofind=pathtofind.replace("HKEY_LOCAL_MACHINE\\","")
        pathtofind=pathtofind.replace('\\','\\\\')
        Log("command : " + "grep -F '[" + pathtofind + "]' '" + regfile + "' -A100 | grep -F '" + '"' + value + '"="' + data + '"' + "'")
        p = subprocess.Popen("grep -F '[" + pathtofind + "]' '" + regfile + "' -A100 | grep -F '" + '"' + value + '"="' + data + '"' + "'",
                     shell=True,
                     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output1 = p.stdout
        valueFound = output1.readline().rstrip()
        Log('Registry - String found : ' + valueFound)
        if (valueFound == '"' + value + '"="' + data + '"') and (not forced):
            Log('No update of registry - String already exists for : [' + path + '] "' + value + '"="' + data + '"')
            return 0
        cmd = ["wine", "REG", "ADD", path,
                "/v", value, "/d", data,"/f"]
        Log(f"command: {str(cmd)}")
        proc = subprocess.Popen(cmd, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.wait()
        out, err = proc.communicate()
        exitcode = proc.returncode
        Log(out.decode())
        Log(err.decode())

    #to add MultiValue String in registry 
    def addMultiStringValueInRegistry(self, path: str, value: str, data: str, env: dict = None):
        if env == None: env = self.env
        cmd = ["wine", "REG", "ADD", path,
                "/v", value, "/t", "REG_MULTI_SZ" , "/d", data,"/f"]
        Log(f"command: {str(cmd)}")
        proc = subprocess.Popen(cmd, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.wait()
        out, err = proc.communicate()
        exitcode = proc.returncode
        Log(out.decode())
        Log(err.decode())

