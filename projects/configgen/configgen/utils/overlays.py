#!/usr/bin/env python3
from typing import List

import os
import subprocess
import sys

import configgen.recalboxFiles as recalboxFiles
from configgen.Emulator import Emulator
from configgen.settings.keyValueSettings import keyValueSettings
import struct

from PIL import Image
from PIL import ImageDraw
from PIL import ImageOps

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

# Much faster than PIL Image.size
def fast_image_size(image_file):
    if not os.path.exists(image_file):
        return -1, -1
    with open(image_file, 'rb') as fhandle:
        head = fhandle.read(32)
        if len(head) != 32:
           # corrupted header, or not a PNG
           return -1, -1
        check = struct.unpack('>i', head[4:8])[0]
        if check != 0x0d0a1a0a:
           # Not a PNG
           return -1, -1
        return struct.unpack('>ii', head[16:24]) #image width, height

def resizeImage(input_png, output_png, screen_width, screen_height, bezel_stretch=False):
    imgin = Image.open(input_png)
    fillcolor = 'black'
    Log(f"Resizing bezel: image mode {imgin.mode}")
    if imgin.mode != "RGBA":
        alphaPaste(input_png, output_png, fillcolor, (screen_width, screen_height), bezel_stretch)
    else:
        if(bezel_stretch):
            imgout = imgin.resize((screen_width, screen_height), Image.BICUBIC)
            imgout.save(output_png, mode="RGBA", format="PNG")
        else:
            imgin.convert("RGBA").quantize(method=2).save(output_png)
            alphaPaste(output_png, output_png, fillcolor, (screen_width, screen_height), bezel_stretch)

def getTargetedResolution(videoMode) -> (str, str):
    # Screen resolution
    from configgen.utils.resolutions import ResolutionParser
    resolution = ResolutionParser(videoMode)
    if resolution.isSet and resolution.selfProcess:
        width = str(resolution.width)
        height = str(resolution.height)
    else: #to propose default value
        width = "1920"
        height = "1080"
    resolution = (width, height)
    Log("The Targeted resolution is : "+ width + ":" + height)
    return resolution
    
def getPictureResolution(picturePath):
    ##get picture resolution
    Log("The picture path is :  " + picturePath)
    im = Image.open(picturePath)
    if os.path.isfile(picturePath):
        resolution = im.size
        Log("The picture resolution is :  " + str(resolution[0]) + ":" + str(resolution[1]))
        return resolution
    else:
        return 0,0

# function to improve way to manage overlay combinaisons
def overlayIsActivated(recalboxOptions: keyValueSettings, systemName: str) -> bool:
        #Principles: 
        # true -> activated
        # false or missing -> not activated
        # global parameter is in priority if system parameter is not yet set from user.
        # In Pegasus-frontend, system option is as global one if never set from menu by user.
        # but if user sets from system options menu himself or from recalbox.conf, the priority become the system one.
        if recalboxOptions.getBool(systemName + ".recalboxoverlays",recalboxOptions.getBool("global.recalboxoverlays",True)):
            Log("Overlay is activated !")
            return True
        else:
            Log("Overlay is not activated !")
            return False

def matchOverlaySizeToScreen(userOverlayApplied: str) -> str:
    #if overlay identified
    if userOverlayApplied != "":
        #get screen resolution to see if overlay if matching with it
        from configgen.utils.videoMode import getCurrentResolution
        screenWidth, screenHeight = getCurrentResolution()
        Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
        Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
    
        #read and copy existing overlay in tmp
        root_overlay_path = userOverlayApplied
        
        ##loading root overlay (usually with "system" or "rom" .cfg as file name)...
        Log("root_overlay_path : " + root_overlay_path)
        root_overlay_file = keyValueSettings(root_overlay_path, True)
        root_overlay_file.loadFile(True)

        ##load overlay existing parameter or take default values for later...
        custom_viewport_height = int(root_overlay_file.getString("custom_viewport_height", "0").replace('"',''))
        custom_viewport_width = int(root_overlay_file.getString("custom_viewport_width", "0").replace('"',''))
        custom_viewport_x = int(root_overlay_file.getString("custom_viewport_x", "0").replace('"',''))
        custom_viewport_y = int(root_overlay_file.getString("custom_viewport_y", "0").replace('"',''))

        
        ##loading input overlay (usually with "system" or "rom" _overlay.cfg as file name)...
        input_overlay_path = root_overlay_file.getString("input_overlay","").replace('//','/').replace('"','')
        Log("input_overlay_path : " + input_overlay_path)
        if input_overlay_path != "":
            input_overlay_file = keyValueSettings(input_overlay_path, True)
            input_overlay_file.loadFile(True)
            
            ##load input overlay existing parameter or take default values for later...
            nb_overlays = input_overlay_file.getString("overlays", "0")
            Log("...nb overlay : " + nb_overlays)
            if(nb_overlays == "0"):
                #issue detected missing overlay configuration / need to return empty one to avoid crash
                userOverlayApplied = ""
                return userOverlayApplied
            Log("...overlay0_overlay : " + input_overlay_file.getString("overlay0_overlay", ""))
            overlay_picture_file = os.path.dirname(input_overlay_path.replace('//','/').replace('"','')) + "/" + input_overlay_file.getString("overlay0_overlay", "").replace('"','')
            Log("overlay_picture_file : " + str(overlay_picture_file))
            #get size of overlay picture
            pictureWidth, pictureHeight = getPictureResolution(overlay_picture_file)
            #check if screen resolution is different of picture resolution
            if((pictureWidth != screenWidth) or (pictureHeight != screenHeight)):
                #Save overlay files (2 .cfg + .png) in tmp before updates
                root_resized_overlay_path = "/tmp/resized_overlay.cfg"
                Log("new tmp root_resized_overlay_path : " + root_resized_overlay_path)
                input_resized_overlay_path = "/tmp/input_resized_overlay.cfg"
                Log("new tmp input_resized_overlay_path : " + input_resized_overlay_path)
                output_resized_png_file = "/tmp/resized_overlay.png"
                Log("new tmp output_resized_png_file : " + output_resized_png_file)
                #remove previous ones to be sure about update ;-)
                os.system("rm " + root_resized_overlay_path)
                os.system("rm " + input_resized_overlay_path)
                os.system("rm " + output_resized_png_file)
                #save in new .cfg files
                input_overlay_file.changeSettingsFile(input_resized_overlay_path)
                #set overlay 0 overlay
                input_overlay_file.setString("overlay0_overlay", "resized_overlay.png")
                input_overlay_file.saveFile()
                #set also the input overlay path with new value
                root_overlay_file.setString("input_overlay", '"' + input_resized_overlay_path + '"')
                root_overlay_file.changeSettingsFile(root_resized_overlay_path)
                #update custom_viewport if needed
                if(custom_viewport_height != "0") or (custom_viewport_width != "0"):
                    #calculate vertical ratio between existing overlay and targeted screen 
                    # because resize of overlay is always done to keep the maximum of vertical display on 16/9 - 16/10 screen
                    heightRatio = pictureHeight/screenHeight # this ratio is to recalculate the size of viewport and y
                    xDelta = int(((pictureWidth/heightRatio) - screenWidth)/2)
                    # this delta is used if we from 16/9 to 16/10
                    viewport_width = int(int(custom_viewport_width)/heightRatio)
                    if( viewport_width > screenWidth ) : 
                        viewport_width = screenWidth
                    root_overlay_file.setString("custom_viewport_width", '"' + str(viewport_width) + '"')
                    viewport_height = int(int(custom_viewport_height)/heightRatio)
                    if( viewport_height > screenHeight ) : 
                        viewport_height = screenHeight
                    root_overlay_file.setString("custom_viewport_height", '"' + str(viewport_height) + '"')
                    viewport_x = int(int(custom_viewport_x)/heightRatio) - xDelta
                    if( viewport_x < 0 ) : 
                        viewport_x = 0
                    root_overlay_file.setString("custom_viewport_x", '"' + str(viewport_x) + '"')
                    viewport_y = int(int(custom_viewport_y)/heightRatio)
                    if( viewport_y < 0 ) : 
                        viewport_y = 0
                    root_overlay_file.setString("custom_viewport_y", '"' + str(viewport_y) + '"')
                root_overlay_file.setString("video_fullscreen_x", '"' + str(screenWidth) + '"')
                root_overlay_file.setString("video_fullscreen_y", '"' + str(screenHeight) + '"')
                #save changes
                root_overlay_file.saveFile()
                #resize overlay png file first
                resizeImage(overlay_picture_file, output_resized_png_file, screenWidth, screenHeight)
                #select overlay from tmp
                userOverlayApplied = root_resized_overlay_path

        else:
            Log("Warning: No input overlay !")
    #return overlay udpated or not
    return userOverlayApplied
    
# Retroarch Overlay management
def processRetroarchOverlays(system: Emulator, romName: str, configs: List[str], recalboxOptions: keyValueSettings, needSindenBorder: bool):
    # User overlays
    userOverlayApplied = ""

    # If we are in crt mode, we only allow recalbox default 240p overlays
    if recalboxOptions.hasOption("system.crt"):
        if system.RecalboxOverlays:
            crtOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_240P_OVERLAYS, system.Name,
                                                       system.Name)
            if os.path.isfile(crtOverlayFile):
                configs.append(crtOverlayFile)
    # Overlays are applied only when we are not in wide core
    else:
        if system.Core not in ["genesisplusgxwide"] and overlayIsActivated(recalboxOptions, system.Name):
            overlayFile = "{}/{}/.overlay.cfg".format(recalboxFiles.OVERLAYS, system.Name)
            if os.path.isfile(overlayFile):
                # System global configuration
                userOverlayApplied = overlayFile
            else:
                overlayFile = "{}/.overlay.cfg".format(recalboxFiles.OVERLAYS)
                if os.path.isfile(overlayFile):
                    # All system global configuration
                    userOverlayApplied = overlayFile
            overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romName)
            if os.path.isfile(overlayFile):
                # Rom file overlay
                userOverlayApplied = overlayFile
            else:
                overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, system.Name)
                if os.path.isfile(overlayFile):
                    # System overlay
                    userOverlayApplied = overlayFile
            if userOverlayApplied == "":
                # The recalbox overlays should be added only if
                # global.recalboxoverlays=1 or system.recalboxoverlays activated 
                # and no user overlays available from /recalbox/share/overlays
                # ratio = we can activate when ratio is not 16/9 and 16/10
                if system.Ratio not in ["16/9", "16/10"]:
                    # screen resolution that can support overlays are over 1.5 ratio (as it is float > 1.51)
                    from configgen.utils.videoMode import getCurrentResolution
                    screenWidth, screenHeight = getCurrentResolution()
                    Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
                    Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
                    if ( screenWidth / screenHeight) > 1.51:
                        defaultOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_OVERLAYS, system.Name,
                                                                   system.Name)
                        if os.path.isfile(defaultOverlayFile):
                            userOverlayApplied = defaultOverlayFile

        #to manage resize of overlay if necessary due to different size of screen
        if userOverlayApplied != "":
            userOverlayApplied = matchOverlaySizeToScreen(userOverlayApplied)
        if needSindenBorder == False :
            #if no sinden border necessary we could configure the overlay now in config
            configs.append(userOverlayApplied)
        else: #to manage sinden lightgun border
            #for the moment we use the system/rom name in /tmp directory to test it
            output_png_file = "" #init this file name
            if userOverlayApplied != "":
                #need to reuse overlay to add border
                #read and copy existing overlay in tmp
                root_overlay_path = userOverlayApplied
                
                ##loading root overlay (usually with "system" or "rom" .cfg as file name)...
                Log("root_overlay_path : " + root_overlay_path)
                root_overlay_file = keyValueSettings(root_overlay_path, True)
                root_overlay_file.loadFile(True)

                ##load overlay existing parameter or take default values for later...
                custom_viewport_height = int(root_overlay_file.getString("custom_viewport_height", "0").replace('"',''))
                custom_viewport_width = int(root_overlay_file.getString("custom_viewport_width", "0").replace('"',''))
                custom_viewport_x = int(root_overlay_file.getString("custom_viewport_x", "0").replace('"',''))
                custom_viewport_y = int(root_overlay_file.getString("custom_viewport_y", "0").replace('"',''))

                
                ##loading input overlay (usually with "system" or "rom" _overlay.cfg as file name)...
                input_overlay_path = root_overlay_file.getString("input_overlay","").replace('//','/').replace('"','')
                Log("input_overlay_path : " + input_overlay_path)
                if input_overlay_path != "":
                    input_overlay_file = keyValueSettings(input_overlay_path, True)
                    input_overlay_file.loadFile(True)
                    
                    ##load input overlay existing parameter or take default values for later...
                    Log("...nb overlay : " + input_overlay_file.getString("overlays", "0"))
                    nb_overlays = input_overlay_file.getString("overlays", "0")
                    Log("...overlay0_overlay : " + input_overlay_file.getString("overlay0_overlay", ""))
                    overlay_picture_file = os.path.dirname(input_overlay_path.replace('//','/').replace('"','')) + "/" + input_overlay_file.getString("overlay0_overlay", "").replace('"','')
                    Log("overlay_picture_file : " + str(overlay_picture_file))
                    
                    #Save overlay files (2 .cfg + .png) in tmp before updates
                    root_overlay_path = "/tmp/sinden.cfg"
                    Log("new tmp root_overlay_path : " + root_overlay_path)
                    input_overlay_path = "/tmp/sinden_overlay.cfg"
                    Log("new tmp input_overlay_path : " + input_overlay_path)
                    output_png_file = "/tmp/sinden.png"
                    Log("new tmp output_png_file : " + output_png_file)
                    #remove previous ones to be sure about update ;-)
                    os.system("rm " + root_overlay_path)
                    os.system("rm " + input_overlay_path)
                    os.system("rm " + output_png_file)
                    #save in new .cfg files
                    input_overlay_file.changeSettingsFile(input_overlay_path)
                    #set overlay 0 overlay
                    input_overlay_file.setString("overlay0_overlay", "sinden.png")
                    input_overlay_file.saveFile()
                    #set also the input overlay path with new value
                    root_overlay_file.setString("input_overlay", '"' + input_overlay_path + '"')
                    root_overlay_file.changeSettingsFile(root_overlay_path)
                    root_overlay_file.saveFile()
                else:
                    Log("Warning: No input overlay !")
            else:
                Log("Info: No overlay available.")
                #create overlay from scratch
                root_overlay_path = "/tmp/sinden_border.cfg"
                Log("empty root_overlay_path : " + root_overlay_path)
                input_overlay_path = "/tmp/sinden_border_overlay.cfg"
                Log("empty input_overlay_path : " + input_overlay_path)
                output_png_file = "/tmp/sinden_border.png"
                Log("empty output_png_file : " + output_png_file)
                #remove previous ones to be sure about update ;-)
                os.system("rm " + root_overlay_path)
                os.system("rm " + input_overlay_path)
                os.system("rm " + output_png_file)
               
                #loading empty root overlay
                root_overlay_file = keyValueSettings(root_overlay_path, True)
                #set input overlay
                root_overlay_file.setString("input_overlay", '"' + input_overlay_path + '"')
                #set input overlay opacity
                root_overlay_file.setString("input_overlay_opacity", '"' + "1.0" + '"')
                #set video message position
                root_overlay_file.setString("video_message_pos_x", '"' + "0.133333333333333" + '"')
                root_overlay_file.setString("video_message_pos_y", '"' + "0.0638888888888889" + '"')
                root_overlay_file.saveFile()
                
                #loading empty root overlay
                input_overlay_file = keyValueSettings(input_overlay_path, True)
                #set overlay 0 descs
                input_overlay_file.setString("overlay0_descs", '0')
                #set overlay 0 full screen
                input_overlay_file.setString("overlay0_full_screen", 'true')
                #set overlay 0 overlay
                input_overlay_file.setString("overlay0_overlay", "sinden_border.png")
                #set overlays
                input_overlay_file.setString("overlays", '1')
                input_overlay_file.saveFile()
                #get targeted resolution
                targetedWidth, targetedHeight = getTargetedResolution(system.VideoMode)
                #create border only
                createTransparentOverlay(output_png_file, int(targetedWidth), int(targetedHeight))
                #in this case we will reuse the same file for input and output
                overlay_picture_file = output_png_file
            Log("output_png_file : " + output_png_file)
            if(output_png_file != ""):
                #generate border from overlay
                innerSize, outerSize = sindenBordersSize(recalboxOptions)
                borderSize = sindenBorderImage(overlay_picture_file, output_png_file, innerSize, outerSize, sindenBordersColor(recalboxOptions))
                # Border "tmp" file overlay
                configs.append(root_overlay_path)
                userOverlayApplied = root_overlay_path

# HUD Overlay management
def processOverlays(system: Emulator, rom: str, recalboxOptions: keyValueSettings, env: dict, commandArray: List[str], needSindenBorder: bool = False):
    overlayFile = ""
    # User overlays
    userOverlayApplied = ""
    Log("processOverlays - rom : " + rom)
    romName = os.path.basename(rom)
    Log("processOverlays - romName : " + romName)
    root_ext = os.path.splitext(rom)
    romExtension = root_ext[1]
    Log("processOverlays - romExtension : " + romExtension)
    
    # If we are in crt mode, we only allow recalbox default 240p overlays
    if recalboxOptions.hasOption("system.crt"):
        if system.RecalboxOverlays:
            crtOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_240P_OVERLAYS, system.Name,
                                                       system.Name)
            if os.path.isfile(crtOverlayFile):
                overlayFile = crtOverlayFile
    # Overlays are applied only when we are not in wide core
    else:
        if system.Core not in ["genesisplusgxwide"] and overlayIsActivated(recalboxOptions, system.Name):
            # User overlays
            overlayFile = "{}/{}/.overlay.cfg".format(recalboxFiles.OVERLAYS, system.Name)
            if os.path.isfile(overlayFile):
                # System global configuration
                userOverlayApplied = overlayFile
            else:
                overlayFile = "{}/.overlay.cfg".format(recalboxFiles.OVERLAYS)
                if os.path.isfile(overlayFile):
                    # All system global configuration
                    userOverlayApplied = overlayFile
                else:
                    overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romName)
                    Log("processOverlays - overlayFile (from rom name) : " + overlayFile)
                    if os.path.isfile(overlayFile):
                        # Rom file overlay
                        userOverlayApplied = overlayFile
                    else:
                        #if not found, we retry if exist any without decoration using () or []
                        result = romName.split("(") # we search first ( to remove decoration using it   
                        result2 = result[0].split("[") # we search first [ to remove decoration using it
                        romNameWithoutDeco = result2[0].strip() # we use strip() to remove space at the begin and end of the string
                        romNameWithoutDeco = romNameWithoutDeco + romExtension # we add extension in this case also as done usually
                        overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romNameWithoutDeco)
                        Log("processOverlays - overlayFile (from rom name without deco) : " + overlayFile)
                        if os.path.isfile(overlayFile):
                            # System overlay
                            userOverlayApplied = overlayFile
                        else:
                            overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, system.Name)
                            if os.path.isfile(overlayFile):
                                # System overlay
                                userOverlayApplied = overlayFile
            if userOverlayApplied == "":
                # The recalbox overlays should be added only if
                # global.recalboxoverlays=1 or system.recalboxoverlays activated 
                # and no user overlays available from /recalbox/share/overlays
                # ratio = we can activate when ratio is not 16/9 and 16/10
                if system.Ratio not in ["16/9", "16/10"]:
                    # screen resolution that can support overlays are over 1.5 ratio (as it is float > 1.51)
                    
                    from configgen.utils.videoMode import getCurrentResolution
                    screenWidth, screenHeight = getCurrentResolution()
                    Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
                    Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
                    if ( screenWidth / screenHeight) > 1.51:
                        defaultOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_OVERLAYS, system.Name,
                                                                   system.Name)
                        if os.path.isfile(defaultOverlayFile):
                            userOverlayApplied = defaultOverlayFile
    Log("processOverlays - userOverlayApplied : " + userOverlayApplied)
    #to manage resize of overlay if necessary due to different size of screen
    if userOverlayApplied != "":
        userOverlayApplied = matchOverlaySizeToScreen(userOverlayApplied)
    #init overlay Picture File string
    overlayPictureFile = ""
    if (userOverlayApplied != "") and (system.Ratio not in ["16/9", "16/10"]):
        # Load configuration
        configOverlay = keyValueSettings(userOverlayApplied, False)
        configOverlay.loadFile(True)
        overlayInputFile = configOverlay.getString("input_overlay", "").strip('"')
        Log("processOverlays - overlayInputFile : " + overlayInputFile)
        if os.path.isfile(overlayInputFile):
            Log("processOverlays - overlayInputFile exists !")
            # Load configuration
            configInputOverlay = keyValueSettings(overlayInputFile, False)
            configInputOverlay.loadFile(True)
            overlay0File = configInputOverlay.getString("overlay0_overlay", "").strip('"')
            Log("processOverlays - overlay0File : " + overlay0File)
            # to search directory of .cfg input overlay file to have it for picture to use as overlay
            cfgName = os.path.basename(overlayInputFile)
            Log("processOverlays - cfgName : " + cfgName)
            overlayPictureFile = overlayInputFile.replace(cfgName,'') + overlay0File
            Log("processOverlays - overlayPictureFile : " + overlayPictureFile)

    if needSindenBorder == True :
        #for the moment we use the system/rom name in /tmp directory to test it
        output_png_file = "" #init this file name
        if overlayPictureFile != "":
            #need to reuse overlay to add border
            #just overlay picture file .png in tmp to manage
            output_png_file = "/tmp/sinden.png"
            Log("new tmp output_png_file : " + output_png_file)
            #remove previous ones to be sure about update (including libretro ones ;-)
            os.system("rm " + output_png_file)
        else:
            Log("Info: No overlay available.")
            #create overlay from scratch
            output_png_file = "/tmp/sinden_border.png"
            Log("empty output_png_file : " + output_png_file)
            #remove previous ones to be sure about update ;-)
            os.system("rm " + output_png_file)
            #get targeted resolution
            targetedWidth, targetedHeight = getTargetedResolution(system.VideoMode)
            #create border only
            createTransparentOverlay(output_png_file, int(targetedWidth), int(targetedHeight))
            #in this case we will reuse the same file for input and output
            overlayPictureFile = output_png_file
        from configgen.utils.videoMode import getCurrentResolution
        screenWidth, screenHeight = getCurrentResolution()
        Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
        Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
        Log("output_png_file : " + output_png_file)
        if(output_png_file != ""):
            #generate border from overlay
            innerSize, outerSize = sindenBordersSize(recalboxOptions)
            borderSize = sindenBorderImage(overlayPictureFile, output_png_file, innerSize, outerSize, sindenBordersColor(recalboxOptions))
            overlayPictureFile = output_png_file

    #init configuration string
    configstr = ""
    if overlayPictureFile != "":
        # configure string to store in mangohud config file if any picture overlay found
        configstr += f"background_image={overlayPictureFile}\nlegacy_layout=false\n"

    if recalboxOptions.getBool("system.monitoring.enabled", False): # monitoring to display stats and monitor GPU/CPU
        if recalboxOptions.getString("system.monitoring.custom", "") != "":
            configstr += recalboxOptions.getString("system.monitoring.custom","").replace("\\n", "\n")
        else:
            #positions
            monitoring_position = recalboxOptions.getString("system.monitoring.position", "top-left")
            # possible values:
            #   "bottom-left"
            #   "top-left"
            #   "top-right"
            #   "bottom-right"
            configstr += "position=" + monitoring_position + "\nbackground_alpha=0.5\nlegacy_layout=false\nround_corners=10"
            configstr += "\nfps\nfps_color_change\nfps_value=30,60\nfps_color=B22222,FDFD09,39F900"
            configstr += "\nengine_version\nvulkan_driver\narch"
            configstr += "\ncpu_stats\ncpu_temp\ncpu_power\ncpu_mhz\ncpu_load_change\ncpu_load_color=39F900,FDFD09,B22222\nram"
            
            #this following line is really significant for nvidia native drivers for the moment
            configstr += "\ngpu_stats\ngpu_temp\ngpu_core_clock\ngpu_mem_temp\ngpu_mem_clock\ngpu_power\ngpu_load_change\ngpu_load_color=39F900,FDFD09,B22222\nvram"
            
            configstr += "\nresolution"
            configstr += "\nfont_size=22\ncustom_text=%GAMENAME%\ncustom_text=%SYSTEMNAME%\ncustom_text=%EMULATORCORE%"
            #RFU
            #configstr += "\nimage_max_width=200\nimage=%THUMBNAIL%"
            
        #for custom or standard monitoring information
        configstr = configstr.replace("%SYSTEMNAME%", "System: " + system.Name)
        configstr = configstr.replace("%GAMENAME%", "Rom: " + romName)
        configstr = configstr.replace("%EMULATORCORE%", "Emulator: " + system.Emulator)
        #RFU
        #configstr = configstr.replace("%THUMBNAIL%", "")
    elif configstr != "": #default mode using overlay only
        configstr = configstr + "background_alpha=0\n" # hide the background

    if configstr != "": #only if any configuration string has been created, we could add mangohud command
        hudconfig = configstr    
        with open('/var/run/hud.config', 'w') as f:
            f.write(hudconfig)
        env["MANGOHUD_CONFIGFILE"] = "/var/run/hud.config"
        
        env["MANGOHUD_DLSYM"] = "1"
        commandArray.insert(0, "mangohud")

# Model2emu Overlay management
def processModel2emuOverlays(system: Emulator, rom: str, recalboxOptions: keyValueSettings, needSindenBorder: bool = False)-> bool:
    overlayFile = ""
    # User overlays
    userOverlayApplied = ""
    Log("processOverlays - rom : " + rom)
    romName = os.path.basename(rom)
    Log("processOverlays - romName : " + romName)
    root_ext = os.path.splitext(rom)
    romExtension = root_ext[1]
    Log("processOverlays - romExtension : " + romExtension)
    from configgen.utils.videoMode import getCurrentResolution
    screenWidth, screenHeight = getCurrentResolution()
    
    # If we are in crt mode, we only allow recalbox default 240p overlays
    if recalboxOptions.hasOption("system.crt"):
        if system.RecalboxOverlays:
            crtOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_240P_OVERLAYS, system.Name,
                                                       system.Name)
            if os.path.isfile(crtOverlayFile):
                overlayFile = crtOverlayFile
    # Overlays are applied only when we are not in wide core
    else:
        if system.Core not in ["genesisplusgxwide"] and overlayIsActivated(recalboxOptions, system.Name):
            # User overlays
            overlayFile = "{}/{}/.overlay.cfg".format(recalboxFiles.OVERLAYS, system.Name)
            if os.path.isfile(overlayFile):
                # System global configuration
                userOverlayApplied = overlayFile
            else:
                overlayFile = "{}/.overlay.cfg".format(recalboxFiles.OVERLAYS)
                if os.path.isfile(overlayFile):
                    # All system global configuration
                    userOverlayApplied = overlayFile
                else:
                    overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romName)
                    Log("processOverlays - overlayFile (from rom name) : " + overlayFile)
                    if os.path.isfile(overlayFile):
                        # Rom file overlay
                        userOverlayApplied = overlayFile
                    else:
                        #if not found, we retry if exist any without decoration using () or []
                        result = romName.split("(") # we search first ( to remove decoration using it   
                        result2 = result[0].split("[") # we search first [ to remove decoration using it
                        romNameWithoutDeco = result2[0].strip() # we use strip() to remove space at the begin and end of the string
                        romNameWithoutDeco = romNameWithoutDeco + romExtension # we add extension in this case also as done usually
                        overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romNameWithoutDeco)
                        Log("processOverlays - overlayFile (from rom name without deco) : " + overlayFile)
                        if os.path.isfile(overlayFile):
                            # System overlay
                            userOverlayApplied = overlayFile
                        else:
                            overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, system.Name)
                            if os.path.isfile(overlayFile):
                                # System overlay
                                userOverlayApplied = overlayFile
            if userOverlayApplied == "":
                # The recalbox overlays should be added only if
                # global.recalboxoverlays=1 or system.recalboxoverlays activated 
                # and no user overlays available from /recalbox/share/overlays
                # ratio = we can activate when ratio is not 16/9 and 16/10
                overlayFile = ""
                # ratio = we can activate when ratio is not 16/9 and 16/10
                if system.Ratio not in ["16/9", "16/10"]:
                    # screen resolution that can support overlays are over 1.5 ratio (as it is float > 1.51)
                    Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
                    Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
                    if ( screenWidth / screenHeight) > 1.51:
                        defaultOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_OVERLAYS, system.Name,
                                                                   system.Name)
                        if os.path.isfile(defaultOverlayFile):
                            overlayFile = defaultOverlayFile
    Log("processOverlays - overlayFile : " + overlayFile)
    #init overlay Picture File string
    overlayPictureFile = ""
    if (overlayFile != "") and (system.Ratio not in ["16/9", "16/10"]):
        #to manage resize of overlay if necessary due to different size of screen
        if overlayFile != "":
            overlayFile = matchOverlaySizeToScreen(overlayFile)
        # Load configuration
        configOverlay = keyValueSettings(overlayFile, False)
        configOverlay.loadFile(True)
        overlayInputFile = configOverlay.getString("input_overlay", "").strip('"')
        Log("processOverlays - overlayInputFile : " + overlayInputFile)
        if os.path.isfile(overlayInputFile):
            Log("processOverlays - overlayInputFile exists !")
            # Load configuration
            configInputOverlay = keyValueSettings(overlayInputFile, False)
            configInputOverlay.loadFile(True)
            overlay0File = configInputOverlay.getString("overlay0_overlay", "").strip('"')
            Log("processOverlays - overlay0File : " + overlay0File)
            # to search directory of .cfg input overlay file to have it for picture to use as overlay
            cfgName = os.path.basename(overlayInputFile)
            Log("processOverlays - cfgName : " + cfgName)
            overlayPictureFile = overlayInputFile.replace(cfgName,'') + overlay0File
            Log("processOverlays - overlayPictureFile : " + overlayPictureFile)
            
    if needSindenBorder == True :
        #for the moment we use the system/rom name in /tmp directory to test it
        output_png_file = "" #init this file name
        if overlayPictureFile != "":
            #need to reuse overlay to add border
            #just overlay picture file .png in tmp to manage
            output_png_file = "/tmp/sinden.png"
            Log("new tmp output_png_file : " + output_png_file)
            #remove previous ones to be sure about update (including libretro ones ;-)
            os.system("rm " + output_png_file)
        else:
            Log("Info: No overlay available.")
            #create overlay from scratch
            output_png_file = "/tmp/sinden_border.png"
            Log("empty output_png_file : " + output_png_file)
            #remove previous ones to be sure about update ;-)
            os.system("rm " + output_png_file)
            #get targeted resolution
            targetedWidth, targetedHeight = getTargetedResolution(system.VideoMode)
            #create border only
            createTransparentOverlay(output_png_file, int(targetedWidth), int(targetedHeight))
            #in this case we will reuse the same file for input and output
            overlayPictureFile = output_png_file
        Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
        Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
        Log("output_png_file : " + output_png_file)
        if(output_png_file != ""):
            #generate border from overlay
            innerSize, outerSize = sindenBordersSize(recalboxOptions)
            borderSize = sindenBorderImage(overlayPictureFile, output_png_file, innerSize, outerSize, sindenBordersColor(recalboxOptions))
            overlayPictureFile = output_png_file

    #prepare overlays now for .lua script for model2emu
    output_png = "/tmp/model2emu_overlay.png"
    if overlayPictureFile != "":
        #create specific "surface iamge" for model2emu overlay
        model2emuSurfaceImage(overlayPictureFile, output_png, screenWidth, screenHeight)
        return True
    else:
        # Delete overlay generated previously in /tmp directory to avoid to be found by Model2emu .lua script
        os.system("rm /tmp/model2emu_overlay.png")
        return False

def model2emuSurfaceImage(input_png, output_png, w, h):
    # we need to do an image with the overlay inside but reduced in a specific size in an empty transparent image 
    # but don't ask me why ;-), I just did reverse engineeting of overlay prodivded by sinden lightgun wiki.
    
    # At the beginning, we created a transparent .png file with 1920x1080
    # and we will include the input overlay (with or without sinden border calculated)
    # but with a resize in 1800 x 571
    
    # now we could manage several resolutions as after:
    # 16/9 RESOLUTIONS	TOTAL PIXELS	DESIGNATION
    # 1280 x 720	    921,600  	    HD (720p)
    # 1920 ✕ 1080	    2,073,600	    FHD / Full HD (1080p)
    # 2560 ✕ 1440	    3,686,400	    WQHD
    # 3200 ✕ 1800	    5,760,000	    QHD+
    # 3840 ✕ 2160	    8,294,400	    4K UHD

    #size tested for the moment in model2emu that could be used to calculate the ratio
    rw = 1920
    rh = 1080
    #overlay targeted size
    rtw = 1800
    rth= 571
    #new size caculated from resolution w,h and from ratio identified in case of 1920*1080
    tw = int(w * (rtw/rw)) # ratio seems 0,9375
    th = int(h * (rth/rh)) # ratio seems 0,5287
    
    # resize existing overlay fist using tmp file TO DO: do it in memory if needed
    output_tmp_png = "/tmp/overlay_resized.png"
    resizeImage(input_png, output_tmp_png, tw, th, True) #for full HD: 1920x1080 as fixed for this emulator
    #open tmp file just resized
    output_tmp_image = Image.open(output_tmp_png)
    #create empty image in full full hd or ultra hd
    if(h <= 1080):
        #use image of 2K to be compatible with resolutions from 720p to 1080p
        imgnew = Image.new("RGBA", (1920,1080), (0,0,0,255))
    else:
        #use image of 4K to be compatible with resolutions from 1800 to 4k
        imgnew = Image.new("RGBA", (3840,2160), (0,0,0,255))
    
    #copy overlay resized in empty image in full hd
    imgnew.paste(output_tmp_image, (0,0,tw,th))
    #save it to finish
    imgnew.save(output_png, mode="RGBA", format="PNG")

def alphaPaste(input_png, output_png, fillcolor, screensize, bezel_stretch):
  # screensize=(screen_width, screen_height)
  imgin = Image.open(input_png)
  # TheBezelProject have Palette + alpha, not RGBA. PIL can't convert from P+A to RGBA.
  # Even if it can load P+A, it can't save P+A as PNG. So we have to recreate a new image to adapt it.
  if not 'transparency' in imgin.info:
      raise Exception("no transparent pixels in the image, abort")
  alpha = imgin.split()[-1]  # alpha from original palette + alpha
  ix,iy = fast_image_size(input_png)
  sx,sy = screensize
  i_ratio = (float(ix) / float(iy))
  s_ratio = (float(sx) / float(sy))

  if (i_ratio - s_ratio > 0.01):
      # cut off bezel sides for 16:10 screens
      new_x = int(ix*s_ratio/i_ratio)
      delta = int(ix-new_x)
      borderx = delta//2
      ix = new_x
      alpha_new = alpha.crop((borderx, 0, new_x+borderx, iy))
      alpha = alpha_new

  imgnew = Image.new("RGBA", (ix,iy), (0,0,0,255))
  imgnew.paste(alpha, (0,0,ix,iy))
  if bezel_stretch:
      imgout = ImageOps.fit(imgnew, screensize)
  else:
      imgout = ImageOps.pad(imgnew, screensize, color=fillcolor, centering=(0.5,0.5))
  imgout.save(output_png, mode="RGBA", format="PNG")

def sindenBordersSize(recalboxOptions: keyValueSettings) -> (float, float):
    bordersSize = recalboxOptions.getString("lightgun.sinden.bordersize","superthin")
    if bordersSize == "crt":
        return -1, -1
    if bordersSize == "superthin":
        return 0.5, 0
    if bordersSize == "thin":
        return 1, 0
    if bordersSize == "medium":
        return 2, 0
    if bordersSize == "big":
        return 2, 1
    return 0.5, 0 # as superthin by default

def sindenBorderImage(input_png, output_png, innerBorderSizePer = 2, outerBorderSizePer = 3, innerBorderColor = "#ffffff", outerBorderColor = "#000000"):
    # good default border that works in most circumstances is:
    # 
    # 2% of the screen width in white.  Surrounded by 3% screen width of
    # black.  I have attached an example.  The black helps the lightgun detect
    # the border against a bright background behind the tv.
    # 
    # The ideal solution is to draw the games inside the border rather than
    # overlap.  Then you can see the whole game.  The lightgun thinks that the
    # outer edge of the border is the edge of the game screen.  So you have to
    # make some adjustments in the lightgun settings to keep it aligned.  This
    # is why normally the border overlaps as it means that people do not need
    # to calculate an adjustment and is therefore easier.
    # 
    # If all the games are drawn with the border this way then the settings
    # are static and the adjustment only needs to be calculated once.

    w,h = fast_image_size(input_png)

    # outer border
    outerBorderSize = h * outerBorderSizePer // 100 # use only h to have homogen border size
    if outerBorderSize < 1: # minimal size
        outerBorderSize = 1
    outerShapes = [ [(0, 0), (w, outerBorderSize)], [(w-outerBorderSize, 0), (w, h)], [(0, h-outerBorderSize), (w, h)], [(0, 0), (outerBorderSize, h)] ]

    # inner border
    innerBorderSize = w * innerBorderSizePer // 100 # use only h to have homogen border size
    if innerBorderSize < 1: # minimal size
        innerBorderSize = 1
    innerShapes = [ [(outerBorderSize, outerBorderSize), (w-outerBorderSize, outerBorderSize+innerBorderSize)],
                    [(w-outerBorderSize-innerBorderSize, outerBorderSize), (w-outerBorderSize, h-outerBorderSize)],
                    [(outerBorderSize, h-outerBorderSize-innerBorderSize), (w-outerBorderSize, h-outerBorderSize)],
                    [(outerBorderSize, outerBorderSize), (outerBorderSize+innerBorderSize, h-outerBorderSize)] ]
    
    back = Image.open(input_png)
    imgnew = Image.new("RGBA", (w,h), (0,0,0,255))
    imgnew.paste(back, (0,0,w,h))
    imgnewdraw = ImageDraw.Draw(imgnew)
    for shape in outerShapes:
        imgnewdraw.rectangle(shape, fill=outerBorderColor)
    for shape in innerShapes:
        imgnewdraw.rectangle(shape, fill=innerBorderColor)
    imgnew.save(output_png, mode="RGBA", format="PNG")

    return outerBorderSize + innerBorderSize

def sindenBorderSize(w, h, innerBorderSizePer = 2, outerBorderSizePer = 3):
    return (h * (innerBorderSizePer + outerBorderSizePer)) // 100

def sindenBordersColor(recalboxOptions: keyValueSettings) -> str:
    color = recalboxOptions.getString("lightgun.sinden.bordercolor","white")
    if color == "red":
        return "#ff0000"
    if color == "green":
        return "#00ff00"
    if color == "blue":
        return "#0000ff"
    if color == "white":
        return "#ffffff"
    return "#ffffff"

def createTransparentOverlay(output_png, width, height):
    imgnew = Image.new("RGBA", (width,height), (0,0,0,0))
    imgnewdraw = ImageDraw.Draw(imgnew)
    imgnew.save(output_png, mode="RGBA", format="PNG")
