#!/usr/bin/env python

from configgen.generators.Generator import Generator
from configgen.Command import Command
import os
from os import path
# from os import environ
import configgen.recalboxFiles as recalboxFiles
from configgen.Emulator import Emulator
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.utils.videoMode import GetHasVulkanSupport
from configgen.utils.videoMode import getCurrentResolution
from xml.dom import minidom
import codecs
from configgen.controllers import controller
import shutil
# import filecmp
import subprocess
# import glob
import configgen.generators.cemu.cemuControllers as cemuControllers

def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(txt)
    return 0

class CemuGenerator(Generator):

    # Game Ratio
    GAME_RATIO = \
    {
       "auto": 0,
       "16/9": 1, 
       "4/3": 2,
       "squarepixel": 3,
    }

    CEMU_LANGUAGES = \
    {
        "ja": 0,
        "en": 1,
        "fr": 2,
        "de": 3,
        "it": 4,
        "es": 5,
        "zh": 6,
        "ko": 7,
        "nl": 8,
        "pt": 9,
        "ru": 10,
    }

    # Show mouse for touchscreen actions    
    def getMouseMode(self, config):
        if "cemu_touchpad" in config and config["cemu_touchpad"] == "1":
            return True
        else:
            return False

    @staticmethod
    def getRoot(config, name):
        xml_section = config.getElementsByTagName(name)

        if len(xml_section) == 0:
            xml_section = config.createElement(name)
            config.appendChild(xml_section)
        else:
            xml_section = xml_section[0]
        return xml_section

    @staticmethod
    def setSectionConfig(config, xml_section, name, value):
        xml_elt = xml_section.getElementsByTagName(name)
        if len(xml_elt) == 0:
            xml_elt = config.createElement(name)
            xml_section.appendChild(xml_elt)
        else:
            xml_elt = xml_elt[0]

        if xml_elt.hasChildNodes():
            xml_elt.firstChild.data = value
        else:
            xml_elt.appendChild(config.createTextNode(value))

    def MainConfiguration(configFile, system):
        # read recalbox.conf for advanced configuration
        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)

        # Language
        language = conf.getString("system.language", "en")[0:2].lower()
        upscaleFilter = conf.getInt("cemu.upscale.filter", 0)
        downscaleFilter = conf.getInt("cemu.downscale.filter", 0)
        asynCompile = conf.getInt("cemu.async.compile", 0)
        vsync = conf.getInt("cemu.vsync", 0)

        # Set video renderer OpenGl / vulkan
        videoDriver = GetHasVulkanSupport()

        if language in CemuGenerator.CEMU_LANGUAGES:
            languageCode = language
        else:
            languageCode ="en" #en
        # set systemLanguage variable
        cemuLanguage = CemuGenerator.CEMU_LANGUAGES[languageCode] if languageCode in CemuGenerator.CEMU_LANGUAGES else 1 #en

        # Get resolution
        width, height = getCurrentResolution()
        resoX = "{}".format(width)
        resoY = "{}".format(height)

        # Game Ratio
        gameRatio = CemuGenerator.GAME_RATIO[system.Ratio] if system.Ratio in CemuGenerator.GAME_RATIO else 0

        # Config file
        config = minidom.Document()
        if os.path.exists(configFile):
            try:
                config = minidom.parse(configFile)
            except:
                pass # reinit the file

        # ROOT NODE
        xml_root = CemuGenerator.getRoot(config, "content")
        # Set defautl path for mlc
        CemuGenerator.setSectionConfig(config, xml_root, "mlc_path", recalboxFiles.cemuSaves + "/mlc01")
        CemuGenerator.setSectionConfig(config, xml_root, "check_update", "false")
        # Skip Wizzard configuration
        CemuGenerator.setSectionConfig(config, xml_root, "gp_download", "true")
        CemuGenerator.setSectionConfig(config, xml_root, "logflag", "0")
        CemuGenerator.setSectionConfig(config, xml_root, "advanced_ppc_logging", "false")
        CemuGenerator.setSectionConfig(config, xml_root, "use_discord_presence", "false")
        CemuGenerator.setSectionConfig(config, xml_root, "fullscreen_menubar", "false")
        CemuGenerator.setSectionConfig(config, xml_root, "vk_warning", "false")
        CemuGenerator.setSectionConfig(config, xml_root, "fullscreen", "true")
        # Language
        CemuGenerator.setSectionConfig(config, xml_root, "console_language", cemuLanguage)

        # WINDOWS 
        CemuGenerator.setSectionConfig(config, xml_root, "window_position", "")
        window_position = CemuGenerator.getRoot(config, "window_position")
        CemuGenerator.setSectionConfig(config, window_position, "x", "0")
        CemuGenerator.setSectionConfig(config, window_position, "y", "0")
        CemuGenerator.setSectionConfig(config, xml_root, "window_size", "")
        window_size = CemuGenerator.getRoot(config, "window_size")
        CemuGenerator.setSectionConfig(config, window_size, "x", resoX)
        CemuGenerator.setSectionConfig(config, window_size, "y", resoY)

        # GAMEPAD
        # if system.isOptSet("cemu_gamepad") and system.config["cemu_gamepad"] == "True":
        #     CemuGenerator.setSectionConfig(config, xml_root, "open_pad", "true")
        # else:
        #     CemuGenerator.setSectionConfig(config, xml_root, "open_pad", "false")
        CemuGenerator.setSectionConfig(config, xml_root, "pad_position", "")
        pad_position = CemuGenerator.getRoot(config, "pad_position")
        CemuGenerator.setSectionConfig(config, pad_position, "x", "0")
        CemuGenerator.setSectionConfig(config, pad_position, "y", "0")
        CemuGenerator.setSectionConfig(config, xml_root, "pad_size", "")
        pad_size = CemuGenerator.getRoot(config, "pad_size")
        CemuGenerator.setSectionConfig(config, pad_size, "x", "640")
        CemuGenerator.setSectionConfig(config, pad_size, "y", "480")

        # GAME PATH
        CemuGenerator.setSectionConfig(config, xml_root, "GamePaths", "")
        game_root = CemuGenerator.getRoot(config, "GamePaths")
        # Default games path
        CemuGenerator.setSectionConfig(config, game_root, "Entry", recalboxFiles.cemuRomdir)

        # GRAPHICS
        CemuGenerator.setSectionConfig(config, xml_root, "Graphic", "")
        graphic_root = CemuGenerator.getRoot(config, "Graphic")
        CemuGenerator.setSectionConfig(config, graphic_root, "api", videoDriver)
        CemuGenerator.setSectionConfig(config, graphic_root, "UpscaleFilter", upscaleFilter)
        CemuGenerator.setSectionConfig(config, graphic_root, "DownscaleFilter", downscaleFilter)
        CemuGenerator.setSectionConfig(config, graphic_root, "VSync", vsync)

        if asynCompile == 1:
            CemuGenerator.setSectionConfig(config, graphic_root, "AsyncCompile", "true")
        else:
            CemuGenerator.setSectionConfig(config, graphic_root, "AsyncCompile", "false")
        # Aspect Ratio
        CemuGenerator.setSectionConfig(config, graphic_root, "FullscreenScaling", gameRatio)

        # GRAPHICS OVERLAYS
        CemuGenerator.setSectionConfig(config, graphic_root, "Overlay", "")
        overlay_root = CemuGenerator.getRoot(config, "Overlay")
        # Display FPS / CPU / GPU / RAM
        CemuGenerator.setSectionConfig(config, graphic_root, "Notification", "")
        notification_root = CemuGenerator.getRoot(config, "Notification")
        CemuGenerator.setSectionConfig(config, overlay_root, "Position", "3")
        CemuGenerator.setSectionConfig(config, overlay_root, "TextColor", "4294967295")
        CemuGenerator.setSectionConfig(config, overlay_root, "TextScale", "100")
        if system.ShowFPS == True:
            CemuGenerator.setSectionConfig(config, overlay_root, "FPS", "true")
            CemuGenerator.setSectionConfig(config, overlay_root, "CPUUsage", "true")
            CemuGenerator.setSectionConfig(config, overlay_root, "RAMUsage", "true")
        else:
            CemuGenerator.setSectionConfig(config, overlay_root, "FPS", "false")
            CemuGenerator.setSectionConfig(config, overlay_root, "CPUUsage", "false")
            CemuGenerator.setSectionConfig(config, overlay_root, "RAMUsage", "false")
        # Other Options fps
        CemuGenerator.setSectionConfig(config, overlay_root, "DrawCalls", "false")
        CemuGenerator.setSectionConfig(config, overlay_root, "CPUPerCoreUsage", "false")
        CemuGenerator.setSectionConfig(config, overlay_root, "VRAMUsage", "false")
        # # Notifications
        CemuGenerator.setSectionConfig(config, graphic_root, "Notification", "")
        notification_root = CemuGenerator.getRoot(config, "Notification")
        CemuGenerator.setSectionConfig(config, notification_root, "Position", "1")
        CemuGenerator.setSectionConfig(config, notification_root, "TextColor", "4294967295")
        CemuGenerator.setSectionConfig(config, notification_root, "TextScale", "100")
        if system.ShowFPS == True:
            CemuGenerator.setSectionConfig(config, notification_root, "ShaderCompiling", "true")
        else:
            CemuGenerator.setSectionConfig(config, notification_root, "ShaderCompiling", "false")

        CemuGenerator.setSectionConfig(config, notification_root, "ControllerProfiles", "false")
        CemuGenerator.setSectionConfig(config, notification_root, "ControllerBattery", "false")
        CemuGenerator.setSectionConfig(config, notification_root, "FriendService", "false")

        # AUDIO
        CemuGenerator.setSectionConfig(config, xml_root, "Audio", "")
        audio_root = CemuGenerator.getRoot(config, "Audio")
        CemuGenerator.setSectionConfig(config, audio_root, "api", "3") # cubeb
        # audio on TV
        CemuGenerator.setSectionConfig(config, audio_root, "TVChannels", "1") # Stereo
        # volume max
        CemuGenerator.setSectionConfig(config, audio_root, "TVVolume", "100")
        # Set the audio device - we choose the 1st device as this is more likely the answer
        # pactl list sinks-raw | sed -e s+"^sink=[0-9]* name=\([^ ]*\) .*"+"\1"+ | sed 1q | tr -d '\n'
        proc = subprocess.run(["/usr/bin/cemu/get-audio-device"], stdout=subprocess.PIPE)
        cemuAudioDevice = proc.stdout.decode('utf-8')
        print("*** audio device = {} ***".format(cemuAudioDevice))
        CemuGenerator.setSectionConfig(config, audio_root, "TVDevice", cemuAudioDevice)
        account_root = CemuGenerator.getRoot(config, "Account")
        # Set option on pegasus at the moment forced on false
        CemuGenerator.setSectionConfig(config, account_root, "OnlineEnabled", "false")
        # Set prentendo server for future support only on test for the moment 
        # More information on 'https://pretendo.network' need account registred and 
        # add fake-online file on your /share/saves/wiiu/ found on 'https://github.com/SmmServer/FakeOnlineFiles'
        # 0=Nintendo server 1=Prentendo 2=Custom (maybe for xlink kai?)
        CemuGenerator.setSectionConfig(config, account_root, "ActiveService", 1)
        
        # Save the config file
        xml = open(configFile, "w")

        # TODO: python 3 - workaround to encode files in utf-8
        xml = codecs.open(configFile, "w", "utf-8")
        dom_string = os.linesep.join([s for s in config.toprettyxml().splitlines() if s.strip()]) # remove ugly empty lines while minicom adds them...
        xml.write(dom_string)

    def generate(self, system, playersControllers, configFile, args):
        # # Create Path for cemu
        if not path.isdir(recalboxFiles.BIOS + "/wiiu"):
            os.mkdir(recalboxFiles.BIOS + "/wiiu")
        if not path.isdir(recalboxFiles.cemuConfig):
            os.mkdir(recalboxFiles.cemuConfig)
        if not path.isdir(recalboxFiles.cemuSaves + "/graphicPacks"):
            os.mkdir(recalboxFiles.cemuSaves + "/graphicPacks")         
        if not path.isdir(recalboxFiles.cemuConfig + "/controllerProfiles"):
            os.mkdir(recalboxFiles.cemuConfig + "/controllerProfiles")
        # Create the settings file
        if not path.isfile(recalboxFiles.cemuSettings):
            shutil.copy2(recalboxFiles.cemuSettingsOrigin, recalboxFiles.cemuSettings)

        if not system.HasConfigFile:
            CemuGenerator.MainConfiguration(recalboxFiles.cemuSettings, system)
            # Controllers
            cemuControllers.generateControllerConfig(system, playersControllers)

        envVariables={}
        envVariables["XDG_CONFIG_HOME"] = recalboxFiles.CONF
        envVariables["XDG_CACHE_HOME"] = recalboxFiles.CACHE
        envVariables["XDG_DATA_HOME"] = recalboxFiles.SAVES
        # envVariables["SDL_GAMECONTROLLERCONFIG"] = controllersConfig.generateSdlGameControllerConfig(playersControllers),
        envVariables["SDL_JOYSTICK_HIDAPI"] = "0"

        commandArray = [recalboxFiles.recalboxBins[system.Emulator], "-g", args.rom]
        if system.HasArgs: commandArray.extend(system.Args)

        return Command( videomode=system.VideoMode, array=commandArray, env=envVariables)
