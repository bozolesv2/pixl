#!/usr/bin/env python
from typing import Dict

from configgen.Command import Command
from configgen.Emulator import Emulator
from configgen.controllers.controller import ControllerPerPlayer
from configgen.controllers.controller import Controller
from configgen.emulatorlauncher import recalboxFiles
from configgen.generators.Generator import Generator
from configgen.settings.iniSettings import IniSettings
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.utils.videoMode import GetHasVulkanSupport
import os
import shutil

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

class Pcsx2Generator(Generator):
    
    SECTION_EMUCORE = "EmuCore"
    SECTION_EMUCORE_GS = "EmuCore/GS"
    SECTION_EMUCORE_SPEEDHACKS = "EmuCore/Speedhacks"
    SECTION_FILENAMES = "Filenames"
    SECTION_UI = "UI"
    SECTION_ACHIEVEMENTS = "Achievements"
    SECTION_GAMELIST = "GameList"
    SECTION_HOTKEYS = "Hotkeys"
    SECTION_INPUT_SOURCES = "InputSources"
    SECTION_FOLDERS = "Folders"

    # Bios names
    BIOS_USA = "ps2-0230a-20080220.bin"
    BIOS_EU  = "ps2-0230e-20080220.bin"
    BIOS_JP  = "ps2-0230j-20080220.bin"
    BIOS_HK  = "ps2-0230h-20080220.bin"

    # Make dirs
    memcards  : str = os.path.join(recalboxFiles.SAVES, "ps2/pcsx2/memorycards")
    sstates: str = os.path.join(recalboxFiles.SAVES, "ps2/pcsx2/savestates")
    cache: str = os.path.join(recalboxFiles.SAVES, "ps2/pcsx2/cache")
    gamesettings: str = os.path.join(recalboxFiles.SAVES, "ps2/pcsx2/gamesettings")
    configFolders = str = os.path.join(recalboxFiles.pcsx2RootFolder, "PCSX2/inis")
    biosFolders = str = os.path.join(recalboxFiles.BIOS, "ps2/pcsx2")
    if not os.path.exists(memcards): os.makedirs(name=memcards, exist_ok=True)
    if not os.path.exists(sstates): os.makedirs(name=sstates, exist_ok=True)
    if not os.path.exists(cache): os.makedirs(name=cache, exist_ok=True)
    if not os.path.exists(gamesettings): os.makedirs(name=gamesettings, exist_ok=True)
    if not os.path.exists(configFolders): os.makedirs(name=configFolders, exist_ok=True)
    if not os.path.exists(biosFolders): os.makedirs(name=biosFolders, exist_ok=True)

    @staticmethod
    def __CheckPs2Bios(rom: str) -> str:
        import os.path
        def Exists(romPath: str) -> bool:
            return os.path.exists( '/recalbox/share/bios/ps2/pcsx2/' + romPath)

        # Try to get Redump region
        lrom = rom.lower()
        # EU region
        if   "europe" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "france" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "ireland" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "germany" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "belgium" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "italy" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "greece" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "croatia" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "denmark" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "spain" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "portugal" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "poland" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "finland" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "russia" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "austria" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "netherlands" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "switzerland" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "norway" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "sweden" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "scandinavia" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "australia" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "india" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "south africa" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        # USA region
        elif "canada" in lrom and Exists(Pcsx2Generator.BIOS_USA): return Pcsx2Generator.BIOS_USA
        elif "brazil" in lrom and Exists(Pcsx2Generator.BIOS_USA): return Pcsx2Generator.BIOS_USA
        elif "latin america" in lrom and Exists(Pcsx2Generator.BIOS_USA): return Pcsx2Generator.BIOS_USA
        # JP region
        elif "japan" in lrom and Exists(Pcsx2Generator.BIOS_JP): return Pcsx2Generator.BIOS_JP
        elif "korea" in lrom and Exists(Pcsx2Generator.BIOS_JP): return Pcsx2Generator.BIOS_JP
        elif "asia" in lrom and Exists(Pcsx2Generator.BIOS_JP): return Pcsx2Generator.BIOS_JP
        # CH
        elif "china" in lrom and Exists(Pcsx2Generator.BIOS_JP): return Pcsx2Generator.BIOS_HK
        elif "taiwan" in lrom and Exists(Pcsx2Generator.BIOS_JP): return Pcsx2Generator.BIOS_HK
        # Shortest name at the end
        elif "uk" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "usa" in lrom and Exists(Pcsx2Generator.BIOS_USA): return Pcsx2Generator.BIOS_USA

        # Check if PS2 bios bin in folders
        for bios in (Pcsx2Generator.BIOS_USA, Pcsx2Generator.BIOS_EU, Pcsx2Generator.BIOS_JP, Pcsx2Generator.BIOS_HK):
            if Exists(bios):
                return bios
        return ""

    def __Configure(self, system, rom: str, playersControllers, args):
        # Game Ratio AspectRatio=Auto 4:3/3:2
        GAME_RATIO: Dict[str, str] = \
        {
            "auto": "Auto 4:3/3:2",
            "16/9": "16:9",
            "4/3": "4:3",
        }
        gameRatio = GAME_RATIO[system.Ratio] if system.Ratio in GAME_RATIO else "4:3"

        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)

        language =  conf.getString("system.language", "en-US")
        resolution = conf.getString("pcsx2.resolution", "1") # Native resolution
        anisotropy = conf.getString("pcsx2.anisotropy", "0") # None
        tvShaders = conf.getString("pcsx2.tvshaders", "0") # None
        cheats = conf.getBool("pcsx2.cheats", "0")
        vsync = conf.getBool("pcsx2.vsync", "0")
        ## Enable retroarchievements
        retroachievements = conf.getBool("global.retroachievements", "0")
        soundEffects = conf.getBool("global.retroachievements.unlock.sound", "0")
        hardcoreMode = conf.getBool("global.retroachievements.hardcore", "0")
        cheevosUsername = conf.getString("global.retroachievements.username", "pixl")
        cheevosToken = conf.getString("global.retroachievements.token", "")
        videoDriver = "14" if GetHasVulkanSupport() == '1' else "12" # OpenGl(12)/vulkan(14)
        
        # test if file is not deleted for create a default basic conf stocked in share_init
        if not os.path.isfile(recalboxFiles.pcsx2ConfigFile):
            shutil.copyfile(recalboxFiles.pcsx2DefaultConfigFile, recalboxFiles.pcsx2ConfigFile)

        inis = IniSettings(recalboxFiles.pcsx2ConfigFile)
        inis.loadFile(True) \
            .defineBool('true', 'false')
        
        # Override UI Configuration
        # lock launch wizzard
        inis.setBool(Pcsx2Generator.SECTION_UI, "SetupWizardIncomplete", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "ConfirmShutdown", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "StartFullscreen", True)
        inis.setBool(Pcsx2Generator.SECTION_UI, "RenderToSeparateWindow", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "DisableWindowResize", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "HideMouseCursor", True)
        inis.setBool(Pcsx2Generator.SECTION_UI, "ShowStatusBar", False)
        inis.setString(Pcsx2Generator.SECTION_UI, "Language", language.replace("_", "-"))

        inis.setString(Pcsx2Generator.SECTION_GAMELIST,"RecursivePaths", "/recalbox/share/roms/ps2")

        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Bios", "/recalbox/share/bios/ps2/pcsx2")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Cache", "/recalbox/share/saves/ps2/pcsx2/cache")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "MemoryCards", "/recalbox/share/saves/ps2/pcsx2/memorycards")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Savestates", "/recalbox/share/saves/ps2/pcsx2/savestates")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Covers", "/recalbox/share/roms/ps2/media/box2dfront")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Snapshots", "/recalbox/share/screenshots")

        # choose bios files 
        inis.setString(Pcsx2Generator.SECTION_FILENAMES, "BIOS", Pcsx2Generator.__CheckPs2Bios(rom))

        inis.setBool(Pcsx2Generator.SECTION_EMUCORE, "EnableCheats", cheats)
        inis.setBool(Pcsx2Generator.SECTION_EMUCORE, "WarnAboutUnsafeSettings", False)
        inis.setBool(Pcsx2Generator.SECTION_EMUCORE, "EnableFastBoot", False)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE, "NotificationsDuration", 7)

        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "VsyncEnable", vsync) # !!!!!!! need to add pegasus option !!!!!!! 
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "VsyncQueueSize", 2)
        inis.setString(Pcsx2Generator.SECTION_EMUCORE_GS, "AspectRatio", gameRatio)
        # overide user hack to default state
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "UserHacks_SkipDraw_Start", 0)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "UserHacks_SkipDraw_End", 0)
        inis.setBool(Pcsx2Generator.SECTION_EMUCORE_GS, "OsdShowFPS", system.ShowFPS)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "Renderer", videoDriver)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "upscale_multiplier", resolution)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "MaxAnisotropy", anisotropy)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "TVShader", tvShaders)

        inis.setBool(Pcsx2Generator.SECTION_ACHIEVEMENTS, "Enabled", retroachievements)
        inis.setBool(Pcsx2Generator.SECTION_ACHIEVEMENTS, "SoundEffects", soundEffects)
        inis.setBool(Pcsx2Generator.SECTION_ACHIEVEMENTS, "ChallengeMode", hardcoreMode)
        inis.setString(Pcsx2Generator.SECTION_ACHIEVEMENTS, "Username", cheevosUsername)
        inis.setString(Pcsx2Generator.SECTION_ACHIEVEMENTS, "Token", cheevosToken)

        inis.setString(Pcsx2Generator.SECTION_INPUT_SOURCES, "Keyboard", True)
        inis.setString(Pcsx2Generator.SECTION_INPUT_SOURCES, "Mouse", False)
        inis.setString(Pcsx2Generator.SECTION_INPUT_SOURCES, "SDL", True)
        inis.setString(Pcsx2Generator.SECTION_INPUT_SOURCES, "SDLControllerEnhancedMode", True)

        # Log level
        inis.setString("Logging", "EnableVerbose", False if args.verbose else True)

        # remove the previous [Padx] sections to avoid phantom controllers
        existingPadX= ["Pad1", "Pad2", "Pad3", "Pad4", "Pad5", "Pad6", "Pad7", "Pad8", "Hotkeys"]
        for padX in existingPadX:
            inis.clearRegion(padX)
            
        for index in playersControllers:
            controller = playersControllers[index]
            Log(" PlayerIndex of " + controller.DeviceName + " : {}".format(controller.PlayerIndex))
            Log(" SdlIndex of " + controller.DeviceName + " : {}".format(controller.SdlIndex))
            Log(" NaturalIndex of " + controller.DeviceName + " : {}".format(controller.NaturalIndex))
            Log(" JsIndex of " + controller.DeviceName + " : {}".format(controller.JsIndex))
            Log(" JsOrder of " + controller.DeviceName + " : {}".format(controller.JsOrder))
            # dynamic Pad and sdl int
            padIndex = str = "Pad{}".format(controller.PlayerIndex)
            sdlIndex = str = "SDL-{}".format(controller.SdlIndex)

            # Define type of controller DualShock2/Guitare and other default parameters
            inis.setString(padIndex, "Type", "DualShock2")
            inis.setString(padIndex, "InvertL", "0")
            inis.setString(padIndex, "InvertR", "0")
            inis.setString(padIndex, "Deadzone", "0.2")
            inis.setString(padIndex, "AxisScale", "1.33")
            inis.setString(padIndex, "TriggerDeadzone", "0.2")
            inis.setString(padIndex, "TriggerScale", "1")
            inis.setString(padIndex, "LargeMotorScale", "1")
            inis.setString(padIndex, "SmallMotorScale", "1")
            inis.setString(padIndex, "ButtonDeadzone", "0.2")
            inis.setString(padIndex, "PressureModifier", "0")
            
            # Mapping controllers
            if controller.HasUp :           inis.setString(padIndex, "Up", "{}/DPadUp".format(sdlIndex))
            if controller.HasRight :        inis.setString(padIndex, "Right", "{}/DPadRight".format(sdlIndex))
            if controller.HasDown :         inis.setString(padIndex, "Down", "{}/DPadDown".format(sdlIndex))
            if controller.HasLeft :         inis.setString(padIndex, "Left", "{}/DPadLeft".format(sdlIndex))
            if controller.HasY :            inis.setString(padIndex, "Triangle", "{}/Y".format(sdlIndex))
            if controller.HasB :            inis.setString(padIndex, "Circle", "{}/B".format(sdlIndex))
            if controller.HasA :            inis.setString(padIndex, "Cross", "{}/A".format(sdlIndex))
            if controller.HasX :            inis.setString(padIndex, "Square", "{}/X".format(sdlIndex))
            if controller.HasSelect :       inis.setString(padIndex, "Select", "{}/Back".format(sdlIndex))
            if controller.HasStart :        inis.setString(padIndex, "Start", "{}/Start".format(sdlIndex))
            if controller.HasL1 :           inis.setString(padIndex, "L1", "{}/LeftShoulder".format(sdlIndex))
            if controller.HasL2 :           inis.setString(padIndex, "L2", "{}/+LeftTrigger".format(sdlIndex))
            if controller.HasR1 :           inis.setString(padIndex, "R1", "{}/RightShoulder".format(sdlIndex))
            if controller.HasR2 :           inis.setString(padIndex, "R2", "{}/+RightTrigger".format(sdlIndex))
            if controller.HasL3 :           inis.setString(padIndex, "L3", "{}/LeftStick".format(sdlIndex))
            if controller.HasR3 :           inis.setString(padIndex, "R3", "{}/RightStick".format(sdlIndex))
            if controller.HasJoy1Up :       inis.setString(padIndex, "LUp", "{}/-LeftY".format(sdlIndex))
            if controller.HasJoy1Right :    inis.setString(padIndex, "LRight", "{}/+LeftX".format(sdlIndex))
            if controller.HasJoy1Down :     inis.setString(padIndex, "LDown", "{}/+LeftY".format(sdlIndex))
            if controller.HasJoy1Left :     inis.setString(padIndex, "LLeft", "{}/-LeftX".format(sdlIndex))
            if controller.HasJoy2Up :       inis.setString(padIndex, "RUp", "{}/-RightY".format(sdlIndex))
            if controller.HasJoy2Right :    inis.setString(padIndex, "RRight", "{}/+RightX".format(sdlIndex))
            if controller.HasJoy2Down :     inis.setString(padIndex, "RDown", "{}/+RightY".format(sdlIndex))
            if controller.HasJoy2Left :     inis.setString(padIndex, "RLeft", "{}/-RightX".format(sdlIndex))

            if controller.PlayerIndex == 1:
                print("P1 found")
                player1 = "SDL-{}".format(controller.SdlIndex)
                Log("Hotkey player1 Index SDL: {}".format(player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "OpenPauseMenu", "{}/Guide & {}/A".format(player1, player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "OpenAchievementsList", "{}/Guide & {}/RightShoulder".format(player1, player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "Screenshot", "{}/Guide & {}/LeftShoulder".format(player1, player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "ToggleSlowMotion", "{}/Guide & {}/DPadLeft".format(player1, player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "HoldTurbo", "{}/Guide & {}/DPadRight".format(player1, player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "LoadStateFromSlot", "{}/Guide & {}/X".format(player1, player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "NextSaveStateSlot", "{}/Guide & {}/DPadUp".format(player1, player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "PreviousSaveStateSlot", "{}/Guide & {}/DPadDown".format(player1, player1))
                inis.setString(Pcsx2Generator.SECTION_HOTKEYS, "SaveStateToSlot", "{}/Guide & {}/Y".format(player1, player1))
            else:
                print("P1 Not Found")

        # Save configuration
        inis.saveFile()

    def generate(self, system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, args) -> Command:

        if not system.HasConfigFile:
            # Systems and other Configurations
            self.__Configure(system, args.rom, playersControllers, args)

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.pcsx2RootFolder
        env["QT_QPA_PLATFORM"] = "xcb"
        env["SDL_JOYSTICK_HIDAPI"] = "0"
        
        #finally for some cases it's better to regenerate also the SDL GAME CONTROLLER CONFIG(S) DYNAMICALLY
        #the file provided by default from pegasus-frontend could have missing values finally
        SDLGameDBLines = ""
        for index in playersControllers:
            controller = playersControllers[index]
            SDLGameDBLines = SDLGameDBLines + controller.generateSDLGameDBLine() + "\n"
        env["SDL_GAMECONTROLLERCONFIG"] = SDLGameDBLines
   
        commandArray = [recalboxFiles.recalboxBins[system.Emulator], args.rom]

        if system.HasArgs: commandArray.extend(system.Args)

        return Command(videomode=system.VideoMode, array=commandArray, env=env)
