#!/usr/bin/env python
from configgen.Command import Command
import configgen.recalboxFiles as recalboxFiles
from configgen.Emulator import Emulator
from configgen.controllers.controller import ControllerPerPlayer
from configgen.generators.Generator import Generator
from configgen.settings.keyValueSettings import keyValueSettings
import configgen.generators.dolphin_triforce.dolphinTriforceControllers as dolphinTriforceControllers
from configgen.utils.videoMode import GetHasVulkanSupport
#utils to manage advanced overlays features for all emulators
import configgen.utils.overlays as overlays

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

class DolphinTriforceGenerator(Generator):

    # Game Ratio
    GAME_RATIO = \
    {
       "auto": 2, # if auto we force in 4/3 to avoid issue of change of ratio during playing
       "16/9": 1, 
       "4/3": 2,
       "squarepixel": 2, # if auto we force in 4/3 to avoid issue of change of ratio during playing
    }

    # Gamecube languages
    #0 = English/Japanese, depending on the region of the console/game.
    #1 = German
    #2 = French
    #3 = Spanish
    #4 = Italian
    #5 = Dutch
    GAMECUBE_LANGUAGES = \
    {
        "en": 0,
        "de": 1,
        "fr": 2,
        "es": 3,
        "it": 4,
        "du": 5,
    }

    # Wii languages
    #0 = Japanese
    #1 = English
    #2 = German
    #3 = French
    #4 = Spanish
    #5 = Italian
    #6 = Dutch
    #7 = Simplified Chinese
    #8 = Traditional Chinese
    #9 = Korean
    WII_LANGUAGES = \
    {
        "jp": 0,
        "en": 1,
        "de": 2,
        "fr": 3,
        "es": 4,
        "it": 5,
        "du": 6,
        "zh": 7, # traditional chinese 8, ignored
        "kr": 9,
    }

    # Neplay servers
    NETPLAY_SERVERS = \
    {
        # Chine
        "CN": "CH",
        "TW": "CH",
        # Est Asia
        "KR": "EA",
        "RU": "EA",
        "JP": "EA",
        # Europe
        "GB": "EU",
        "DE": "EU",
        "FR": "EU",
        "ES": "EU",
        "IT": "EU",
        "PT": "EU",
        "TR": "EU",
        "SU": "EU",
        "NO": "EU",
        "NL": "EU",
        "PL": "EU",
        "HU": "EU",
        "CZ": "EU",
        "GR": "EU",
        "LU": "EU",
        "LV": "EU",
        "SE": "EU",
        # South America
        "BR": "SA",
        # North America
        "US": "NA",
    }
    ## Setting on dolphin.ini
    SECTION_ANALYTICS = "Analytics"
    SECTION_BTPASSTHROUGH = "BluetoothPassthrough"
    SECTION_CONTROLS = "Controls"
    SECTION_CORE = "Core"
    SECTION_GAMELIST = "GameList"
    SECTION_GENERAL = "General"
    SECTION_INTERFACE = "Interface"
    SECTION_NETPLAY = "NetPlay"
    SECTION_DISPLAY = "Display"
    SECTION_DSP = "DSP"
    ## Setting on GFX.ini
    SECTION_SETTINGS = "Settings"
    SECTION_HARDWARE = "Hardware"
    # Setting on Logger.ini
    SECTION_LOGS = "Logs"

    def mainConfiguration(self, system: Emulator, recalboxOptions: keyValueSettings, args):

        # read recalbox.conf for advanced configuration
        language =  recalboxOptions.getString("system.language", "en")[0:2].lower()
        systemLanguage = recalboxOptions.getString("system.language", "US")[2:2].upper()
        nickname = recalboxOptions.getString("global.netplay.nickname", "pixL")

        # Set video renderer OpenGl / vulkan
        videoDriver = "Vulkan" if GetHasVulkanSupport() == '1' else "OGL"

        # set languageCode variable
        Log("language:{}".format(language));
        if language in self.GAMECUBE_LANGUAGES:
            languageCode = language
        else:
            languageCode ="en" # english
        Log("languageCode:{}".format(languageCode));

        # set systemLanguage variable
        Log("systemLanguage:{}".format(systemLanguage));
        gamecubeLanguage = self.GAMECUBE_LANGUAGES[languageCode] if languageCode in self.GAMECUBE_LANGUAGES else 0 #en

        # set gamecubeLanguage variable
        Log("gamecubeLanguage:{}".format(gamecubeLanguage));
        wiiLanguage = self.WII_LANGUAGES[languageCode] if languageCode in self.WII_LANGUAGES else 1 #en
        Log("wiiLanguage:{}".format(wiiLanguage));

        # Get Netplay configs
        lobbyServer = self.NETPLAY_SERVERS[systemLanguage] if systemLanguage in self.NETPLAY_SERVERS else None
        
        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        dolphinTriforceSettings = IniSettings(recalboxFiles.dolphinIni, True)
        dolphinTriforceSettings.loadFile(True) \
                       .defineBool('True', 'False')

        # Resolution
        from configgen.utils.resolutions import ResolutionParser
        resolution = ResolutionParser(system.VideoMode)
        if resolution.isSet and resolution.selfProcess:
            dolphinTriforceSettings.setString(self.SECTION_DISPLAY, "FullscreenResolution", resolution.string)
        
        #finally better to deactivate full screen else the windows bar is visible in some cases :-(
        dolphinTriforceSettings.setBool(self.SECTION_DISPLAY, "Fullscreen", False)
        # Logging
        dolphinTriforceSettings.setBool(self.SECTION_DSP, "CaptureLog", args.verbose)
        # Force audio backend on Pulse Audio
        dolphinTriforceSettings.setString(self.SECTION_DSP, "Backend", "Pulse")
        # Analytics
        dolphinTriforceSettings.setBool(self.SECTION_ANALYTICS, "Enabled", True)
        dolphinTriforceSettings.setBool(self.SECTION_ANALYTICS, "PermissionAsked", True)
        # BluetoothPasstrough
        dolphinTriforceSettings.setBool(self.SECTION_BTPASSTHROUGH, "Enabled", False)
        # Core
        dolphinTriforceSettings.setInt(self.SECTION_CORE, "SelectedLanguage", gamecubeLanguage) #Gamecube 


        dolphinTriforceSettings.setBool(self.SECTION_CORE, "WiimoteContinuousScanning", False)
        dolphinTriforceSettings.setBool(self.SECTION_CORE, "WiiKeyboard", False)

        dolphinTriforceSettings.setBool(self.SECTION_CORE, "SkipIpl", True)

        dolphinTriforceSettings.setInt(self.SECTION_CORE, "SIDevice0", 11)
        dolphinTriforceSettings.setInt(self.SECTION_CORE, "SIDevice1", 6)
        dolphinTriforceSettings.setInt(self.SECTION_CORE, "SIDevice2", 6)
        dolphinTriforceSettings.setInt(self.SECTION_CORE, "SIDevice3", 6)
        dolphinTriforceSettings.setInt(self.SECTION_CORE, "SlotA", 255)
        dolphinTriforceSettings.setInt(self.SECTION_CORE, "SlotB", 255)
        dolphinTriforceSettings.setInt(self.SECTION_CORE, "SerialPort1", 6)
        # Set video renderer OpenGl / vulkan
        dolphinTriforceSettings.setInt(self.SECTION_CORE, "GFXBackend", videoDriver)
        # Enable always cheats because could be needed to start/run some triforce games
        dolphinTriforceSettings.setBool(self.SECTION_CORE, "EnableCheats", True)
        # GameList
        dolphinTriforceSettings.setBool(self.SECTION_GAMELIST, "ColumnID", True)
        # General
        dolphinTriforceSettings.setInt(self.SECTION_GENERAL, "ISOPaths", 1)
        dolphinTriforceSettings.setString(self.SECTION_GENERAL, "ISOPath0", "/recalbox/share/roms/triforce")
        dolphinTriforceSettings.setBool(self.SECTION_GENERAL, "RecursiveISOPaths", True)
        # Interface
        dolphinTriforceSettings.setString(self.SECTION_INTERFACE, "LanguageCode", languageCode)
        dolphinTriforceSettings.setBool(self.SECTION_INTERFACE, "AutoHideCursor", True)
        dolphinTriforceSettings.setBool(self.SECTION_INTERFACE, "ConfirmStop", False)
        dolphinTriforceSettings.setInt(self.SECTION_INTERFACE, "Language", wiiLanguage)
        dolphinTriforceSettings.setInt(self.SECTION_INTERFACE, "PauseOnFocusLost", True)
        dolphinTriforceSettings.setInt(self.SECTION_INTERFACE, "UsePanicHandlers", False)

        # Netplay
        dolphinTriforceSettings.setString(self.SECTION_NETPLAY, "Nickname", nickname)
        dolphinTriforceSettings.setBool(self.SECTION_NETPLAY, "UseUPNP", True)
        dolphinTriforceSettings.setString(self.SECTION_NETPLAY, "IndexName", nickname)
        dolphinTriforceSettings.setString(self.SECTION_NETPLAY, "TraversalChoice", "traversal")
        dolphinTriforceSettings.setBool(self.SECTION_NETPLAY, "UseIndex", True)
        dolphinTriforceSettings.setString(self.SECTION_NETPLAY, "IndexRegion", lobbyServer)

        # Save configuration
        dolphinTriforceSettings.saveFile()

        # Setting on Logger.ini
        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        LogSettings = IniSettings(recalboxFiles.dolphinLoggerIni, True)
        LogSettings.loadFile(True)

        # Prevent the constant log spam.
        LogSettings.setString(self.SECTION_LOGS, "DVD", "False")

        # Save Logger.ini
        LogSettings.saveFile()
        
    def gfxConfiguration(self, system: Emulator, recalboxOptions: keyValueSettings):
        if (recalboxOptions.getBool("dolphin-triforce.widescreenhack", False)):
            # Get Ratio / Forced to 16/9 for wide screen hack
            gameRatio = self.GAME_RATIO["16/9"]
        else:
            # Get Ratio from system configuration
            gameRatio = self.GAME_RATIO[system.Ratio] if system.Ratio in self.GAME_RATIO else 2 # "4/3" is the default mode in arcade and to avoid issue
        
        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        gfxSettings = IniSettings(recalboxFiles.dolphinGFX, True)
        gfxSettings.loadFile(True)

        resolution = recalboxOptions.getString("dolphin-triforce.resolution", "1") # Native by default
        vsync = recalboxOptions.getBool("dolphin-triforce.vsync", False)
        antialiasing = recalboxOptions.getString("dolphin-triforce.antialiasing", "0x00000001")  # None by default
        wideScreenHack = recalboxOptions.getBool("dolphin-triforce.widescreenhack", False)
        
        # Hardware
        gfxSettings.setBool(self.SECTION_HARDWARE, "VSync", vsync)
        # Settings
        gfxSettings.setBool(self.SECTION_SETTINGS, "ShowFPS", system.ShowFPS)
        gfxSettings.setInt(self.SECTION_SETTINGS, "AspectRatio", gameRatio)
        gfxSettings.setBool(self.SECTION_SETTINGS, "HiresTextures", True)
        gfxSettings.setBool(self.SECTION_SETTINGS, "CacheHiresTextures", True)
        gfxSettings.setInt(self.SECTION_SETTINGS, "InternalResolution", resolution)
        gfxSettings.setInt(self.SECTION_SETTINGS, "MSAA", antialiasing)
        gfxSettings.setBool(self.SECTION_SETTINGS, "wideScreenHack", wideScreenHack)

        # Save configuration
        gfxSettings.saveFile()

    def generate(self, system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, args) -> Command:
        if not system.HasConfigFile:

            #read gameID from iso file
            Log("from {}".format(args.rom))
            file = open(args.rom, "rb")
            gameID = file.read(6).decode('utf-8')
            Log("gameID : {}".format(gameID))
            file.close()

            # Controllers
            dolphinTriforceControllers.generateControllerConfig(system, playersControllers, recalboxOptions, gameID)
            #main configuration
            self.mainConfiguration(system, recalboxOptions, args)
            #graphics configuration
            self.gfxConfiguration(system, recalboxOptions)

        commandArray = [recalboxFiles.recalboxBins[system.Emulator], "-e", args.rom]

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.CONF
        env["XDG_DATA_HOME"] = recalboxFiles.CONF
        env["XDG_CACHE_HOME"] = recalboxFiles.CACHE

        if system.HasArgs: commandArray.extend(system.Args)
        
        # To manage Vulkan/OpenGL overlay activated for monitoring or bezel display
        # Remark: the aspect ratio for the triforce games is not a proper 4:3 .. it's more of an 8:7 
        # like old school Nintendo consoles (nes and snes) ... 
        # and there is a lot of gamecube games that have this aspect ratio .. super smash melee is a great example
        if not (recalboxOptions.getBool("dolphin-triforce.widescreenhack", False)):
            overlays.processOverlays(system, args.rom, recalboxOptions, env, commandArray)

        return Command(videomode=system.VideoMode, array=commandArray, env=env)
