#!/usr/bin/env python
from configgen.Command import Command
import configgen.recalboxFiles as recalboxFiles
from configgen.generators.Generator import Generator
import shutil
import os.path
from configgen.controllers.controller import ControllerPerPlayer
from shutil import copyfile
from . import xemuConfig

class XemuGenerator(Generator):

    # Game Ratio
    GAME_RATIO = \
    {
       "auto": "'scale'",
       "16/9": "'scale_16_9'", 
       "4/3": "'scale_4_3'",
       "squarepixel": "'stretch'",
    }

    # To select eeprom by language
    XBOX_LANGUAGES = \
    {
        "jp": "japan",
        "en": "english",
        "de": "german",
        "fr": "french",
        "es": "spanish",
        "it": "italian",
        "pt": "portuguese",
    }

    
    ## Setting on xemu.ini
    SECTION_GENERAL = "general"
    SECTION_SYS = "sys"
    SECTION_SYS_FILES = "sys.files"
    SECTION_AUDIO = "audio"
    SECTION_DISPLAY_UI = "display.ui"
    SECTION_DISPLAY_QUALITY = "display.quality"
    SECTION_DISPLAY_WINDOW = "display.window"
    SECTION_INPUT_BINDINGS = "input.bindings"
    SECTION_NET = "net"
    SECTION_PERF = "perf"
    SECTION_MISC = "network"

    def generate(self, system, playersControllers: ControllerPerPlayer, recalboxSettings, args):
        xemuConfig.createXemuConfig(self, system, args, playersControllers)

        if system.Name == "chihiro":
            # Same for Chihiro arcade system
            if not os.path.exists("/recalbox/share/saves/chihiro/xbox_hdd.qcow2"):
                if not os.path.exists("/recalbox/share/saves/chihiro/"):
                    os.makedirs("/recalbox/share/saves/chihiro")
                copyfile("/usr/share/xemu/data/xbox_hdd.qcow2", "/recalbox/share/saves/chihiro/xbox_hdd.qcow2")
        else:
            # copy the hdd if it doesn't exist
            if not os.path.exists("/recalbox/share/saves/xbox/xbox_hdd.qcow2"):
                if not os.path.exists("/recalbox/share/saves/xbox"):
                    os.makedirs("/recalbox/share/saves/xbox")
                copyfile("/usr/share/xemu/data/xbox_hdd.qcow2", "/recalbox/share/saves/xbox/xbox_hdd.qcow2")

        commandArray = [recalboxFiles.recalboxBins[system.Emulator]]
        #"-mem-path", "/tmp", "-mem-prealloc"
        commandArray.extend(["-config_path", recalboxFiles.xemuIni, "-dvd_path", args.rom, "-mem-path", "/tmp", "-mem-prealloc"])

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.HOME
        env["XDG_DATA_HOME"] = recalboxFiles.HOME
        env["XDG_CACHE_HOME"] = recalboxFiles.CONF

        return Command(videomode=system.VideoMode, array=commandArray, env=env)
