from typing import List, Dict

from configgen import recalboxFiles
from configgen.Emulator import Emulator
from configgen.controllers.controller import ControllerPerPlayer
from configgen.controllers.inputItem import InputItem
from configgen.generators.Generator import Generator
from configgen.Command import Command
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.utils.videoMode import GetHasVulkanSupport
import shutil
import os

def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(txt)
    return 0

class DuckstationGenerator(Generator):

    SECTION_MAIN = "Main"
    SECTION_ACHIEVEMENTS = "Cheevos"
    SECTION_DISPLAY = "Display"
    SECTION_FOLDERS = "Folders"
    SECTION_BIOS = "BIOS"
    SECTION_UI = "UI"
    SECTION_HOTKEYS = "Hotkeys"
    SECTION_GAMELIST = "GameList"
    SECTION_INPUT_SOURCES = "InputSources"
    SECTION_LOGGING = "Logging"
    SECTION_GPU = "GPU"


    def generate(self, system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, args) -> Command:

        # Make save dirs
        memcards: str = os.path.join(recalboxFiles.SAVES, "psx/duckstation/memcards")
        savestates: str = os.path.join(recalboxFiles.SAVES, "psx/duckstation/savestates")
        gamesettings: str = os.path.join(recalboxFiles.SAVES, "psx/duckstation/gamesettings")
        textures: str = os.path.join(recalboxFiles.SAVES, "psx/duckstation/textures")
        configFolder: str = os.path.join(recalboxFiles.duckstationRootFolder, "duckstation")
        if not os.path.exists(memcards): os.makedirs(name=memcards, exist_ok=True)
        if not os.path.exists(savestates): os.makedirs(name=savestates, exist_ok=True)
        if not os.path.exists(gamesettings): os.makedirs(name=gamesettings, exist_ok=True)
        if not os.path.exists(textures): os.makedirs(name=textures, exist_ok=True)
        if not os.path.exists(configFolder): os.makedirs(name=configFolder, exist_ok=True)

        # Config file
        from configgen.settings.iniSettings import IniSettings
        # test if file is not deleted for create a default basic conf stocked in share_init
        if not os.path.isfile(recalboxFiles.duckstationConfigIni):
            shutil.copyfile(recalboxFiles.duckstationDefaultConfigIni, recalboxFiles.duckstationConfigIni)

        configFile = IniSettings(recalboxFiles.duckstationConfigIni, True)
        configFile.defineBool("true", "false") \
                  .loadFile(True)

        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)

        ## Enable retroarchievements
        language =  conf.getString("system.language", "en")[0:2].lower()
        resolution = conf.getString("duckstation.resolution", "1") # Native resolution # !!!!!!! need to add pegasus option !!!!!!! 
        cheats = conf.getBool("duckstation.cheats", "0") # !!!!!!! need to add pegasus option !!!!!!! 
        vsync = conf.getBool("duckstation.vsync", "0") # !!!!!!! need to add pegasus option !!!!!!! 
        videoDriver = "Vulkan" if GetHasVulkanSupport() == '1' else "OpenGl" # OpenGl/Vulkan
        retroachievements = conf.getBool("global.retroachievements", "0")
        soundEffects = conf.getBool("global.retroachievements.unlock.sound", "0")
        hardcoreMode = conf.getBool("global.retroachievements.hardcore", "0")
        cheevosUsername = conf.getString("global.retroachievements.username", "pixl")
        cheevosToken = conf.getString("global.retroachievements.token", "")

        # Cheevos
        configFile.setBool(DuckstationGenerator.SECTION_ACHIEVEMENTS, "Enabled", retroachievements)
        configFile.setBool(DuckstationGenerator.SECTION_ACHIEVEMENTS, "SoundEffects", soundEffects)
        configFile.setBool(DuckstationGenerator.SECTION_ACHIEVEMENTS, "ChallengeMode", hardcoreMode)
        configFile.setString(DuckstationGenerator.SECTION_ACHIEVEMENTS, "Username", cheevosUsername)
        configFile.setString(DuckstationGenerator.SECTION_ACHIEVEMENTS, "Token", cheevosToken)
        
        # Main
        configFile.setBool(DuckstationGenerator.SECTION_MAIN, "StartFullscreen", True)
        configFile.setBool(DuckstationGenerator.SECTION_MAIN, "ConfirmPowerOff", False)
        configFile.setBool(DuckstationGenerator.SECTION_MAIN, "SaveStateOnExit", False)
        configFile.setBool(DuckstationGenerator.SECTION_MAIN, "DisableWindowResize", False)
        configFile.setBool(DuckstationGenerator.SECTION_MAIN, "RenderToSeparateWindow", False)
        configFile.setBool(DuckstationGenerator.SECTION_MAIN, "AutoLoadCheats", cheats) # !!!!!!! need to add pegasus option !!!!!!!
        configFile.setString(DuckstationGenerator.SECTION_MAIN, "Language", language)

        # UI
        configFile.setBool(DuckstationGenerator.SECTION_UI, "ShowStatusBar", False)

        # Display
        configFile.setBool(DuckstationGenerator.SECTION_DISPLAY, "VSync", vsync) # !!!!!!! need to add pegasus option !!!!!!! 
        configFile.setBool(DuckstationGenerator.SECTION_DISPLAY, "IntegerScaling", system.IntegerScale)
        configFile.setBool(DuckstationGenerator.SECTION_DISPLAY, "LinearFiltering", system.Smooth)
        configFile.setBool(DuckstationGenerator.SECTION_DISPLAY, "ShowFPS", system.ShowFPS)
        configFile.setBool(DuckstationGenerator.SECTION_DISPLAY, "ShowOSDMessages", True)

        # Bios
        configFile.setString(DuckstationGenerator.SECTION_BIOS, "SearchDirectory", "/recalbox/share/bios")

        # Gamelist
        configFile.setString(DuckstationGenerator.SECTION_GAMELIST,"RecursivePaths", "/recalbox/share/roms/psx")

        # Others Path configuration
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "Cache", "/recalbox/share/saves/psx/duckstation/cache")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "Cheats", "/recalbox/share/cheats/psx/duckstation/cheats")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "MemoryCards", "/recalbox/share/saves/psx/duckstation/memcards")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "SaveStates", "/recalbox/share/saves/psx/duckstation/savestates")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "Covers", "/recalbox/share/roms/psx/media/box2dfront")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "Screenshots", "/recalbox/share/screenshots")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "GameSettings", "/recalbox/share/saves/psx/duckstation/gamesettings")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "Dumps", "/recalbox/share/roms/psx/dump")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "Shaders", "/recalbox/share/shaders/psx/duckstation/shaders")
        configFile.setString(DuckstationGenerator.SECTION_FOLDERS, "Textures", "/recalbox/share/saves/psx/duckstation/textures")

        # input sources
        configFile.setString(DuckstationGenerator.SECTION_INPUT_SOURCES, "SDL", True)
        configFile.setString(DuckstationGenerator.SECTION_INPUT_SOURCES, "SDLControllerEnhancedMode", True)

        configFile.setString(DuckstationGenerator.SECTION_GPU, "Renderer", videoDriver)
        configFile.setInt(DuckstationGenerator.SECTION_GPU, "ResolutionScale", resolution) # !!!!!!! need to add pegasus option !!!!!!! 

        # Log level
        configFile.setString(DuckstationGenerator.SECTION_LOGGING, "LogLevel", "Info" if args.verbose else "Info Error")
        configFile.setString(DuckstationGenerator.SECTION_LOGGING, "LogToFile ", True)

        # Cleaning existing controllers before mapping newer
        existingPadX= ["Pad1", "Pad2", "Pad3", "Pad4", "Pad5", "Pad6", "Pad7", "Pad8" "Hotkeys"]
        for padX in existingPadX:
            configFile.clearRegion(padX)

        # Controllers
        for index in playersControllers:
            controller = playersControllers[index]
            padIndex: str = "Pad{}".format(controller.PlayerIndex)
            sdlIndex = str = "SDL-{}".format(controller.SdlIndex)

            # Digital or analog?
            configFile.setString(padIndex, "Type", "AnalogController" if controller.HasJoy1Left else "DigitalController")
            configFile.setBool(padIndex, "AnalogDPadInDigitalMode", controller.HasJoy1Left)
            configFile.setString(padIndex, "SmallMotor" ,"{}/SmallMotor".format(sdlIndex))
            configFile.setString(padIndex, "LargeMotor" ,"{}/LargeMotor".format(sdlIndex))
            # Digital part
            # if controller.HasHotkey   : configFile.setString(padIndex, "Analog"  , "{}/Guide".format(sdlIndex))
            if controller.HasUp       : configFile.setString(padIndex, "Up"      , "{}/DPadUp".format(sdlIndex))
            if controller.HasRight    : configFile.setString(padIndex, "Right"   , "{}/DPadRight".format(sdlIndex))
            if controller.HasDown     : configFile.setString(padIndex, "Down"    , "{}/DPadDown".format(sdlIndex))
            if controller.HasLeft     : configFile.setString(padIndex, "Left"    , "{}/DPadLeft".format(sdlIndex))
            if controller.HasSelect   : configFile.setString(padIndex, "Select"  , "{}/Back".format(sdlIndex))
            if controller.HasStart    : configFile.setString(padIndex, "Start"   , "{}/Start".format(sdlIndex))
            if controller.HasB        : configFile.setString(padIndex, "Cross"   , "{}/B".format(sdlIndex))
            if controller.HasA        : configFile.setString(padIndex, "Circle"  , "{}/A".format(sdlIndex))
            if controller.HasX        : configFile.setString(padIndex, "Triangle", "{}/X".format(sdlIndex))
            if controller.HasY        : configFile.setString(padIndex, "Square"  , "{}/Y".format(sdlIndex))
            if controller.HasL1       : configFile.setString(padIndex, "L1"      , "{}/LeftShoulder".format(sdlIndex))
            if controller.HasR1       : configFile.setString(padIndex, "R1"      , "{}/RightShoulder".format(sdlIndex))
            if controller.HasL2       : configFile.setString(padIndex, "L2"      , "{}/+LeftTrigger".format(sdlIndex))
            if controller.HasR2       : configFile.setString(padIndex, "R2"      , "{}/+RightTrigger".format(sdlIndex))
            if controller.HasJoy1Left : configFile.setString(padIndex, "LLeft"   , "{}/-LeftX".format(sdlIndex))
            if controller.HasJoy1Up   : configFile.setString(padIndex, "LUp"     , "{}/-LeftY".format(sdlIndex))
            if controller.HasJoy1Up   : configFile.setString(padIndex, "LDown"   , "{}/+LeftY".format(sdlIndex))
            if controller.HasJoy1Up   : configFile.setString(padIndex, "LRight"  , "{}/+LeftX".format(sdlIndex))
            if controller.HasJoy2Left : configFile.setString(padIndex, "RLeft"   , "{}/-RightX".format(sdlIndex))
            if controller.HasJoy2Up   : configFile.setString(padIndex, "RUp"     , "{}/-RightY".format(sdlIndex))
            if controller.HasJoy2Left : configFile.setString(padIndex, "RDown"   , "{}/+RightY".format(sdlIndex))
            if controller.HasJoy2Up   : configFile.setString(padIndex, "RRight"  , "{}/+RightX".format(sdlIndex))

            # Hotkey for player one?
            if controller.PlayerIndex == 1:
                print("P1 found")
                player1 = "SDL-{}".format(controller.SdlIndex)
                Log("player1 SDL Index: {}".format(player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "FastForward", "{}/Guide & {}/DPadRight".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "LoadSelectedSaveState", "{}/Guide & {}/X".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "SaveSelectedSaveState", "{}/Guide & {}/Y".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "OpenPauseMenu", "{}/Guide & {}/B".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "Screenshot", "{}/Guide & {}/LeftShoulder".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "SelectNextSaveStateSlot", "{}/Guide & {}/DPadDown".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "SelectPreviousSaveStateSlot", "{}/Guide & {}/DPadUp".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "ToggleFastForward", "{}/Guide & {}/DPadRight".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "PowerOff", "{}/Guide & {}/Start".format(player1, player1))
                configFile.setString(DuckstationGenerator.SECTION_HOTKEYS, "OpenAchievements", "{}/Guide & {}/RightShoulder".format(player1, player1))

        # Save config
        configFile.saveFile()

        # Command line arguments
        commandArray: List[str] = [recalboxFiles.recalboxBins[system.Emulator]]

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.duckstationRootFolder
        env["QT_QPA_PLATFORM"] = "xcb"
        env["SDL_JOYSTICK_HIDAPI"] = "0"

        # Add extra arguments
        if system.HasArgs: commandArray.extend(system.Args)

        # finally add rom
        commandArray.extend([args.rom])

        return Command(videomode=system.VideoMode, array=commandArray, env=env)
