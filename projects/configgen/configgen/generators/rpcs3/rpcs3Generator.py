#!/usr/bin/env python

from configgen.generators.Generator import Generator
from configgen.emulatorlauncher import recalboxFiles
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.Emulator import Emulator
from configgen.Command import Command
from configgen.utils.videoMode import GetHasVulkanSupport
import shutil
import os
import subprocess
from os import path
import configparser
import yaml
import json
import re
import configgen.generators.rpcs3.rpcs3Controllers as rpcs3Controllers

def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(txt)
    return 0

class Rpcs3Generator(Generator):

    @staticmethod
    def getGameId(args):
        if args.rom.lower().endswith(".psn"):
            for file in os.listdir("{}/".format(args.rom)):
                if file.endswith(".pkg"):
                    pkgFiles = os.path.join(args.rom + "/", file)

            commandHexdump = "hexdump -n 9 -Cs 55 '{}' | grep 'NP[AEHJKUIX][A-Z][00-99][001-999]' | awk -F'|' '{{ print $2 }}' | tr -d ' \n'".format(str(pkgFiles))
            processHexdump = subprocess.run(commandHexdump, text=True, capture_output=True, shell=True, stderr=None)
            gameId = processHexdump.stdout
        if args.rom.lower().endswith(".ps3"):
            paramPath= "{}/PS3_GAME/PARAM.SFO".format(args.rom)
            commandGrep = "grep 'B[CL][ACEHJKU][BCDMSTVXZ][00-99][001-999]' '{}' | tr -d ' \n'".format(str(paramPath))
            processGrep = subprocess.run(commandGrep, text=True, capture_output=True, shell=True, stderr=None)
            gameId = processGrep.stdout
        if args.rom.lower().endswith(".iso"):
            isoGamesPath = "{}/ps3/games".format(recalboxFiles.ROMS)
            paramPath= "{}/PS3_GAME/PARAM.SFO".format(isoGamesPath)
            commandGrep = "grep 'B[CL][ACEHJKU][BCDMSTVXZ][00-99][001-999]' '{}' | tr -d ' \n'".format(str(paramPath))
            processGrep = subprocess.run(commandGrep, text=True, capture_output=True, shell=True, stderr=None)
            gameId = processGrep.stdout
        if args.rom.lower().endswith(".sys357"):
            # identify game launched on SYS357 all game have GAMEID = SCEEXE000
            paramPath= "{}/dev_hdd0/game/SCEEXE000/PARAM.SFO".format(args.rom)
            commandGrep = "grep 'SCEEXE000' '{}' | tr -d ' \n'".format(str(paramPath))
            processGrep = subprocess.run(commandGrep, text=True, capture_output=True, shell=True, stderr=None)
            gameId = processGrep.stdout
        if args.rom.lower().endswith(".self"):
            # Force GameId for xmb menu
            gameId = "vsh.self"
        return gameId

    @staticmethod
    def mountUnmountIso(args):
        if args.rom.lower().endswith(".iso"):
            # For iso mount on default games path in rpcs3 configs
            isoGamesPath =  "{}/ps3/games".format(recalboxFiles.ROMS)
            # the unmount is already configured in evmapy on close emulator 
            # but for security we need to check before new mount iso
            unmnt = "fusermount -uz "
            cmdUnMount = os.system(unmnt + '"' + isoGamesPath + '"')
            mnt = "mount -t iso9660 -o loop "
            cmdMount = os.system(mnt + '"' + args.rom + '"' + " " + '"' + isoGamesPath + '"')
            if os.path.exists(os.path.dirname("{}/PS3_GAME".format(isoGamesPath))):
                Log("Remove existing iso mounted on: " + isoGamesPath)
                cmdUnMount
                Log("Mount New Iso: " + args.rom)
                cmdMount
            else:
                Log("Mount Iso: " + args.rom)
                cmdMount

    @staticmethod
    def getNamcoShortName(args):
        if args.rom.lower().endswith(".sys357"):
            # List of Actual launch game on Namco Sys357
            paramPath= "{}/dev_hdd0/game/SCEEXE000/PARAM.SFO".format(args.rom)
            listNameGame = "Deadstorm Pirates Special Edition|DZB3|RazingStorm|Sailor zombie|TEKKEN TAG TOURNAMENT 2|TEKKEN6|TEKKEN6BR|DarkEscape|Taiko no Tatsujin"
            commandHexdump = "grep -E '{}' '{}' | head -1 | tr -d ' \n'".format(str(listNameGame), str(paramPath))
            processHexdump = subprocess.run(commandHexdump, text=True, capture_output=True, shell=True, stderr=None)
            gameName = processHexdump.stdout
        return gameName

    @staticmethod
    def getUsbDongleData(args, file_path, search_string):
        # Adding Usb dongle for decrypt game launch on racine game folder like 
        # /recalbox/share/roms/sys357/Razing Storm.sys357/usb000.key with value of 
        # serial=xxxxxxxxxxxx
        # pid=xxxx
        # vid=xxxx
        if args.rom.lower().endswith(".sys357"):
            with open(file_path, 'r') as file:
                for line in file:
                    if search_string in line:
                        option, value = line.split("=", 1)
                        option = option.strip()
                        value = value.strip()
                return value

    def getFirmwareVersion():
        try:
            with open(recalboxFiles.rpcs3Saves + "/rpcs3/dev_flash/vsh/etc/version.txt", "r") as stream:
                lines = stream.readlines()
                for line in lines:
                    matches = re.match("^release:(.*):", line)
                    if matches:
                        return matches[1]
        except:
            return None
        return None
    
    def getPkgNeedInstall(args):
        try:
            if args.rom.lower().endswith(".psn"):
                if not os.path.exists("{}/rpcs3/dev_hdd0/game/{}".format(recalboxFiles.rpcs3Saves, Rpcs3Generator.getGameId(args))):
                    return True
        except:
            return None
        return None

    @staticmethod
    def getCustomConfigPerGame(args):
        # Improve launch game with custom config per game 
        Log(gameCustomConfigs)
        try:
                print("launch with a custom configurations: config_" + Rpcs3Generator.getGameId(args) + ".yml")
                return True
        except:
            return None
        return None

    # Game Ratio
    GAME_RATIO = \
    {
       "None": "16:9",
       "auto": "16:9",
       "16/9": "16:9", 
       "4/3": "4:3",
       "squarepixel": "16:9",
    }

    # https://github.com/RPCS3/rpcs3/blob/master/rpcs3/Emu/Cell/Modules/cellSysutil.h#L213
    PS3_LICENSE_AREA = \
    {
        "en": "SCEA", # America
        "de": "SCEE", # Europe
        "fr": "SCEE", # Europe
        "es": "SCEE", # Europe
        "it": "SCEE", # Europe
        "du": "SCEE", # Europe
        "ch": "SCH",  # China
        "kr": "SCEK", # Korean
        "jp": "SCEJ", # Japan
    }

    # https://github.com/RPCS3/rpcs3/blob/master/rpcs3/rpcs3qt/emu_settings.cpp#L1205
    PS3_LANGUAGE = \
    {
        "jp": "Japanese",
        "en": "English (US)",
        "fr": "French",
        "es": "Spanish",
        "de": "German",
        "it": "Italian",
        "du": "Dutch",
        "pt": "Portuguese (Portugal)",
        "ru": "Russian",
        "kr": "Korean",
        "ch": "Chinese (Simplified)",
        "sw": "Swedish",
        "da": "Danish",
        "nv": "Norwegian",
        "br": "Portuguese (Brazil)",
        "tr": "Turkish",
    }

    # https://github.com/RPCS3/rpcs3/blob/master/rpcs3/rpcs3qt/emu_settings.cpp#L1231
    PS3_KEYBOARD = \
    {
        "US": "English keyboard (US standard)",
        "JP": "Japanese keyboard",
        "GE": "German keyboard",
        "ES": "Spanish keyboard",
        "FR": "French keyboard",
        "IT": "Italian keyboard",
        "DU": "Dutch keyboard",
        "RU": "Russian keyboard",
        "UK": "English keyboard (UK standard)",
        "ZH": "French keyboard (Switzerland)",
        "CA": "French keyboard (Canada)",
        "BE": "French keyboard (Belgium)",
    }

    def generate(self, system: Emulator, playersControllers, guns, args):

        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)

        nickname: str = conf.getString("global.netplay.nickname", "pixL")
        systemLanguage: str = conf.getString("system.language", "US")[0:2].upper()
        language: str = conf.getString("system.language", "en")[0:2].lower()
        # Set video renderer OpenGl / vulkan / Null
        videoDriver: str = "Vulkan" if GetHasVulkanSupport() == '1' else "OpenGL"
        gameRatio: str = self.GAME_RATIO[system.Ratio] if system.Ratio in self.GAME_RATIO else "16:9"

        # define controller configuration
        rpcs3Controllers.generateControllerConfig(system, playersControllers, args.rom)

        # set keyboard layout variable
        if systemLanguage in self.PS3_KEYBOARD:
            languageArea = systemLanguage
        else:
            languageArea = "en" # America

        ps3Keyboard = self.PS3_KEYBOARD[languageArea] if systemLanguage in self.PS3_KEYBOARD else "English keyboard (US standard)"

        # set license area variable
        if language in self.PS3_LICENSE_AREA:
            languageArea = language
        else:
            languageArea = "en" # America

        ps3LicenseArea = self.PS3_LICENSE_AREA[languageArea] if languageArea in self.PS3_LICENSE_AREA else "SCEA" # America

        # set Language variable
        if language in self.PS3_LANGUAGE:
            languageCode = language
        else:
            languageCode = "en" # America

        ps3Language = self.PS3_LANGUAGE[languageCode] if languageCode in self.PS3_LANGUAGE else "English (US)" # America

        # Force creating hdd0, hdd1, dev_flash, dev_flash2 for games caches in saves directory
        rpcs3ConfigPath: str = recalboxFiles.rpcs3Config
        rpcs3CurrentConfigPath: str = recalboxFiles.rpcs3CurrentConfig
        rpcs3customConfigsPath: str = os.path.join(recalboxFiles.rpcs3Config, "custom_configs")
        hdd0Path: str = os.path.join(recalboxFiles.rpcs3Saves, "rpcs3/dev_hdd0")
        hdd1Path: str = os.path.join(recalboxFiles.rpcs3Saves, "rpcs3/dev_hdd1")
        flashPath: str = os.path.join(recalboxFiles.rpcs3Saves, "rpcs3/dev_flash")
        flash2Path: str = os.path.join(recalboxFiles.rpcs3Saves, "rpcs3/dev_flash2")
        bdvdPath: str = os.path.join(recalboxFiles.rpcs3Saves, "rpcs3/dev_bdvd")
        gamesPath: str = os.path.join(recalboxFiles.ROMS, "ps3/games")
        if not os.path.exists(rpcs3ConfigPath): os.makedirs(name=rpcs3ConfigPath, exist_ok=True)
        if not os.path.exists(rpcs3CurrentConfigPath): os.makedirs(os.path.dirname(rpcs3CurrentConfigPath))
        if not os.path.exists(rpcs3customConfigsPath): os.makedirs(name=rpcs3customConfigsPath, exist_ok=True)
        if not os.path.exists(hdd0Path): os.makedirs(name=hdd0Path, exist_ok=True)
        if not os.path.exists(hdd1Path): os.makedirs(name=hdd1Path, exist_ok=True)
        if not os.path.exists(flashPath): os.makedirs(name=flashPath, exist_ok=True)
        if not os.path.exists(flash2Path): os.makedirs(name=flash2Path, exist_ok=True)
        if not os.path.exists(bdvdPath): os.makedirs(name=bdvdPath, exist_ok=True)
        if not os.path.exists(gamesPath): os.makedirs(name=gamesPath, exist_ok=True)

        # Generates CurrentSettings.ini with values to disable prompts on first run
        rpcsCurrentSettings = configparser.ConfigParser(interpolation=None)
        # To prevent ConfigParser from converting to lower case
        rpcsCurrentSettings.optionxform = str

        # Sets Gui Settings to close completely and disables some popups
        if not rpcsCurrentSettings.has_section("main_window"):
            rpcsCurrentSettings.add_section("main_window")

        rpcsCurrentSettings.set("main_window", "confirmationBoxExitGame", "false")
        rpcsCurrentSettings.set("main_window", "confirmationObsoleteCfg", "false")
        rpcsCurrentSettings.set("main_window", "infoBoxEnabledInstallPKG", "false")
        rpcsCurrentSettings.set("main_window", "infoBoxEnabledInstallPUP", "false")
        rpcsCurrentSettings.set("main_window", "infoBoxEnabledWelcome", "false")

        if not rpcsCurrentSettings.has_section("Meta"):
            rpcsCurrentSettings.add_section("Meta")

        rpcsCurrentSettings.set("Meta", "checkUpdateStart", "false")
        rpcsCurrentSettings.set("Meta", "currentStylesheet", "Darker Style by TheMitoSan")
        rpcsCurrentSettings.set("Meta", "useRichPresence", "false")
        
        if not rpcsCurrentSettings.has_section("Localization"):
            rpcsCurrentSettings.add_section("Localization")

        # Menu language
        rpcsCurrentSettings.set("Localization", "language", language)

        with open(recalboxFiles.rpcs3CurrentConfig, "w") as configfile:
            rpcsCurrentSettings.write(configfile)

        # if not os.path.exists(os.path.dirname(recalboxFiles.rpcs3Config)):
        #     os.makedirs(os.path.dirname(recalboxFiles.rpcs3Config))

        # Generate a default config if it doesn't exist otherwise just open the existing
        rpcs3ymlconfig = {}
        if os.path.isfile(recalboxFiles.rpcs3YmlConfig):
            with open(recalboxFiles.rpcs3YmlConfig, "r") as stream:
                rpcs3ymlconfig = yaml.safe_load(stream)

        if rpcs3ymlconfig is None: # in case the file is empty
            rpcs3ymlconfig = {}

        # Add Nodes if not in the file
        if "Core" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Core"] = {}
        if "VFS" not in rpcs3ymlconfig:
            rpcs3ymlconfig["VFS"] = {}
        if "Video" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Video"] = {}
        if "Performance Overlay" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Video"]["Performance Overlay"] = {}
        if "Vulkan" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Video"]["Vulkan"] = {}
        if "Shader Loading Dialog" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Video"]["Shader Loading Dialog"] = {}
        if "Audio" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Audio"] = {}
        if "Input/Output" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Input/Output"] = {}
        if "System" not in rpcs3ymlconfig:
            rpcs3ymlconfig["System"] = {}
        if "Net" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Net"] = {}
        if "Savestate" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Savestate"] = {}
        if "Miscellaneous" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Miscellaneous"] = {}
        if "Log" not in rpcs3ymlconfig:
            rpcs3ymlconfig["Log"] = {}

        # [Core]
        # rpcs3ymlconfig["Core"]["Accurate xfloat"] = False // depricated
        rpcs3ymlconfig["Core"]["XFloat Accuracy"] = "Approximate"
        # rpcs3ymlconfig["Core"]["Approximate xfloat"] = True // depricated
        rpcs3ymlconfig["Core"]["PPU Decoder"] = "Recompiler (LLVM)"
        rpcs3ymlconfig["Core"]["Preferred SPU Threads"] = 0 # Auto
        # rpcs3ymlconfig["Core"]["Relaxed xfloat"] = True // depricated
        rpcs3ymlconfig["Core"]["RSX FIFO Accuracy"] = "Fast" # "Atomic"
        # rpcs3ymlconfig["Core"]["Accurate RSX reservation access"] = True
        rpcs3ymlconfig["Core"]["SPU Block Size"] = "Safe" # default safe # "Mega" "Giga"
        rpcs3ymlconfig["Core"]["SPU Cache"] = True
        rpcs3ymlconfig["Core"]["SPU Decoder"] = "Recompiler (LLVM)"

        # [Video]
        rpcs3ymlconfig["Video"]["Accurate ZCULL stats"] = True
        rpcs3ymlconfig["Video"]["Anisotropic Filter Override"] = 0
        rpcs3ymlconfig["Video"]["Aspect ratio"] = gameRatio
        rpcs3ymlconfig["Video"]["Disable Vertex Cache"] = True # False
        rpcs3ymlconfig["Video"]["Frame limit"] = "Auto"
        rpcs3ymlconfig["Video"]["Minimum Scalable Dimension"] = 16 # default value 16 # 512 # Gta5
        rpcs3ymlconfig["Video"]["MSAA"] = "Auto"
        rpcs3ymlconfig["Video"]["Multithreaded RSX"] = False
        rpcs3ymlconfig["Video"]["Output Scaling Mode"] = "Bilinear"
        rpcs3ymlconfig["Video"]["Performance Overlay"]["Detail level"] = "Minimal"
        rpcs3ymlconfig["Video"]["Performance Overlay"]["Enabled"] = system.ShowFPS
        rpcs3ymlconfig["Video"]["Performance Overlay"]["Position"] = "Top Right"
        rpcs3ymlconfig["Video"]["Relaxed ZCULL Sync"] = False
        rpcs3ymlconfig["Video"]["Renderer"] = videoDriver
        rpcs3ymlconfig["Video"]["Resolution"] = "1280x720"
        rpcs3ymlconfig["Video"]["Second Frame Limit"] = False
        rpcs3ymlconfig["Video"]["Shader Compiler Threads"] = 0 # Auto
        rpcs3ymlconfig["Video"]["Shader Mode"] = "Async Shader Recompiler"
        rpcs3ymlconfig["Video"]["Shader Precision"] = "Auto"
        rpcs3ymlconfig["Video"]["VSync"] = True # False # Default     
        rpcs3ymlconfig["Video"]["ZCULL accuracy"] = "Precise" # "Approximate" # "Relaxed" # CODBO
        # Minimum Scalable Dimension = 16 # Default
        rpcs3ymlconfig["Video"]["Write Color Buffers"] = False #True
        rpcs3ymlconfig["Video"]["Write Depth Buffer"] = False #True
        rpcs3ymlconfig["Video"]["Asynchronous Texture Streaming 2"] = False

        # [Audio]
        rpcs3ymlconfig["Audio"]["Renderer"] = "Cubeb"
        rpcs3ymlconfig["Audio"]["Master Volume"] = 100
        rpcs3ymlconfig["Audio"]["Audio Format"] = "Automatic" #"Stereo"
        rpcs3ymlconfig["Audio"]["Enable Buffering"] = False #True
        rpcs3ymlconfig["Audio"]["Desired Audio Buffer Duration"] = 100
        rpcs3ymlconfig["Audio"]["Enable Time Stretching"] = False #True
        rpcs3ymlconfig["Audio"]["Time Stretching Threshold"] = 75

        # [System]
        # Console Zone 
        rpcs3ymlconfig["System"]["License Area"] = ps3LicenseArea
        # Game language
        rpcs3ymlconfig["System"]["Language"] = ps3Language
        rpcs3ymlconfig["System"]["Keyboard Type"] = ps3Keyboard

        # [Net]
        rpcs3ymlconfig["Net"]["Internet enabled"] = "Connected" # "Connected"
        rpcs3ymlconfig["Net"]["DNS address"] = "8.8.8.8"
        rpcs3ymlconfig["Net"]["UPNP Enabled"] = True
        rpcs3ymlconfig["Net"]["PSN status"] = "RPCN"

        # [Input/Output]
        # rpcs3ymlconfig["Input/Output"]["Move"] = "Gun"
        # rpcs3ymlconfig["Input/Output"]["Camera"] = "Fake"
        # rpcs3ymlconfig["Input/Output"]["Camera type"] = "PS Eye"

        # [Miscellaneous]
        rpcs3ymlconfig["Miscellaneous"]["Exit RPCS3 when process finishes"] = True
        rpcs3ymlconfig["Miscellaneous"]["Start games in fullscreen mode"] = False
        rpcs3ymlconfig["Miscellaneous"]["Show shader compilation hint"] = system.ShowFPS
        rpcs3ymlconfig["Miscellaneous"]["Show PPU compilation hint"] = system.ShowFPS
        rpcs3ymlconfig["Miscellaneous"]["Prevent display sleep while running games"] = False
        rpcs3ymlconfig["Miscellaneous"]["Show trophy popups"] = True
        rpcs3ymlconfig["Miscellaneous"]["Pause Emulation During Home Menu"] = True
        rpcs3ymlconfig["Miscellaneous"]["Use native user interface"] = True
        # Force window name for simply switch to menu in game
        rpcs3ymlconfig["Miscellaneous"]["Window Title Format"] = "GameView-%V"
        rpcs3ymlconfig["Miscellaneous"]["Pause emulation on RPCS3 focus loss"] = True


        with open(recalboxFiles.rpcs3YmlConfig, "w") as file:
            # yaml.dump(rpcs3ymlconfig, file, default_flow_style=False)
            yaml.dump(rpcs3ymlconfig, file,)

        # Force value For Custom config per game
        gameCustomConfigs: str = "{}/rpcs3/custom_configs/config_{}.yml".format(recalboxFiles.rpcs3Saves, Rpcs3Generator.getGameId(args))
        rpcs3ymlCustomconfig = {}
        if os.path.isfile(gameCustomConfigs):
            print(gameCustomConfigs)
            with open(gameCustomConfigs, "r") as stream:
                rpcs3ymlCustomconfig = yaml.safe_load(stream)

            if "Video" not in rpcs3ymlCustomconfig:
                rpcs3ymlCustomconfig["Video"] = {}
            if "Performance Overlay" not in rpcs3ymlCustomconfig:
                rpcs3ymlCustomconfig["Video"]["Performance Overlay"] = {}
            if "Miscellaneous" not in rpcs3ymlCustomconfig:
                rpcs3ymlCustomconfig["Miscellaneous"] = {}
            if rpcs3ymlCustomconfig is None: # in case the file is empty
                rpcs3ymlCustomconfig = {}
            
            # Force value For Custom config per game
            rpcs3ymlCustomconfig["Video"]["Aspect ratio"] = gameRatio
            rpcs3ymlCustomconfig["Video"]["Performance Overlay"]["Detail level"] = "Minimal"
            rpcs3ymlCustomconfig["Video"]["Performance Overlay"]["Enabled"] = system.ShowFPS
            rpcs3ymlCustomconfig["Video"]["Performance Overlay"]["Position"] = "Top Right"
            rpcs3ymlCustomconfig["Miscellaneous"]["Show shader compilation hint"] = system.ShowFPS
            rpcs3ymlCustomconfig["Miscellaneous"]["Show PPU compilation hint"] = system.ShowFPS
            rpcs3ymlCustomconfig["Miscellaneous"]["Exit RPCS3 when process finishes"] = True
            rpcs3ymlCustomconfig["Miscellaneous"]["Start games in fullscreen mode"] = False
            rpcs3ymlCustomconfig["Miscellaneous"]["Window Title Format"] = "GameView-%V"
            rpcs3ymlCustomconfig["Miscellaneous"]["Pause emulation on RPCS3 focus loss"] = True
        
            with open(gameCustomConfigs, "w") as file:
                # yaml.dump(rpcs3ymlCustomconfig, file, default_flow_style=False)
                yaml.dump(rpcs3ymlCustomconfig, file)
        
        # create local username and force pixL username
        pathLocalUsername = "{}/rpcs3/dev_hdd0/home/00000001/localusername".format(recalboxFiles.rpcs3Saves)
        if not os.path.exists(os.path.dirname(pathLocalUsername)):
            os.makedirs(os.path.dirname(pathLocalUsername))
        with open(pathLocalUsername,'w+') as fileobj:
            fileobj.write(nickname)

        # Pre configuration of RPCN Network games
        # Need create a account in emulator > Configuration > RPCN.
        rpcnconfig = {}
        if os.path.isfile(recalboxFiles.rpcnConfig):
            with open(recalboxFiles.rpcnConfig, "r") as stream:
                rpcnconfig = yaml.safe_load(stream)

        if rpcnconfig is None: # in case the file is empty
            rpcnconfig = {}

        # Add Nodes if not in the file
        if "Version" not in rpcnconfig:
            rpcnconfig["Version"] = {}
        if "Host" not in rpcnconfig:
            rpcnconfig["Host"] = {}
        if "NPID" not in rpcnconfig:
            rpcnconfig["NPID"] = {}
        if "Password" not in rpcnconfig:
            rpcnconfig["Password"] = {}
        if "Token" not in rpcnconfig:
            rpcnconfig["Token"] = {}
        if "Hosts" not in rpcnconfig:
            rpcnconfig["Hosts"] = {}

        rpcnconfig["Version"] = 2
        rpcnconfig["Host"] = "np.rpcs3.net"
        # rpcnconfig["NPID"] = ""
        # rpcnconfig["Password"] = ""
        # rpcnconfig["Token"] = ""
        rpcnconfig["Hosts"] = "Official RPCN Server|np.rpcs3.net|||RPCN Test Server|test-np.rpcs3.net"
        
        with open(recalboxFiles.rpcnConfig, "w") as file:
            yaml.dump(rpcnconfig, file)

        # copy icon files to config
        icon_source = "/usr/share/rpcs3/Icons/"
        icon_target = recalboxFiles.CONF + "/rpcs3/Icons"
        if not os.path.exists(icon_target):
            os.makedirs(icon_target)
        shutil.copytree(icon_source, icon_target, dirs_exist_ok=True, copy_function=shutil.copy2)

        # determine the rom name and prepare launch or install maybe game update if possible 
        if args.rom.lower().endswith(".iso"):
            isoGamesPath = "{}/ps3/games".format(recalboxFiles.ROMS)
            Rpcs3Generator.mountUnmountIso(args)
            Log("GameId: " + Rpcs3Generator.getGameId(args))
            romName = "{}/PS3_GAME/USRDIR/EBOOT.BIN".format(isoGamesPath)
        
        if args.rom.lower().endswith(".ps3"):
            Log("GameId: " + Rpcs3Generator.getGameId(args))
            romName = args.rom + str("/PS3_GAME/USRDIR/EBOOT.BIN")
        
        if args.rom.lower().endswith(".psn"):
            Log("GameId: " + Rpcs3Generator.getGameId(args))
            for file in os.listdir("{}/".format(args.rom)):
                fileExt = (".edat", ".rap")
                if file.endswith(".pkg"):
                    pkgFilesName = file
                    pkgFilesPath = os.path.join("{}/{}".format(args.rom, pkgFilesName))
                if file.endswith(fileExt):
                    pkgValidationName = file
                    pkgValidationPath = os.path.join("{}/{}".format(args.rom, pkgValidationName))
            if Rpcs3Generator.getPkgNeedInstall(args) is None:
                Log("PSN Game " + Rpcs3Generator.getGameId(args) + " is already installed")
                Log("Launch game !")
                romName = "{}/rpcs3/dev_hdd0/game/{}/USRDIR/EBOOT.BIN".format(recalboxFiles.rpcs3Saves, Rpcs3Generator.getGameId(args))

        if args.rom.lower().endswith(".self"):
            # Launch XMB menu ps3 
            romName = "{}/rpcs3/dev_flash/vsh/module/vsh.self".format(recalboxFiles.rpcs3Saves)

        if args.rom.lower().endswith(".sys357"):
            romName = "{}/dev_hdd0/game/SCEEXE000/USRDIR/EBOOT.BIN".format(args.rom)
            # Force creating dev_hdd1 special in rompath for save cache game
            if not os.path.exists(os.path.dirname("{}/dev_hdd1".format(args.rom))):
                os.makedirs(os.path.dirname("{}/dev_hdd1".format(args.rom)))
            # Reconfigure Usio panel configuration
            rpcs3Controllers.generateControllerConfigUsio(args, playersControllers)
            # perGame usb dongle and more tips
            # Dongle Path for read file 
            usb000 = "{}/usb000.key".format(args.rom)
            usb001 = "{}/usb001.key".format(args.rom)
            usb0Serial = ""
            usb0Pid = ""
            usb0Vid = ""
            usb1Serial = ""
            usb1Pid = ""
            usb1Vid = ""
            gameName = Rpcs3Generator.getNamcoShortName(args)
            Log("GameId: " + Rpcs3Generator.getGameId(args))
            Log("Sony/Namco Game: " + gameName)
            if (gameName == "DZB3"):
                usb0Serial = Rpcs3Generator.getUsbDongleData(args, usb000, 'serial')
                usb0Pid = Rpcs3Generator.getUsbDongleData(args, usb000, 'pid')
                usb0Vid = Rpcs3Generator.getUsbDongleData(args, usb000, 'vid')
            if (gameName == "RazingStorm"):
                usb0Serial = Rpcs3Generator.getUsbDongleData(args, usb000, 'serial')
                usb0Pid = Rpcs3Generator.getUsbDongleData(args, usb000, 'pid')
                usb0Vid = Rpcs3Generator.getUsbDongleData(args, usb000, 'vid')
                # Need to fix 'Error 19-11' clean /SCEEXE000/USRDIR/s357secr.bin on erase
                s357secrFile = "{}/dev_hdd0/game/SCEEXE000/USRDIR/s357secr.bin".format(args.rom)
                f = open(s357secrFile, 'r+')
                f.truncate()
            if (gameName == "DarkEscape"):
                usb0Serial = Rpcs3Generator.getUsbDongleData(args, usb000, 'serial')
                usb0Pid = Rpcs3Generator.getUsbDongleData(args, usb000, 'pid')
                usb0Vid = Rpcs3Generator.getUsbDongleData(args, usb000, 'vid')
                # Need to fix 'Error 19-1' clean /USRDIR/s357security.bin on erase" 
                s357secrFile = "{}/dev_hdd0/game/SCEEXE000/USRDIR/s357security.bin".format(args.rom)
                f = open(s357secrFile, 'r+')
                f.truncate()
            if (gameName == "Sailorzombie"):
                usb0Serial = Rpcs3Generator.getUsbDongleData(args, usb000, 'serial')
                usb0Pid = Rpcs3Generator.getUsbDongleData(args, usb000, 'pid')
                usb0Vid = Rpcs3Generator.getUsbDongleData(args, usb000, 'vid')
                # Need to fix 'Error 19-1' clean /USRDIR/s357security.bin on erase"
                s357secrFile = "{}/dev_hdd0/game/SCEEXE000/USRDIR/s357security.bin".format(args.rom)
                f = open(s357secrFile, 'r+')
                f.truncate()
            if (gameName == "TaikonoTatsujin(S111)"):
                usb0Serial = Rpcs3Generator.getUsbDongleData(args, usb000, 'serial')
                usb0Pid = Rpcs3Generator.getUsbDongleData(args, usb000, 'pid')
                usb0Vid = Rpcs3Generator.getUsbDongleData(args, usb000, 'vid')
                usb1Serial = Rpcs3Generator.getUsbDongleData(args, usb001, 'serial')
                usb1Pid = Rpcs3Generator.getUsbDongleData(args, usb001, 'pid')
                usb1Vid = Rpcs3Generator.getUsbDongleData(args, usb001, 'vid')
            # if (gameName == "TEKKENTAGTOURNAMENT2"):
            #     usb0Serial = ""
            #     usb0Pid = ""
            #     usb0Vid = ""
            # if (gameName == "TEKKEN6"):
            #     usb0Serial = ""
            #     usb0Pid = ""
            #     usb0Vid = ""
            # if (gameName == "TEKKEN6BR"):
            #     usb0Serial = ""
            #     usb0Pid = ""
            #     usb0Vid = ""
            # if (gameName == "DeadstormPiratesSpecialEdition"):
            #     usb0Serial = ""
            #     usb0Pid = ""
            #     usb0Vid = ""

        # Force creating vfs.yml
        if not os.path.exists(recalboxFiles.rpcs3VfsConfig):
            os.mknod(recalboxFiles.rpcs3VfsConfig)

        rpcs3VfsConfig = {}
        if os.path.isfile(recalboxFiles.rpcs3VfsConfig):
            with open(recalboxFiles.rpcs3VfsConfig, "r") as stream:
                rpcs3VfsConfig = yaml.safe_load(stream)

        if rpcs3VfsConfig is None: # in case the file is empty
            rpcs3VfsConfig = {}

        if "$(EmulatorDir)" not in rpcs3VfsConfig:
            rpcs3VfsConfig["$(EmulatorDir)"] = {}
        if "/dev_hdd0/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_hdd0/"] = {}
        if "/dev_hdd1/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_hdd1/"] = {}
        if "/dev_flash/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_flash/"] = {}
        if "/dev_flash2/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_flash2/"] = {}
        if "/dev_flash3/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_flash3/"] = {}
        if "/dev_bdvd/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_bdvd/"] = {}
        if "/games/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/games/"] = {}
        if "/app_home/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/app_home/"] = {}                 
        if "/dev_usb***/" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_usb***/"] = {}
        if "/dev_usb000" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"] = {}
        if "/dev_usb001" not in rpcs3VfsConfig:
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"] = {}

        # Dev flash have firmware install need to preserve in saves 
        rpcs3VfsConfig["/dev_flash/"] = "{}/rpcs3/dev_flash/".format(recalboxFiles.rpcs3Saves)
        rpcs3VfsConfig["/dev_flash2/"] = "{}/rpcs3/dev_flash2/".format(recalboxFiles.rpcs3Saves)
        rpcs3VfsConfig["/dev_flash3/"] = "{}/rpcs3/dev_flash3/".format(recalboxFiles.rpcs3Saves)
        rpcs3VfsConfig["/games/"] = "{}/ps3/games/".format(recalboxFiles.ROMS)

        if args.rom.lower().endswith(".sys357"):
            rpcs3VfsConfig["$(EmulatorDir)"] = "{}/".format(args.rom) 
            rpcs3VfsConfig["/dev_hdd0/"] = "{}/dev_hdd0/".format(args.rom)
            rpcs3VfsConfig["/dev_hdd1/"] = "{}/dev_hdd1/".format(args.rom)
            rpcs3VfsConfig["/dev_bdvd/"] = ""
            rpcs3VfsConfig["/app_home/"] = ""
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"]["Path"] = "{}/dev_usb000/".format(args.rom)
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"]["Serial"] = usb0Serial
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"]["VID"] = usb0Vid
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"]["PID"] = usb0Pid
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"]["Path"] = "{}/dev_usb001/".format(args.rom)
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"]["Serial"] = usb1Serial
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"]["VID"] = usb1Vid
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"]["PID"] = usb1Pid
        else:
            rpcs3VfsConfig["$(EmulatorDir)"] = "{}/rpcs3/".format(recalboxFiles.rpcs3Saves)
            rpcs3VfsConfig["/dev_hdd0/"] = "{}/rpcs3/dev_hdd0/".format(recalboxFiles.rpcs3Saves)
            rpcs3VfsConfig["/dev_hdd1/"] = "{}/rpcs3/dev_hdd1/".format(recalboxFiles.rpcs3Saves)
            rpcs3VfsConfig["/dev_bdvd/"] = ""
            rpcs3VfsConfig["/app_home/"] = ""
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"]["Path"] = "{}/rpcs3/dev_usb000/".format(recalboxFiles.rpcs3Saves)
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"]["Serial"] = ""
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"]["VID"] = ""
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb000"]["PID"] = ""
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"]["Path"] = "{}/rpcs3/dev_usb001/".format(recalboxFiles.rpcs3Saves)
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"]["Serial"] = ""
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"]["VID"] = ""
            rpcs3VfsConfig["/dev_usb***/"]["/dev_usb001"]["PID"] = ""

        with open(recalboxFiles.rpcs3VfsConfig, "w") as file:
            yaml.dump(rpcs3VfsConfig, file)

        # firmware not installed and available : instead of starting the game, install it
        Log("Firmware on version: " + str(Rpcs3Generator.getFirmwareVersion()))
        if Rpcs3Generator.getFirmwareVersion() is None:
            if os.path.exists(recalboxFiles.BIOS + "/ps3/PS3UPDAT.PUP"):
                commandArray = [recalboxFiles.recalboxBins[system.Emulator], "--installfw", recalboxFiles.BIOS + "/ps3/PS3UPDAT.PUP"]
            
        # check if psn game need to install on firts launch only
        elif Rpcs3Generator.getPkgNeedInstall(args) is True:
            if args.rom.lower().endswith(".psn"):
                Log("Psn game need installation:" + str(Rpcs3Generator.getPkgNeedInstall(args)))
                commandArray = [recalboxFiles.recalboxBins[system.Emulator], "--installpkg", pkgFilesPath]
                if os.path.isfile(pkgValidationPath):
                    edatRap_source = pkgValidationPath
                    edatRap_target = "{}/rpcs3/dev_hdd0/home/00000001/exdata/".format(recalboxFiles.rpcs3Saves)
                    if not os.path.isfile(edatRap_target + pkgValidationName):
                        Log("Copy " + str(pkgValidationName) + " to '/rpcs3/dev_hdd0/home/00000001/exdata/' ")
                        shutil.copy2(edatRap_source, edatRap_target)
                    else:
                        Log(str(pkgValidationName) + "Already exist in directory")
        else:
            commandArray = [recalboxFiles.recalboxBins[system.Emulator], romName]
            # commandArray = [recalboxFiles.recalboxBins[system.Emulator], "--no-gui", romName]
            # Check if have Custom config 
            gameCustomConfigs: str = "{}/rpcs3/custom_configs/config_{}.yml".format(recalboxFiles.rpcs3Saves, Rpcs3Generator.getGameId(args))
            if os.path.isfile(gameCustomConfigs):
                commandArray.extend(["--config", gameCustomConfigs])

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.CONF
        env["XDG_CACHE_HOME"] = recalboxFiles.rpcs3Saves
        env["QT_QPA_PLATFORM"] = "xcb"

        return Command(array=commandArray, videomode=system.VideoMode, env=env)
