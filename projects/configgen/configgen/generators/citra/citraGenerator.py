#!/usr/bin/env python
# -*- coding: utf-8 -*-

from configgen.Command import Command
import configgen.recalboxFiles as recalboxFiles
from configparser import ConfigParser
from configgen.generators.Generator import Generator
from configgen.settings.iniSettings import IniSettings
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.controllers.inputItem import InputItem
from configgen.utils.videoMode import GetHasVulkanSupport

class CitraGenerator(Generator):

    SECTION_CONTROLS = "Controls"
    SECTION_DATASTORAGE = "Data Storage"
    SECTION_MISCELLANEOUS = "Miscellaneous"
    SECTION_SYSTEM = "System"
    SECTION_UI = "UI"
    SECTION_UTILITY = "Utility"
    SECTION_WEBSERVICE = "WebService"
    SECTION_RENDERER = "Renderer"

    # Set language, country, birthday and nickname in the 3DS NAND config file
    # Ref: https://www.3dbrew.org/wiki/Config_Savegame
    # Ref: https://wiibrew.org/wiki/Country_Codes
    @staticmethod
    def SetSystemLocale():

        import datetime
        import os.path
        import struct

        # Set dict of language codes
        languages = {
            'de' : chr(3)  ,  # German
            'en' : chr(1)  ,  # English
            'es' : chr(5)  ,  # Spanish
            'fr' : chr(2)  ,  # French
            'it' : chr(4)  ,  # Italian
            'ja' : chr(0)  ,  # Japanese
            'ko' : chr(7)  ,  # Korean
            'nl' : chr(8)  ,  # Dutch
            'pt' : chr(9)  ,  # Portuguese
            'ru' : chr(10) ,  # Russian
            'tw' : chr(11) ,  # Traditional Chinese
            'zh' : chr(6)     # Simplified Chinese
        }

        # Set dict of contry codes (only countries listed in pegasus-frontend language_choice)
        countries = {
            'BR' : chr(16)  ,  # BRAZIL
            'CN' : chr(160) ,  # CHINA
            'CZ' : chr(73)  ,  # CZECH REPUBLIC
            'DE' : chr(78)  ,  # GERMANY
            'ES' : chr(105) ,  # SPAIN
            'FR' : chr(77)  ,  # FRANCE
            'GB' : chr(110) ,  # UNITED KINGDOM
            'GR' : chr(79)  ,  # GREECE
            'HU' : chr(80)  ,  # HUNGARY
            'IT' : chr(83)  ,  # ITALY
            'JP' : chr(1)   ,  # JAPAN
            'KR' : chr(136) ,  # SOUTH KOREA
            'LV' : chr(84)  ,  # LATVIA
            'LV' : chr(84)  ,  # LUXEMBOURG
            'NL' : chr(94)  ,  # NETHERLANDS
            'NO' : chr(96)  ,  # NORWAY
            'PL' : chr(97)  ,  # POLAND
            'RU' : chr(100) ,  # RUSSIA
            'SE' : chr(107) ,  # SWEDEN
            'TR' : chr(109) ,  # TURKEY
            'TW' : chr(128) ,  # TAIWAN
            'US' : chr(49)     # USA
        }

        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)

        # Get language
        language = conf.getString("system.language", "en_US")[:2].lower()
        # get language value
        languageValue = languages[language] if language in languages else languages['en']
        languageValue = languageValue.encode()

        # Get country
        country = conf.getString("system.language", "en_US")[3:].upper()
        # Get country value
        countryValue = countries[country] if country in countries else countries['US']
        # Format to 0x4 size
        countryValue = '{:\x00>4}'.format(countryValue)
        countryValue = countryValue.encode('utf-8')

        # Get birthday
        birthday = conf.getString("global.3dsnetplay.birthday", "01-01")
        # Verify it is a valid date else set to default
        try:
            datetime.datetime.strptime(birthday, '%m-%d')
        except ValueError:
            birthday = "01-01"
        # Set to utf-8 month, utf-8 day
        birthdayValue = chr(int(birthday[:2])) + chr(int(birthday[3:]))
        birthdayValue = birthdayValue.encode()
        
        # Get nickname (decoded in case of special char)
        nickname = conf.getString("global.netplay.nickname", "pixL")
        # Cut nickname if too long (citra support 10 chars max)
        nickname = nickname[:10]
        # Format to utf-16 little endian w/o BOM, size 0x1C
        nicknameValueString = '{:\x00<28}'.format(nickname)
        nicknameValue = nicknameValueString.encode('utf-16-le')
        # Set dict of Block ID's : values
        BlockIDs = {
            '\x00\x00\x0A\x00' : nicknameValue ,  # BlkID : 0x000A0000
            '\x01\x00\x0A\x00' : birthdayValue ,  # BlkID : 0x000A0001
            '\x02\x00\x0A\x00' : languageValue ,  # BlkID : 0x000A0002
            '\x00\x00\x0B\x00' : countryValue     # BlkID : 0x000B0000
        }

        # Write values to config file
        if os.path.exists(recalboxFiles.citraSysConf):
            with open(recalboxFiles.citraSysConf, 'rb+') as configFile:
                buf = configFile.read()
                for BlockID, value in BlockIDs.items() :
                    if buf.find(BlockID.encode()) > -1 : 
                        keyAddr = buf.find(BlockID.encode()) + len(BlockID)
                        configFile.seek(keyAddr)
                        if len(value) > 4 :
                            #read offset and jump to it
                            offsetData = configFile.read(0x4)
                            offset = struct.unpack('<I', offsetData)[0]
                            configFile.seek(offset)
                        configFile.write(value)

    @staticmethod
    def setButton(key, padGuid, padInputs):
        for input in padInputs:
            if key == input.Item:
                #print("Type:" + str(input.Type) + " Id:" + str(input.Id) + " Value:" + str(input.Value))
                if input.Type == 2: #"button"
                    return ("button:{},guid:{},engine:sdl").format(input.Id, padGuid)
                elif input.Type == 1: #"hat"
                    return ("engine:sdl,guid:{},hat:{},direction:{}").format(padGuid, input.Id, CitraGenerator.hatdirectionvalue(input.Value))
                elif input.Type == 0: #"axis"
                    # Untested, need to configure an axis as button / triggers buttons to be tested too
                    return ("engine:sdl,guid:{},axis:{},direction:{},threshold:{}").format(padGuid, input.Id, "+", 0.5)

    @staticmethod
    def setAxis(key, padGuid, padInputs):
        for input in padInputs:
            if key == "joystick1":
                if input.Item == InputItem.ItemJoy1Left:
                   inputx = input
                if input.Item == InputItem.ItemJoy1Up:
                   inputy = input
            
            elif key == "joystick2":
                
                if input.Item == InputItem.ItemJoy2Left:
                   inputx = input
                if input.Item == InputItem.ItemJoy2Up:
                   inputy = input

        return ("axis_x:{},guid:{},axis_y:{},engine:sdl").format(inputx.Id, padGuid, inputy.Id)

    @staticmethod
    def hatdirectionvalue(value):
        if int(value) == 1:
            return "up"
        if int(value) == 4:
            return "down"
        if int(value) == 2:
            return "right"
        if int(value) == 8:
            return "left"
        return "unknown"

    def mainConfiguration(self, playersControllers):

        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)
        # Get Languages
        language = conf.getString("system.kblayout", conf.getString("system.language", "en")[0:2]).lower()
        # Get NetplayInfo
        username = conf.getString("global.3dsnetplay.nickname", "pixL")
        token = conf.getString("global.3dsnetplay.token", "")
        resolution = conf.getString("citra.resolution", "1") # Native
        textureFilter = conf.getString("citra.texture.filter", "0") # None
        videoDriver = "2" if GetHasVulkanSupport() == '1' else "1" # Logiciel(0)/OpenGl(1)/vulkan(2)

        # Load Configuration
        citraSettings = IniSettings(recalboxFiles.citraIni, True)
        citraSettings.loadFile(True).defineBool('true', 'false')

        CITRA_BUTTONS = {
            "button_a":      InputItem.ItemA,
            "button_b":      InputItem.ItemB,
            "button_x":      InputItem.ItemX,
            "button_y":      InputItem.ItemY,
            "button_up":     InputItem.ItemUp,
            "button_down":   InputItem.ItemDown,
            "button_left":   InputItem.ItemLeft,
            "button_right":  InputItem.ItemRight,
            "button_l":      InputItem.ItemL1,
            "button_r":      InputItem.ItemR1,
            "button_start":  InputItem.ItemStart,
            "button_select": InputItem.ItemSelect,
            "button_zl":     InputItem.ItemL2,
            "button_zr":     InputItem.ItemR2,
            "button_home":   InputItem.ItemHotkey
        }

        CITRA_JOYSTICK = {
            "circle_pad":    "joystick1",
            "c_stick":       "joystick2"
        }

        for idx, playerController in playersControllers.items():
            controller = playerController
            # Remove profiles config on firts
            citraSettings.removeOptionStartingWith(self.SECTION_CONTROLS, "")
            # Force option For default Controllers
            citraSettings.setInt(self.SECTION_CONTROLS, "profile", 0)
            citraSettings.setBool(self.SECTION_CONTROLS, r"profile\default", True)
            citraSettings.setInt(self.SECTION_CONTROLS, r"profiles\size", 1)
            citraSettings.setString(self.SECTION_CONTROLS, r"profiles\1\name","3dsPad")
            citraSettings.setBool(self.SECTION_CONTROLS, r"profiles\1\name\default", False)

            # We only care about player 1
            if controller.PlayerIndex != 1:
                continue
            for x in CITRA_BUTTONS:
                print("Citra value: " + x)
                citraSettings.setString(self.SECTION_CONTROLS, "profiles\\1\\" + x, '"{}"'.format(CitraGenerator.setButton(CITRA_BUTTONS[x], controller.GUID, controller.AvailableInput)))
                citraSettings.setBool(self.SECTION_CONTROLS, "profiles\\1\\" + x + r"\default", False)
            for x in CITRA_JOYSTICK:
                citraSettings.setString(self.SECTION_CONTROLS, "profiles\\1\\" + x, '"{}"'.format(CitraGenerator.setAxis(CITRA_JOYSTICK[x], controller.GUID, controller.AvailableInput)))
                citraSettings.setBool(self.SECTION_CONTROLS, "profiles\\1\\" + x + r"\default", False)            
            break
        
        # Force option For default Controllers
        # Data storage
        citraSettings.setBool(self.SECTION_DATASTORAGE, "use_virtual_sd", True)
        citraSettings.setBool(self.SECTION_DATASTORAGE, r"use_virtual_sd\default", False)
        # Miscellaneous
        citraSettings.setString(self.SECTION_MISCELLANEOUS, "log_filter", "*:Error *:Critical")
        # System
        # Force option to auto selection 
        citraSettings.setInt(self.SECTION_SYSTEM, "region_value", -1)
        citraSettings.setBool(self.SECTION_SYSTEM, r"region_value\default", False)
        # UI
        # force main option
        citraSettings.setBool(self.SECTION_UI, "confirmClose", False)
        citraSettings.setBool(self.SECTION_UI, r"confirmClose\default", False)

        citraSettings.setBool(self.SECTION_UI, "enable_discord_presence", False)
        citraSettings.setBool(self.SECTION_UI, r"enable_discord_presence\default", False)
        citraSettings.setBool(self.SECTION_UI, "firstStart", False)
        citraSettings.setBool(self.SECTION_UI, r"firstStart\default", False)
        citraSettings.setBool(self.SECTION_UI, "fullscreen", True)
        citraSettings.setBool(self.SECTION_UI, r"fullscreen\default", False)
        citraSettings.setBool(self.SECTION_UI, "pauseWhenInBackground", True)
        citraSettings.setBool(self.SECTION_UI, r"pauseWhenInBackground\default", False)
        citraSettings.setBool(self.SECTION_UI, "showFilterBar", False)
        citraSettings.setBool(self.SECTION_UI, r"showFilterBar\default", False)
        citraSettings.setString(self.SECTION_UI, r"Paths\language", language)
        citraSettings.setBool(self.SECTION_UI, r"Paths\language\default", False)
        citraSettings.setBool(self.SECTION_UI, r"Updater\check_for_update_on_start", False)
        citraSettings.setBool(self.SECTION_UI, r"Updater\check_for_update_on_start\default", False)
        # Force single Windows Mode on false for stay fullscreen in game
        citraSettings.setBool(self.SECTION_UI, "singleWindowMode", True)
        citraSettings.setBool(self.SECTION_UI, r"singleWindowMode\default", False)
        citraSettings.setString(self.SECTION_UI, r"Paths\screenshotPath", "/recalbox/share/screenshots/")
        citraSettings.setBool(self.SECTION_UI, r"Paths\screenshotPath\default", False)
        citraSettings.setString(self.SECTION_UI, r"Paths\romsPath", "/recalbox/share/roms/3ds/")
        citraSettings.setBool(self.SECTION_UI, r"Paths\romsPath\default", False)
        # Set default rompath for netplay 
        citraSettings.setBool(self.SECTION_UI, r"Paths\gamedirs\1\expanded",True)
        citraSettings.setString(self.SECTION_UI, r"Paths\gamedirs\1\path", "/recalbox/share/roms/3ds/")
        citraSettings.setInt(self.SECTION_UI, r"Paths\gamedirs\size", 1)
        ## force recalbox in room description 
        citraSettings.setString(self.SECTION_UI, r"Multiplayer\room_description", "Play Again!")
        citraSettings.setString(self.SECTION_UI, r"Multiplayer\nickname", username)
        citraSettings.setString(self.SECTION_UI, "theme", "colorful_dark")
        citraSettings.setString(self.SECTION_UI, r"theme\default", False)
        # Web service
        # set id and username for connect to webservice citra-web
        citraSettings.setString(self.SECTION_WEBSERVICE, "citra_token", token)
        citraSettings.setString(self.SECTION_WEBSERVICE, "citra_username", username)
        citraSettings.setString(self.SECTION_WEBSERVICE, "web_api_url", "https://api.citra-emu.org")
        citraSettings.setBool(self.SECTION_WEBSERVICE, r"web_api_url\default", False)
        # Dump texture on png
        # disable on nightly-2062 version of citra crash on boot
        # citraSettings.setBool(self.SECTION_UTILITY, "dump_textures", True)
        # # citraSettings.setBool(self.SECTION_UTILITY, r"dump_textures\default", True)
        # citraSettings.setBool(self.SECTION_UTILITY, "preload_textures", True)
        # # citraSettings.setBool(self.SECTION_UTILITY, r"preload_textures\default", True)
        # citraSettings.setBool(self.SECTION_UTILITY, "async_custom_loading", False)
        # citraSettings.setBool(self.SECTION_UTILITY, r"async_custom_loading\default", False)

        citraSettings.setString(self.SECTION_RENDERER, "resolution_factor", resolution)
        citraSettings.setBool(self.SECTION_RENDERER, r"resolution_factor\default", False)
        citraSettings.setString(self.SECTION_RENDERER, "texture_filter", textureFilter)
        citraSettings.setBool(self.SECTION_RENDERER, r"texture_filter\default", False)
        citraSettings.setInt(self.SECTION_RENDERER, "graphics_api", videoDriver)
        citraSettings.setBool(self.SECTION_RENDERER, r"graphics_api\default", False)

        # Set language and country in the 3DS NAND config file 
        self.SetSystemLocale()

        # Save configuration
        citraSettings.saveFile()

    def generate(self, system, playersControllers, recalboxSettings, args):
        if not system.HasConfigFile:
            self.mainConfiguration(playersControllers)
            commandArray = [recalboxFiles.recalboxBins[system.Emulator], args.rom]

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.CONF
        env["XDG_DATA_HOME"] = recalboxFiles.CONF
        env["QT_QPA_PLATFORM"] = "xcb"
        env["XDG_CACHE_HOME"] = recalboxFiles.CACHE

        if system.HasArgs: commandArray.extend(system.Args)
        return Command(videomode=system.VideoMode, array=commandArray, env=env)
