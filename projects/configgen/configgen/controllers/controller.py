#!/usr/bin/env python
from __future__ import annotations
from typing import Dict, List, Optional, ValuesView

# Do not simplify - Allow mocking of Input file path
from configgen.controllers.inputItem import InputItem, InputCollection
import configgen.recalboxFiles as recalboxFiles

##to run subprocess as shell command
import subprocess
import json

##for regular expression
import re

pegasusInputs = recalboxFiles.pegasusInputs

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

class Controller:

    def __init__(self, deviceName: str, typeName: str, guid: str, playerIndex: int, sdlIndex: int,
                 inputs: Optional[InputCollection], devicePath: str, axesCount: int, hatsCount: int, buttonCount: int):
        self.__type: str = typeName
        self.__deviceName: str = deviceName
        self.__guid: str = guid
        self.__sdlIndex: int = sdlIndex       # SDL index - starting from 0
        self.__playerIndex: int = playerIndex # Player index - starting from 1
        self.__naturalIndex: int = 0          # Natural index - starting from 0
        self.__jsIndex: int = -1 #joystick index - starting from 0, set to -1 by default
        self.__jsOrder: int = -1 #joystick order - starting from 0, set to -1 by default
        self.__devicePath:str = devicePath
        self.__axesCount: int = axesCount
        self.__hatsCount: int = hatsCount
        self.__buttonsCount: int = buttonCount
        self.__inputs: Optional[InputCollection] = inputs if inputs is not None else {}
        self.axisesNumber: List[int] = self.setAxisNumberList() if inputs is not None else []

    # Getter
    @property
    def Type(self) -> str: return self.__type

    @property
    def GUID(self) -> str: return self.__guid

    @property
    def DeviceName(self) -> str: return self.__deviceName

    @property
    def DevicePath(self) -> str: return self.__devicePath

    @property
    def ButtonCount(self) -> int: return self.__buttonsCount

    @property
    def HatCount(self) -> int: return self.__hatsCount

    @property
    def AxisCount(self) -> int: return self.__axesCount

    @property
    def PlayerIndex(self) -> int: return self.__playerIndex

    @property
    def SdlIndex(self) -> int: return self.__sdlIndex

    @property
    def JsIndex(self) -> int: 
        if self.__jsIndex == -1:
            #command to know /dev/input/js? from /dev/input/event? used
            #cat /proc/bus/input/devices | grep -ni event16 | awk -v FS='(js|)' '{print $2}'
            cmd = "cat /proc/bus/input/devices | grep -ni " + self.__devicePath.replace("/dev/input/", "").rstrip() + " | awk -v FS='(js|)' '{print $2}'"
            Log(f"command: {str(cmd)}")
            p = subprocess.Popen(cmd, shell=True, 
                stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output = p.stdout 
            jsIndex = "-1"
            for line in output:
                jsIndex = line.rstrip().split(' ')[0]
            self.__jsIndex = int(jsIndex) # Joystick index from udev - starting from 0
        return self.__jsIndex

    @property
    def JsOrder(self) -> int: 
        if self.__jsOrder == -1:
            #command to know order of joystick from udev - starting from 0
            cmd = "cat /proc/bus/input/devices | grep -ni js"
            Log(f"command: {str(cmd)}")
            p = subprocess.Popen(cmd, shell=True, 
                stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output = p.stdout 
            jsOrder = -1
            index = 0
            for line in output:
                if self.__devicePath.replace("/dev/input/", "").rstrip() in line:
                    jsOrder = index
                    break
                index = index + 1;
            self.__jsOrder = jsOrder # Joystick index from udev - starting from 0
        return self.__jsOrder

    @property
    def NaturalIndex(self) -> int: return self.__naturalIndex

    @property
    def HasStart(self) -> bool: return InputItem.ItemStart in self.__inputs
    @property
    def Start(self) -> InputItem: return self.__inputs[InputItem.ItemStart]

    @property
    def HasSelect(self) -> bool: return InputItem.ItemSelect in self.__inputs
    @property
    def Select(self) -> InputItem: return self.__inputs[InputItem.ItemSelect]

    @property
    def HasHotkey(self) -> bool: return InputItem.ItemHotkey in self.__inputs
    @property
    def Hotkey(self) -> InputItem: return self.__inputs[InputItem.ItemHotkey]

    @property
    def HasA(self) -> bool: return InputItem.ItemA in self.__inputs
    @property
    def A(self) -> InputItem: return self.__inputs[InputItem.ItemA]

    @property
    def HasB(self) -> bool: return InputItem.ItemB in self.__inputs
    @property
    def B(self) -> InputItem: return self.__inputs[InputItem.ItemB]

    @property
    def HasX(self) -> bool: return InputItem.ItemX in self.__inputs
    @property
    def X(self) -> InputItem: return self.__inputs[InputItem.ItemX]

    @property
    def HasY(self) -> bool: return InputItem.ItemY in self.__inputs
    @property
    def Y(self) -> InputItem: return self.__inputs[InputItem.ItemY]

    @property
    def L1(self) -> InputItem: return self.__inputs[InputItem.ItemL1]
    @property
    def HasL1(self) -> bool: return InputItem.ItemL1 in self.__inputs

    @property
    def R1(self) -> InputItem: return self.__inputs[InputItem.ItemR1]
    @property
    def HasR1(self) -> bool: return InputItem.ItemR1 in self.__inputs

    @property
    def L2(self) -> InputItem: return self.__inputs[InputItem.ItemL2]
    @property
    def HasL2(self) -> bool: return InputItem.ItemL2 in self.__inputs

    @property
    def R2(self) -> InputItem: return self.__inputs[InputItem.ItemR2]
    @property
    def HasR2(self) -> bool: return InputItem.ItemR2 in self.__inputs

    @property
    def L3(self) -> InputItem: return self.__inputs[InputItem.ItemL3]
    @property
    def HasL3(self) -> bool: return InputItem.ItemL3 in self.__inputs

    @property
    def R3(self) -> InputItem: return self.__inputs[InputItem.ItemR3]
    @property
    def HasR3(self) -> bool: return InputItem.ItemR3 in self.__inputs

    @property
    def Up(self) -> InputItem: return self.__inputs[InputItem.ItemUp]
    @property
    def HasUp(self) -> bool: return InputItem.ItemUp in self.__inputs

    @property
    def Right(self) -> InputItem: return self.__inputs[InputItem.ItemRight]
    @property
    def HasRight(self) -> bool: return InputItem.ItemRight in self.__inputs

    @property
    def Down(self) -> InputItem: return self.__inputs[InputItem.ItemDown]
    @property
    def HasDown(self) -> bool: return InputItem.ItemDown in self.__inputs

    @property
    def Left(self) -> InputItem: return self.__inputs[InputItem.ItemLeft]
    @property
    def HasLeft(self) -> bool: return InputItem.ItemLeft in self.__inputs

    @property
    def Joy1Up(self) -> InputItem: return self.__inputs[InputItem.ItemJoy1Up]
    @property
    def HasJoy1Up(self) -> bool: return InputItem.ItemJoy1Up in self.__inputs

    @property
    def Joy1Right(self) -> InputItem: return self.__inputs[InputItem.ItemJoy1Right]
    @property
    def HasJoy1Right(self) -> bool: return InputItem.ItemJoy1Right in self.__inputs

    @property
    def Joy1Down(self) -> InputItem: return self.__inputs[InputItem.ItemJoy1Down]
    @property
    def HasJoy1Down(self) -> bool: return InputItem.ItemJoy1Down in self.__inputs

    @property
    def Joy1Left(self) -> InputItem: return self.__inputs[InputItem.ItemJoy1Left]
    @property
    def HasJoy1Left(self) -> bool: return InputItem.ItemJoy1Left in self.__inputs

    @property
    def Joy2Up(self) -> InputItem: return self.__inputs[InputItem.ItemJoy2Up]
    @property
    def HasJoy2Up(self) -> bool: return InputItem.ItemJoy2Up in self.__inputs

    @property
    def Joy2Right(self) -> InputItem: return self.__inputs[InputItem.ItemJoy2Right]
    @property
    def HasJoy2Right(self) -> bool: return InputItem.ItemJoy2Right in self.__inputs

    @property
    def Joy2Down(self) -> InputItem: return self.__inputs[InputItem.ItemJoy2Down]
    @property
    def HasJoy2Down(self) -> bool: return InputItem.ItemJoy2Down in self.__inputs

    @property
    def Joy2Left(self) -> InputItem: return self.__inputs[InputItem.ItemJoy2Left]
    @property
    def HasJoy2Left(self) -> bool: return InputItem.ItemJoy2Left in self.__inputs

    @property
    def AvailableInput(self) -> ValuesView[InputItem]: return self.__inputs.values()

    # Unitary access to avoid value injection
    def Input(self, index: int) -> InputItem: return self.__inputs[index]

    def HasInput(self, item: int) -> bool: return item in self.__inputs

    OppositeHat: Dict[int, int] = {
         1 :  4,
         2 :  8,
         3 : 12,
         4 :  1,
         5 :  0, # Not possible
         6 :  9,
         7 :  0, # Not possible
         8 :  2,
         9 :  6,
        10 :  0, # Not possible
        11 :  0, # Not possible
        12 :  3,
        13 :  0, # Not possible
        14 :  0, # Not possible
        15 :  0, # Not possible
    }

    def generateSDLGameDBLine(self, platform: bool = True):
        result: str = "{},{},".format(self.__guid, self.__deviceName.replace(',', ' '))
        if platform: result += "platform:Linux,"
        for inputItem in self.__inputs.values():
            sdlName: str = inputItem.GameDBName
            if sdlName == '': continue
            if inputItem.IsAxis:  result += "{}:{}a{},".format(sdlName, '-' if inputItem.Value < 0 else '+', inputItem.Id)
            elif inputItem.IsHat: result += "{}:h{}.{},".format(sdlName, inputItem.Id, inputItem.Value)
            else:                 result += "{}:{}{},".format(sdlName, inputItem.GameDBType, inputItem.Id)

        return result

    def count(self, inputType: int):
        count: int = 0
        for idx, inp in self.__inputs.items():
            if inp.Type == inputType:
                count += 1
        return count

    def setAxisNumberList(self) -> List[int]:
        # First let's find and sort all those available axes in the controller
        # Remember : a pad can have 6 axes and 1 hat, but only 4 axes are mapped
        # All those tricks are just to mimic https://github.com/xbmc/peripheral.joystick/blob/master/src/api/udev/JoystickUdev.cpp#L321-L334
        axises = []
        for idx, inp in self.__inputs.items():
            if inp.IsAxis:
                # As of now our patched SDL2 gives the same axes number to up/down and left/right. But it's wrong
                leftcode = self.__inputs[InputItem.ItemLeft].Code if InputItem.ItemLeft in self.__inputs else -1
                if inp.Item in (InputItem.ItemUp, InputItem.ItemDown) and inp.Code == leftcode:
                    code = inp.Code + 1
                else:
                    code = inp.Code
                if code not in axises:
                    axises.append(code)
        axises.sort()
        # Complete with existing axes but not in input.cfg
        # In the case of a 6 axes pad, complete with the 2 unassigned axes
        for i in range(len(axises), int(self.__axesCount)):
            axises.append(None)

        # Now add hats because they are after all axes
        hats = []
        for idx, inp in self.__inputs.items():
            if inp.IsHat:
                # As of now our patched SDL2 gives the same axes number to up/down and left/right. But it's wrong
                leftcode = self.__inputs[InputItem.ItemLeft].Code if InputItem.ItemLeft in self.__inputs else -1
                if inp.Item in (InputItem.ItemUp, InputItem.ItemDown) and inp.Code == leftcode:
                    code = inp.Code + 1
                else:
                    code = inp.Code
                if code not in hats:
                    hats.append(code)
        hats.sort()
        # Time to sort this
        axises.extend(hats)
        return axises

    def getAxisNumber(self, inputItem: InputItem):
        if inputItem.Type not in (InputItem.TypeAxis, InputItem.TypeHat): return None
        if inputItem.IsHat:
            if inputItem.Item in (InputItem.ItemUp, InputItem.ItemDown) and inputItem.Code == self.__inputs[InputItem.ItemLeft].Code:
                code = inputItem.Code + 1
            else:
                code = inputItem.Code
        else:
            code = inputItem.Code
        return self.axisesNumber.index(code)

    def getTotalAxisNumber(self):
        return int(self.__axesCount) + 2 * int(self.__hatsCount)

    @staticmethod
    def __CompositeIdentifierFrom(configName: str, guid: str, axeCount: int, hatCount: int, buttonCount: int) -> str:
        return "{}-{}-{}:{}:{}".format(configName, guid,
                                       str(axeCount) if axeCount >= 0 else '*',
                                       str(hatCount) if hatCount >= 0 else '*',
                                       str(buttonCount) if buttonCount >= 0 else '*')

    def __CompositeIdentifier(self) -> str:
        return "{}-{}-{}:{}:{}".format(self.__deviceName, self.__guid, self.__axesCount, self.__hatsCount, self.__buttonsCount)

    def __CompositeAbstractIdentifier(self) -> str:
        return "{}-{}-*:*:*".format(self.__deviceName, self.__guid)

    @staticmethod
    def __HasOpposite(item: InputItem) -> bool:
        return item.Item in (InputItem.ItemJoy1Up, InputItem.ItemJoy1Left,
                             InputItem.ItemJoy2Up, InputItem.ItemJoy2Left)

    @staticmethod
    def __CreateOpposite(item: InputItem) -> (int, InputItem):
        opposites: Dict[int, int] = \
        {
            InputItem.ItemJoy1Up  : InputItem.ItemJoy1Down ,
            InputItem.ItemJoy1Left: InputItem.ItemJoy1Right,
            InputItem.ItemJoy2Up  : InputItem.ItemJoy2Down,
            InputItem.ItemJoy2Left: InputItem.ItemJoy2Right,
        }
        if item.Item in opposites:
            if item.IsAxis:
                opposite = item.Copy(opposites[item.Item], -item.Value)
                return opposite.Item, opposite
            elif item.IsHat:
                opposite = item.Copy(opposites[item.Item], Controller.OppositeHat[item.Value])
                return opposite.Item, opposite
            else:
                Log("[Configgen.Controller] Buttons mapped on analog joystick. Opposite calculation aborted")
                return item.Item, item

        raise ValueError

    # Load all controllers from the input.cfg
    @staticmethod
    def LoadControllerConfigurationFromFile() -> ControllerCollection:
        controllers: ControllerCollection = {}
        import xml.etree.ElementTree as ET
        tree = ET.parse(pegasusInputs)
        root = tree.getroot()
        for controller in root.findall(".//inputConfig"):
            newController = Controller(deviceName=controller.get("deviceName"), typeName=controller.get("type"),
                                       guid=controller.get("deviceGUID"), playerIndex=0, sdlIndex=0, inputs=None,
                                       devicePath="", axesCount=int(controller.get("deviceNbAxes")),
                                       hatsCount=int(controller.get("deviceNbHats")),
                                       buttonCount=int(controller.get("deviceNbButtons")))
            controllers[newController.__CompositeIdentifier()] = newController
            controllers[newController.__CompositeAbstractIdentifier()] = newController
            for inp in controller.findall("input"):

                # Read input item attributes
                itemName  = inp.get("name" , None)
                itemType  = inp.get("type" , None)
                itemId    = inp.get("id"   , None)
                itemValue = inp.get("value", None)
                itemCode  = inp.get("code" , None)
                if itemName  is None: Log("[Controller] Missing name in pad definition");  continue;
                if (len(itemName) == 0): Log("[Controller] Missing name in pad definition");  continue;
                if itemType  is None: Log("[Controller] Missing type in pad definition");  continue;
                if (itemId   is None) or (not itemId.isdigit()): Log("[Controller] Missing/Wrong id in pad definition"); continue;
                if itemValue is None: Log("[Controller] Missing value in pad definition"); continue;
                if itemCode  is None: Log("[Controller] Missing code in pad definition");  continue;

                # Store input item
                inputItem = InputItem(itemName, itemType, int(itemId), int(itemValue), int(itemCode))
                newController.__inputs[inputItem.Item] = inputItem

                # Create opposite?
                if Controller.__HasOpposite(inputItem):
                    (oppositeItem, itemOpposite) = Controller.__CreateOpposite(inputItem)
                    #recheck if not already exists in input - new method to manage opposites since 13/10/2022
                    Log("inputItem.Name : " + inputItem.Name)
                    Log("oppositeItem.Name : " + itemOpposite.Name)
                    oppositeFound = False
                    for checkinp in controller.findall("input[@name='" + itemOpposite.Name + "']"):
                        # Read input name only to know if opposite exists already in conf
                        Log("Found Name in file : " + checkinp.get("name" , None));
                        oppositeFound = True
                    if not oppositeFound : #not found in configuration of inputs.cfg
                        newController.__inputs[oppositeItem] = itemOpposite

        return controllers

    @staticmethod
    def __LoadStrFromKwargs(key: str, defaultvalue: str, **kwargs) -> str:
        value: Optional[str] = kwargs.get(key, None)
        return value if value is not None else defaultvalue

    @staticmethod
    def __LoadIntFromKwargs(key: str, defaultvalue: int, **kwargs) -> int:
        value: Optional[str] = kwargs.get(key, None)
        return int(value) if value is not None else defaultvalue

    @staticmethod
    def __StoreNaturalIndexes(controllers: ControllerPerPlayer):
        orderedControllers: ControllerCollection = {}
        for controller in controllers.values():
            orderedControllers[controller.DevicePath] = controller
        i = 0
        for key in sorted(orderedControllers):
            controller: Controller = orderedControllers[key]
            controller.__naturalIndex = i
            i += 1

    @staticmethod
    def LoadDemoControllerConfigurations(**kwargs) -> Dict[int, int]:
        startPerPlayer: Dict[int, int] = {}

        controllers: ControllerCollection = Controller.LoadControllerConfigurationFromFile()
        for i in range(1, 11):
            nbbuttons: int = Controller.__LoadIntFromKwargs('p{}nbbuttons'.format(i), -1, **kwargs)
            nbhats: int    = Controller.__LoadIntFromKwargs('p{}nbhats'.format(i), -1, **kwargs)
            nbaxes: int    = Controller.__LoadIntFromKwargs('p{}nbaxes'.format(i), -1, **kwargs)
            dev: str       = Controller.__LoadStrFromKwargs('p{}devicepath'.format(i), "missing-device", **kwargs)
            guid: str      = Controller.__LoadStrFromKwargs('p{}guid'.format(i), "missing-guid", **kwargs)
            name: str      = Controller.__LoadStrFromKwargs('p{}name'.format(i), "missong-name", **kwargs)
            index: int     = Controller.__LoadIntFromKwargs('p{}index'.format(i), -1, **kwargs)

            newController = Controller.__FindBestControllerConfig(controllers, i, guid, index, name, dev, nbaxes, nbhats, nbbuttons)
            if newController:
                startPerPlayer[i] = newController.__inputs[InputItem.ItemStart].Id

        return startPerPlayer

    @staticmethod
    def LoadUserControllerConfigurations(**kwargs) -> ControllerPerPlayer:
        playerControllers: ControllerPerPlayer = {}

        controllers: ControllerCollection = Controller.LoadControllerConfigurationFromFile()
        for i in range(1, 11):
            index: int     = Controller.__LoadIntFromKwargs('p{}index'.format(i), -1, **kwargs)
            name: str      = Controller.__LoadStrFromKwargs('p{}name'.format(i), "missing-name", **kwargs)
            guid: str      = Controller.__LoadStrFromKwargs('p{}guid'.format(i), "missing-guid", **kwargs)
            dev: str       = Controller.__LoadStrFromKwargs('p{}devicepath'.format(i), "missing-device", **kwargs)
            nbaxes: int    = Controller.__LoadIntFromKwargs('p{}nbaxes'.format(i), -1, **kwargs)
            nbhats: int    = Controller.__LoadIntFromKwargs('p{}nbhats'.format(i), -1, **kwargs)
            nbbuttons: int = Controller.__LoadIntFromKwargs('p{}nbbuttons'.format(i), -1, **kwargs)

            newController = Controller.__FindBestControllerConfig(controllers, i, guid, index, name, dev, nbaxes, nbhats, nbbuttons)
            if newController:
                playerControllers[i] = newController

        Controller.__StoreNaturalIndexes(playerControllers)

        return playerControllers

    @staticmethod
    def __FindBestControllerConfig(controllers: ControllerCollection, playerIndex: int, guid: str, sdlIndex, deviceName: str,
                                   devicePath: str, axesCount: int, hatsCount: int, buttonsCount: int) -> Optional[Controller]:
        compositeId = Controller.__CompositeIdentifierFrom(deviceName, guid, axesCount, hatsCount, buttonsCount)
        if compositeId in controllers:
            controller = controllers[compositeId]
            return Controller(deviceName=deviceName, typeName=controller.Type, guid=guid,
                              playerIndex=playerIndex, sdlIndex=sdlIndex, inputs=controller.__inputs,
                              devicePath=devicePath, axesCount=axesCount, hatsCount=hatsCount,
                              buttonCount=buttonsCount)
        return None

    @staticmethod
    def GenerateSDLGameDatabase(controllers, outputFile ="/tmp/gamecontrollerdb.txt"):
        finalData = ["# Recalbox controller list"]
        for idx, controller in controllers.items(): finalData.append(controller.generateSDLGameDBLine())
        with open(outputFile, "w") as text_file: text_file.write("\n".join(finalData))
        return outputFile

    @staticmethod
    def GenerateSDLPadsOrderConfig(controllers):
        res = ""
        for idx, controller in controllers.items():
            if res != "":
                res = res + ";"
            res = res + str(controller.SdlIndex)
        return res

    @staticmethod
    # to create a evmapy json file to well configure buttons from Sinden lightgun keyboard mapping to exit game
    def createEvmapySindenGameExitP1(keyboardnode: str):
        configfile = "/var/run/evmapy/{}.json" .format(keyboardnode.replace("/dev/input/", ""))
        Log("Sinden Lightgun Player 1 config file is {}" .format(configfile))
        # create mapping
        padConfig = {}
        padConfig["actions"] = []
        padConfig["actions"].append({
                                     "trigger": [ "KEY_ESC", "KEY_ENTER" ],
                                     "type": "key",
                                     "target": [ "KEY_LEFTMETA" ]
                                   })
        padConfig["grab"] = False #in case of Sinden Lightgun we doesn't want to grab to avoid issue with emulator itself and especially with escape/enter key
        padConfig["axes"] = []
        padConfig["buttons"] = []
        padConfig["buttons"].append({
                                     "name": "KEY_LEFTMETA",
                                     "code": 125
                                   })
        padConfig["buttons"].append({
                                     "name": "KEY_ENTER",
                                     "code": 28
                                   })
        padConfig["buttons"].append({
                                     "name": "KEY_ESC",
                                     "code": 1
                                   })

        # example of content expected for wiimote commands
        # {
            # "actions": [
                # {
                    # "trigger": "KEY_ESC",
                    # "type": "key",
                    # "target": [ "KEY_5" ]
                # },
                # {
                    # "trigger": "KEY_ENTER",
                    # "type": "key",
                    # "target": [ "KEY_1" ]
                # },
                # {
                    # "trigger": "KEY_LEFTMETA",
                    # "type": "key",
                    # "target": [
                        # "KEY_LEFTALT",
                        # "KEY_F4"
                    # ]
                # }
            # ],
            # "grab": true,
            # "axes": [],
            # "buttons": [
                # {
                    # "name": "KEY_LEFTMETA",
                    # "code": 125
                # },
                # {
                    # "name": "KEY_ENTER",
                    # "code": 28
                # },
                # {
                    # "name": "KEY_ESC",
                    # "code": 1
                # }
            # ]
        # }
        # save config file (overwriting in all cases)
        with open(configfile, "w+") as fd:
            fd.write(json.dumps(padConfig, indent=4))

    @staticmethod
    # to create a evmapy json file to well configure buttons from Player 2 lightgun keyboard mapping to start or select/coin
    def createEvmapyStartSelectP2(keyboardnode: str):
        configfile = "/var/run/evmapy/{}.json" .format(keyboardnode.replace("/dev/input/", ""))
        Log("Player 2 Lightgun config file is {}" .format(configfile))
        # create mapping
        padConfig = {}
        padConfig["actions"] = []
        # for select/coin, to switch from "escape" to "C" (first letter of "COIN" ;-)
        padConfig["actions"].append({
                                     "trigger": [ "KEY_ESC" ],
                                     "type": "key",
                                     "target": [ "KEY_C" ]
                                   })
        # for start, to switch from "enter" to "S" (first letter of "START" ;-)
        padConfig["actions"].append({
                                     "trigger": [ "KEY_ENTER" ],
                                     "type": "key",
                                     "target": [ "KEY_S" ]
                                   })
        padConfig["grab"] = True #in this case, we need to grad to avoid to use default commands
        padConfig["axes"] = []
        padConfig["buttons"] = []
        padConfig["buttons"].append({
                                     "name": "KEY_ENTER",
                                     "code": 28
                                   })
        padConfig["buttons"].append({
                                     "name": "KEY_ESC",
                                     "code": 1
                                   })

        # save config file (overwriting in all cases)
        with open(configfile, "w+") as fd:
            fd.write(json.dumps(padConfig, indent=4))

    @staticmethod
    # to create a evmapy json file to well configure buttons from Player 3 lightgun keyboard mapping to start or select/coin
    def createEvmapyStartSelectP3(keyboardnode: str):
        configfile = "/var/run/evmapy/{}.json" .format(keyboardnode.replace("/dev/input/", ""))
        Log("Player 3 Lightgun config file is {}" .format(configfile))
        # create mapping
        padConfig = {}
        padConfig["actions"] = []
        # for select/coin, to switch from "escape" to "O" (second letter of "COIN" ;-)
        padConfig["actions"].append({
                                     "trigger": [ "KEY_ESC" ],
                                     "type": "key",
                                     "target": [ "KEY_O" ]
                                   })
        # for start, to switch from "enter" to "T" (second letter of "START" ;-)
        padConfig["actions"].append({
                                     "trigger": [ "KEY_ENTER" ],
                                     "type": "key",
                                     "target": [ "KEY_T" ]
                                   })
        padConfig["grab"] = True #in this case, we need to grad to avoid to use default commands
        padConfig["axes"] = []
        padConfig["buttons"] = []
        padConfig["buttons"].append({
                                     "name": "KEY_ENTER",
                                     "code": 28
                                   })
        padConfig["buttons"].append({
                                     "name": "KEY_ESC",
                                     "code": 1
                                   })

        # save config file (overwriting in all cases)
        with open(configfile, "w+") as fd:
            fd.write(json.dumps(padConfig, indent=4))

    @staticmethod
    # to create a evmapy json file to well configure buttons from "Model2emu" Player 1 lightgun keyboard mapping to start or select/coin
    def createEvmapyModel2emuStartSelectP1(keyboardnode: str):
        configfile = "/var/run/evmapy/{}.json" .format(keyboardnode.replace("/dev/input/", ""))
        Log("Model2emu Player 1 Lightgun config file is {}" .format(configfile))
        # create mapping
        padConfig = {}
        padConfig["actions"] = []
        padConfig["actions"].append({
                                     "trigger": "KEY_ESC",
                                     "type": "key",
                                     "target": [ "KEY_5"]
                                   })
        padConfig["actions"].append({
                                     "trigger": "KEY_ENTER",
                                     "type": "key",
                                     "target": [ "KEY_1" ]
                                   })
        #for lightgun with home button/hotkey
        padConfig["actions"].append({
                                     "trigger": "KEY_LEFTMETA",
                                     "type": "exec",
                                     "target": [ "xdotool key alt+F4 ; sleep 2.0 ; /usr/wine/wineserver -k" ]
                                     #"type": "key",
                                     #"target": [ "KEY_LEFTALT" , "KEY_F4" ]
                                   })
        #for lightgun without home button/hotkey
        padConfig["actions"].append({
                                     "trigger": [ "KEY_ESC", "KEY_ENTER" ],
                                     "type": "exec",
                                     "target": [ "xdotool key alt+F4 ; sleep 2.0 ; /usr/wine/wineserver -k" ]
                                     #"type": "key",
                                     #"target": [ "KEY_LEFTALT" , "KEY_F4" ]
                                   })
        padConfig["grab"] = True #in case of wiimote we want to grab to avoid issue with emulator itself and especially with escape key
        padConfig["axes"] = []
        padConfig["buttons"] = []
        padConfig["buttons"].append({
                                     "name": "KEY_LEFTMETA",
                                     "code": 125
                                   })
        padConfig["buttons"].append({
                                     "name": "KEY_ENTER",
                                     "code": 28
                                   })
        padConfig["buttons"].append({
                                     "name": "KEY_ESC",
                                     "code": 1
                                   })
        # save config file (overwriting in all cases)
        with open(configfile, "w+") as fd:
            fd.write(json.dumps(padConfig, indent=4))

    @staticmethod
    # to create a evmapy json file to well configure buttons from "Model2emu" Player 1 lightgun keyboard mapping to start or select/coin
    def createEvmapyModel2emuStartSelectP2(keyboardnode: str):
        configfile = "/var/run/evmapy/{}.json" .format(keyboardnode.replace("/dev/input/", ""))
        Log("Model2emu Player 1 Lightgun config file is {}" .format(configfile))
        # create mapping
        padConfig = {}
        padConfig["actions"] = []
        padConfig["actions"].append({
                                     "trigger": "KEY_ESC",
                                     "type": "key",
                                     "target": [ "KEY_6"]
                                   })
        padConfig["actions"].append({
                                     "trigger": "KEY_ENTER",
                                     "type": "key",
                                     "target": [ "KEY_2" ]
                                   })
        padConfig["grab"] = True #in case of wiimote we want to grab to avoid issue with emulator itself and especially with escape key
        padConfig["axes"] = []
        padConfig["buttons"] = []
        padConfig["buttons"].append({
                                     "name": "KEY_ENTER",
                                     "code": 28
                                   })
        padConfig["buttons"].append({
                                     "name": "KEY_ESC",
                                     "code": 1
                                   })
        # save config file (overwriting in all cases)
        with open(configfile, "w+") as fd:
            fd.write(json.dumps(padConfig, indent=4))

    @staticmethod
    # to find/confirm dolphin bar/sinden lightgun(s) is/are present and provide indexes
    def findLightguns(emulatorName: str = "") -> (List, bool): #manage specific case using the emulatorName (optional)
        playerindex = 0
        nodeindex = 0
        
        ## re-init for all indexes before searching
        ## first value is "node" index (used by retroarch)
        ## second value is "device" name
        ## third value is "mouse" index (used by some standalone emulator)
        MouseIndexByPlayer = [["nul","nul","nul"],["nul","nul","nul"],["nul","nul","nul"]]

        NeedPotentiallySindenBorder = False
        ## Take order and number of mouse from udev DB
        p = subprocess.Popen('udevadm info -e | grep /dev/input/mouse', shell=True,
                             stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output1 = p.stdout
        for line1 in output1:
            mousenode = line1.replace("E: DEVNAME=", "").rstrip()
            Log('mouse node found: ' + line1.rstrip())

            p = subprocess.Popen('cat /proc/bus/input/devices | grep -ni {}'.format(mousenode.replace("/dev/input/", "").rstrip()), shell=True,
                                 stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output2 = p.stdout

            p = subprocess.Popen("udevadm info " + mousenode + " | grep -ni S: | awk -F'S:' '{print $2}'", shell=True,
                                 stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output3 = p.stdout

            p = subprocess.Popen('udevadm info {} | grep -ni ID_INPUT_MOUSE'.format(mousenode), shell=True,
                                 stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output4 = p.stdout
            for line2 in output2:
                Log('   Handlers found:' + line2.rstrip())
                mouseIndex = line2.rstrip().split('mouse')[1][0]
                Log('   MouseIndex found:' + mouseIndex)
                device = output3.readline().rstrip()
                Log("   Device found : " + device)
                path = output3.readline().strip()
                Log("   Path found : " + path)
                id_input_mouse = output4.readline().rstrip()
                Log("   ID_INPUT_MOUSE found : " + id_input_mouse)
                #is it dolphin bar at this index ?
                if (re.search("Mayflash_Wiimote_PC_Adapter",device)):
                    MouseIndexByPlayer[playerindex][0] = str(nodeindex)
                    MouseIndexByPlayer[playerindex][1] = "mayflash"
                    MouseIndexByPlayer[playerindex][2] = mouseIndex
                    Log('           Player : ' + str(playerindex+1))
                    Log('           Mouse Index for UDEV : ' + MouseIndexByPlayer[playerindex][0])
                    Log('           Mouse device named : ' + MouseIndexByPlayer[playerindex][1])
                    Log('           Mouse Index from handlers : ' + MouseIndexByPlayer[playerindex][2])
                    playerindex = playerindex + 1
                    #search keyboard corresponding keyboard
                    searchpath = path.replace("-mouse", "")[:-1] + "*-kbd"
                    Log("   Search Path for Mayflash Dolphin bar Keyboard : '" + searchpath + "'")
                    p = subprocess.Popen('udevadm info /dev/{} | grep DEVNAME='.format(searchpath), shell=True,
                                         stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
                    p.wait()
                    output5 = p.stdout
                    keyboardnode = output5.readline().replace("E: DEVNAME=", "").rstrip()
                    Log("   Mayflash Dolphin bar Keyboard event found : " + keyboardnode)
                    if playerindex == 1: # for player 1
                        if emulatorName == "model2emu":
                            Controller.createEvmapyModel2emuStartSelectP1(keyboardnode)
                    elif playerindex == 2: # for player 2
                        if emulatorName == "model2emu":
                            Controller.createEvmapyModel2emuStartSelectP2(keyboardnode)
                        else:
                            Controller.createEvmapyStartSelectP2(keyboardnode)
                    elif playerindex == 3: # for player 3
                        Controller.createEvmapyStartSelectP3(keyboardnode)
                #or is it sinden lightgun at this index ? 
                elif (re.search("Unknown_SindenLightgun",device)):
                    MouseIndexByPlayer[playerindex][0] = str(nodeindex)
                    MouseIndexByPlayer[playerindex][1] = "sinden"
                    MouseIndexByPlayer[playerindex][2] = mouseIndex
                    Log('           Player : ' + str(playerindex+1))
                    Log('           Mouse Index for UDEV : ' + MouseIndexByPlayer[playerindex][0])
                    Log('           Mouse device named : ' + MouseIndexByPlayer[playerindex][1])
                    Log('           Mouse Index from handlers : ' + MouseIndexByPlayer[playerindex][2])
                    playerindex = playerindex + 1
                    #we set this flag to inform that we could need potentially a border for sinden lightgun if lightgun game is launched
                    NeedPotentiallySindenBorder = True
                    #search keyboard corresponding keyboard
                    searchpath = path.replace("-mouse", "") + "*-kbd"
                    Log("   Search Path for Sinden Lightgun Keyboard : '" + searchpath + "'")
                    p = subprocess.Popen('udevadm info /dev/{} | grep DEVNAME='.format(searchpath), shell=True,
                                         stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
                    p.wait()
                    output5 = p.stdout
                    keyboardnode = output5.readline().replace("E: DEVNAME=", "").rstrip()
                    Log("   Sinden Lightgun Keyboard event found : " + keyboardnode)
                    if playerindex == 1: #only manage player 1, if it is a sinden, we ahve to manage start + select from gun to exit game
                        if emulatorName == "model2emu":
                            #include sinden lightgun case and constraint of model2emu
                            Controller.createEvmapyModel2emuStartSelectP1(keyboardnode)
                        else:
                            Controller.createEvmapySindenGameExitP1(keyboardnode)
                    elif playerindex == 2: # for player 2
                        if emulatorName == "model2emu":
                            Controller.createEvmapyModel2emuStartSelectP2(keyboardnode)
                        else:
                            Controller.createEvmapyStartSelectP2(keyboardnode)
                    elif playerindex == 3: # for player 3
                        Controller.createEvmapyStartSelectP3(keyboardnode)
                #count number of nodes found for the same devices    
                #we count spaces finally to know number of nodes
                #and only for MOUSE identified (and not TOUCHPAD for example)
                if(re.search("ID_INPUT_MOUSE=1",id_input_mouse)):
                    nodeindex = nodeindex + 1
                else:
                    Log("   WARNING: It's not considered as usual mouse for udev : ignored in the count of mouse index")
                Log("") #to jump one line in log
        return MouseIndexByPlayer, NeedPotentiallySindenBorder

    @staticmethod
    # to find/confirm mouse(s) is/are present and provide indexes (needed only if no lightgun connected)
    def findMice() -> List:
        playerindex = 0
        nodeindex = 0
        ## re-init for all indexes before searching
        ## first value is "node" index (used by retroarch)
        ## second value is "device" name
        ## third value is "mouse" index (used by some standalone emulator)
        MouseIndexByPlayer = [["nul","nul","nul"],["nul","nul","nul"],["nul","nul","nul"]]
        
        ## Take order and number of mouse from udev DB
        p = subprocess.Popen('udevadm info -e | grep /dev/input/mouse', shell=True,
                             stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output1 = p.stdout
        for line1 in output1:
            mousenode = line1.replace("E: DEVNAME=", "").rstrip()
            Log('mouse node found: ' + line1.rstrip())

            p = subprocess.Popen('cat /proc/bus/input/devices | grep -ni {}'.format(mousenode.replace("/dev/input/", "").rstrip()), shell=True,
                                 stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output2 = p.stdout

            p = subprocess.Popen("udevadm info " + mousenode + " | grep -ni S: | awk '{print $2}'", shell=True,
                                 stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output3 = p.stdout

            p = subprocess.Popen('udevadm info {} | grep -ni ID_INPUT_MOUSE'.format(mousenode), shell=True,
                                 stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
            p.wait()
            output4 = p.stdout
            
            for line2 in output2:
                Log('   Handlers found:' + line2.rstrip())
                mouseIndex = line2.rstrip().split('mouse')[1][0]
                Log('   MouseIndex found:' + mouseIndex)
                device = output3.readline().rstrip()
                Log("   Device found : " + device)
                path = output3.readline().strip()
                Log("   Path found : " + path)
                id_input_mouse = output4.readline().rstrip()
                Log("   ID_INPUT_MOUSE found : " + id_input_mouse)
                #Take all mouses in this case
                MouseIndexByPlayer[playerindex][0] = str(nodeindex)
                MouseIndexByPlayer[playerindex][1] = "mouse"
                MouseIndexByPlayer[playerindex][2] = mouseIndex
                Log('           Player : ' + str(playerindex+1))
                Log('           Mouse Index for UDEV : ' + MouseIndexByPlayer[playerindex][0])
                Log('           Mouse device named : ' + MouseIndexByPlayer[playerindex][1])
                Log('           Mouse Index from handlers : ' + MouseIndexByPlayer[playerindex][2])
                playerindex = playerindex + 1
                #count number of nodes found for the same devices    
                #we count spaces finally to know number of nodes
                #and only for MOUSE identified (and not TOUCHPAD for example)
                if(re.search("ID_INPUT_MOUSE=1",id_input_mouse)):
                    nodeindex = nodeindex + 1
                else:
                    Log("   WARNING: It could not be considered as usual mouse by Supermodel using evdev : ignored in the count of mouse index")
                Log("") #to jump one line in log
        return MouseIndexByPlayer

# Type def for convenience
ControllerCollection = Dict[str, Controller]
ControllerPerPlayer = Dict[int, Controller]