# recalbox-configgen
##Emulators configuration tools for pixl

*WIP*

The purpose of the lib is to automatically configure emulators, using command line args and environment available in pixl.

How to test:
```
python -m runtest discover -p "*test*"
```

How to start:
```
python configgen/emulatorlauncher.py  -system neogeo -rom myrom.zip
```
