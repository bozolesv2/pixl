#!/bin/bash
# example of call of this script for dev version using raw: ./generate_json.sh -t dev -f raw -v Dev
# example of call of this script for dev version using image: ./generate_json.sh -t dev -f image -v Dev
# example of call of this script for beta version using image: ./generate_json.sh -t beta -f image -v v35

#to manage mandatory parameters
helpFunction()
{
   echo "Usage: $0 -t update_type -f update_format -v version"
   echo "-t type of update as 'dev', 'beta' or 'release'"
   echo "-f format of update as 'image' (default) or 'raw'"
   echo "-v version of update as 'Beta 35' or '1.0'"
   exit 1 # Exit script after printing help
}

while getopts ":t:f:v:?" opt; do
   case "$opt" in
      # Define type of update: could be dev/beta/release
      t ) update_type="$OPTARG" ;;
      # Define format of update: could be "image" (for update and fresh install) or "raw" (decompressed version for "quickess" update only)
      f ) update_format="$OPTARG" ;;
      v ) update_version="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$update_type" ] || [ -z "$update_format" ] || [ -z "$update_version" ]
then
   echo "Some or all of the parameters are empty"
   helpFunction
fi

# Get the changelog content
changelog="../../CHANGELOG.md"
# Get arguments
start_marker="\[pixL-master\]"
end_marker="\[pixL-master\]"
# Read the file content into last 2 versions of pixL-master
version_content=$(grep -m1 -A100000 $start_marker $changelog | grep -m2 -B100000 $end_marker | head -n -1 )
#echo "$version_content"
# Escape any special characters in the content
body_content=$(printf '%s' "$version_content" | sed 's/\\/\\\\/g; s/"/\\"/g' | awk '{gsub(/\t/, "\\t"); print}')
body=""
# convert end of line by "\n" string
while IFS= read -r line; do
  #echo "Processing line: $line"
  body=$( echo "${body}${line}\n")
done <<< "$body_content"

# Targeted files/directories/urls
if [[ "$update_format" != "raw" ]]; then
  output_file="${update_type}-pixl-os.json"
  ressource_path="../update-resources/image_update"
  update_content_path="../../output/images/recalbox"
  # Base URL for download assets
  browser_download_url_base="https://updates.pixl-os.com/${update_type}/x86_64"
else
  output_file="${update_type}-${update_format}-pixl-os.json"
  ressource_path="../update-resources/raw_update"
  update_content_path="../../output/images/pc-boot"
  # Base URL for download assets
  browser_download_url_base="https://updates.pixl-os.com/${update_type}/x86_64_${update_format}"
fi

#JSON header data
name="pixL OS (${update_type^})"
draft=false
prerelease=true
created_at=$(LC_ALL=en_UK.UTF-8 date +%Y-%m-%dT%H:%M:%SZ)
published_at=$(LC_ALL=en_UK.UTF-8 date +%Y-%m-%dT%H:%M:%SZ)

# Initialize the JSON string
echo "[" > "$output_file"
echo "    {" >> "$output_file"
echo "        \"tag_name\": \"$update_version\"," >> "$output_file"
echo "        \"name\": \"$name\"," >> "$output_file"
echo "        \"published_at\": \"$published_at\"," >> "$output_file"
echo "        \"prerelease\": \"$prerelease\"," >> "$output_file"
echo "        \"draft\": \"$draft\"," >> "$output_file"
echo "        \"created_at\": \"$created_at\"," >> "$output_file"
echo "        \"body\": \"$body\"," >> "$output_file"
echo "        \"published_at\": \"$published_at\"," >> "$output_file"

#JSON assets data (from path to parse)
echo "        \"assets\": [" >> "$output_file"

# Define the directory path (install.sh/version.sh)
dir_path=$ressource_path
# Check if directory exists and has read permissions
if [[ ! -d "$dir_path" || ! -r "$dir_path" ]]; then
  echo "Error: Directory '$dir_path' does not exist or is not readable."
  exit 1
fi
# variable to manage "," needed if previous asset
previousasset=""
# Loop through each file in the directory
files=$(find "$dir_path" -type f -print)
for file in $files; do
  # Skip directories
  if [[ -d "$file" ]]; then
	continue
  fi
  echo "            {" >> "$output_file"
  # Get creation time using stat
  creation_time=$(LC_ALL=en_UK.UTF-8 stat "$file" | grep -i "Birth" | awk -F ' ' '{print $2"T"$3}' | awk -F '.' '{print $1"Z"}')
  # Get modify time using stat
  modify_time=$(LC_ALL=en_UK.UTF-8 stat "$file" | grep -i "Modify" | awk -F ' ' '{print $2"T"$3}' | awk -F '.' '{print $1"Z"}')
  if [[ -z "$creation_time" ]]; then
    creation_time=$modify_time
  fi
  #echo "File creation date/time (ISO 8601 format): $creation_time"
  #echo "File modify date/time (ISO 8601 format): $modify_time"

  # Get file size (bytes)
  #echo "$file"
  file_size=$(LC_ALL=en_UK.UTF-8 du -b "$file" | awk -F ' ' '{print $1}')

  # to remove dir_path
  # Assuming path separator is "/" (modify if different)
  path_separator="/"
  dir_path_length=${#dir_path}  # Get length of dir_path
  file_name=${file:((dir_path_length + 1))}  # Extract without root dir_path

  #echo "Filename without dir_path ${dir_path}: $file_name"
  # Add file information to JSON array (without the "root" directory)
  echo "                \"browser_download_url\": \"${browser_download_url_base}/${file_name}\"," >> "$output_file"
  echo "                \"created_at\": \"${creation_time}\"," >> "$output_file"
  echo "                \"name\": \"${file_name}\"," >> "$output_file"
  echo "                \"size\": ${file_size}," >> "$output_file"
  echo "                \"updated_at\": \"${modify_time}\"" >> "$output_file"
  echo -n "            }," >> "$output_file"
done

# Define the directory path for update (replace with your actual path)
dir_path=$update_content_path
# Check if directory exists and has read permissions
if [[ ! -d "$dir_path" || ! -r "$dir_path" ]]; then
  echo "Error: Directory '$dir_path' does not exist or is not readable."
  exit 1
fi
# variable to manage "," needed if previous asset
previousasset=""
# Loop through each file in the directory
files=$(find "$dir_path" -type f -print)
for file in $files; do
  # Skip directories
  if [[ -d "$file" ]]; then
	continue
  fi
  # To manage "," if previous asset exists
  if [[ -n "$previousasset" ]]; then
      echo "," >> "$output_file"
  fi
  
  echo "            {" >> "$output_file"
  
  # Get creation time using stat
  creation_time=$(LC_ALL=en_UK.UTF-8 stat "$file" | grep -i "Birth" | awk -F ' ' '{print $2"T"$3}' | awk -F '.' '{print $1"Z"}')
  # Get modify time using stat
  modify_time=$(LC_ALL=en_UK.UTF-8 stat "$file" | grep -i "Modify" | awk -F ' ' '{print $2"T"$3}' | awk -F '.' '{print $1"Z"}')
  if [[ -z "$creation_time" ]]; then
    creation_time=$modify_time
  fi
  #echo "File creation date/time (ISO 8601 format): $creation_time"
  #echo "File modify date/time (ISO 8601 format): $modify_time"

  # Get file size (bytes)
  #echo "$file"
  file_size=$(LC_ALL=en_UK.UTF-8 du -b "$file" | awk -F ' ' '{print $1}')

  # to remove dir_path
  # Assuming path separator is "/" (modify if different)
  path_separator="/"
  dir_path_length=${#dir_path}  # Get length of dir_path
  file_name=${file:((dir_path_length + 1))}  # Extract without root dir_path

  #echo "Filename without dir_path ${dir_path}: $file_name"
  # Add file information to JSON array (without the "root" directory)
  echo "                \"browser_download_url\": \"${browser_download_url_base}/${file_name}\"," >> "$output_file"
  echo "                \"created_at\": \"${creation_time}\"," >> "$output_file"
  echo "                \"name\": \"${file_name}\"," >> "$output_file"
  echo "                \"size\": ${file_size}," >> "$output_file"
  echo "                \"updated_at\": \"${modify_time}\"" >> "$output_file"
  echo -n "            }" >> "$output_file"
  #to flag that we already manage one asset
  previousasset=${file_name}
done

echo "" >> "$output_file"
echo "        ]" >> "$output_file"
echo "    }" >> "$output_file"
echo "]" >> "$output_file"

echo "File size information saved to: $output_file"

#cat $output_file
