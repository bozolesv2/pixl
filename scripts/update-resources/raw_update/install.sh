#!/bin/bash

#to manage mandatory parameters

helpFunction()
{
   echo "Usage: $0 -e existingVersion -n newVersion -c componentName"
   echo "-e Version of the existing installed version as '0.0.1'"
   echo "-n Version of the existing installed version as '0.0.2'"
   echo "-c name of the component as know by Pegasus"
   exit 1 # Exit script after printing help
}

while getopts ":e:n:c:?" opt; do
   case "$opt" in
      e ) existingVersion=${OPTARG} ;;
      n ) newVersion="$OPTARG" ;;
      c ) componentName="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

#echo for debug
#echo "$existingVersion"
#echo "$newVersion"
#echo "$componentName"

#initialization of progress and state
echo "0.1" > /tmp/$componentName/progress.log
echo "Installation of $componentName..." > /tmp/$componentName/install.log
echo "" > /tmp/$componentName/install.err
sleep 2

#authorize file system udpate on root
mount -o remount,rw /
mount -o remount,rw /boot

# Print helpFunction in case parameters are empty
if [ -z "$existingVersion" ] || [ -z "$newVersion" ] || [ -z "$componentName" ]
then
   echo "Some or all of the parameters are empty" > /tmp/$componentName/install.log
   echo "1" > /tmp/$componentName/install.err
   helpFunction
fi

#***************************************begin of part to customize in case of OS******************************************************
# since may 2024, share upgrade directory is used directly to avoid to saturate /boot/update and reduce number of copy in this process
UPDATEDIR=/boot/update
echo "0.2" > /tmp/$componentName/progress.log
sleep 1

#boot.lst should content files to update during the reboot after install
if test -n "$(find $UPDATEDIR -maxdepth 1 -name 'boot.lst' -print -quit)"
then
	echo "0.4" > /tmp/$componentName/progress.log
	sleep 1
else
	echo "No 'boot.lst' available in '$UPDATEDIR'" > /tmp/$componentName/install.log
	echo "3" > /tmp/$componentName/install.err
	exit 3
fi
#linux file is the linux kernel containing modules
if test -n "$(find $UPDATEDIR/boot -maxdepth 1 -name 'linux' -print -quit)"
then
	echo "0.6" > /tmp/$componentName/progress.log
	sleep 1
else
	echo "No 'linux' available in '$UPDATEDIR/boot'" > /tmp/$componentName/install.log
	echo "3" > /tmp/$componentName/install.err
	exit 3
fi
#recalbox file is the OS part
if test -n "$(find $UPDATEDIR/boot -maxdepth 1 -name 'recalbox' -print -quit)"
then
	echo "0.8" > /tmp/$componentName/progress.log
	sleep 1
else
	echo "No 'recalbox' available in '$UPDATEDIR/boot'" > /tmp/$componentName/install.log
	echo "3" > /tmp/$componentName/install.err
	exit 3
fi

echo "1.0" > /tmp/$componentName/progress.log
echo "Need to reboot to finalize installation" > /tmp/$componentName/install.log
#set 0 if end of installation without other action needed, -1 if need restart of Pegasus, -2 if need reboot
echo "-2" > /tmp/$componentName/install.err
#add flag file to avoid to launch splash video during install of update
echo "skip_video_splash" > /overlay/.configs/skipvideosplash
exit 0
#***************************************end of part to customize*****************************************************

