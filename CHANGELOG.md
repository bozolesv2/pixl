# Change Log
All notable changes to this project will be documented in this file (focus on change done on pixL-master branch).

## [pixL-master] - Beta 1.37 - 2024-09-16
- Emulation:
	- Suyu:
		- remove
	- Yuzu:
		- remove
	- bump for support retroachievements for dolphin wii/gamecube emulator (version 2407.68 - July 16 2024)
	- add retroachievements to Gamecube system

- OS:
	- introduce mount of data storage data from additional "internal" partition(s) and in .../share/internals/drive0 to drive 7
	- add interaction with Pegasus http api for usbmount
	- fix on cd-rom mount with several detection
	- new service script to mount directories dynamically from configuration
	- retrode & usb-nes rom dumper introduction/detection
	- add tools for nes roms to well manage header/checksum from dumpers
	- be able to manage sr0 to sr9 device for cd-rom reader
	- manage hardware ejection and link with pegasus-frontend
	- add mappings for SNES original controllers in input.cfg using retrode ports

- Buildroot:
	- to have device name from script called by usbmount rules

- Configgen:
	- Dolphin:
		- add Log function in DolphinControllers
		- add driver detection to know XINPUT/DINPUT compatibility
		- add evdev events buttons collection for XINPUT device
		- fix to have until 4 players activated for simulated wiimote
		- fix to respect position of A/B or 2/1 buttons of wiimote simulated when we use gamepad
		- fix to well detect xinput for hotkeys also
		- uniformization of dophin/dolphin-triforce to help maintenance
		- add overlay management for 4/3 Gamecube games
		- 2 mappings possible now for emulated wiimote now
		- wiimote orientation (upright/sideway) now available in game
		- activation/deactivation of nunchuk
	- Dolphin-Triforce:
		- fix to replicate changes for XINPUT/DINPUT controllers
		- force to 16/9 if widescreen hack activated
		- uniformization of dophin/dolphin-triforce to help maintenance

- Pegasus-Frontend 0.1.8:
	See here for more details on features and fixes: https://github.com/pixl-os/pegasus-frontend/blob/pixL-master/CHANGELOG.md

- gameOS-pixL 1.28:
	See here for more details on features and fixes: https://github.com/pixl-os/gameOS/blob/pixL-master/CHANGELOG.md

## [pixL-master] - Release 1.36 - 2024-07-08
- Buildroot 2023.08.2
	- new features:
		- add refind and refind-bin from https://sourceforge.net/projects/refind/

	- updates:
		- refind 0.14.2
		- add & update vulkan-headers 1.3.288
		- add & update vulkan-tools 1.3.288 link with vulkan-headers
		- add & update vulkan-loader 1.3.288 link with vulkan-headers
		- add & update spirv-tools 1.3.283.0
		- add & update spirv-headers 1.3.283.0
		- fix warning vulkan-tools, vulkan-loader when you launch a build

- OS
	- change example of json file for beta online update and site download
	- fixes:
		- force nvidia driver for vulkan if nvidia drivers installed (to avoid issue if "double" GPUs detected and have only 1 GPU declared in vulkan)
		- ignore all bin into the package folder use by update online
		- deactivate information popup activated by BTN_BASE (tested previously for steamdeck and from triggerhappy)
	    - fix install.sh to clean /boot/update before moving files during update online
	- pixl update/upgarde improvements :
		- fix install.sh to clean /boot/update before moving files during update online
		- fix to use rsync to have progression during big file copy/move
		- refactoring to be able to install from share directly after reboot
		- add best support of framebuffer to manage video before X launch
		- activate nvidia framebuffer to display image during update/boot/console
		- add cleaning of update directories in case of issue
		- clean all before to reboot in all cases
		- separate or group progress display for offline/online updates
		- come back to delete files asynchronously due to issue of space not free after update
		- detect update online or offline and don't display video in this case
		- fix to shutdown and reboot to well load framebuffer drivers
		- fix to using finally the SKIPVIDEOSPLASHFLAG for online update also
		- update .png file used for install to remove taglines
		- removing of mp4 video not used and obsolete now
		- remove offline or online specific files to simplify
		- add new image for update without tag lines
		- use new upgrade to see better progression during upgrade
		- fix vertical update pictures (change exif tag by rotation)
		- reduce install script since download directly to share now
		- add parameter to manage 'format' of update (as image/raw) for install.sh
		- add set of generic scripts for json generation/ftp upload for pixL upgrade from pixL site
		- add set of generic scripts to create github version/github upload for core/emulator update from github
		- add specific scripts to launch update of libretro mame cores (including .dats generation) and get mame repo information
		- add additional check/messages in github upload script
		- change name of repo targeted for libretro mame versions
		- first set of file to do online update and automatized for standalone mame

	- Change size-partition boot 6Gb to 9Gb
	- Add media for games into share_init
	- bootloader: chainload to refind in EFI mode adding support for probing OS and mouse/touchscreen
	- Nvidia drivers
		- Bump NVIDIA Modprobe 535.146.02 -> 550.78
		- Bump NVIDIA DRIVERS 535.146.02 -> 550.78
			- Fixed a bug that could cause the GPU driver to hang when running some Vkd3d games, such as F1 2021.
			- Fixed a bug that caused eglExportDMABUFImageQueryMESA() to return invalid DRM formats for the images that use the sRGB color space.
			- Fixed a bug that caused wgpu applications to hang on Wayland.
			- Fixed a bug that caused "Flip event timeout" messages to be printed to the system log when the system is suspended without using /usr/bin/nvidia-sleep.sh when nvidia-drm is loaded with the `fbdev=1` kernel module parameter.
			- Updated the nvidia-settings control panel to ensure that the entire Display Configuration page can be used when the Layout window is shown.
			- Updated the nvidia-settings control panel to allow the primary display to be set on any GPU in a multi-GPU system.
			- Fixed Xid error when playing Alan Wake 2 with ray tracing enabled.
		- Fixed symlink vulkan x86_64 and i686
	- add vulkan-volk 1.3.283.0 for vulkan-tools 1.3.288
	- update vulkan-samples Dec 18, 2023 -> Jun 05, 2024
	- fix openbox to work better on legacy pc (Intel core 2 duo, nouveau drivers)
	- fix openbox to switch between windows and to ignore feh background
	- fix to be able to launch build in background using script "recalboxdocker-in-background.sh"
	
- Emulation:
	- fix easyrpg rom2fs configuration to use only .ini game file and not RPG_RT.ini as historically (could help to scrap simpler now)
	- libretro Caprice 32:
		- bump from version 4.5.3 to 4.5.4 (see https://github.com/libretro/libretro-cap32/blob/449e9239098423fea69b48db3f47a3a60d5f31f1/CHANGES.md)
		- activate netplay and more extension for amstrad cpc and gx4000
	- libretro mame fix many games don't work [316]
	- Suyu:
		- Add suyu standalone emulator for switch
		- bump commit on May 29, 2024
		- Change location folder nand for firmware and save game
		- Change repo official
	- Pcsx2:
		- update to v1.7.5326
	- Xemu:
		- Update to v0.7-122
		- Update link repo
	- Cemu:
		- Update to v2.0-86
		- Update patch
		- add update online
	- solarus-engine:
		- add this package Apr 27, 2024
		- remove old version by recalbox
	- citra-emu:
		- change repo
		- update patch
		- update package Jun 16, 2024
	- Dolphin-emu(gamecube+wii):
		- update package Jun 27, 2024
	- Mame(standalone):
		- bump and patches for 0.265
		- add online update
		- fix to set "sdljoy" joystick controllers configuration to stay compatible with configgen
		- fix to remove 2 legacy patches
	- Libretro Mame:
		- fix to well display commit in version in retroarch or pegasus-frontend

- Configgen:
		- fix to use model "6128+ (experimental)" now in libretro cap32 core for Amstrad gx4000 console [302]
		- new configgen to manage suyu
		- fix to force language to "en_US" to avoid issue during installation and settings of wine

- Pegasus-Frontend 0.1.7:
	- Fixes:
		- fix ip display in settings menu
		- reduce size of navigation.wav file sound effect to win in velocity during scrollling of sound bar
	- Features:
		- add new menu for emulators configuration
		- add dedicated qml(menu) for suyu
		- add vsync option for suyu
		- pixL upgrade/update improvements (see 
		- add Cemu update online
		- add Standalone Mame online update (mamedev-mame) + Change repo naming for libretro mame (libretro-mame) (should be change in pixL repo after pixL Beta 36 deployement)
	See here for more details on features and fixes: https://github.com/pixl-os/pegasus-frontend/blob/pixL-master/CHANGELOG.md

- gameOS-pixL 1.27:
	- add missing color logos for port and engine ones (still to do for the others... steel, carbon, white and black)
	- reduce size of .wav file sound effect to win in velocity during navigation

## [pixL-master] - Beta35 - 2024-05-08
- Buildroot 2023.08.2
	- new features:
		- active support for gpu and igpu amd vulkan api
		- Enable full mesa3d OpenGL (ES, EGL, GLX) and OpenCL api

	- updates:
		- llvm 15.0.3 -> 17.0.6
		- qt6 6.4.3 -> 6.6.1
		- sdl2 2.28.1 -> 2.28.5
		- vulkan headers 1.3.257 -> 1.3.270
		- fmt 10.0.0 -> 10.1.1

	- fixes:
		- fix dolphin-triforce build since qt6.6.1 update
		- fix qt6-for-emulators
		- fix qt5-qmake-for-prgasus
		- fix add host-libcurl for mangohud & cemu
		- fix glm: dbuild_shared_libs=on
		- fix vulkan-headers depends
		- fix qt5-webengine-chromium
		- fix skeleton
		- fix usbmount
		- fix skeleton-init
		- fix sdl_sound for sdl2
		- fix mosquitto customize S50mosquitto to S29mosquitto
		- fix initscripts customize rck and rcs fir init.d launch
		- fix dhcpcd change S41 to S10 for network share
		- fix dropbear
		- fix bluez5_utils
		- fix binutils add strings on binutils_tools
		- fix nodejs patch v8-qemu-wrapper and mk
		- fix openbox shorcut alt-tab and alt-F4
		- fix steamdeck lcd device to use vulkan by default
		- fix quickly R2 mistake for Xbox 360 Wireless Receiver in input.cfg

- Emulation:
	- yuzu
		- bump 0-1676
		- bump qt6
		- fix sdl2 crc16
		- fix to use local binary for the moment
	- Cemu:
		- Update to v2.0-61
		- Move shaders cache in /share/saves/wiiu/
		- Move keys to /share/bios/wiiu/cemu/
	- Pcsx2:
		- update to v1.7.5320 
		- support  Cheevos retroachievement
		- Move folder bios to share/bios/ps2/pcsx2/
		- support to Qt6
		- fix on controllers mapping and order
		- remove unused patches
	- Duckstation:
		- update to v0.1-6168
		- support retroachievement
		- support Qt6
	- Citra-emu:
		- Bump to nightly-2062
		- support vulkan api
		- support for Qt6
		- fix to use local binary for the moment
	- Rpcs3
		- Add rpcs3 emulator for sony playstation 3
		- Game format iso supported
		- Auto launch per game configs
		- fix to remove Open GL ES for the moment, to avoid retroarch to use it (issue with libretro-pcsx2 core)
	- Libretro-Mame:
		- Bump to 0.265 (April 2024)

- OS:
	- for lightguns:
		- add package for sinden lightgun integration (using v2.05c)
		- add camera support from kernel for sinden lightgun
		- add more UVC support from kernel
		- new service to launch sinden lightgun at start or at plug
		- use configuration file from share (to be updatable)
		- new script/udev rules to manage add/remove and to count
		- add random wait function to avoid to add device in parallel
		- add Python Imaging Library to manipulate overlays to add borders
		- set sinden lightgun default buttons as expected by existing game mappings from lightgun.cfg
		- add any default sega model 2 overlay
		- activation/deactivation of crosshair/overlays/scanlines from .lua scripts in model2emu and for all games
		- add rules and script to manage detection/count of doplhin bar in mode 1/2 (to display crosshair in Pegasus-Frontend)
		- avoid to restart when all sinden lightguns are unplugged
		- set lightgun property for model3 from romfs2
	- active retroachievements for PS2 system from romfs2
	- add simply management of sound multimedia keyboard keys in openbox (volume+, volume- and mute for the moment)
	- add package vdpauinfo 1.5
	- Nvidia drivers [299]
		- Bump NVIDIA DRIVERS 535.113.01 -> 535.146.02
		- Bump NVIDIA Modprobe 535.113.01 -> 535.146.02
		- Bump vulkan headers 1.3.273
		- Bump vulkan loader 1.3.273
		- vulkan tools 1.3.273
		- vulkan sample commit 2023/12/19
		- spirv headers vulkan 1.3.268.0
		- spirv tools vulkan 1.3.268.0
		- fix missing libOpenGl for nvidia-install
	- input.cfg controller mappings:
		- add new mapping of dragonrise usb arcade encoder
		- add new mapping for Switch Online N64 controller
		- add new mapping for Switch Online SNES/MD controllers
		- add steam deck lcd model controller
		- fix 8bitdo sfc30 usb and bluetooth mappings
		- fix mapping for MAYFLASH N64 Controller Adapter MF103
		- add retro-bit X 8Bitdo N64 bluetooth controller
	- Embedded games:
		- deactivation of moving games from "share_init" roms to share
		- remove doubloons of media/gamelist from romfs2
	- Steam deck support:
		- drivers/lib:
			- add codec/lib CS35L41 for steamdeck sound support
			- add package to manage gestures called ToucheGG (but not used for the moment)
			- activate really the power management tools
			- add "use case manager" configuration for alsa
			- add sound open firmwares
			- fix linux dependency not well declared for the kernel 6.4
			- touchegg: add sdl2 x11 and input synaptics dependencies
			- touchegg: add HID over I2C support for FTS3528 and dependencies
			- come back to use sound open firmwares v2.2.6 for kernel 6.4.16
			- add package XOSD to manage information display using "On Screen Display" for X
			- deactivate lizard mode from hid-steam driver that could disturb emulators
		- linux kernel:
			- add SPI AMD & SND INTEL
			- add more dependencies for sound from kernel
			- patch for linux kernel 6.4 and steam deck l2/r2 inputs
			- add comments and move few lines to clarify modules used
			- add more sound modules for intel, amd and sof
			- add modules to well manage suspend modes
		- improve primary/secondary screens management:
			- set primary screen default value
			- manage more cases when secondary screen is not yet set
			- screen mode(switch, clone or extended) from device conf
			- add management of screen mode when start x also
		- manage "vertical phone screens" :
			- turn splash video + remove subtitle for 'vertical' screen + vertical background
			- stretching slides (installation) + vertical screen rotation
			- feh option to adapt background size when resolution change
			- re-center background on 'tagline'
			- fix screenshot feature to manage vertical screen as on steam deck
			- change font and messagebox sizes from width of the screen
			- management of "vertical" touchscreen in X11 but without touchegg
			- alert now pegasus-frontend when screen enabling change using HTTP API
		- sound volume management:
			- with triggerhappy/http api to change in pegasus-frontend
		- backlight brightness management:
			- +/- 10% per press on volume bittons + hotkey
		- battery management:
			- add device script to manage battery-info (not used by pegasus finally)
		- network improvement:
			- fix to wait to have any network ip before to share from network
			- manage several access points now (historical bug found)
			- add a way to connect to a selected wifi now from pegasus-fe
		- new management of overlays:
			- manage resize of overlay png file when it's necessary now
			- resize/crop overlay for ratio different (libretro + hud)
		- "device" management:
			- hardware scripts to manage devices in same pixL image
			- add feature to manage suspend mode
			- new script + trigger happy commands to manage backlight brightness
			- add management of specific backlight device for primary screen
			- pixl-info script from device to be able to popup info using "..." button
		- "hotkey" management:
			- new commands using hotkeys in Trigger Happy (using select or mode now)
			- improve udev rule to know the display index needed to start it well
			- hotkeys for sound, brightness, switching windows and other examples to use Trigger Happy
			- alert now pegasus-frontend to show "shutdown dialog box"on long press and using HTTP API
		- "suspend" mode:
			- prepare suspend/quick menu display using press and release on power button
		- emulation:
			- dolphin: 
				- remove steamdeck hidapi usage not compatible with pixL
			- model2emu:
				- test with need 8.X for wine
				- working on steamdeck with wine 8.X and XInput
				- manage resize of overlay to other ratio
				- activate/deactivate sound as declared in pegasus-fe
				- improve for xinput and dinput still not ok for wine 8.X

- Configgen:
	- fix xemu crash if none selected in ratio setting [297]
	- add advanced emulators settings yuzu docked or not [294]
	- improve logging in few python module/class from configgen
	- global overlay option is now the default system option value
	- manage scanlines until 4k resolutions for model2emu
	- manage overlays until 4k resolutions for model2emu
	- Model2 "Wine" management rework:
		- rework configgen to be used with new generic wine class (as prepared/tested with teknoparrot)
		- install systematically libraries for all wine versions now
	- for lightguns:
		- new architecture to centralize lightguns detection (for sinden but also wiimote/dolphin)
		- retroarch overlay/border management in 'overlays' utils
		- add exit using select and start on sinden lightgun via evmapy trick
		- manage better the crosshair for super nintendo games
		- activate crosshair from type of lightgun and options
		- fix to manage flycast cross and add comments for info
		- fix pedal issue for time crisis on libretro mame
		- fix grenad trigger for 'born to fight' game on mame
		- add capacity to have a 'custom' lightgun.cfg from share
		- add management of crosshair in libretro mame
		- fix 'cursor' management for master system core
		- fix 'cross' management for sega saturn core
		- add 'carnevil' game on libretro mame
		- fix crosshair state change and improve robustness
		- new management of start/coin buttons between players
		- exclude "beast busters 2nd nightmare" for the moment
		- fix to well support 2 players in ninja assault on naomi
		- Sega model 3 multi-lightguns management for supermodel
		- use same keys for supermodel as libretro for start/coin to reuse some functions
		- add wait on popen command to improve detection results
		- add HUD Overlay management of border/crosshair for sinden
		- clarify crosshair megadrive/master system/psx values
		- support of overlay/border + sinden lightgun for model2emu
		- Model 2 'DrawCross' option restored to know if we need cross or not from lightgun.cfg
		- fix to manage differently the mouse index for supermodel/model2emu
		- improve crosshair management for dolphin bar cases
		- add <roms> tag information for "combat" mame games in lightgun.cfg
		- manage <roms> tag from configgen for libretro cores to avoid bad mistmatch for lightgun games

- Pegasus-Frontend 0.1.6:
	- features:
		- add advanced emulators settings yuzu docked or not [294]
		- multi-lightguns-management (borders/crosshair/lightgun detection)
		- update menu for overlays(model2/3, triforce + libretro)
		- update menu for scanlines(model2 only)
		- add possibility to use 'dev' updates to help testing of OS update by developers
		- use recalbox.conf "updates." parameters now
		- add docked mode options in advanced emulator settings for Yuzu
		- add more log at start for QT Library Information about version and paths
		- add token saving also in recalbox.conf parameters now for configgen/emulators
		- use hash of login+password now to discriminate cache and change about retroachievement token
		- add menu advanced emulator settings for Duckstation
		- add Vsync options in advanced emulator settings for Pcsx2
		- add 8bitdo sfc30 and snes30 as snes controller layout/icon
		- add details on "bluetooth autopairing" in menu to confirm that pairing is lost after reboot
		- add management of "embedded" pixL games from share_init (could be hide from "interface/games library loading" menu)
		- gamecontrollerdb updated with a 24th Feb 2024 version
		- to select connnected/available audio output in priority to first boot or when previous output selected is missing
		- use new MultivalueOption/MultivalueBox design for all menus using parameters list
		- lang submodule updated to have last translations for last features
	- Significant other features:
		- 'embedded' games management
		- lightguns feature (including sinden lightgun)
		- new 'screens' management
		- steamdeck/handheld pc features
		- model2emu menu for wine
	See here for more details on features and fixes: https://github.com/pixl-os/pegasus-frontend/blob/pixL-master/CHANGELOG.md

- gameOS-pixL 1.25 -> 1.26:
	- features:
		- management of icons/filters/colections for lightgun games
		- more filters for regions and countries in list/grid view
		- Add new PS3 system logos
		- Add new NAMCO357 system logos
		- Add new SYS2x6 system logos
		- feature to display now the battery level also if available (using .svg pictures)
		- add parameter in settings to display battery level in horizontally (by default) or vertically
		- add last french translation needed

	- fixes:
		- fix grouplist to change group logo from click
		- fix "helpbar" generation
		- fix to manage better cases of multi-screens game videos

- shinretro 0.201.6:
	- Add new logos for system PS3
	- Add new logos for system sys2x6
	- Add new logos for system namco357

## [pixL-master] - Beta34 - 2023-11-14

- OS:
	- sdl-jstest: update new repo gitlab to github [143]
	- remove evmapy usage for supermodel emulator for sega model 3 because available in emulator itself [121]
	- keep finally a supermodel.keys to manage wiimote remapping from configgen and activate multi-windows switch [121]
	- adding to a tool to hide mouse cursor/pointer dynamically [121]
	- creation new package evsieve in version 1.4.0 (tools to overload evdev events) for future used (tested during lightgun model 3 updates) [121]
	- Add grub2 theme [290]
	- Nvidia drivers [126]
		- Bump NVIDIA DRIVERS 535.86.05 -> 535.113.01
		- Bump NVIDIA Modprobe 535.86.05 -> 535.113.01
	- Add vulkan support in mesa3d for AMD Gpu's 

- Configgen:
	- Sega Model 3 - supermodel : [121]
		- fix to use gamepad or arcade stick with or without analog sticks
		- fix/improve to avoid "NONE" values in some cases on few inputs
		- fix indexations of controllers
		- adding lightgun feature:
			- keep gamepad compatibility
			- manage mode using mouse
			- manage mode using wiiimote/dolphinbar
			- update lightgun.cfg file from share_init for model 3 roms (including clones)
			- using 1 or 2 players
			- force hidding mouse cursor using unclutter for supermodel using unclutter
			- manage crosshairs display depending gamepad, mouse and wiimote/dolphinbar available
		- adding overlays feature:
			- with default overlay for sega model 3 system
		- fix to generate "savestate" directory from "saves" if missing
		- fix to activate crosshairs from .bmp asset files
		- improve Log() function with date/time
		- manage case when no Z axis, the RZ axis should be set as Z axis for supermodel
		- fix new case found where axis could be set to (null) 

	- improve Log() function with date/time for evmapy parts
	- fix post execution from configgen runner + add log function with date/time
	- fix hardcore retroachievement activation for fbneo [117]

- Emulation:
	- Sega Model 3 - supermodel: [121]
		- fix patch on folder-directiory due to issue of bad end of line (CRLF)
		- add verbose option to have more info during emulation if needed
		- manage multi-mouse using evdev input system from supermodel patched
	- Libretro Flycast:
		- fix to add core version/hash in libretro library info from .mk [293]
	- Famicom disk system:
		- Activate retroachievements for Famicom Disk System also (it was missing)

- gameOS-pixL 1.24:
	- features:
		- management of "screenshots" specifically to be able to display it in this theme
		- add filters for languages and regions from grid/list (using info from game title, depending scraps)
		- using hash library to display cup icon in dynamic/boxart grid for retroachievements [295]
		- add favorites/retroachievements icons management in vertical list [295]
	- fixes:
		- fix banner video launching when no favorite exists [123]
		- fix overlays usage in "embedded" mode used in vertical list
		- fix scrapped video using overlay with 16/9 ratio/black bars [46]
		- fix to avoid to zoom video in case of "game & watch" system for preview video
		- fix to avoid to display system logo for embedded views in vertical lists

- Pegasus-Frontend 0.1.5:
	- features:
		- management of more flags from systemlist.xml
		- new retroachievements management [295]
	- fixes:
		- fix some warning about url references
		- fix to bump easily rcheevos/libretro-common in pegasus-frontend project [295]
		- fix to rework logs
		- fix for naming and icons display in controllers list

## [pixL-master] - Beta33 - 2023-08-04

- Buildroot:
	- change submodule to buildroot pixl-os fork
	- move custom and many patch in pixl-os submodule

- CI:
	- enable Continuous Integration gitlab for primary build test
		- enable test commit by conventionnal commits
		- check .mk Config.in .hash package format
		- check licence in package
		- check input and bios files

- Configgen:
	- add auto configuration language for retroarch [65]
	- enable cheats on default for ppsspp
	- add default configuration for netplay for ppsspp
	- remove system checks to set cheevos parameters in retroarch [110]
	- Fix show fps in game for ppsspp
	- Dolphin Advanced Settings :
		- Cheats enable/disable
		- Resolution x1 > x8
		- antialiasing msaa x1 > x8
	- Pcsx2 Advanced Settings :
		- Cheats enable/disable
		- Resolution x1 > x8
		- Tv shaders 
		- anisotropy x1 > x8
	- Citra Advanced Settings
		- Internal resolution 1 > 10
		- Textures filter
	- Cemu Advanced Settings :
		- Upscale filter
		- Downscale filter
		- Async Compile
		- Vsync
	- Xemu Advanced Settings :
		- Internal resolution 1 > 10
	- Supermodel Advanced Settings
		- Internal resolution 1 > 5
		- Multi texture
		- Quad rendering
		- Legacy sound engine
		- Flip stereo
		- Gpu threaded
		- Address Out
		- Network
		- Port In/Out
	- PPSSPP Advanced Settings
		- Resolution
		- Force 60 fps
		- Msaa
		- Texture scaling level
		- Texture scaling type
		- Anisotropy level
		- Texture filter
		- Texture shader
	- Retroarch Advanced Settings
		- Swap validate button in menu [70]
		- change theme color in menu
		- Enable/disable load content animation on launch a game
	- Yuzu advanced Settings
		- Internal resolution
		- Scaling filter
		- Fsr sharpening 
		- Async shaders
		- Extended memory
		- Vsync

- OS:
	- Resolve "mesa3d driver nouveau flickering screen on pegasus"
	- Fix remove themes checking in recalbox-includes.sh
	- Fix game-boy-retro-shader for opengl and vulkan [60]
	- Fix nes and fds retro shaders for opengl and vulkan [68]
	- Nvidia drivers
		- Bump NVIDIA DRIVERS 390.143 -> 390.157
		- Bump NVIDIA DRIVERS 460.84 -> 470.199.02 [86]
		- Bump NVIDIA DRIVERS 510.60.02 -> 535.86.05 [113]
	- Vulkan drivers
		- Bump vulkan-headers 1.3.231 -> 1.3.254
		- Bump vulkan-Loader 1.3.231 -> 1.3.254
		- Bump vulkan-tools 1.3.231 -> 1.3.254
		- Bump vulkan-samples March 13,2023 -> Jun 20, 2023
		- Bump spirv-headers-sdk-1.3.231.0 -> 1.3.250.0
		- Bump spirv-tools-sdk-1.3.231.1 -> 1.3.250.0
	- nvidia-modprobe 510.60.02 -> 535.86.05 [113]
	- glslang 11.9.0 -> 12.2.0
	- Change naming Recalbox to pixL for all readme.txt
	- upgrade gcc-initial on 12.2.0
	- Nvidia drivers 32 bits [74]
		- Bump NVIDIA DRIVERS 390.157
		- Bump NVIDIA DRIVERS 470.182.03 [86]
		- Bump NVIDIA DRIVERS 535.86.05 [113]
	- Change naming recalbox img to pixl img building
	- Fix Window's menu bar font size
	- Add mapping for: [67]
		- Sony Interactive Entertainment Controller wired ps4 Style
		- Xinmotek Controller
		- Nintendo Switch Pro Controller HAC-013
		- Microsoft Controller :
			- Xbox One, Series X, S - Model: 1697
			- Xbox Series X|S powerA wired Style
			- XBox One Wired Controller M1142084-007
			- Xbox 360 usb pad Model: X811616-006

- Emulation:
	- Set overlay behind the retroarch menu. [56]
	- Add ppsspp default cheat database. [64]
	- Fix cache path for many standalone emulators
	- Fix for bluetooth controllers using extended name in dolphin-triforce [69]
	- Remove log windows on launch pcsx2 emulator
	- Add yuzu standalone emulator for switch (version 0-1472 - Jun 14, 2023) [75]
		- add build and romfs2
		- new configgen to manage Yuzu
		- fix missing core version and fix keys exec
		- add more games extension : .xci .nsp .nso .nro .ncz
		- add xdg cache path and more default options
		- remove force option extented memory layout
		- patch to bios keys prod
		- add more moved path in pixl path 
		- change load path to save for patch games
		- add QT_WEB_ENGINE= ON build option 
		- fix recalbox.CONF for the rest of yuzu conf stay on configs
		- add ressources for update online
		- bump gcc and gcc-intial on 12
		- change all bool to string best for qt for configgen
		- add advanced settings for yuzu
	- Bump xemu to v0.7.98 [88]
	- Sega Model 2 new system: [78]
		- Using model2emu windows emulator
		- Using wine appimage 5.11 PLayonlinux
		- Need installation of windows files at first start
		- Using cabextract and winetricks
		- Using Popup message during installation/configuration
		- Using evmapy for Hotkey + Start to exit
		- With .lua/.dat/.input files to manage options and cheats for model2emu
		- New standalone configgen (including wine class to manage windows registry/version)
		- Pre-configure model 2 game mappings for controllers (to avoid to go in emulator menu to configure)
		- Lightgun mode using wiimote/dolphinbar (work with mouse also) - only one player for the moment :
			- Compatible with lightgun.cfg file in version 1.1.0 (only for game/emulator/core definition)
			- Management of crossair if not necessary to diplay
		- Management of 4/3 (default), 16/9 and 16/10
		- Increasing of overlay to add app images
		- Management of audio/voice outputs using alsa device enumaration
		- Management of Dinput/Xinput for controllers
		- Additional graphics optimization from parameters set in pegasus-frontend
		- Management of deadzone for Dinput and Xinput
		- Management of index/order of controller (limited to 4 controllers plugged - emulator can't manage more)
		- Restoring of saves data if deleted by mistake
		- Additional commands from keyboard, see documentation
		- Deactivate one apple macbook hardware that generate conflict in wine for controllers indexes
		- See documentation from:
			https://doc.pixl-os.com/sega-model-2/
			https://doc.pixl-os.com/model2emu/
	- Triforce (by Sega, Nintendo and Namco): [105]
		- Rework to use a fork of dolphin (in version 5.0) done by pixL to merge with Crediar's new implementation and ported for linux.
		- Remove previous build patch for dolphin-triforce 4.X
		- Stop to build dolphin tool from dolphin-triforce to avid doubloons with Dolphin-emu one
		- New configgen dedicated but re-using the dolphin's code partially
		- Update romfs2 to match with new dolphin-triforce
		- Remove gc/wii parts not used fro Triforce
		- Enable patches & cheats for dolphin-triforce needed for some games
		- Add management of gameID to detect/select game launched
		- Add dedicated input mappings management for games
		- Set "auto" ratio to 4/3 (only 16/9,16/10 & 4/3 are supported)
		- Add possibility to display monitoring HUD for dolphin-triforce only for the moment. (Using MangoHUD)
		- Add capacity to display overlays for dolphin-triforce only for the moment. (Using MangoHUD)
		- Add parameters to manage vsync, resolution, anti-aliasing and widescreen hack
		- Add Triforce default system overlay from "share_init"
		- Remove previous settings directory/savestates/gamesettings used by dolphin-triforce 4.X
		- See documentation updated from: https://doc.pixl-os.com/dolphin-triforce/
		- See new set of overlays for Triforce from: https://gitlab.com/pixl-os/pixl-overlays/-/tree/pixL-master/overlays/triforce
	- Cemu for wiiu emulator (2.0-42): [76]
		- Add romfs2 for Wiiu
		- Fix cache path
		- Add patch for keys.txt
		- Manage corenames version
		- Manage .wua .wup .wud .wux .rpx .squashfs extensions
		- Manage settings.origin.xml
		- Add cemutil fix audio selection and more
		- Change cemutil for graphic packs for best games config
		- Add evmap cemu and post install target
		- Fixed windows size to active resolution
		- Include graphic packs in cemu make file
		- Add advanced settings for cemu
		- Add options vsync
		- Add screen switch using evmapy
		- Add config path for cemu build using pixL buildroot (branch pixl-2022.05.3 + branch sdl2-upgrade)

- gameOS-pixL 1.23:
	- add new sega model 2 system logos [78]
	- add new Nintendo Wii U system logos
	- add feature to keep possibility to find overlays without decorations in his .cfg name
	- improvement to manage several types of overlays with custom, 720p and 1080p resolutions

- Pegasus-Frontend 0.1.4:
	- Features:
		- set visible vulkan option only if compatible with hardware [72]
		- add new menu for emulators configuration :
			- add dedicated qml(menu) for model2emu
			- add dedicated qml(menu) for dolphin
			- add dedicated qml(menu) for pcsx2
			- add dedicated qml(menu) for citra
			- add dedicated qml(menu) for cemu
			- add dedicated qml(menu) for xemu
			- add dedicated qml(menu) for supermodel
			- add dedicated qml(menu) for ppsspp
			- add dedicated qml(menu) for yuzu
			- add dedicated qml(menu) for retroarch
			- add dedicated qml(menu) for dolphin-triforce
			- fix missing crosshairs option in menu for supermodel
			- refactoring configgen and user options
			- add vsync option for yuzu and cemu
	- Fixes:
		- new way to have local ip for wifi/ethernet and to manage more cases of ethernet/wifi adapter naming [50]
	
- pixL-manager [102]
	- change naming recalbox to pixl for all language
	- add other log for script support
	- Add various fixes
	- change naming recalbox to pixl
	- Add Patreon features

## [pixL-master] - Beta32 - 2023-05-26

- OS:
	- Add scripts and json examples to manage online update for pixL OS
	- Evmapy fix to move/update files after online/offline updates
	- Add mapping for Power A Wireless Controller - Nintendo GameCube Style
	- Add redshift adjusts the color temperature of the screen at night
	- Bump Package RTL8821CU-20210916 commits 2023/03/14
	- Change naming partition RECALBOX to PIXL
	- Change naming search networking \\RECALBOX to \\PIXL
	- Remove xow user mode
	- Add xone kernel driver to manage xbox one/series controllers using xbox dongle
	- Add xpadneo kernel driver to manage xbox one/series controllers using bluetooth intergrated/dongle
	- Add xpad-noone to have aby xpad kernel driver without xbox one support but keep xbox/xbox 360 features
	- Fix ps4/ps5 layout selection from 'es_input.cfg' now due to common name detection (need new version of Pegasus-Frontend)
	- Fix unlock sound achievement option
	- Add new GUID/Mapping for Dualsense PS5 controller + fix device layout
	- Rebase on master of recalbox 9 - see after impacts on pixL:
		- Fixes for bluetooth since autopair management
		- Update mapping for xbox 360 wireless receiver using axes now for L2/R2 + some ids
		- Fix to avoid issue when input name is empty from es_input.cfg
		- Nvidia driver: bump to 390.157 and 510.108.03
		- Fix on qt5webengine build
		- Fix on dolphin-emu for last beta version to avoid issue with minizip dependency
		- Fix rb5000 to use binary directly due to repo not yet available
		- Fix on configgen
		- Fix many package not compile for vulkan
		- Bump Mesa to 22.2.2 for api 1.3 vulkan
		- Fix to restore nvidia hardware project as before rebase
		- Fix to restore previous S31 init.d file
		- Fix VIRTUALGAMEPADS package
		- Fix disabled galluim driver iris
		- Fix to add add missing xdriver_xf86 
		- Fix yaml conffigen test in python 3.10
		- Feat for zmachine remove extension .zip
		- bump kernelfirmware 20210713 to 20230117 and fix [#51]
	- Fix hp_accel recognized as a controller need to blacklist
	- Fix pre made shaderset for Opengl and Vulkan renderer [41]
	- Fix Alsa-base.conf with many option
	- Remove EmulationStation package in pixL project
		- fork pixl-manger
			- add vulkaninfo in recalbox-support.sh
			- add more logs
	- Remove Moonlight package in pixL project
	- Remove Hyperion package in pixL project
	- Remove Kodi package in pixL project
	- Fix Configgen test for input.cfg

- Pegasus-Frontend v0.1.2:
	- Features:
		- Add display in GB for online update >= than 1024 Mo + translate in french
		- Add new way to have "system manufacturer" from game object (as from collections)
		- Add filtering by array of indexes from 'sortfilterproxymodel' to help filtering/caching for 'MyCollections' from gameOS-pixL
		- Manage 'deviceLayout' parameter from es_input.cfg to force layout usage by GUID but also for icons (except in bluetooth interface where it's not yet possible)
		- Add icon for ps5 controllers
		- Refine icons affectation for xbox360, ps5 and xbox
		- Add redshift option + translate in french
		- Add autopair option for bluetooth controllers + translate in french
		- Request reboot if bluetooth/autopair parameters changed + translate in french
		- Add cheevos option challenge indicators + translate in french
		- show vulkan option for all intel/nvidia + translate in french
	- Lot of fixes : check Pegasus-frontend changelog for more information

- Pegasus-Frontend v0.1.3:
	- Features:
		- change settings for "share" path from "/.emulationstation/" to "/.config/pegasus-frontend/"
		- change settings for "dev" path from "/etc/emulationstation" to "/etc/pegasus-frontend"
		- change name/path from  es_bios.xml to bios.xml
		- change paremeter for debug logs from "emulationstation.debuglogs" to "frontend.debuglogs" (same for warn and info)
		- change parameter from "emulationstation.gamelistonly" to "pegasus.gamelistonly" (same for gamelistfirst also)
		- change name/path from es_input.cfg to input.cfg
		- replace usage of es_log.txt by recalbox.log now
	- fix rename hostname RECALBOX to PIXL

- GameOS-pixL theme v1.22:
	- New features:
		- Restore gradient effect on system logos for gameView/grid/verticalList using parameters #[49]
		- logo:
			- Add new logos for system switch
			- Add new logos for system Megaduck
			- Add new logos for system Philips cdi
	- Improvements on 'my Collections':
		- Add "cache" feature and fixes
		- Add more possibility about parameters/filtering/sorting
		- New translation fr
	- Fixes:
		- Video fix to avoid memory leaks for showCaseViewMenu
		- Remove top genre/publisher buggy for showCaseViewMenu
		- Dynamic collections loading for showcaseViewMenu / best memory management

- shinretro-pixL theme v0.201.4:
	- New features:
		- logo:
			- Add new logos for system switch
			- Add new logos for system Megaduck
			- Add new logos for system Philips cdi

- shinretro-pixL theme v0.201.3:
	- New features:
		- In Games page add date of game
		- Remove Keys.onPressed event action for SELECT button
		- Remove order by 'Title', 'Release Date', 'Rating', 'Genre', 'Last Played', 'Favorite'
		- Move order by 'Release Date' to Filter function

- Emulation:
	- Bump libretro-2048 core
	- Bump libretro-81 core and remove gcc10 patch
	- Bump libretro-a5200 core
	- Bump libretro-atari 800 core
	- Bump libretro-beetle lynx core
	- Bump libretro-beetle ngp core
	- Bump libretro-beetle pce fast core
	- Bump libretro-beetle pcfx core
	- Bump libretro-beetle psx core
	- Bump libretro-beetle psx hw core
	- Bump libretro-beetle saturn and delete patch
	- Bump libretro-beetle supergrafx core
	- Bump libretro-beetle vb core
	- Bump libretro-beetle wswan core
	- Bump libretro-bk emulator core
	- Bump libretro-bluemsx core
	- Bump libretro-boom3 core
	- Bump libretro-bsnes core
	- Bump libretro-cannonball core
	- Bump libretro-cap32 core
	- Bump libretro-craft core
	- Bump libretro-crocods core
	- Bump libretro-desmume core and remove arm patch
	- Bump libretro-dinothawr core
	- Bump libretro-dosbox-pure core
	- Bump libretro-fceumm core
	- Bump libretro-fceunext core
	- Bump libretro-flycast core
	- Bump libretro-fmsx core and remove patch
	- Bump libretro-freechaf core
	- Bump libretro-freeintv core
	- Bump libretro-fuse core
	- Bump libretro-gambatte core
	- Bump libretro-genesis plus gx core
	- Bump libretro-genesis plus gx wide
	- Bump libretro-gong core
	- Bump libretro-gpsp core
	- Bump libretro-gw core
	- Bump libretro-lowres nx core
	- Bump libretro-lutro core and remove patch Wantjit
	- Bump libretro-mame to 0.251
	- Bump libretro-melonds core and remove arm patch
	- Bump libretro-mesen core
	- Bump libretro-mesen-s core
	- Bump libretro-meteor core
	- Bump libretro-mgba core
	- Bump libretro-mrboom core
	- Bump libretro-mu core and remove gcc10 patch
	- Bump libretro-mupen64plus-nx core
	- Bump libretro-neocd core
	- Bump libretro-nestopia core
	- Bump libretro-np2kai core
	- Bump libretro-nxengine core
	- Bump libretro-o2em core
	- Bump libretro-opera core
	- Bump libretro-parallel-n64 core and remove patch
	- Bump libretro-pcsx_rearmed core
	- Bump libretro-ppsspp
	- Bump libretro-prosystem core
	- Bump libretro-px86k core
	- Bump libretro-quicknes core
	- Bump libretro-uasi88 core
	- Bump libretro-kronos core
	- Bump libretro-yabause core
	- Bump libretro-yabasanshiro core and remove patch
	- Bump libretro-quasi88 core
	- Bump libretro-xmil core and remove patch
	- Bump libretro-px86k core
	- Bump libretro-vitaquake3 core
	- Bump libretro-vitaquake2 core
	- Bump libretro-prosystem core
	- Bump libretro-virtual-jaguar core
	- Bump libretro-vice core
	- Bump libretro-np2kai core
	- Bump libretro-vecx core
	- Bump libretro-easyrpg and easyrpg liblcf 2023's versions
	- Introduce standalone Mame build in 0.251 :
		- Manage .corenames file for standalone Mame
		- Manage evmapy .keys file for standalone Mame
		- Deactivate Mess for the moment
		- Fix configgen setup.py file + add __init__.py for mame generator
	- Bump Retroarch v1.14.0 with 'limited' number of patches:
		- using pixl-os repo to simplify rebase of patches for next versions
		- Bump also Retroarch-assets (Oct 24, 2022)
		- Add libretro core info to manage cores capacity for RA v1.14.0
		- Update retroarchcustom.cfg.origin to manage cores info + add more comments
		- Update retroarchcustom.cfg.origin to add parameters to avoid cache on core info and test ping (to activate)
	- Bump libretro FBNeo  v1.0.0.03 (Oct 10, 2022) :
		- remove patch not use for x86-64 platform
		- improve fbneo build stability using shorter hash from commit + fix some parameters
	- Philips CDi :
		- Bump libretro cdi2015 (Jun 14, 2022)
		- Add libretro same_cdi (Dec 11, 2022) with support of chd, iso and cue/bin (included in romfs2)
		- Add/update bios/firmware list for Philips CDi
	- Neo-geo : add libretro-mame and Mame to neo-geo compatibility
	- Bump dolphin-emu to v5.0-19277
		- Enable upnp build option
		- Add patch to fix saves folders (wii, gamecube) and configs path
	- Bump Citra-emu to v1886 and remove lpthread patch
		- Add on default new option dump textures in configgen
		- Fix folders configs and saves 
 	- Fix path and active shaders cache on xemu

- Configgen:
	- First version for Mame standalone emulator including generator/controller
	- Change XDG_DATA_HOME = recalboxFiles.SAVES to recalboxFiles.CONF in dolphinGenrator.py

- other:
	- update readme (command for building) and (command to install docker)

## [RETRO-X] - Beta31 - 2023-01-21
- OS:
	- use outputclass sections and its powerfull parameters now for amd, intel and radeon GPUs
	- filter intel GPU use case by matching of driver i915
	- add 'update' ressources for Pegasus-frontend online updates in  own Pegasus package
	
- Pegasus-Frontend v0.1.1:
	- features:
		- add generic functions to be able to manage theme reloading itself
		- reorganize "interface" menu and add section "Games loading" + annotation "(Beta)" in few menu
		- new 'Beta' parameters/features for interface:
			- "Gamelist First" : to well manage systems with and without gamelist in the same scan.
			- "Medialist" : to create media.xml and use this list for media not present in gamelists (improve loading after first boot or changes)
			- "Media 'on Demand'" to load media dynamically when it's requested, to avoid initial Games loading
		- management of media.xml regenration using gamelists size/date changes
	- few fixes : check Pegasus-frontend changelog for more information

- Theme gameOS-pixL v1.21:
	- features:
		- add management of systems group activable in theme's general settings
		- adapt default parameters to well manage activation of groups, display of overlay/video/grid elements
		- sorting systems by name, release date or manufacturer
		- add a second sorting by name, release date or manufacturer criteria
		- 2 ways to display groups using one or 2 slots on screen
		- dynamic help to know how to pass from systems to group when we one slot
		- display of 'release date' under systems logo when sorting use it.
		- propose now to reload theme itself when settings changed
		- remove limit on favorite displayed in banner
		- 'My Collections' feature: 
			- add internal new flag to avoid process all List of Collectiions during change of settings
			- add filtering on several  systems as planned initially
	- lot of fixes : check gameOS-pixL changelog for more information

## [RETRO-X] - Beta30 - 2022-12-09
- OS:
	- fix to add a reboot after first boot of fresh install to finalise resize of partitions as overlay [#44]
	- fix vsync issue on intel GPU (reactive vsync + force DRI3) [#40]
	- add tearfree/triplebuffer options for Intel GPUs
	- deactivate "pre-release" (beta) for "local" nvidia drivers to anticipate when pixL-os will be released
	- fix to update version/commit reference for all retroarch cores to each boot

- Pegasus-Frontend v0.1.0:
	- Features:
		- add version display in theme selection
		- add switch pro controller layout
		- manage pixL-OS online update for beta and release
	- Fixes:
		- fix x/y config field selection for default controller layout in gamepad editor 
		- fixes and improvements for updates management

## [RETRO-X] - Beta29 - 2022-11-21
- OS:
	- Save online update FbNeo resources in libretro-fbneo package
	- Bump Pcsx2 and save online update Pcsx2 resources
	- Bump Xemu to v0.7.70 and add online update-resources
	- move repository to github for supermodel and bump to v906
	- Bump dolphin-emu to 5.0-17269
	- Bump Duckstation new latest version 2022-05-19
	- Fix force disabled Xemu cache disk option 
	- Change Mame default emulator to libretro-mame
	- Change Nintendo 64 default emulator to libretro-parallel_n64 #[20]
	- Fix error when save/change favorites #[19]
	- Add wheel mapping :
		- Microsoft SideWinder Precision Racing Wheel USB version 1.0
	- Fix vulkan for librerto-pcsx2 #[15]
	- Fix N64 mupen64plus glide64mk2 no FPS print #[2]
	- Bump Libretro-mame to 0.248 and add online update-resources
	- Add another Microsoft Xbox One X|S Controllers default configuration
	- Add volume only for boot video
	- Add folder share/videos for load videos per system in the theme shinretro
	- Resize first partition 9Go to 6Go
	- Update Nintendo Swicth N64 controller mapping (bluetooth & usb) to well manage C Buttons
	- Add MAYFLASH N64 Controller Adapter MF103 mapping
	- Add folder and readme.txt for themes from share_init
	- Add new package to copy last version of gameOS in share_init/themes folder during build
	- Add new package to copy last version of shinretro in share_init/themes folder during build
	- Add all media for all roms in base
	- Add Mayflash N64 Controller Adpater v1 mapping
	- Add shinretro update online
	- Change deactivateskrapermedia to position off #[34]
	- Add variable for color pegasus and debuglog in recalbox.conf
		- kodi.enabled=0
		- pegasus.deactivateskrapermedia=0
		- wifi.region=EU
	- Add Nacon Revolution Unlimited Pro Controller
	- Bump shinretro v0.201.1

- Configgen:
 	- Fix Pcsx2 vsync option

- Other:
	- Update README.md change recalbox by pixL
	- Update DOCUMENTATION.md change recalbox by pixL
	- Update LICENSE.md change recalbox by pixL
	- Update TESTING.md change recalbox by pixL

- Emulation:
	- Configgen improved to manage opposite for sticks axes (support of joystick1/2right/down from es_inputs.cfg)
	- Configgen manage Libretro sticks minus and plus axe, button and hat now
	- Remap B button automatically for pad without X button for retroarch N64 cores
	- Add evmapy command for mupen64plus_next and parallel_n64 cores to manage N64 controller game exit using START+B

- Pegasus-Frontend v0.0.9:
	- Optimize scan folder for load all media download by scraper
	- Fixes on wifi menu
	- Add system videos from 'share/videos' directory in collections (to be used with Shinretro theme)
	- Add volume only for video boot
	- Translation about: parameter lists, vulkan driver, boot video volume, new axes for N64, color settings
	- Gamepad editor custom view for N64 Controllers (switch one, 8bitdo one, original ones using adapter)
	- Support of +r/-r and -y/+y axis management for buttons of right stick as for N64 controllers
	- Support of N64 controllers (with specific visual of N64 controller) and extended SDL 2 mappings using +/-righy & +/-rightx
	- Extraction of gameOS theme from Pegasus
	- Add update online for supermodel, citra-emu, pcsx2, dolphin-emu, libretro-mame, xemu
	- Optimization folder for all pad
	- Add detection of Mayflash N64 controler adapter V1 as N64 controller for gamepad editor
	- Change color of background, text, and selected in settings menu 
	- Add layout controller PS5
	- Add shinretro update online
	- Add xboxone controller layout
	- Bump last commit pegasus 2022-11-20

- Theme (gameOS-pixL-master) v1.20:
	- new design management :
	- Fix demo mode launched in game #[16]

	- other new feature:
		- optimize scan folder for load all media download by scraper
		- fix list of media
		- add logo for roms in base

	- multi-languages-support :
		- Fix in qml code to translate in french for collection 6 to 10 #[23]
		- Fix in qml code to be able to translate in french for platform page style #[24]
		- Add fr for wide and tall #[25]
		- correction wording "Meileurs" to "Meilleurs"

- Theme (shinretro-pixL-master) v0.201.1:
	- Add virtual Keyboard in #[67]

## [RETRO-X] - Beta28 - 2022-09-12
- OS:
	- Add support for vulkan (only for nvidia-driver)
	- Add vulkan shaders and update opengl shaders
	- Fix bios xml cleaning
	- Move arcade dat generator to pixl-os repos

- Emulation:
	- Add missing evmapy for citra and supermodel
	- Bump libretro-flycast to v2.0
	- Add evmapy file for retroarch to be able to manage multi-windows using Hotkey+R1
	- Bump libretro-mame to v0.247

- Pegasus-Frontend:
	- Add usersettings folder in recalbox/share/saves/ for preserve favorite game file on first
	- New API features ('ScreenScraperId','type', 'releasedate' & 'manufacturer')
	- Theme translation feature support
	- Multi-windows support
	- GameOS:
		- new design management : include a designer for main view + vertical List feature
		- Multi-languages-support : introduce translation capacity in theme using linquist tools
		- select random game in games list with R1+L1 (from Grid or VerticalList)
	- rework video settings
	- see https://github.com/pixl-os/pegasus-frontend/blob/pixL-master/CHANGELOG.md for other features/fixes

## [RETRO-X] - Beta27 - 2022-07-09
- OS:
	- Change size-partition boot 6Gb to 9Gb
	- Change size-partition overlay 1Gb to 2Gb
	- Add 12 images pixl when the install OS
	- Add 10 images pixl when the update OS
	- solved issues textes in pictures
	- Add video create partition pixL
	- Add 10 videos intro pixL
	- delete all intro video recalbox
	- Add 5 gamepad mapping USB
		- Sony Interactive Entertainment Wireless Controller USB
		- Generic X-Box pad : Microsoft X-Box One Series pad USB
		- Microsoft X-Box One pad (Firmware 2015) USB
		- Nacon PS4 Revolution Pro Controller 2 USB
		- PS5 Official Weifang Goertek Electronics Co.Ltd wireless
	- Add 2 wheel mapping
		- Thrustmaster F430 Cockpit Wireless
		- ThrustMaster Ferrari 458 Racing Wheel
	- Add Mesa3d Drivers :
		- GALLIUM_DRIVER_VC4
		- GALLIUM_DRIVER_V3D
		- GALLIUM_DRIVER_R600
		- GALLIUM_DRIVER_R300
		- OSMESA_GALLIUM
		- VDPAU
		- INTEL_MEDIADRIVER
	- Add lmsensors for better temp informations
	- Add walppaper in openbox with feh package
	- Add Naomi2 Arcade system thanks Akkeoss
	- Adding evmapy tools to map pad to keyboard for emulators hotkeys
	- Adding evdev python library to use with evmapy
	- Populate share from share_init for evmapy also
	- Add notify-tools for evmapy usage
	- Add python setuptools also for evmapy usage
	- Integration xdotool to manage simulation of key/mouse
	- fix configgen-recalbox package to build python-evdev also
	- fix build for python-evdev package
	- fix build for evmapy package
	- fix on nvidia version/install scripts to ignore igpu if it's a Desktop PC. (no considered as optimus case/conflict)
	- fix PATCH change to BlendFunc in flycast for crosshair
	- Change password root

- Emulation:
	- Dolphin-triforce standalone emulator:
		- Add triforce core details in .corenames files
		- Add evmapy keys in build for dolphin-triforce emulator
		- New mapping dolphin-triforce to have menu in full screen
		- Fix evmapy mappings for dolphin-triforce: remove "sequence" mode not applicable for alt+tab
		- Add hotkey+r1 in triforce .keys evmapy file to switch between emulator and pegasus using multi-windows feature
		- fix to improve switching between game/pegasus in case of multi-windows from evmapy
		- fix to improve also pause/unpause and screenshot behaviors for evmapy .keys file for dolphin-triforce
	
	- Dolphin-emu standalone emulator:
		- fix to add missing extension gcz for wii system
		- Add evmapy keys in build for dolphin-emu emulator (impacts for wii & gamecube systems)
		- Add a click of mouse in evmapy after alt+tab to keep focus for dolphin
		- Fix evmapy mappings for dolphin (wii & gamecube): remove "sequence" mode not applicable for alt+tab
		- Usage of a unique evmapy 'dolphin.keys' file to use a new management of multi-windows and menu/pegasus swicth
		- Fix evmapy .keys file for dolphin to detect game window
		- Add sideways toogle for emulated wiimote on dolphin using hotkey+L2
		- fix to improve switching between game/pegasus in case of multi-windows from evmapy
	
	- Xemu standalone emulator:
		- Add patch for xemu to display menu using Hotkey+A (xbox buttons mapping)
		- Fix on xemu build to use libpcap/libglu when we build emulator independently
		- Workaround to manage SLD2 custom mappings in xemu from Pegasus-Frontend ones
		- add eeprom files for 7 languages in configs of xemu for XBOX
		- add evmapy .keys file for xemu and in .mk file
		- update boot script to copy eeprom files from share_init for system/configs/xemu
	
	- Pcsx2 standalone emulator:
		- fix to remove deprecated hotkey patch (replaced by evmapy usage)
		- fix mappings using pegasus-frontend sdl controllers file (in one line ;-)
		- fix to use a new way to manage better pad order using env variable
		- add evmapy .key file for PCSX2 to have all hotkeys ready
	
	- Bump Configgen to have changes from configgen for evmapy and last improvements:
		- for dolphin-emu:
			- best language management (using update of SYSCONF)
			- improve hotkey management/pause/switching
			- fix buttons position on gamecube for Z/L/R
			- fix buttons mapping for B/2 buttons on wii
			- manage hotkey for wiimote direction to help gameplay with wiimote in games
			- fix for gamecube to replicate L/R to L-Analog & R-Analog
		- for dolphin-triforce:
			- controllers mapping
			- best hotkey+b management
			- force widescreen hack on ratio to 16/9
			- add load save state automatically if needed using xdotool
			- add cleaning of "card"
			- remove auto replacements of controls using auto-detection of axis/buttons
		- for pcsx2 
			- new pad order management and remove usage of PAD.ini
		- for xemu: 
			- move initial hdd file only for selected and used system
			- add languages management for xbox

- Pegasus-Frontend:
	- Bump of version 0.0.7:
		- fix on management of shaders display/selection
		- i915 Driver force-probe activation integration (to correct some issues with sound outputs)
		- fix refresh functions for wifi, bluetooth and settings view (due to bad timeout parameter)
		- add fr translation for i915 force-probe menu + fixes (wifi notes)
		- GameOS-pixL:
			- adding logos for new 'naomi2' system and also new 5 logos for screenshots 'system' and also ports system

## [RETRO-X] - Beta 26 - 2022-06-03
- OS:
	- Change grub name Recalbox by PixL
	- Remove video recalbox for introduction
	- Add video pixl for introduction
	- Remove image recalbox when the install OS
	- Add image pixl when the install OS
	- Fix boot script for nvidia-install to be executed in all cases (with or without execution right on file)
	- Bump last 'hardware' pixL branch
	- Fix nvidia install execution to use bash and not only execution right
	- Disable Asrock led controller because is not a USB joystick
 - Lightgun:
	- Recovering of original lightgun feature using lightgun.cfg initial format
	- Take into account last games introduction done previously, version 1.0.5 of file restored
	- Compatible with libretro lightgun configgen 1.6.7
	- New bump of configgen necessary


- Emulation:
	- Fix libretro-swanstation build and bump to lastest version
	- Bump librerto-mame on 0243 mame romset
	- Bump pcsx2 to v1.7.2744
	- Bump citra-emu to nightly-1764
	- Bump Hypseus to v2.8.2a
	- Bump Supermodel to 882 
	- Introduction of new system for microsoft xbox
	- Introduction of new system for sega chihiro (alpha version)
	- Introduction of new system for triforce (alpha version)
	- Bump of corresponding configgen

- Pegasus-Frontend:
	- New bump of version 0.0.6:
		- including new feature to manage Wifi (with fr translation + issue 15 corrected)
		- Fix on controllers naming
		- New GameOS-pixL theme:
			- including new system logos
			- fix on odyssey 2 logos
			- Use L1/R1 for letter nav & L2/R2 for system nav in platform page
			- fix on gridview width in platform page
			- Introduction of overlays & logos options in gameView
			- adding "beta" logo on system using an emulator 'low'

## [RETRO-X] - Beta 25 - 2022-05-06
- OS:
	- Improvements on Nvidia drivers:
		- Use a unique local repo for nvidia driver in the OS (to avoid to propose several version of drivers)
		- fix bug on install of version 510.XX.XX
		- fix to compare versions installed and proposed to let capacity to install a more recent version.

- Pegasus-Frontend:
	- New bump of version 0.0.5:
		- including new feature to manage multiversion as for Nvidia drivers
		- fix to manage index of version depending of compatibility and version scripts
		- fix to delete some version.sh/install.sh from /tmp to avoid mismatch or corrupted files

## [RETRO-X] - Beta 24 - 2022-05-05
- OS:
	- Improvements on Nvidia drivers:
		- Add new way to install Nvidia drivers as any update proposed by pegasus
		- including install/version scripts and corresponding json files for nvidia local updates

- Pegasus-Frontend:
	- Bump of version 0.0.5:
		- new feature including nvidia drivers installation as udpate proposed by Pegasus (need reboot)
		- improvement and fix to well manage updates remotly but also local.
		- fix to clarify the progress bar animation and using colors on results (green for ok, red for not ok)
		- fix on lang: update for controller helps/views [Thanks Sebio]

## [RETRO-X] - Beta 23 - 2022-05-01
- OS:
	- Improvements on Nvidia drivers:
		- Manage 3 versions now: 390/460 & 510
		- New files for compatibility with all M devices (especially for 390 & 460 driver versions)
		- Improve check of reference using compatibility files including "tab" characters to well detect GPUID
		- change of Nvidia-install to manage nvidia optimus cases (using nvidia + intel GPUs)
		- Change recalbox-boot.conf to activate nvidia optimus case (nvidia-optimus=true)

- Emulation:
	- fix .corenames copy to do from share_init for standalone cores

- Pegasus-Frontend:
	- Bump of version 0.0.4:
		- lang: bump last translation fr (update on bios, restart and controller menu)
		- introduction of online update for cores
		- first updatable core: libretro fbneo

## [RETRO-X] - Beta 22 - 2022-04-22
- OS:
	- Add feature to activate installation or not from recalbox-boot.conf for nvidia drivers
	- Change recalbox-hardware source before to use the mono-repo
	- New nvidia-install to manage 'nvidia-driver=true' using new recalbox-hardware
	- hid-nintendo: update mapping for snes, megadrive/genesis & n64 to improve buttons usage
	- Set default settings in recalbox.conf for bluetooth, pegasus, overlays, controllers and default mame core for x86 64 bits

- Emulation:
	- remove ps4 legacy mapping
	- add/fix xbox 360 wireless and corrected ps4 mappings
	- add Nintendo Switch snes/genesis/megadrive pads mappings
	- add Nintendo Switch nes pad mapping
	- add Nintendo Switch N64 pad mapping
	- update Nintendo Switch joycon L & R mappings adapted (need joycond)
	- update Snakebyte idroid:con to have L1/R1
	- add NEOGEO mini PAD mapping
	- Bump Configgen to remove fix on Left stick for Flycast

- Pegasus-Frontend:
	- Bump pegasus including:
		- Refactoring of menu using icons in sections
		- Refactoring of menu using hide/display using button on sections

		- Bluetooth improvements:
			- fix asynchronous disconnection using bluetooth
			- fix to remove not well paired device or not identified as paired.
			- fix to add devices already paired in recalbox.conf
			- fix to improve verification/timing for devices already paired
			- Introduce parameter and menu to be able to reset bluetooth at each start of Pegasus
			
		- Controller improvements
			- change way to find mapping (es_input -> sdl2)
			- set way to manage order  of player/device from menu
			- add icon for 8BitDo SN30pro+
			- icon: to add arcade sticks (8 buttons) black & white
			- add icon for Snakebyte idroid:con
			- game controller database: updated to accept SDL 2.0.16 format from SDL 2.0.14 
			- game controller database: add last version from https://github.com/gabomdq/SDL_GameControllerDB
			- add matching for drangonrise and xinmo using arcade panel icon
			- sdl2: fix for build using SDL_JoystickDevicePathById
			- udev: add code to integrate indexation from udev
			- icons: add detection of ultimarc j-pac as arcade panel device
			- icons: use icon of sn30 pro+ for pro 2 from 8bitdo
			- icons: to well distinguish 8bitdo sn30 pro+/pro plus & pro 2

			- custom layout introduction:
				- nes & snes controllers layout
				- xbox360 controller layout
				- ps4 controller layout

			- add xow daemon management for xbox one/series controllers
			- layout: force to default layout if doesn't exist for any controller

		- sysinfo:
			- more info added as temp and gpu in 2 columns
			- fix to have best temperature display to ignore some cases and doubloon

		- lang : add last fr translation
		- confirm in title/comments that Sony part is only for PS3 sixasis
		- bluetoohth: fix to restart bluetooth only during starting and not reloading after game session
		- accountmain: add fix on SectionTitle to set font indendently
		- controllersmain: reduce controller icon of 10% and centralize verticaly

## [RETRO-X] - Beta 21 - 2022-03-11
- OS:
	- feature : hid-nintendo driver patches to add nes, snes, n64 and genesis (and avoid crashes)
	- fix: hid-nintendo driver to disable imu devices

- Tools:
	- arcade dat generator introduction
	
- Emulators:
	- bump supermodel: bump and fix dump texture option
	- fix for citra emu configs
	- bump libretro mame: 0.239
	
## [RETRO-X] - Beta 20 - 2022-02-13
- OS:
	- Add QT5XmlPattern library for bios checking
	- Add QT library for usage of controller with virtual keyboard

- Emulators:
	- add version for all standalone emulators during build in dedicated *.corenames files
	- Supermodel bump to 880 : add dump of textures

- Pegasus:
	- introduction of "information system" view to display cpu, memory, mass storage, etc...
	- introduction of "virtual keyboard" component
	- introduction of "updates" management feature
	- introduction of "Bios Checking menu" using API with md5 calculation for bios
	- Significant other things:
		- restart feature added in menu with popup of confirmation
		- add display of version for all standalone emulator also as for libretro ones
		- improvements on loading performance (add skraper media option / change collections management in post-processing)
		- fix support of "hidden" tag from gamelists
		- versionning: add version from git release and OS version in Pegasus
		- localization: fix "american english" multiple menu display in buildroot
	
- Theme (gameOS-pixL):
	- demo mode: remove fading/lists and highlight in demo mode
	- select search directly from y/triangle button directly and not only filter
	- setup of virtualkeyboard for search in theme
	- introduce demo resetting at OnRelease + fix on settings
	- add virtual keyboard support in settings of theme
	- fix in settings to use good variable type
	- fix gridviewmenu to avoid to launch game from empty list and to gameview
	- add Change & Edit word in help for settings
	- fix showcaseLoader to load in all cases at start and after game ending
	- fix for white/color logo in gameview

## [RETRO-X] - Beta 19 - 2021-12-31
- Emulators:
	- Introduction of Citra emulator for 3DS and adapted to new configgen using Python 3.9 (still issue on language selection)
	- fix citra and ps2 romfs2
	- bump standalone supermodel: r874
	- bump standalone dolphin-emu: 5.0-15445
	- bump libretro mame: 0.237
	- bump pcsx2(+ patch reduced) : v1.7.2148
	- fix flycast: to forced left analog on dpad mode for Atomiswave, Naomi and NaomiGD

- Pegasus:
	- fix in controllers naming
	- translation updated for French and English
	- fix on bluetooth to support legacy scripts now (since rebase recalbox 8.0)
	- menu: overlay options / icons added in menu/section
	- cd-rom management:
		- introduction using new dialog box / assets 
		- new API principle: the single play 
		- but still under progress (not yet fully available for users)
	- Netplay feature introduction:
		- Include lobby with medida display / icons / logo
		- Friend's room management
		- options of filtering (friend, playable games, etc..)
		- Enhanced check before to launch game to avoid "black screen effect"

- Theme (gameOS-pixL):
	- introduction of Netplay in helpbar & Gameview
	- gameView: to use L1/R1 to change game in a system
	- introduction of demo mode in the theme itself using gameView
	- fix L2/R2 letter scrolling in system view

- OS:
	- rebase on master of recalbox 8.0 electron
	- add new externalscreen for 2 display layout
	- project-cd:
		- add launch_cdrom and udev rules 
		- add eject fonction and change udev rules

## [RETRO-X] - Beta 18 - 2021-10-13
- Pegasus :
	- Change version to "PEGASUS (pixL version)"
	- Improve Pegasus loading bar using more information and steps (including fixes now)
	- Display all controllers connected in menu Controllers
	- Fixes on GamepadEditor for new controllers
	- New bluetooth feature for controllers and more (as bluetooth speaker and headset):
		- Several methods of scanning/pairing and to forget devices
		- Show vendor name if available
		- Hide no name devices
		- Visibility of paired devices
		- Possibility to ignore devices
		- Forget and disconnect feature independently by device
		- Icon avaialble for several devices		
		- Show device status as paired/no success pairing and connected
		- Battery display (sony/nintendo devices)
	
- Theme (GameOS-pixL):
	- add play time, play count and last played in game info and also in settings
	- add new feature to change automatically favorites displayed in header of showcaseview	

- OS:
	- Add QT5 Connectivity dependencies for bluetooth features

## [RETRO-X] - Beta 17 - 2021-09-09
- fix SQLITE Drivers for QT (for Pegasus DB)
- Pegasus : 
	- fix to correct issue with "unknown" controllers by SDL
- GameOS theme:
	- performance and visibility optimization especially for main menu
	- Collections management (settings and display in 'main menu' collections for the moment)
	- arcade button in helps
	- gameinfo improvements
	
## [RETRO-X] - Beta 16 - 2021-08-28
- fix citra and pcsx2 not launch migration python3.9
- fix romfs v2 ps2
- bump configgen for fix citra and pcsx2
- bump configgen for fix citra and pcsx2
- bump pegasus for lastest fix/improvements:
	- fixes: fonts / helps / keyboard menu / logo selection in settings menu / binding improved / warning removed
	- Performance improvement on main menu

## [RETRO-X] - Beta 15 - 2021-08-09
- add ps2 and 3ds romfs v2 (bump configgen)
- "double" boot partition on pc x86-64 to prepare the future
- Pegasus:
	- popup for controllers connection including icons (ps/xbox & wheels)
	- new controller experience proposing to configure/test during first connection
	- several improvements in gamepad editor (colors, buttons, help)
	- Warning: 'step by step' not yet available but help instruction are displayed ;-)
	- New systemList.xml from romfs v2 supported
	- Fixes for issues on gameOS theme for default value from settings (as language and logos) - (#11,#13)
	- new API from Pegasus to manage list of parameters listed from system command.

## [RETRO-X] - Beta 14 - 2021-07-07
- set mame on default arcade emulator (#12)
- add pdfjs component in scripts directory as for gameos-pixl
- Pegasus:
		- fix keyboard qwerty retroachievement menu (and netplay) (#9)
		- retroachievements feature (including new gameos with new gameview retroachievements display)
		- logo optimization
		- pdfjs library from OS
		- rework menu and several fixes
		- link between languages of pegasus and systems
		- keyboard region management and change in real-time
		- add shaders selection in menu

## [RETRO-X] - Beta 13 - 2021-06-19
- Fix Ci gitlab (#6)
- Fix lags/skips during cinematics or in game mode 3DS citra-emu (#3)
- Fix fullscreen is never kept on 3DS Citra-emu (#5)
- Bump configgen :
	- fix L and R not work
- add gstreamer full plugins :
	- fix mute video preview in gameOs
	- fix many video preview not work

## [RETRO-X] - Beta 12 - 2021-05-25
- add nintendo 3ds citra emu:
	- add citra on verion nightly-1311
	- add recalbox romf package
	- change to citra qt and change repository
	- bump citra to nightly 1661
	- fix compilation mistake on defconfigs
	- bump configgen and citra emu
- bump last version of Pegasus (commit: 42c2... ):
	- including new translation in french for new menu
	- mouse fix in menu (with possibility to deactivate or not)
- bump last version of Configgen (commit: 609a...) for ratio improvement
- supermodel: remove option for video engine and fix ratio option / save states fix

## [RETRO-X] - Beta 11 - 2021-05-23
- add THEMES to manage themes directory from share
- bump last version of Pegasus (commit: 6f0d...) / come back to use commit and not branch name to avoid problem of build:
	- with best performance (Gamelist Only)
	- add hash management for theme as GameOS usage
- test version for Supermodel (new compilation/configgen)

## [change-frontend-to-pegasus] - Beta 10 "dirty" - 2021-04-29
- including PS2 test version

## [change-frontend-to-pegasus] - Beta 10 - 2021-04-22
- pegasus version with storage selection as "NETWORK, INTERNAL, etc.." and with sound management
	
## [change-frontend-to-pegasus] - Beta 9 - 2021-04-16
- new pegasus version:  
	- with new manual management and more media for our GameOS Theme fork ;-)
	- management of keyboard simulation with pad for browser browsing used in pdf manual display.
- based on recalbox 7.2 Beta 23 (with Mame 0.230... and other things)
- nvidia 460 drivers

## [change-frontend-to-pegasus] - Beta 8 - 2021-04-10
- add QT5 WEB ENGINE for future used ;-)

## [change-frontend-to-pegasus] - Beta 7 - 2021-04-02
- fix on S14migrate boot script: remove migration part about virtual keyboard
- re-add qt5 quick control in defconfig + virtual keyboard in .in/.mk

## [change-frontend-to-pegasus] - Beta 6 - 2021-04-01 (including change done for beta 1 to 5)
- fix(intel): disable VSYNC using "vblank_mode=0" to launch "pegasus-fe"
- kill pegasus if "es stop" command launched 
- add gstreamer for qt5multimedia
- add reboot and shutdown for buildroot (patch on pegasus)
- fix sound and video launch for pegasus
- change repository for pegasus (to use our fork finally)
- remove reboot and shutdown patch of pegasus
- add QT virtualkeyboard
- add qt5base_xml for xml and qt5quickcontrols for next popups
- use recalbox-integration branch for pegasus
- add qt5 quick controls 2 
- re-add qt5 quick control in defconfig + virtual keyboard in .in/.mk

## [change-frontend-to-pegasus] - 2021-02-03
- New branch created to integrate the new front-end for recalbox - Pegasus Front-end
- init package pegasus frontend
